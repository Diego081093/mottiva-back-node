"use strict";

var _passport = _interopRequireDefault(require("passport"));

var _passportJwt = require("passport-jwt");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var LocalStrategy = require('passport-local').Strategy;

var JWTStrategy = require('passport-jwt').Strategy;

var ExtractJWT = require('passport-jwt').ExtractJwt; // import Users, { isValidPassword } from '../models/users_model'


var _require = require('../models'),
    tbl_user = _require.tbl_user;

var _require2 = require('../helpers'),
    isValidPassword = _require2.isValidPassword;

_passport["default"].use('signin', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password'
}, /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(username, password, done) {
    var user, validate;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_user.findOne({
              where: {
                username: username
              }
            });

          case 3:
            user = _context.sent;

            if (user) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return", done(null, false, {
              message: 'User not found'
            }));

          case 6:
            _context.next = 8;
            return isValidPassword(password, user.password);

          case 8:
            validate = _context.sent;

            if (validate) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("return", done(null, false, {
              message: 'Wrong password'
            }));

          case 11:
            return _context.abrupt("return", done(null, user, {
              message: 'Login successfully'
            }));

          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](0);
            done(_context.t0, false, 'Something went wrong');

          case 17:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 14]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}()));

_passport["default"].use(new JWTStrategy({
  secretOrKey: 'globoazul_token',
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
}, /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(token, done) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            return _context2.abrupt("return", done(null, token.user));

          case 4:
            _context2.prev = 4;
            _context2.t0 = _context2["catch"](0);
            console.error('You have error passport.use JWTStrategy: ', _context2.t0);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 4]]);
  }));

  return function (_x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}()));