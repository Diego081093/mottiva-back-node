"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _attributes_controller = require("../controllers/attributes_controller");

var router = (0, _express.Router)();
router.get('/', _attributes_controller.getListAttributes);
var _default = router;
exports["default"] = _default;