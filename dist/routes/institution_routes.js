"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _institution_controller = require("../controllers/institution_controller");

var router = (0, _express.Router)();
router.get('/', _institution_controller.getAll);
var _default = router;
exports["default"] = _default;