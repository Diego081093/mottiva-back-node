"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _user_secret_question_controller = require("../controllers/user_secret_question_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//api/rol
router.get('/', _user_secret_question_controller.getUserSecretQuestion); //corregir a findone

router.post('/validate', _passport["default"].authenticate('jwt', {
  session: false
}), _user_secret_question_controller.validateUserSecretQuestion);
var _default = router;
exports["default"] = _default;