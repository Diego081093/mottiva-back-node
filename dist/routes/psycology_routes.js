"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _profiles_controller = require("../controllers/profiles_controller");

var _profile_attribute_controller = require("../controllers/profile_attribute_controller");

var _blocked_controller = require("../controllers/blocked_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//patient
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.getPsycology);
router.get('/list', _passport["default"].authenticate('jwt', {
  session: false
}), _profile_attribute_controller.getListPsicology);
router.post('/:id/block', _passport["default"].authenticate('jwt', {
  session: false
}), _blocked_controller.createBlocked);
router.put('/', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updatePsycology);
router.put('/photo', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updatePhoto);
var _default = router;
exports["default"] = _default;