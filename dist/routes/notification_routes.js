"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _notification_controller = require("../controllers/notification_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.getNotifications);
router.get('/sessions', _notification_controller.setTempoSession);
router.get('/sendPushNotificationUser/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationUser);
router.get('/sendPushNotificationUsers/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationUsers);
router.get('/sendPushNotificationSessionStart', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationSessionStart);
router.get('/sendPushNotificationRoomStart', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationRoomStart);
router.get('/sendPushNotificationBenefits', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationBenefits);
router.get('/sendPushNotificationMedias', _passport["default"].authenticate('jwt', {
  session: false
}), _notification_controller.sendPushNotificationMedias);
var _default = router;
exports["default"] = _default;