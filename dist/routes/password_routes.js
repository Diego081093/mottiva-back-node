"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _users_controller = require("../controllers/users_controller");

var router = (0, _express.Router)();
//api/rol
// router.get('/', getListRole);
router.put('/', _users_controller.updatePassword); // router.delete('/',deleteRole);

var _default = router;
exports["default"] = _default;