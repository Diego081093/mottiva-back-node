"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _secret_question_controller = require("../controllers/secret_question_controller");

var router = (0, _express.Router)();
//api/rol
router.get('/', _secret_question_controller.getListSecretQuestion); //cuando va a traer una lista de varios
//router.get('/:id', getSecretQuestion); //traer solo uno

router.get('/:dni', _secret_question_controller.getSecretQuestion);
router.post('/validate', _secret_question_controller.postValidateSecretQuestion);
router.post('/', _secret_question_controller.createSecretQuestion);
router.put('/:id', _secret_question_controller.updateSecretQuestion);
router["delete"]('/:id', _secret_question_controller.deleteSecretQuestion);
var _default = router;
exports["default"] = _default;