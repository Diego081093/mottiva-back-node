"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _task_controller = require("../controllers/task_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//api/rol
// router.get('/', getListRole);
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.getListTask);
router.get('/week/:week', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.getListTaskByWeek);
router.get('/patient', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.getTasksPatient);
router.get('/challenges', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.getChallenge);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.getTask);
router.post('/sessionTask', _passport["default"].authenticate('jwt', {
  session: false
}), _task_controller.createSessionTask);
var _default = router;
exports["default"] = _default;