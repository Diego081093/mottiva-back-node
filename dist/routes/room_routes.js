"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _express = require("express");

var _room_controller = require("../controllers/room_controller");

var _session_controller = require("../controllers/session_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('./../helpers'),
    s3Upload = _require.s3Upload;

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.getListRoomByProfile);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.getRoomDetails);
router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.postRoom); // router.post('/enter', passport.authenticate('jwt', { session: false }), postEnterRoom)

router.post('/:id/signup', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.postSignupRoom);
router.put('/', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.closedRoom);
router["delete"]('/:id/signup', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.deleteUnsubscribeRoom);
router.post('/:id/notes', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.addRoomNote);
var _default = router;
exports["default"] = _default;