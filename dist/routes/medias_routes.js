"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _medias_controller = require("../controllers/medias_controller");

var _passport = _interopRequireDefault(require("passport"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.getListMedias);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.getMedia);
router.post('/favorite', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.postFavoriteMedia);
router.post('/desactivate-favorite', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.postDesactivateFavoriteMedia);
router.post('/:id/watch-later', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.watchLaterMedia);
router.post('/done', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.postDoneMedia);
var _default = router;
exports["default"] = _default;