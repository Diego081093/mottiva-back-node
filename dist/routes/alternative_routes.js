"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _alternative_controller = require("../controllers/alternative_controller");

var router = (0, _express.Router)();
router.get('/', _alternative_controller.getListAlternative);
router.post('/', _alternative_controller.createAlternative);
var _default = router;
exports["default"] = _default;