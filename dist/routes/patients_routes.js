"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _session_controller = require("../controllers/session_controller");

var _profiles_controller = require("../controllers/profiles_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.getPatientsSession);
router.get('/list', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.getListPatients);
var _default = router;
exports["default"] = _default;