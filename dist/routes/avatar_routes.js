"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _avatar_controller = require("../controllers/avatar_controller");

var _upload_controller = require("../controllers/upload_controller");

var router = (0, _express.Router)();

var _require = require('./../helpers'),
    s3Upload = _require.s3Upload;

//avatar
router.post('/', _avatar_controller.createAvatar);
router.post('/upload', s3Upload.single('file'), _upload_controller.uploadAvatar); //avatar

router.get('/', _avatar_controller.getListAvatar); //avatar?idProfile=100

router.get('/:idProfile', _avatar_controller.getAvatar); //lista de perfiles y cada uno con su avatar

var _default = router;
exports["default"] = _default;