"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _session_controller = require("../controllers/session_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//patient
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.getNotesList);
router.put('/:id/disable', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.disableNote);
var _default = router;
exports["default"] = _default;