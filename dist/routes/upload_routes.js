"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _upload_controller = require("../controllers/upload_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('./../helpers'),
    s3Upload = _require.s3Upload;

var router = (0, _express.Router)();
//api/rol
// router.get('/', getListRole);
router.post('/local', _passport["default"].authenticate('jwt', {
  session: false
}), _upload_controller.uploadFile, _upload_controller.storeFile);
router.post('/s3', _passport["default"].authenticate('jwt', {
  session: false
}), s3Upload.single('file'), _upload_controller.uploadFileS3);
router.post('/base64', _upload_controller.uploadBase64File);
var _default = router;
exports["default"] = _default;