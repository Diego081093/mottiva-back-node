"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _benefit_controller = require("../controllers/benefit_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.getListBenefits);
router.get('/company', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.getListCompanyBenefits);
router.get('/management', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.getBenefitsProfile);
router.get('/history', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.getHistoryBenefits);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.getBenefit);
router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.createBenefit);
router.post('/exchange', _passport["default"].authenticate('jwt', {
  session: false
}), _benefit_controller.exchangeBenefits);
var _default = router;
exports["default"] = _default;