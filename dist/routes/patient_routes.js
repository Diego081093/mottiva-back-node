"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _profiles_controller = require("../controllers/profiles_controller");

var router = (0, _express.Router)();
//patient
router.post('/', _profiles_controller.createPatient);
var _default = router;
exports["default"] = _default;