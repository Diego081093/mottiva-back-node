"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _medias_controller = require("../controllers/medias_controller");

var _passport = _interopRequireDefault(require("passport"));

var _express = require("express");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.post('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.watchLaterMedia);
var _default = router;
exports["default"] = _default;