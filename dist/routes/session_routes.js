"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _session_controller = require("../controllers/session_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//api/rol
// router.post('/',createRole);
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.getSessions);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.getSession);
router.get('/cases', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.getSessionCases);
router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.postSession);
router.post('/:id/next', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.postNextSession);
router.post('/:id/notes', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.addSessionNote);
router.post('/:id/completed', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.completeSession);
router.post('/:id/case', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.postCaseSession);
router.put('/', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.closedSession);
router.put('/:id/reschedule', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.rescheduleSession);
router.put('/:id/replace', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.rescheduleReplaceSession);
router.put('/:id/assign', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.assignSession);
router.put('/:id/cancel', _passport["default"].authenticate('jwt', {
  session: false
}), _session_controller.cancelSession); // router.delete('/',deleteRole);

var _default = router;
exports["default"] = _default;