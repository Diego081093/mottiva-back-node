"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _therapy_controller = require("../controllers/therapy_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//patient
router.get('/filter', _passport["default"].authenticate('jwt', {
  session: false
}), _therapy_controller.filterAvailability);
var _default = router;
exports["default"] = _default;