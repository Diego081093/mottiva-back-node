"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _profiles_controller = require("../controllers/profiles_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//api/profile
router.get('/', _profiles_controller.getListProfile);
router.get('/me', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.getProfile);
router.get('/points-used', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.getPointsUsed);
router.post('/', _profiles_controller.createProfile);
router.put('/', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updateProfile);
router.put('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updateProfileOnly);
router.put('/:id/photo', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updateProfilePhoto);
router.put('/:id/phone', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updateProfilePhone);
router.put('/:id/avatar', _passport["default"].authenticate('jwt', {
  session: false
}), _profiles_controller.updateProfileAvatar);
router["delete"]('/:id', _profiles_controller.deleteProfile);
var _default = router;
exports["default"] = _default;