"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _users_controller = require("../controllers/users_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
//api/rol
// router.get('/', getListRole);
router.put('/', _passport["default"].authenticate('jwt', {
  session: false
}), _users_controller.updatePhone); // router.delete('/',deleteRole);

var _default = router;
exports["default"] = _default;