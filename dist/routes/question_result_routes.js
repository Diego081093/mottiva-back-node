"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _question_result_controller = require("../controllers/question_result_controller");

var router = (0, _express.Router)();
router.get('/:score', _question_result_controller.getResult);
var _default = router;
exports["default"] = _default;