"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _roles_controller = require("../controllers/roles_controller");

var router = (0, _express.Router)();
//api/rol
router.get('/', _roles_controller.getListRole);
router.post('/', _roles_controller.createRole);
router["delete"]('/:id', _roles_controller.deleteRole);
var _default = router;
exports["default"] = _default;