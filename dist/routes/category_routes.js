"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _medias_controller = require("../controllers/medias_controller");

var _passport = _interopRequireDefault(require("passport"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/media', _passport["default"].authenticate('jwt', {
  session: false
}), _medias_controller.ListCategoryMedia);
var _default = router;
exports["default"] = _default;