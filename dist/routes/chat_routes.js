"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _chat_controller = require("../controllers/chat_controller");

var _chat_message_controller = require("../controllers/chat_message_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/helpdesk', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.getHelpdesk);
router.post('/helpdesk', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.joinHelpdesk);
router.get('/helpdesk/list', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.getAllHelpdesk);
router.get('/private', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.getChatPrivate);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.getChat);
router.get('/sessions/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_controller.getChatSession);
router.post('/message', _passport["default"].authenticate('jwt', {
  session: false
}), _chat_message_controller.postChatMessage);
var _default = router;
exports["default"] = _default;