"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _express = require("express");

var _room_controller = require("../controllers/room_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/video/rooms', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.twilioListRooms);
router.post('/video/rooms', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.twilioCreateRoom);
router.put('/video/rooms/:sid', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.twilioUpdateRoom);
router.post('/accessToken', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.twilioAccessToken);
var _default = router;
exports["default"] = _default;