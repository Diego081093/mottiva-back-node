"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _upload_controller = require("../controllers/upload_controller");

var router = (0, _express.Router)();
//api/rol
// router.get('/', getListRole);
router.post('/', _upload_controller.uploadFiles, _upload_controller.storeFiles); // router.delete('/',deleteRole);

var _default = router;
exports["default"] = _default;