"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _training_controller = require("../controllers/training_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('./../helpers'),
    s3UploadWithDest = _require.s3UploadWithDest;

var s3Upload = s3UploadWithDest('trainings/');
var router = (0, _express.Router)();
//api/training
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _training_controller.getTrainings);
router.get('/filters', _passport["default"].authenticate('jwt', {
  session: false
}), _training_controller.getTrainingsFilters);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _training_controller.getTraining);
router.post('/video', _passport["default"].authenticate('jwt', {
  session: false
}), _training_controller.createTrainingVideo);
router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), s3Upload.fields([{
  name: 'file',
  maxCount: 1
}, {
  name: 'image',
  maxCount: 1
}]), _training_controller.createTraining);
var _default = router;
exports["default"] = _default;