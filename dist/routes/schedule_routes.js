"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _express = require("express");

var _schedule_controller = require("../controllers/schedule_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _schedule_controller.getListSchedule);
router.get('/history', _passport["default"].authenticate('jwt', {
  session: false
}), _schedule_controller.getListHistorySchedule);
var _default = router;
exports["default"] = _default;