"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _blocked_controller = require("../controllers/blocked_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), _blocked_controller.createBlocked);
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _blocked_controller.getListBlocked);
var _default = router;
exports["default"] = _default;