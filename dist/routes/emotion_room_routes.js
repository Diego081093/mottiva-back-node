"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _room_controller = require("../controllers/room_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.getEmotionRooms);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.showEmotionRoom);
router.get('/participant/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.getParticipants);
router.put('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _room_controller.putEmotionRooms);
var _default = router;
exports["default"] = _default;