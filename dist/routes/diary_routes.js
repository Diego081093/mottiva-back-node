"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _diary_controller = require("../controllers/diary_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();

var _require = require('./../helpers'),
    s3Upload = _require.s3Upload;

router.post('/', _passport["default"].authenticate('jwt', {
  session: false
}), s3Upload.single('file'), _diary_controller.putDiary);
router.get('/dates', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.getListDateDiary);
router.get('/dates-details', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.getListDateDiaryDetails);
router.get('/viewPsychologist', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.getListDiaryViewPsychologist);
router.put('/:id/clear', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.clearDiary);
router.get('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.getDetailDiary);
router.get('/', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.getListDiary);
router.put('/seen/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _diary_controller.seenDiary);
var _default = router;
exports["default"] = _default;