"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _profile_attribute_controller = require("../controllers/profile_attribute_controller");

var router = (0, _express.Router)();
//api/profile
router.get('/:id', _profile_attribute_controller.getProfileAttribute);
router.post('/:id', _profile_attribute_controller.setProfileAttribute);
var _default = router;
exports["default"] = _default;