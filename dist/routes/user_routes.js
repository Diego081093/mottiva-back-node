"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _passport = _interopRequireDefault(require("passport"));

var _users_controller = require("../controllers/users_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import { getListProfile } from '../controllers/profiles_controller';
var router = (0, _express.Router)();
// /api/users/
router["delete"]('/:id', _passport["default"].authenticate('jwt', {
  session: false
}), _users_controller.deleteUser);
router.put('/tutorial', _passport["default"].authenticate('jwt', {
  session: false
}), _users_controller.updateTutorial);
router.put('/update', _passport["default"].authenticate('jwt', {
  session: false
}), _users_controller.updatePassword); //router.get('/getProfile',getProfile);
// router.post('/', createUser);
// router.get('/:idprofile', getListProfile)
// router.put('/tokenDevice', updateTokenDevice)
//router.get('/avatar2',getAvatar);

var _default = router;
exports["default"] = _default;