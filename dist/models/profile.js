'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _require = require('sequelize'),
    Model = _require.Model;

module.exports = function (sequelize, DataTypes) {
  var tbl_profile = /*#__PURE__*/function (_Model) {
    _inherits(tbl_profile, _Model);

    var _super = _createSuper(tbl_profile);

    function tbl_profile() {
      _classCallCheck(this, tbl_profile);

      return _super.apply(this, arguments);
    }

    _createClass(tbl_profile, null, [{
      key: "associate",
      value:
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      function associate(models) {
        // define association here
        this.belongsTo(models.tbl_role, {
          //necesito de este modelo
          foreignKey: 'tbl_role_id',
          as: 'Role' // as:Role               //este campo

        });
        this.belongsTo(models.tbl_user, {
          //necesito de este modelo
          foreignKey: 'tbl_user_id',
          as: 'User' //este campo

        });
        this.belongsTo(models.tbl_avatar, {
          //necesito de este modelo
          foreignKey: 'tbl_avatar_id',
          as: 'Avatar' //este campo

        });
        this.belongsTo(models.tbl_district, {
          //necesito de este modelo
          foreignKey: 'tbl_district_id',
          as: 'District' //este campo

        });
        this.hasOne(models.tbl_profile_alternative, {
          //le brindo a este modelo
          foreignKey: 'profile_id',
          as: 'ProfileAlternative' //este campo

        });
        this.hasOne(models.tbl_profile_attribute, {
          foreignKey: 'tbl_profile_id',
          as: 'ProfileAttribute'
        });
        this.hasMany(models.tbl_profile_attribute, {
          foreignKey: 'tbl_profile_id',
          as: 'Agenda'
        });
        this.hasMany(models.tbl_profile_institution, {
          foreignKey: 'tbl_profile_id',
          as: 'ProfileInstitutions'
        });
        this.belongsToMany(models.tbl_specialty, {
          through: models.tbl_profile_specialty,
          foreignKey: 'tbl_profile_id',
          otherKey: 'tbl_specialty_id',
          // as: 'Specialties'
          as: {
            plural: 'Specialties',
            singular: 'Specialty'
          }
        });
        this.hasOne(models.tbl_blocked, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_pacient_id',
          as: 'BlockedPacient' //este campo

        });
        this.hasOne(models.tbl_blocked, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_psychologist_id',
          as: 'BlockedPsychologist' //este campo

        });
        this.hasOne(models.tbl_diary, {
          foreignKey: 'tbl_profile_id',
          as: 'Diary' //este campo

        });
        this.hasOne(models.tbl_diary_psychologist, {
          foreignKey: 'tbl_profile_id',
          as: 'DiaryPsychologist ' //este campo

        }); // Was causing issues
        // this.hasOne(models.tbl_session ,{
        //   foreignKey: 'tbl_profile_id' ,
        //   as:'Session'                      //este campo
        // })

        this.hasOne(models.tbl_profile_specialty, {
          as: 'ProfileSpecialty',
          foreignKey: 'tbl_profile_id'
        });
        this.hasOne(models.tbl_chat_participants, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'ChatParticipant' //este campo

        });
        this.hasOne(models.tbl_media_profile, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'MediaProfile' //este campo

        });
        this.hasOne(models.tbl_task_answer, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'TaskAnswer' //este campo

        });
        this.hasOne(models.tbl_benefit_profile, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'BenefitProfile' //este campo

        });
        this.hasOne(models.tbl_availability, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'Availability' //este campo

        });
        this.hasMany(models.tbl_availability, {
          //le brindo a este modelo
          foreignKey: 'tbl_profile_id',
          as: 'Availabilities' //este campo

        });
        this.hasMany(models.tbl_room_participants, {
          foreignKey: 'tbl_profile_participant_id',
          as: 'RoomParticipants'
        });
      }
    }]);

    return tbl_profile;
  }(Model);

  ;
  tbl_profile.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_role_id: DataTypes.INTEGER,
    tbl_user_id: DataTypes.INTEGER,
    tbl_avatar_id: DataTypes.INTEGER,
    tbl_district_id: DataTypes.INTEGER,
    slug: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    dni: DataTypes.STRING,
    photo_dni: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    gender: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    points: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    req_sent_at: DataTypes.DATE,
    req_state: DataTypes.BOOLEAN,
    req_approved_at: DataTypes.DATE,
    civil_state: DataTypes.STRING,
    ocupation: DataTypes.STRING,
    study_nivel: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE
    }
  }, {
    sequelize: sequelize,
    modelName: 'tbl_profile'
  });
  return tbl_profile;
};