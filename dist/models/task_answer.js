'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _require = require('sequelize'),
    Model = _require.Model;

module.exports = function (sequelize, DataTypes) {
  var _tbl_task_answer$init;

  var tbl_task_answer = /*#__PURE__*/function (_Model) {
    _inherits(tbl_task_answer, _Model);

    var _super = _createSuper(tbl_task_answer);

    function tbl_task_answer() {
      _classCallCheck(this, tbl_task_answer);

      return _super.apply(this, arguments);
    }

    _createClass(tbl_task_answer, null, [{
      key: "associate",
      value:
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      function associate(models) {
        // define association here
        this.belongsTo(models.tbl_profile, {
          //necesito de este modelo
          foreignKey: 'tbl_profile_id',
          as: 'Profile' //este campo

        });
        this.belongsTo(models.tbl_task_question, {
          //le brindo a este modelo
          foreignKey: 'tbl_task_question_id',
          as: 'TaskQuestion' //este campo

        });
      }
    }]);

    return tbl_task_answer;
  }(Model);

  ;
  tbl_task_answer.init((_tbl_task_answer$init = {
    tbl_task_question_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER
  }, _defineProperty(_tbl_task_answer$init, "tbl_profile_id", DataTypes.INTEGER), _defineProperty(_tbl_task_answer$init, "sort", DataTypes.INTEGER), _defineProperty(_tbl_task_answer$init, "answer", DataTypes.TEXT), _defineProperty(_tbl_task_answer$init, "state", {
    type: DataTypes.INTEGER,
    defaultValue: 1
  }), _defineProperty(_tbl_task_answer$init, "created_by", DataTypes.STRING), _defineProperty(_tbl_task_answer$init, "createdAt", {
    field: 'created_at',
    type: DataTypes.DATE
  }), _defineProperty(_tbl_task_answer$init, "edited_by", DataTypes.STRING), _defineProperty(_tbl_task_answer$init, "updatedAt", {
    field: 'edited_at',
    type: DataTypes.DATE
  }), _tbl_task_answer$init), {
    sequelize: sequelize,
    modelName: 'tbl_task_answer'
  });
  return tbl_task_answer;
};