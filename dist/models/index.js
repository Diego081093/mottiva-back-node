'use strict';

var fs = require('fs');

var path = require('path');

var Sequelize = require('sequelize'); // Load env vars


require('dotenv').config();

var basename = path.basename(__filename);

var config = require('../config/database.js');

var db = {};
var sequelize;

if (process.env.NODE_ENV === 'development') {
  sequelize = new Sequelize(config.development.database, config.development.username, config.development.password, {
    logging: false,
    dialect: 'postgres',
    query: true,
    host: config.development.host
  });
} else {
  sequelize = new Sequelize(config.production);
}

fs.readdirSync(__dirname).filter(function (file) {
  return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
}).forEach(function (file) {
  var model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);

  db[model.name] = model;
});
Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});
db.sequelize = sequelize;
module.exports = db;