'use strict';

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

var bcrypt = require('bcrypt');

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, hashed, data_1, data_2;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              /**
               * Add seed commands here.
               *
               * Example:
               * await queryInterface.bulkInsert('People', [{
               *   name: 'John Doe',
               *   isBetaMember: false
               * }], {});
              */
              date = new Date();
              _context.next = 3;
              return bcrypt.hash('123123', parseInt(process.env.BCRYPT_ROUNDS));

            case 3:
              hashed = _context.sent;
              _context.next = 6;
              return bcrypt.hash('abc', parseInt(process.env.BCRYPT_ROUNDS));

            case 6:
              _context.t0 = _context.sent;
              _context.t1 = date;
              _context.t2 = date;
              _context.t3 = {
                username: 'usuario1',
                password: _context.t0,
                token_device: 'dIF4NRGZRr-hyqBsxpXquA:APA91bGsYhdkPg6MOB9ilRRx2lwInOKgVRB6R4v1DWIQuydDVOuN-eAby1UsXgzQ5Xzal38EehCWDsW2p2yOgYVYKN9dX-E3H3ZJ8lEZIeoYCkoBwQ6pISV6pGRPB_BLoXGuU_wPU4-m',
                state: 1,
                created_by: 'root',
                created_at: _context.t1,
                edited_by: 'root',
                edited_at: _context.t2
              };
              _context.next = 12;
              return bcrypt.hash('abc', parseInt(process.env.BCRYPT_ROUNDS));

            case 12:
              _context.t4 = _context.sent;
              _context.t5 = date;
              _context.t6 = date;
              _context.t7 = {
                username: 'marco.condoric@gmail.com',
                password: _context.t4,
                state: 1,
                created_by: 'root',
                created_at: _context.t5,
                edited_by: 'root',
                edited_at: _context.t6
              };
              _context.next = 18;
              return bcrypt.hash('1234', parseInt(process.env.BCRYPT_ROUNDS));

            case 18:
              _context.t8 = _context.sent;
              _context.t9 = date;
              _context.t10 = date;
              _context.t11 = {
                username: '72766963',
                password: _context.t8,
                state: 1,
                created_by: 'root',
                created_at: _context.t9,
                edited_by: 'root',
                edited_at: _context.t10
              };
              data_1 = [_context.t3, _context.t7, _context.t11];
              data_2 = _toConsumableArray(Array(8)).map(function (user, i) {
                return {
                  username: faker.internet.userName().toLowerCase(),
                  password: hashed,
                  // phonenumber: '9' + faker.random.number({min: 20, max: 99}) + faker.random.number({min: 100000, max: 999999}),
                  // state: faker.random.number({min: 1, max: 3}),
                  state: 1,
                  created_by: 'root',
                  edited_by: 'root',
                  created_at: date,
                  edited_at: date
                };
              });
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_users', data_1.concat(data_2)));

            case 25:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};