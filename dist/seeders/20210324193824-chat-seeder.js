'use strict';

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

var _require = require('../models'),
    tbl_chat = _require.tbl_chat,
    tbl_room = _require.tbl_room;

function chatAll() {
  return _chatAll.apply(this, arguments);
}

function _chatAll() {
  _chatAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return tbl_chat.findAll();

          case 2:
            return _context3.abrupt("return", _context3.sent);

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _chatAll.apply(this, arguments);
}

function roomById(_x) {
  return _roomById.apply(this, arguments);
}

function _roomById() {
  _roomById = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(idRoom) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return tbl_room.findOne({
              where: {
                id: idRoom
              }
            });

          case 2:
            return _context4.abrupt("return", _context4.sent);

          case 3:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _roomById.apply(this, arguments);
}

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, chats, roomMatrix, j, roomData, data;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              date = new Date();
              _context.next = 3;
              return chatAll();

            case 3:
              chats = _context.sent;
              roomMatrix = [];
              j = 0;

            case 6:
              if (!(j < 10)) {
                _context.next = 14;
                break;
              }

              _context.next = 9;
              return roomById(j + 1);

            case 9:
              roomData = _context.sent;
              roomMatrix.push({
                id: roomData.id,
                start: roomData.start
              });

            case 11:
              j++;
              _context.next = 6;
              break;

            case 14:
              data = _toConsumableArray(Array(10)).map(function (chat, i) {
                var start = new Date(roomMatrix[i].start);
                start.setDate(start.getDate() + faker.random.number({
                  min: 1,
                  max: 3
                }));
                return {
                  id: chats.length + i + 1,
                  tbl_room_id: roomMatrix[i].id,
                  start: start,
                  state: 1,
                  helpdesk: 0,
                  created_by: 'root',
                  created_at: date,
                  edited_by: 'root',
                  edited_at: date
                };
              });
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_chats', data));

            case 16:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x2, _x3) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x4, _x5) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};