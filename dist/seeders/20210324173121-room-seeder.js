'use strict';

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

var _require = require('../models'),
    tbl_room = _require.tbl_room;

function roomAll() {
  return _roomAll.apply(this, arguments);
}

function _roomAll() {
  _roomAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return tbl_room.findAll();

          case 2:
            return _context3.abrupt("return", _context3.sent);

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _roomAll.apply(this, arguments);
}

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, rooms, data;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              date = new Date();
              _context.next = 3;
              return roomAll();

            case 3:
              rooms = _context.sent;
              data = _toConsumableArray(Array(10)).map(function (room, i) {
                var start = faker.date.past(0, date);
                var end = faker.date.future(0, start);
                var startRoom = faker.date.between(start, end);
                var endRoom = new Date(startRoom);
                endRoom.setDate(end.getDate() + faker.random.number({
                  min: 10,
                  max: 25
                }));
                return {
                  id: rooms.length + i + 1,
                  tbl_method_id: faker.random.number({
                    min: 1,
                    max: 4
                  }),
                  tbl_thematic_id: faker.random.number({
                    min: 1,
                    max: 5
                  }),
                  title: faker.name.title(),
                  start: startRoom,
                  end: faker.date.between(startRoom, endRoom),
                  rules: faker.lorem.slug(),
                  participants: 0,
                  maxParticipants: 0,
                  inscription: '¿!?',
                  image: 'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png',
                  twilio_uniquename: faker.random.alphaNumeric(30),
                  state: 1,
                  created_by: 'root',
                  created_at: date,
                  edited_by: 'root',
                  edited_at: date
                };
              });
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_rooms', data));

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};