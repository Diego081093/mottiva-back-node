'use strict';

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, common_data, data_1, data_2, dataDescription, dataDescriptionShort, data;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              date = new Date();
              common_data = {
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              };
              data_1 = _toConsumableArray(Array(10)).map(function (profileAttribute, i) {
                var days_week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
                var value = [];

                for (var _i = 0, _days_week = days_week; _i < _days_week.length; _i++) {
                  var day = _days_week[_i];
                  value.push({
                    days: day,
                    hours: [{
                      name: '06:00 - 06:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '06:45 - 07:30',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '07:30 - 08:15',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '08:15 - 09:00',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '09:00 - 09:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '09:45 - 10:30',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '10:30 - 11:15',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '11:15 - 12:00',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '12:00 - 12:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '12:45 - 13:30',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '13:30 - 14:15',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '14:15 - 15:00',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '15:00 - 15:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '15:45 - 16:30',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '16:30 - 17:15',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '17:15 - 18:00',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '18:00 - 18:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '18:45 - 19:30',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '19:30 - 20:15',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '20:15 - 21:00',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }, {
                      name: '21:00 - 21:45',
                      state: faker.random.number({
                        min: 0,
                        max: 1
                      })
                    }]
                  });
                }

                var valueData = JSON.stringify(value);
                return _objectSpread({
                  tbl_profile_id: i + 1,
                  key: 'available',
                  value: valueData,
                  state: 1
                }, common_data);
              });
              data_2 = _toConsumableArray(Array(10)).map(function (e, i) {
                return _objectSpread({
                  tbl_profile_id: i + 1,
                  key: 'children',
                  value: faker.random.number({
                    min: 1,
                    max: 3
                  }),
                  state: 1
                }, common_data);
              });
              dataDescription = _toConsumableArray(Array(10)).map(function (profileAttribute, i) {
                return _objectSpread({
                  tbl_profile_id: i + 1,
                  key: 'description',
                  value: faker.lorem.words(25),
                  state: 1
                }, common_data);
              });
              dataDescriptionShort = _toConsumableArray(Array(10)).map(function (profileAttribute, i) {
                return _objectSpread({
                  tbl_profile_id: i + 1,
                  key: 'short_description',
                  value: ['Coach en familia', 'Coach en trabajo', 'Coach en trabajo y autoestima', 'Coach en Autoestima', 'Coach en amor'][faker.random.number({
                    min: 1,
                    max: 3
                  })],
                  state: 1
                }, common_data);
              });
              data = [].concat(_toConsumableArray(data_1), _toConsumableArray(data_2), _toConsumableArray(dataDescription), _toConsumableArray(dataDescriptionShort));
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_profile_attributes', data));

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};