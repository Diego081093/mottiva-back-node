'use strict';

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, data;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              /**
               * Add seed commands here.
               *
               * Example:
               * await queryInterface.bulkInsert('People', [{
               *   name: 'John Doe',
               *   isBetaMember: false
               * }], {});
              */
              date = new Date();
              data = [{
                tbl_profile_id: 2,
                tbl_task_id: 3,
                text: 'nota en mi diario 1',
                audio: 'diary1.mp3',
                feel: 0,
                date: date,
                "public": 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_profile_id: 3,
                tbl_task_id: 2,
                text: 'nota en mi diario 2',
                audio: 'diary2.mp3',
                feel: 0,
                date: date,
                "public": 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_profile_id: 1,
                tbl_task_id: 3,
                text: 'nota en mi diario 3',
                audio: 'diary3.mp3',
                feel: 0,
                date: date,
                "public": 1,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_profile_id: 4,
                tbl_task_id: 4,
                text: 'nota en mi diario 4',
                audio: 'diary4.mp3',
                feel: 0,
                date: date,
                "public": 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_profile_id: 3,
                tbl_task_id: 5,
                text: 'nota en mi diario 5',
                audio: 'diary5.mp3',
                feel: 0,
                date: date,
                "public": 1,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }];
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_diaries', data));

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};