'use strict';

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, root, data;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              /**
               * Add seed commands here.
               *
               * Example:
               * await queryInterface.bulkInsert('People', [{
               *   name: 'John Doe',
               *   isBetaMember: false
               * }], {});
              */
              date = new Date();
              root = 'root';
              data = [{
                name: 'Técnicas sencillas de meditación',
                tbl_category_media_id: 2,
                tbl_method_id: 1,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '15 min',
                description: 'Video o Audio de meditación',
                points: 100,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'La hidratación en el desarrollo',
                tbl_category_media_id: 1,
                tbl_method_id: 1,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '17 min',
                description: 'Video o Audio de meditación',
                points: 200,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Sonidos relajantes para meditar',
                tbl_category_media_id: 2,
                tbl_method_id: 3,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '12 min',
                description: 'Video o Audio de meditación',
                points: 100,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Técnicas de respiración anti estrés',
                tbl_category_media_id: 1,
                tbl_method_id: 2,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '10 min',
                description: 'Video o Audio de meditación',
                points: 200,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Técnicas sencillas de meditación',
                tbl_category_media_id: 2,
                tbl_method_id: 4,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '16 min',
                description: 'Video o Audio de meditación',
                points: 100,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'La hidratación en el desarrollo',
                tbl_category_media_id: 1,
                tbl_method_id: 2,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '11 min',
                description: 'Video o Audio de meditación',
                points: 300,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Sonidos relajantes para meditar',
                tbl_category_media_id: 2,
                tbl_method_id: 2,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '12 min',
                description: 'Video o Audio de meditación',
                points: 100,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Técnicas de respiración anti estrés',
                tbl_category_media_id: 1,
                tbl_method_id: 3,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '5 min',
                description: 'Video o Audio de meditación',
                points: 150,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'Técnicas sencillas de meditación',
                tbl_category_media_id: 2,
                tbl_method_id: 1,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '03 min',
                description: 'Video o Audio de meditación',
                points: 100,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }, {
                name: 'La hidratación en el desarrollo',
                tbl_category_media_id: 1,
                tbl_method_id: 4,
                type: 1,
                file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5',
                image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
                duration: '7 min',
                description: 'Video o Audio de meditación',
                points: 200,
                state: 1,
                created_by: root,
                created_at: date,
                edited_by: root,
                edited_at: date
              }];
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_medias', data));

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};