'use strict';

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var faker = require('faker');

module.exports = {
  up: function () {
    var _up = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(queryInterface, Sequelize) {
      var date, data_1, data_2;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              /**
               * Add seed commands here.
               *
               * Example:
               * await queryInterface.bulkInsert('People', [{
               *   name: 'John Doe',
               *   isBetaMember: false
               * }], {});
              */
              date = new Date();
              data_1 = [{
                tbl_role_id: 1,
                tbl_user_id: 1,
                tbl_avatar_id: 1,
                tbl_district_id: 1,
                slug: faker.lorem.slug(),
                firstname: faker.name.firstName(),
                lastname: faker.name.lastName(),
                dni: faker.random.number({
                  min: 10000000,
                  max: 99999999
                }),
                email: faker.internet.email(),
                phone: '9' + faker.random.number({
                  min: 20,
                  max: 99
                }) + faker.random.number({
                  min: 100000,
                  max: 999999
                }),
                gender: ['M', 'F'][faker.random.number({
                  min: 0,
                  max: 1
                })],
                birthday: date,
                state: 1,
                points: 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_role_id: 2,
                tbl_user_id: 2,
                tbl_avatar_id: 2,
                tbl_district_id: 2,
                slug: faker.lorem.slug(),
                firstname: faker.name.firstName(),
                lastname: faker.name.lastName(),
                dni: faker.random.number({
                  min: 10000000,
                  max: 99999999
                }),
                email: faker.internet.email(),
                phone: '9' + faker.random.number({
                  min: 20,
                  max: 99
                }) + faker.random.number({
                  min: 100000,
                  max: 999999
                }),
                gender: ['M', 'F'][faker.random.number({
                  min: 0,
                  max: 1
                })],
                birthday: date,
                state: 1,
                points: 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }, {
                tbl_role_id: 1,
                tbl_user_id: 3,
                tbl_avatar_id: 3,
                tbl_district_id: 3,
                slug: 'erick-valdez-3',
                firstname: 'erick',
                lastname: 'valdez',
                dni: 72766963,
                email: faker.internet.email(),
                phone: 922373214,
                gender: 'M',
                birthday: date,
                state: 1,
                points: 0,
                created_by: 'root',
                created_at: date,
                edited_by: 'root',
                edited_at: date
              }];
              data_2 = _toConsumableArray(Array(7)).map(function (profile, i) {
                return {
                  tbl_role_id: faker.random.number({
                    min: 1,
                    max: 3
                  }),
                  tbl_user_id: i + 4,
                  tbl_avatar_id: i + 4,
                  tbl_district_id: i + 4,
                  firstname: faker.name.firstName(),
                  lastname: faker.name.lastName(),
                  slug: faker.lorem.slug(),
                  dni: faker.random.number({
                    min: 10000000,
                    max: 99999999
                  }),
                  email: faker.internet.email(),
                  phone: '9' + faker.random.number({
                    min: 20,
                    max: 99
                  }) + faker.random.number({
                    min: 100000,
                    max: 999999
                  }),
                  gender: ['M', 'F'][faker.random.number({
                    min: 0,
                    max: 1
                  })],
                  birthday: date,
                  state: 1,
                  points: 0,
                  created_by: 'root',
                  created_at: date,
                  edited_by: 'root',
                  edited_at: date
                };
              });
              return _context.abrupt("return", queryInterface.bulkInsert('tbl_profiles', data_1.concat(data_2)));

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function up(_x, _x2) {
      return _up.apply(this, arguments);
    }

    return up;
  }(),
  down: function () {
    var _down = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(queryInterface, Sequelize) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function down(_x3, _x4) {
      return _down.apply(this, arguments);
    }

    return down;
  }()
};