"use strict";

var _socket = require("./socket");

require("@babel/polyfill");

var _app = _interopRequireDefault(require("./app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

var fs = require('fs');

var https = require('https');

function main() {
  return _main.apply(this, arguments);
}

function _main() {
  _main = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var node_port;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            node_port = process.env.NODE_PORT;

            if (!(process.env.NODE_ENV === 'development')) {
              _context.next = 9;
              break;
            }

            _context.next = 4;
            return _app["default"].listen(node_port);

          case 4:
            console.log('Server on port ' + node_port);
            _context.next = 7;
            return (0, _socket.initSocket)();

          case 7:
            _context.next = 12;
            break;

          case 9:
            //production
            https.createServer({
              key: fs.readFileSync('../../cert/privkey.pem', 'utf8'),
              cert: fs.readFileSync('../../cert/cert.pem', 'utf8'),
              ca: fs.readFileSync('../../cert/chain.pem', 'utf8')
            }, _app["default"]).listen(node_port, function () {
              console.log("My HTTPS server listening on port " + node_port + "...");
            });
            _context.next = 12;
            return (0, _socket.initSocketHttps)();

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _main.apply(this, arguments);
}

main();