"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListUser = getListUser;
exports.createUser = createUser;
exports.sendCod = sendCod;
exports.updateCod = updateCod;
exports.updatePhoneOld = updatePhoneOld;
exports.updatePhone = updatePhone;
exports.signin = signin;
exports.getUserById = getUserById;
exports.authDni = authDni;
exports.getA = getA;
exports.updatePassword = updatePassword;
exports.updateTokenDevice = updateTokenDevice;
exports.deleteUser = deleteUser;
exports.updateTutorial = updateTutorial;

var _passport = _interopRequireDefault(require("passport"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _token_controller = require("./token_controller");

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import Users from '../models/users_model'
var _require = require('../models'),
    tbl_user = _require.tbl_user,
    tbl_profile = _require.tbl_profile,
    tbl_avatar = _require.tbl_avatar;

var _require2 = require('../helpers'),
    isValidPassword = _require2.isValidPassword;

function getListUser(_x, _x2) {
  return _getListUser.apply(this, arguments);
}

function _getListUser() {
  _getListUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var users;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_user.findAll({
              where: {
                state: 1
              }
            });

          case 3:
            users = _context.sent;
            res.json({
              users: users
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListUser.apply(this, arguments);
}

function createUser(_x3, _x4) {
  return _createUser.apply(this, arguments);
}

function _createUser() {
  _createUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, username, password, role_id, passwordEncrypt, newUser;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _req$body = req.body, username = _req$body.username, password = _req$body.password, role_id = _req$body.role_id;
            _context2.next = 4;
            return _bcrypt["default"].hash(password, parseInt(process.env.BCRYPT_ROUNDS));

          case 4:
            passwordEncrypt = _context2.sent;
            _context2.next = 7;
            return tbl_user.create({
              username: username,
              password: passwordEncrypt,
              role_id: role_id
            }, {
              fields: ['username', 'password', 'role_id']
            });

          case 7:
            newUser = _context2.sent;
            if (newUser) signin(req, res);else res.status(500).json({
              message: 'Something goes wrong',
              data: {}
            });
            _context2.next = 15;
            break;

          case 11:
            _context2.prev = 11;
            _context2.t0 = _context2["catch"](0);
            console.error('You have a error createUser: ', _context2.t0);
            res.status(500).json({
              message: 'Something goes wrong',
              data: {}
            });

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 11]]);
  }));
  return _createUser.apply(this, arguments);
}

function sendCod(_x5, _x6) {
  return _sendCod.apply(this, arguments);
}

function _sendCod() {
  _sendCod = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var accountSid, authToken, client, phone, generateRandom, random, response;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            generateRandom = function _generateRandom(min, max) {
              return Math.round(Math.random() * (max - min) + min);
            };

            accountSid = 'ACc70e0bada40bf92e5fce7623325b9b88';
            authToken = '964357445c25fe2cf26efb21f3b7e86c'; // const MY_PHONE_NUMBER= '+51934575282';

            client = require('twilio')(accountSid, authToken);
            phone = req.body.phone;
            _context3.prev = 5;
            random = generateRandom(1000, 9999);
            _context3.next = 9;
            return client.messages.create({
              to: "+51".concat(phone),
              from: '+19706707670',
              body: ' Su codigo de verificacion para entrar a Mottiva es :' + random
            }).then(function (message) {
              return console.log(message.sid);
            });

          case 9:
            response = _context3.sent;
            res.status(200).send({
              message: "Verification is sent!!",
              data: {
                phone: phone,
                random: random,
                response: response
              }
            });
            _context3.next = 17;
            break;

          case 13:
            _context3.prev = 13;
            _context3.t0 = _context3["catch"](5);
            res.status(400).send({
              message: "Wrong phone number : ",
              phone: phone,
              error: _context3.t0
            });
            console.log(_context3.t0);

          case 17:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[5, 13]]);
  }));
  return _sendCod.apply(this, arguments);
}

function updateCod(_x7, _x8) {
  return _updateCod.apply(this, arguments);
}

function _updateCod() {
  _updateCod = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var accountSid, authToken, client, phonenumber, generateRandom, random, response;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            generateRandom = function _generateRandom2(min, max) {
              return Math.round(Math.random() * (max - min) + min);
            };

            accountSid = 'ACc70e0bada40bf92e5fce7623325b9b88';
            authToken = '964357445c25fe2cf26efb21f3b7e86c'; // const MY_PHONE_NUMBER= '+51934575282';

            client = require('twilio')(accountSid, authToken);
            phonenumber = req.body.phonenumber;
            _context4.prev = 5;
            random = generateRandom(1000, 9999);
            _context4.next = 9;
            return client.messages.create({
              to: "+51".concat(phonenumber),
              from: '+19706707670',
              body: ' Su codigo de verificacion para entrar a Mottiva es :' + random
            }).then(function (message) {
              return console.log(message.sid);
            });

          case 9:
            response = _context4.sent;
            res.status(200).send({
              message: "Verification is sent!!",
              data: {
                phonenumber: phonenumber,
                random: random,
                response: response
              }
            });
            _context4.next = 17;
            break;

          case 13:
            _context4.prev = 13;
            _context4.t0 = _context4["catch"](5);
            res.status(400).send({
              message: "Wrong phone number : ",
              phonenumber: phonenumber,
              error: _context4.t0
            });
            console.log(_context4.t0);

          case 17:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[5, 13]]);
  }));
  return _updateCod.apply(this, arguments);
}

function updatePhoneOld(_x9, _x10) {
  return _updatePhoneOld.apply(this, arguments);
}

function _updatePhoneOld() {
  _updatePhoneOld = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var user, users;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            /* const accountSid = 'ACc33dc3c8a5b3872d521207859fd805e6';
             const authToken = 'f4040991ee9c722905f094fbf094384a';
            // const MY_PHONE_NUMBER= '+51934575282';
             const client = require('twilio')(accountSid, authToken);*/
            user = req.params.user;
            /*function generateRandom(min,max) {
                return Math.round( Math.random() * (max - min)+ min);
            }*/

            _context5.next = 3;
            return tbl_user.findAll({
              attributes: ['username', 'phonenumber'],
              where: {
                username: user
              }
            });

          case 3:
            users = _context5.sent;
            // const cel = users.phonenumber;
            res.json({
              users: users
            });
            /*
                try {
                    let random = generateRandom(1000, 9999)
                    const response = await client.messages.create({
                        to: `+51${phone}`,
                        from: '+14154292145',
                        body: ' su codigo de verificacion es :' + random
                    })
                        .then(message => console.log(message.sid));
                    res.status(200).send({
                        message: "Verification is sent!!",
                        phone,
                        random,
                        response
                      })
                } catch (error) {
                    res.status(400).send({
                        message: "Wrong phone number : ",
                        phone,
                        error
                    });
                    console.log(error);
                  }*/

          case 5:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _updatePhoneOld.apply(this, arguments);
}

function sendTwilioPhoneCode(_x11) {
  return _sendTwilioPhoneCode.apply(this, arguments);
}

function _sendTwilioPhoneCode() {
  _sendTwilioPhoneCode = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(phone) {
    var accountSid, authToken, client, generateRandom, random;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            generateRandom = function _generateRandom3(min, max) {
              return Math.round(Math.random() * (max - min) + min);
            };

            // await new Promise((resolve, reject) => {
            //     // Llamamos a resolve(...) cuando lo que estabamos haciendo finaliza con éxito, y reject(...) cuando falla.
            //     // En este ejemplo, usamos setTimeout(...) para simular código asíncrono.
            //     // En la vida real, probablemente uses algo como XHR o una API HTML5.
            //     setTimeout(function(){
            //     //   resolve("¡Éxito!"); // ¡Todo salió bien!
            //         reject('Any')
            //     // throw 'Couldn\'t send SMS'
            //     }, 2000);
            //   }).then((successMessage) => {
            //     // succesMessage es lo que sea que pasamos en la función resolve(...) de arriba.
            //     // No tiene por qué ser un string, pero si solo es un mensaje de éxito, probablemente lo sea.
            //     console.log("¡Sí! " + successMessage);
            //   }).catch(e => {
            //     console.log('')
            //     console.log('e')
            //     console.log(e)
            //     console.log('')
            //     throw 'Error 123'
            //   });
            // throw 'Error 123'
            // return true
            accountSid = 'AC5ca0cbf2ffee60449e85f9fb88cb4d6e';
            authToken = 'cab1a08b148aab0fd98bbfec1b841c64';
            client = require('twilio')(accountSid, authToken);
            random = generateRandom(1000, 9999);
            _context6.next = 7;
            return client.messages.create({
              to: "+51".concat(phone),
              from: '+17707497065',
              body: 'Su codigo de verificacion es :' + random
            }).then(function (message) {
              return console.log(message.sid);
            })["catch"](function (e) {
              console.log('');
              console.log('e');
              console.log(e);
              console.log('');
              throw "Couldn't send SMS";
            });

          case 7:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _sendTwilioPhoneCode.apply(this, arguments);
}

function updatePhone(_x12, _x13) {
  return _updatePhone.apply(this, arguments);
}

function _updatePhone() {
  _updatePhone = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var user, _req$body2, old_phone, new_phone, profile, usuarioID, userName, token;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            user = req.user;
            _req$body2 = req.body, old_phone = _req$body2.old_phone, new_phone = _req$body2.new_phone;
            _context7.next = 5;
            return tbl_profile.findOne({
              where: {
                tbl_user_id: user.id,
                phone: old_phone
              }
            });

          case 5:
            profile = _context7.sent;

            if (!profile) {
              _context7.next = 15;
              break;
            }

            _context7.next = 9;
            return profile.update({
              phone: new_phone
            });

          case 9:
            usuarioID = user.id;
            userName = user.name;
            _context7.next = 13;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 13:
            token = _context7.sent;
            return _context7.abrupt("return", res.json({
              message: 'Phone Update!',
              data: {
                token: token
              }
            }));

          case 15:
            return _context7.abrupt("return", res.status(400).json({
              message: 'Incorrect phone number'
            }));

          case 18:
            _context7.prev = 18;
            _context7.t0 = _context7["catch"](0);
            console.error(_context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 18]]);
  }));
  return _updatePhone.apply(this, arguments);
}

function signin(_x14, _x15) {
  return _signin.apply(this, arguments);
}

function _signin() {
  _signin = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res) {
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            try {
              _passport["default"].authenticate('signin', /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(err, user, info) {
                  return regeneratorRuntime.wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          _context9.prev = 0;

                          if (!(err || !user)) {
                            _context9.next = 3;
                            break;
                          }

                          return _context9.abrupt("return", res.status(401).json({
                            message: 'Contraseña incorrecta',
                            data: {}
                          }));

                        case 3:
                          req.login(user, {
                            session: false
                          }, /*#__PURE__*/function () {
                            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(err) {
                              var usuarioID, userName, token;
                              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                while (1) {
                                  switch (_context8.prev = _context8.next) {
                                    case 0:
                                      if (!err) {
                                        _context8.next = 2;
                                        break;
                                      }

                                      return _context8.abrupt("return", res.status(402).json({
                                        message: 'Contraseña incorrecta',
                                        data: {}
                                      }));

                                    case 2:
                                      usuarioID = user.id;
                                      userName = user.username;
                                      _context8.next = 6;
                                      return (0, _token_controller.createToken)(req, res, usuarioID, userName);

                                    case 6:
                                      token = _context8.sent;
                                      return _context8.abrupt("return", res.json({
                                        message: 'User successfull',
                                        data: {
                                          token: token
                                        }
                                      }));

                                    case 8:
                                    case "end":
                                      return _context8.stop();
                                  }
                                }
                              }, _callee8);
                            }));

                            return function (_x34) {
                              return _ref2.apply(this, arguments);
                            };
                          }());
                          _context9.next = 10;
                          break;

                        case 6:
                          _context9.prev = 6;
                          _context9.t0 = _context9["catch"](0);
                          console.error('You have a error passport.authenticate', _context9.t0);
                          return _context9.abrupt("return", res.status(500).json({
                            message: 'Something went wrong',
                            data: {}
                          }));

                        case 10:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9, null, [[0, 6]]);
                }));

                return function (_x31, _x32, _x33) {
                  return _ref.apply(this, arguments);
                };
              }())(req, res);
            } catch (err) {
              console.error(err);
              res.status(500).json({
                message: 'Something goes wrong',
                data: {}
              });
            }

          case 1:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _signin.apply(this, arguments);
}

function getUserById(_x16, _x17) {
  return _getUserById.apply(this, arguments);
}

function _getUserById() {
  _getUserById = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(req, res) {
    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;
            console.log(req.body.a);
            _passport["default"].authenticate('jwt', {
              session: false
            }), function (req, res) {
              res.json({
                message: 'You did it! dahsfbaushd',
                user: req.user,
                token: req.query
              });
            };
            _context11.next = 9;
            break;

          case 5:
            _context11.prev = 5;
            _context11.t0 = _context11["catch"](0);
            console.error('You have a error getUserById', _context11.t0);
            return _context11.abrupt("return", res.status(500).json({
              message: 'Something goes wrong',
              data: {}
            }));

          case 9:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[0, 5]]);
  }));
  return _getUserById.apply(this, arguments);
}

function authDni(_x18, _x19) {
  return _authDni.apply(this, arguments);
}

function _authDni() {
  _authDni = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(req, res) {
    var _req$body3, dni, issue;

    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _req$body3 = req.body, dni = _req$body3.dni, issue = _req$body3.issue;
            res.status(200).send({
              message: "Dni & date correct!",
              data: {
                dni: dni,
                issue: issue
              }
            });

          case 2:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));
  return _authDni.apply(this, arguments);
}

function getA(_x20, _x21, _x22) {
  return _getA.apply(this, arguments);
}

function _getA() {
  _getA = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(req, res, next) {
    var token, decoded, user;
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            token = req.headers['x-access-token'];

            if (token) {
              _context13.next = 3;
              break;
            }

            return _context13.abrupt("return", res.status(401).json({
              auth: false,
              message: ' no token provided'
            }));

          case 3:
            decoded = _jsonwebtoken["default"].verify(token, 'globoazul_token');
            _context13.next = 6;
            return User.findByPk(decoded.user.id_user);

          case 6:
            user = _context13.sent;

            if (user) {
              _context13.next = 9;
              break;
            }

            return _context13.abrupt("return", res.status(404).send('no user found'));

          case 9:
            res.json(user);

          case 10:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));
  return _getA.apply(this, arguments);
}

function updatePassword(_x23, _x24) {
  return _updatePassword.apply(this, arguments);
}
/**export async function verifyphone(req,res){
    const accountSid = 'ACb7192d5962a2abf4df526e4604a007d2';
    const authToken = 'b0910ee5b3568393bc7cdd014e71237a';
    //const serviceid = 'VA09c4463ba2a961edeac414bbbc263334';
    const client = require('twilio')(accountSid, authToken);
    //const service = require('twilio')(serviceid);
    const {  phonenumber } = req.body;

    try {

        console.log(client);
        const response = await client.verify.services('VA09c4463ba2a961edeac414bbbc263334')
        .verifications
        .create({to:`+51${phonenumber}`, channel:'sms'})
        .then(verification => console.log(verification));;
        res.status(200).send({
            message: "Verification is sent!!",
            phonenumber,
            response

        })

    } catch (error) {
        res.status(400).send({
            message: "Wrong phone number :(",
            phonenumber,
            error
        });
        console.log(error);
    }

}
 */


function _updatePassword() {
  _updatePassword = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(req, res) {
    var username, _req$body4, password, password_new, passwordNewEncrypt, user, usuarioID, userName, token;

    return regeneratorRuntime.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.prev = 0;
            username = req.user.username;
            _req$body4 = req.body, password = _req$body4.password, password_new = _req$body4.password_new;
            _context14.next = 5;
            return _bcrypt["default"].hash(password_new, parseInt(process.env.BCRYPT_ROUNDS));

          case 5:
            passwordNewEncrypt = _context14.sent;
            _context14.next = 8;
            return tbl_user.findOne({
              where: {
                id: req.user.id
              }
            });

          case 8:
            user = _context14.sent;
            _context14.next = 11;
            return tbl_user.update({
              password: passwordNewEncrypt
            }, {
              where: {
                id: req.user.id
              }
            });

          case 11:
            usuarioID = user.id;
            userName = user.username;
            _context14.next = 15;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 15:
            token = _context14.sent;
            return _context14.abrupt("return", res.json({
              message: 'Password Update!',
              data: {
                token: token
              }
            }));

          case 19:
            _context14.prev = 19;
            _context14.t0 = _context14["catch"](0);
            console.error(_context14.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 23:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14, null, [[0, 19]]);
  }));
  return _updatePassword.apply(this, arguments);
}

function updateTokenDevice(_x25, _x26) {
  return _updateTokenDevice.apply(this, arguments);
}

function _updateTokenDevice() {
  _updateTokenDevice = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(req, res) {
    var userID, tokenDevice, TokenDevice;
    return regeneratorRuntime.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.prev = 0;
            userID = req.user.id;
            console.log(userID);
            tokenDevice = req.body.tokenDevice;
            _context15.next = 6;
            return tbl_user.findOne({
              where: {
                id: userID,
                token_device: tokenDevice
              }
            });

          case 6:
            TokenDevice = _context15.sent;

            if (TokenDevice) {
              _context15.next = 11;
              break;
            }

            _context15.next = 10;
            return tbl_user.update({
              token_device: tokenDevice
            }, {
              where: {
                id: userID
              }
            });

          case 10:
            return _context15.abrupt("return", res.json({
              message: 'Token Device Update!'
            }));

          case 11:
            return _context15.abrupt("return", res.json({
              message: 'the token device already exists!'
            }));

          case 14:
            _context15.prev = 14;
            _context15.t0 = _context15["catch"](0);
            console.error(_context15.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 18:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15, null, [[0, 14]]);
  }));
  return _updateTokenDevice.apply(this, arguments);
}

function deleteUser(_x27, _x28) {
  return _deleteUser.apply(this, arguments);
}

function _deleteUser() {
  _deleteUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(req, res) {
    return regeneratorRuntime.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.prev = 0;
            _context16.next = 3;
            return tbl_user.update({
              state: 0
            }, {
              where: {
                id: req.params.id
              }
            });

          case 3:
            return _context16.abrupt("return", res.json({
              message: 'User deleted'
            }));

          case 6:
            _context16.prev = 6;
            _context16.t0 = _context16["catch"](0);
            console.error(_context16.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 10:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16, null, [[0, 6]]);
  }));
  return _deleteUser.apply(this, arguments);
}

function updateTutorial(_x29, _x30) {
  return _updateTutorial.apply(this, arguments);
}

function _updateTutorial() {
  _updateTutorial = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(req, res) {
    return regeneratorRuntime.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            _context17.prev = 0;
            _context17.next = 3;
            return tbl_user.update({
              tutorial: 1
            }, {
              where: {
                id: req.user.id
              }
            });

          case 3:
            return _context17.abrupt("return", res.json({
              message: 'Update tutorial succeful'
            }));

          case 6:
            _context17.prev = 6;
            _context17.t0 = _context17["catch"](0);
            console.error(_context17.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 10:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17, null, [[0, 6]]);
  }));
  return _updateTutorial.apply(this, arguments);
}