"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createPoll = createPoll;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_poll = _require.tbl_poll;

function createPoll(_x, _x2) {
  return _createPoll.apply(this, arguments);
}

function _createPoll() {
  _createPoll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _req$body, session_id, experience, qualification, comment, newPoll, poll;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, session_id = _req$body.session_id, experience = _req$body.experience, qualification = _req$body.qualification, comment = _req$body.comment;
            _context.prev = 1;
            _context.next = 4;
            return tbl_poll.create({
              tbl_session_id: session_id,
              experience: experience,
              qualification: qualification,
              comment: comment,
              created_by: 'root',
              edited_by: 'root'
            });

          case 4:
            newPoll = _context.sent;

            if (newPoll) {
              poll = {
                id: newPoll.id,
                idSession: newPoll.tbl_session_id,
                experience: newPoll.experience,
                qualification: newPoll.qualification,
                comment: newPoll.comment
              };
              res.json({
                message: 'Poll created',
                data: poll
              });
            }

            _context.next = 12;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 8]]);
  }));
  return _createPoll.apply(this, arguments);
}