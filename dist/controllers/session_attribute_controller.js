"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setSessionAttribute = setSessionAttribute;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_session_attribute = _require.tbl_session_attribute;

function setSessionAttribute(_x, _x2) {
  return _setSessionAttribute.apply(this, arguments);
}

function _setSessionAttribute() {
  _setSessionAttribute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var id, _req$body, key, value, sessionAttribute, sessionAttributeUpdate;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            id = req.params.id;
            _req$body = req.body, key = _req$body.key, value = _req$body.value;
            _context.next = 5;
            return tbl_session_attribute.findOne({
              where: {
                state: 1,
                tbl_session_id: id,
                key: key
              },
              order: [['id', 'DESC']]
            });

          case 5:
            sessionAttribute = _context.sent;

            if (!sessionAttribute) {
              _context.next = 12;
              break;
            }

            _context.next = 9;
            return tbl_session_attribute.update({
              value: value
            }, {
              where: {
                id: sessionAttribute.dataValues.id
              }
            });

          case 9:
            sessionAttributeUpdate = _context.sent;
            _context.next = 15;
            break;

          case 12:
            _context.next = 14;
            return tbl_session_attribute.create({
              tbl_session_id: id,
              key: key,
              value: value
            });

          case 14:
            sessionAttributeUpdate = _context.sent;

          case 15:
            if (sessionAttributeUpdate) res.status(200).json({
              state: true,
              message: 'Save attribute'
            });else res.status(200).json({
              state: false,
              message: 'Don´t save'
            });
            _context.next = 22;
            break;

          case 18:
            _context.prev = 18;
            _context.t0 = _context["catch"](0);
            console.log('You have a error:', _context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 18]]);
  }));
  return _setSessionAttribute.apply(this, arguments);
}