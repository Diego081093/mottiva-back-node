"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllProtocol = getAllProtocol;
exports.getProtocol = getProtocol;
exports.createProtocol = createProtocol;

var _sequelize = require("sequelize");

var _luxon = require("luxon");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_protocol = _require.tbl_protocol,
    tbl_protocol_process = _require.tbl_protocol_process,
    tbl_profile = _require.tbl_profile;

var MottivaConstants = require('./../helpers');

function getAllProtocol(_x, _x2) {
  return _getAllProtocol.apply(this, arguments);
}

function _getAllProtocol() {
  _getAllProtocol = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var processId, date, whereProtProcess, whereDate, profileAttributes, _created_at, protocols;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            processId = req.query.processId, date = req.query.date;
            whereProtProcess = {}, whereDate = {};
            profileAttributes = ['id', 'slug', 'firstname', 'lastname', 'state'];

            if (processId) {
              whereProtProcess = {
                tbl_prot_process_id: processId
              };
            }

            if (date) {
              whereDate = {
                created_at: (_created_at = {}, _defineProperty(_created_at, _sequelize.Op.gte, date), _defineProperty(_created_at, _sequelize.Op.lt, _luxon.DateTime.fromFormat(date, 'yyyy-MM-dd').plus({
                  days: 1
                }).toISO()), _created_at)
              };
            }

            _context.next = 8;
            return tbl_protocol.findAll({
              where: _objectSpread(_objectSpread({
                state: 1
              }, whereProtProcess), whereDate),
              include: [{
                model: tbl_protocol_process,
                as: 'ProtocolProcess',
                attributes: ['id', 'name']
              }, {
                model: tbl_profile,
                as: 'Responsible',
                attributes: profileAttributes
              }, {
                model: tbl_profile,
                as: 'Approver',
                attributes: profileAttributes
              }],
              attributes: ['id', 'code', 'goal', 'state', 'createdAt']
            });

          case 8:
            protocols = _context.sent;
            res.json({
              message: 'Protocols found',
              data: protocols
            });
            _context.next = 16;
            break;

          case 12:
            _context.prev = 12;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 12]]);
  }));
  return _getAllProtocol.apply(this, arguments);
}

function getProtocol(_x3, _x4) {
  return _getProtocol.apply(this, arguments);
}

function _getProtocol() {
  _getProtocol = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var profileAttributes, protocol;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            profileAttributes = ['id', 'slug', 'firstname', 'lastname', 'state'];
            _context2.next = 4;
            return tbl_protocol.findOne({
              where: {
                id: req.params.id,
                state: 1
              },
              include: [{
                model: tbl_protocol_process,
                as: 'ProtocolProcess',
                attributes: ['id', 'name']
              }, {
                model: tbl_profile,
                as: 'Responsible',
                attributes: profileAttributes
              }, {
                model: tbl_profile,
                as: 'Approver',
                attributes: profileAttributes
              }],
              attributes: ['id', 'code', 'goal', 'introduction', 'procedure', 'state', 'createdAt']
            });

          case 4:
            protocol = _context2.sent;

            if (!protocol) {
              _context2.next = 7;
              break;
            }

            return _context2.abrupt("return", res.json({
              message: 'Protocol found',
              data: protocol
            }));

          case 7:
            res.status(404).json({
              message: 'Protocol not found'
            });
            _context2.next = 14;
            break;

          case 10:
            _context2.prev = 10;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 10]]);
  }));
  return _getProtocol.apply(this, arguments);
}

function createProtocol(_x5, _x6) {
  return _createProtocol.apply(this, arguments);
}

function _createProtocol() {
  _createProtocol = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, code, goal, responsibleId, processId, introduction, procedure;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;

            if (!(req.user.idRole == MottivaConstants.HELPDESK_ROLE)) {
              _context3.next = 6;
              break;
            }

            _req$body = req.body, code = _req$body.code, goal = _req$body.goal, responsibleId = _req$body.responsibleId, processId = _req$body.processId, introduction = _req$body.introduction, procedure = _req$body.procedure;
            _context3.next = 5;
            return tbl_protocol.create({
              code: code,
              tbl_prot_process_id: processId,
              goal: goal,
              tbl_profile_responsible_id: responsibleId,
              introduction: introduction,
              procedure: procedure
            });

          case 5:
            return _context3.abrupt("return", res.json({
              message: 'Protocol created'
            }));

          case 6:
            res.status(401).json({
              message: 'Access denied with provided credentials'
            });
            _context3.next = 13;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _createProtocol.apply(this, arguments);
}