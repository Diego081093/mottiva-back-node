"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListProfile = getListProfile;
exports.getProfile = getProfile;
exports.getPointsUsed = getPointsUsed;
exports.createProfile = createProfile;
exports.getListPatients = getListPatients;
exports.getListPsychologist = getListPsychologist;
exports.indexPatients = indexPatients;
exports.createPatient = createPatient;
exports.updateProfile = updateProfile;
exports.updateProfileOnly = updateProfileOnly;
exports.updateProfilePhoto = updateProfilePhoto;
exports.updatePhoto = updatePhoto;
exports.updateProfileAvatar = updateProfileAvatar;
exports.updateProfilePhone = updateProfilePhone;
exports.deleteProfile = deleteProfile;
exports.getPsycology = getPsycology;
exports.updatePsycology = updatePsycology;

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _passport = require("passport");

var _token_controller = require("./token_controller");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_profile = _require.tbl_profile,
    tbl_role = _require.tbl_role,
    tbl_institution = _require.tbl_institution,
    tbl_district = _require.tbl_district,
    tbl_city = _require.tbl_city,
    tbl_countrie = _require.tbl_countrie,
    tbl_user = _require.tbl_user,
    tbl_user_secret_question = _require.tbl_user_secret_question,
    tbl_profile_attribute = _require.tbl_profile_attribute,
    tbl_profile_specialty = _require.tbl_profile_specialty,
    tbl_specialty = _require.tbl_specialty,
    tbl_profile_institution = _require.tbl_profile_institution,
    tbl_session = _require.tbl_session,
    tbl_session_attribute = _require.tbl_session_attribute,
    tbl_avatar = _require.tbl_avatar,
    tbl_benefit_profile = _require.tbl_benefit_profile,
    tbl_benefit = _require.tbl_benefit,
    tbl_company_benefit = _require.tbl_company_benefit,
    tbl_availability = _require.tbl_availability;

var _require2 = require('./../helpers'),
    isValidBase64Img = _require2.isValidBase64Img,
    s3UploadBase64Image = _require2.s3UploadBase64Image;

var _require3 = require('sequelize'),
    Op = _require3.Op;

var _require4 = require('luxon'),
    DateTime = _require4.DateTime;

function getListProfile(_x, _x2) {
  return _getListProfile.apply(this, arguments);
}

function _getListProfile() {
  _getListProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var profiles;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_profile.findAll({
              where: {
                state: 1
              }
            });

          case 3:
            profiles = _context.sent;
            res.json({
              message: 'Profiles found',
              data: profiles
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListProfile.apply(this, arguments);
}

function getProfile(_x3, _x4) {
  return _getProfile.apply(this, arguments);
}

function _getProfile() {
  _getProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var profile, profile_attributes, attributes;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return tbl_profile.findByPk(req.user.idProfile, {
              where: {
                state: 1
              },
              include: [{
                model: tbl_role,
                as: 'Role',
                attributes: ['id', 'name', 'alias']
              }, {
                model: tbl_avatar,
                as: 'Avatar',
                attributes: ['image']
              }, {
                model: tbl_profile_institution,
                as: 'ProfileInstitutions',
                include: {
                  model: tbl_institution,
                  as: 'Institution',
                  attributes: ['name']
                },
                attributes: ['academic_degree', 'start_date', 'ending_date', 'document']
              }, {
                model: tbl_specialty,
                as: 'Specialties',
                through: {
                  where: {
                    state: 1
                  },
                  attributes: []
                },
                attributes: ['id', 'name', 'alias', 'icon']
              }, {
                model: tbl_district,
                as: 'District',
                include: {
                  model: tbl_city,
                  as: 'City',
                  include: {
                    model: tbl_countrie,
                    as: 'Country',
                    attributes: ['name']
                  },
                  attributes: ['name']
                },
                attributes: ['name']
              }, {
                model: tbl_availability,
                as: 'Availabilities',
                where: {
                  state: 1
                },
                required: false,
                attributes: ['id', 'day', 'start_hour', 'end_hour']
              }],
              attributes: ['id', 'slug', 'firstname', 'lastname', 'dni', 'photo_dni', 'email', 'phone', 'gender', 'birthday', 'state', 'req_sent_at', 'req_state', 'req_approved_at']
            });

          case 3:
            profile = _context2.sent;
            _context2.next = 6;
            return tbl_profile_attribute.findAll({
              where: {
                tbl_profile_id: req.user.idProfile
              },
              attributes: ['key', 'value']
            });

          case 6:
            profile_attributes = _context2.sent;
            attributes = {};
            profile_attributes.map(function (item) {
              attributes[item.key] = item.value;
            });
            profile.dataValues['Agenda'] = attributes;
            res.json({
              message: 'Profile found',
              data: profile
            });
            _context2.next = 17;
            break;

          case 13:
            _context2.prev = 13;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 13]]);
  }));
  return _getProfile.apply(this, arguments);
}

function getPointsUsed(_x5, _x6) {
  return _getPointsUsed.apply(this, arguments);
}

function _getPointsUsed() {
  _getPointsUsed = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var benefits;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return tbl_benefit_profile.findAll({
              where: {
                tbl_profile_id: req.user.idProfile
              },
              include: {
                model: tbl_benefit,
                as: 'Benefit',
                attributes: ['id', 'name', 'image', 'description', 'point'],
                include: {
                  model: tbl_company_benefit,
                  as: 'CompanyBenefit',
                  attributes: ['name', 'image']
                }
              },
              attributes: ['id', 'state']
            });

          case 3:
            benefits = _context3.sent;
            res.json({
              message: 'Benefits found',
              data: benefits
            });
            _context3.next = 11;
            break;

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 7]]);
  }));
  return _getPointsUsed.apply(this, arguments);
}

function createProfile(_x7, _x8) {
  return _createProfile.apply(this, arguments);
}

function _createProfile() {
  _createProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _req$body, password, dni, firstname, lastname, email, phone, birthday, gender, role, image, lastnameNew, emailNew, phoneNew, passwordEncrypt, newUser, usuarioID, userName, string, slug, photo, avatar, dniExist, newProfile, token;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _req$body = req.body, password = _req$body.password, dni = _req$body.dni, firstname = _req$body.firstname, lastname = _req$body.lastname, email = _req$body.email, phone = _req$body.phone, birthday = _req$body.birthday, gender = _req$body.gender, role = _req$body.role, image = _req$body.image;
            lastnameNew = lastname ? lastname : '';
            emailNew = email ? email : '';
            phoneNew = phone ? phone : '';
            _context4.next = 7;
            return _bcrypt["default"].hash(password, parseInt(process.env.BCRYPT_ROUNDS));

          case 7:
            passwordEncrypt = _context4.sent;
            _context4.next = 10;
            return tbl_user.create({
              username: emailNew,
              password: passwordEncrypt
            });

          case 10:
            newUser = _context4.sent;
            usuarioID = newUser.id;
            userName = newUser.username;
            string = "".concat(firstname, " ").concat(lastnameNew, " ").concat(newUser.id);
            slug = string.toLowerCase().replace(/\s+/g, '-');
            _context4.next = 17;
            return savePhoto(req, res, image, 'uploads/profile-photo/');

          case 17:
            photo = _context4.sent;

            if (photo.success) {
              _context4.next = 20;
              break;
            }

            return _context4.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 20:
            _context4.next = 22;
            return tbl_avatar.create({
              image: photo.data.Location,
              "default": 0
            });

          case 22:
            avatar = _context4.sent;
            dniExist = tbl_profile.findOne({
              where: {
                dni: dni
              }
            });

            if (!(avatar && !dniExist)) {
              _context4.next = 35;
              break;
            }

            _context4.next = 27;
            return tbl_profile.create({
              slug: slug,
              firstname: firstname,
              'lastname': lastnameNew,
              'dni': dni,
              'email': emailNew,
              'phone': phoneNew,
              gender: gender,
              birthday: birthday,
              'tbl_user_id': newUser.id,
              'tbl_district_id': null,
              'tbl_role_id': role,
              'tbl_avatar_id': avatar.id
            });

          case 27:
            newProfile = _context4.sent;
            _context4.next = 30;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 30:
            token = _context4.sent;

            if (newProfile) {
              res.json({
                message: 'profile created',
                data: {
                  token: token
                }
              });
            }

            if (dniExist) {
              res.json({
                message: 'dni exist!!',
                data: []
              });
            }

            _context4.next = 36;
            break;

          case 35:
            res.status(500).json({
              message: 'Something went error'
            });

          case 36:
            _context4.next = 42;
            break;

          case 38:
            _context4.prev = 38;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 42:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 38]]);
  }));
  return _createProfile.apply(this, arguments);
}

function getListPatients(_x9, _x10) {
  return _getListPatients.apply(this, arguments);
}

function _getListPatients() {
  _getListPatients = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _req$query, city, search, institution, age, age_range_min, age_range_max, sessions, whereQuery, whereCityQuery, whereInstQuery, instRequired, now, commonAttrib, _birthday, _birthday2, patient, data;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _req$query = req.query, city = _req$query.city, search = _req$query.search, institution = _req$query.institution, age = _req$query.age, age_range_min = _req$query.age_range_min, age_range_max = _req$query.age_range_max, sessions = _req$query.sessions;
            whereQuery = {}, whereCityQuery = {}, whereInstQuery = {}, instRequired = false;

            if (city) {
              whereCityQuery = {
                id: city
              };
            }

            if (institution) {
              instRequired = true;
              whereInstQuery = {
                id: institution
              };
            }

            now = DateTime.now();
            commonAttrib = {
              day: 1,
              month: now.month + 1
            };

            if (age) {
              whereQuery = {
                birthday: (_birthday = {}, _defineProperty(_birthday, Op.gte, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                  year: now.year - age - 1
                })).toISO()), _defineProperty(_birthday, Op.lt, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                  year: now.year - age
                })).toISO()), _birthday)
              }; // Fecha minima
              // dia: 1
              // month: current month + 1
              // year: current year - age - 1
              // Fecha maxima
              // dia: last current month
              // month: current month
              // year: current year - age
            } else {
              if (age_range_min && age_range_max) {
                whereQuery = {
                  birthday: (_birthday2 = {}, _defineProperty(_birthday2, Op.gte, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                    year: now.year - age_range_max - 1
                  })).toISO()), _defineProperty(_birthday2, Op.lt, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                    year: now.year - age_range_min
                  })).toISO()), _birthday2)
                };
              }
            }

            if (search) {
              whereQuery[Op.or] = [{
                firstname: _defineProperty({}, Op.like, "%".concat(search, "%"))
              }, {
                lastname: _defineProperty({}, Op.like, "%".concat(search, "%"))
              }];
            }

            console.log('whereQuery', whereQuery);
            _context6.next = 12;
            return tbl_profile.findAll({
              where: _objectSpread(_objectSpread({}, whereQuery), {}, {
                tbl_role_id: 1
              }),
              include: [{
                model: tbl_district,
                as: 'District',
                attributes: ['name'],
                required: false,
                include: {
                  model: tbl_city,
                  as: 'City',
                  where: whereCityQuery,
                  attributes: ['id', 'name']
                }
              }, {
                model: tbl_profile_institution,
                as: 'ProfileInstitutions',
                attributes: ['tbl_institution_id'],
                required: instRequired,
                include: {
                  model: tbl_institution,
                  as: 'Institution',
                  where: whereInstQuery,
                  attributes: ['name']
                }
              }]
            });

          case 12:
            patient = _context6.sent;
            data = [];
            _context6.next = 16;
            return Promise.all(patient.map(function (item) {
              return new Promise( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(resolve) {
                  var fechaActual, year, birthdayPatient, newBirthdayPatient, yearPatient, age;
                  return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          _context5.next = 2;
                          return tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: item.id,
                              state: 1
                            }
                          });

                        case 2:
                          item.dataValues.sessions = _context5.sent;
                          _context5.next = 5;
                          return tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: item.id,
                              completed: true,
                              state: 1
                            }
                          });

                        case 5:
                          item.dataValues.sessionsCompleted = _context5.sent;
                          _context5.next = 8;
                          return tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: item.id,
                              state: 0
                            }
                          });

                        case 8:
                          item.dataValues.sessionsTruncate = _context5.sent;
                          _context5.next = 11;
                          return tbl_session.findOne({
                            where: {
                              tbl_profile_pacient_id: item.id
                            },
                            order: [['id', 'DESC']],
                            attributes: ['id', 'completed']
                          });

                        case 11:
                          item.dataValues.program = _context5.sent;
                          fechaActual = new Date();
                          year = fechaActual.getFullYear();
                          birthdayPatient = item.birthday;
                          newBirthdayPatient = new Date(birthdayPatient);
                          yearPatient = newBirthdayPatient.getFullYear();
                          age = year - yearPatient;
                          item.setDataValue('age', age);
                          /*item.setDataValue('notificationState','Sesión cancelada')
                          item.setDataValue('psychologist',psychologist)*/

                          /*item.setDataValue('notificationState','Sesión cancelada')
                          item.setDataValue('psychologist',psychologist)*/
                          data.push(item);
                          resolve(1);

                        case 21:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5);
                }));

                return function (_x39) {
                  return _ref.apply(this, arguments);
                };
              }());
            }));

          case 16:
            if (sessions) {
              data = data.filter(function (profile) {
                return profile.dataValues.sessions == sessions;
              });
            }

            res.json({
              message: 'Patients found',
              data: data
            });
            _context6.next = 24;
            break;

          case 20:
            _context6.prev = 20;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 24:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 20]]);
  }));
  return _getListPatients.apply(this, arguments);
}

function getListPsychologist(_x11, _x12) {
  return _getListPsychologist.apply(this, arguments);
}

function _getListPsychologist() {
  _getListPsychologist = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var _req$query2, city, institution, age, age_range_min, age_range_max, patients, now, whereCityQuery, whereInstQuery, whereQuery, commonAttrib, _whereQuery$birthday, _whereQuery$birthday2, patient, data;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            _req$query2 = req.query, city = _req$query2.city, institution = _req$query2.institution, age = _req$query2.age, age_range_min = _req$query2.age_range_min, age_range_max = _req$query2.age_range_max, patients = _req$query2.patients;
            now = DateTime.now();
            whereCityQuery = {}, whereInstQuery = {}, whereQuery = {};
            if (city) whereCityQuery.id = city;
            if (institution) whereInstQuery.id = institution;
            commonAttrib = {
              day: 1,
              month: now.month + 1
            };

            if (age) {
              whereQuery.birthday = (_whereQuery$birthday = {}, _defineProperty(_whereQuery$birthday, Op.gte, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                year: now.year - age - 1
              })).toISO()), _defineProperty(_whereQuery$birthday, Op.lt, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                year: now.year - age
              })).toISO()), _whereQuery$birthday); // Fecha minima
              // dia: 1
              // month: current month + 1
              // year: current year - age - 1
              // Fecha maxima
              // dia: last current month
              // month: current month
              // year: current year - age
            } else {
              if (age_range_min && age_range_max) whereQuery.birthday = (_whereQuery$birthday2 = {}, _defineProperty(_whereQuery$birthday2, Op.gte, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                year: now.year - age_range_max - 1
              })).toISO()), _defineProperty(_whereQuery$birthday2, Op.lt, DateTime.fromObject(_objectSpread(_objectSpread({}, commonAttrib), {}, {
                year: now.year - age_range_min
              })).toISO()), _whereQuery$birthday2);
            }

            _context8.next = 10;
            return tbl_profile.findAll({
              where: _objectSpread(_objectSpread({}, whereQuery), {}, {
                tbl_role_id: 2
              }),
              include: [{
                model: tbl_district,
                as: 'District',
                attributes: ['name'],
                required: false,
                include: {
                  model: tbl_city,
                  as: 'City',
                  where: whereCityQuery,
                  attributes: ['id', 'name']
                }
              }, {
                model: tbl_profile_institution,
                as: 'ProfileInstitutions',
                attributes: ['tbl_institution_id'],
                required: false,
                include: {
                  model: tbl_institution,
                  as: 'Institution',
                  where: whereInstQuery,
                  attributes: ['name']
                }
              }]
            });

          case 10:
            patient = _context8.sent;
            data = [];
            _context8.next = 14;
            return Promise.all(patient.map(function (item) {
              return new Promise( /*#__PURE__*/function () {
                var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(resolve) {
                  return regeneratorRuntime.wrap(function _callee7$(_context7) {
                    while (1) {
                      switch (_context7.prev = _context7.next) {
                        case 0:
                          _context7.next = 2;
                          return tbl_session.count({
                            where: {
                              tbl_profile_psychologist_id: item.id
                            }
                          });

                        case 2:
                          item.dataValues.patients = _context7.sent;
                          _context7.next = 5;
                          return tbl_session.count({
                            where: {
                              completed: false,
                              tbl_profile_psychologist_id: item.id
                            }
                          });

                        case 5:
                          item.dataValues.patientsActives = _context7.sent;
                          // item.setDataValue('notificationState','Sesión cancelada')
                          // item.setDataValue('psychologist',psychologist)
                          data.push(item);
                          resolve(1);

                        case 8:
                        case "end":
                          return _context7.stop();
                      }
                    }
                  }, _callee7);
                }));

                return function (_x40) {
                  return _ref2.apply(this, arguments);
                };
              }());
            }));

          case 14:
            if (patients) data = data.filter(function (profile) {
              return profile.dataValues.patients == patients;
            });
            res.json({
              message: 'Psychologist found',
              data: data
            });
            _context8.next = 22;
            break;

          case 18:
            _context8.prev = 18;
            _context8.t0 = _context8["catch"](0);
            console.log(_context8.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 18]]);
  }));
  return _getListPsychologist.apply(this, arguments);
}

function indexPatients(_x13, _x14) {
  return _indexPatients.apply(this, arguments);
}

function _indexPatients() {
  _indexPatients = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res) {
    var _date, _date2, _date3, _date4;

    var to, whereQuery, now, weekday, today, patients, data;
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            to = req.query.to;
            console.log('');
            console.log('to');
            console.log(to);
            console.log('');
            // Today, 00:00:00
            now = DateTime.fromObject({
              day: DateTime.now().day
            });
            _context10.t0 = to;
            _context10.next = _context10.t0 === 'today' ? 10 : _context10.t0 === 'week' ? 12 : _context10.t0 === 'month' ? 15 : _context10.t0 === 'year' ? 17 : 19;
            break;

          case 10:
            whereQuery = {
              date: (_date = {}, _defineProperty(_date, Op.gte, now.toISO()), _defineProperty(_date, Op.lt, now.plus({
                days: 1
              }).toISO()), _date)
            };
            return _context10.abrupt("break", 21);

          case 12:
            weekday = now.weekday;
            whereQuery = {
              date: (_date2 = {}, _defineProperty(_date2, Op.gte, now.plus({
                days: 1 - weekday
              }).toISO()), _defineProperty(_date2, Op.lt, now.plus({
                days: 8 - weekday
              }).toISO()), _date2)
            };
            return _context10.abrupt("break", 21);

          case 15:
            whereQuery = {
              date: (_date3 = {}, _defineProperty(_date3, Op.gte, DateTime.fromObject({
                day: 1
              }).toISO()), _defineProperty(_date3, Op.lt, DateTime.fromObject({
                day: 1,
                month: now.month + 1
              }).toISO()), _date3)
            };
            return _context10.abrupt("break", 21);

          case 17:
            whereQuery = {
              date: (_date4 = {}, _defineProperty(_date4, Op.gte, DateTime.fromObject({
                day: 1,
                month: 1
              }).toISO()), _defineProperty(_date4, Op.lt, DateTime.fromObject({
                day: 1,
                month: 1,
                year: now.year + 1
              }).toISO()), _date4)
            };
            return _context10.abrupt("break", 21);

          case 19:
            whereQuery = {};
            return _context10.abrupt("break", 21);

          case 21:
            today = new Date();
            _context10.next = 24;
            return tbl_session.findAll({
              where: _objectSpread(_objectSpread({}, whereQuery), {}, {
                tbl_profile_psychologist_id: req.user.idProfile,
                order: 1
              }),
              group: 'tbl_profile_pacient_id',
              attributes: [['tbl_profile_pacient_id', 'id']]
            });

          case 24:
            patients = _context10.sent;
            data = [];
            _context10.next = 28;
            return Promise.all(patients.map(function (_ref3) {
              var id = _ref3.id;
              return new Promise( /*#__PURE__*/function () {
                var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(resolve) {
                  var p_profile, p_sessions, _yield$Promise$all, _yield$Promise$all2, patient, sessions, current_session, last_session, cdltsd, index_last_session, w_i;

                  return regeneratorRuntime.wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          p_profile = tbl_profile.findByPk(id, {
                            attributes: ['firstname', 'lastname'],
                            include: {
                              model: tbl_avatar,
                              as: 'Avatar',
                              attributes: ['image']
                            }
                          });
                          p_sessions = tbl_session.findAll({
                            where: {
                              tbl_profile_psychologist_id: req.user.idProfile,
                              tbl_profile_pacient_id: id,
                              state: 1
                            },
                            include: {
                              model: tbl_session_attribute,
                              as: 'SessionAttributes',
                              attributes: ['key', 'value']
                            },
                            attributes: ['id', 'date', 'order'],
                            order: [['order', 'DESC']]
                          });
                          _context9.next = 4;
                          return Promise.all([p_profile, p_sessions]);

                        case 4:
                          _yield$Promise$all = _context9.sent;
                          _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 2);
                          patient = _yield$Promise$all2[0];
                          sessions = _yield$Promise$all2[1];
                          current_session = sessions[0].order;

                          if (sessions.length > 1) {
                            last_session = sessions[0];

                            if (today < last_session.date) {
                              // Current Date Less Than Session Date
                              // cdltsd
                              cdltsd = true, index_last_session = 1, w_i = 0;

                              while (cdltsd) {
                                if (w_i < sessions.length) {
                                  if (!(today < sessions[w_i].date)) {
                                    index_last_session = w_i;
                                    cdltsd = false;
                                  }

                                  w_i += 1;
                                } else {
                                  // console.log('')
                                  // console.log('Salvado por el length')
                                  // console.log('')
                                  cdltsd = false;
                                }
                              }

                              current_session = sessions[index_last_session].order;
                            }
                          }

                          data.push({
                            id: id,
                            firstname: patient.firstname,
                            lastname: patient.lastname,
                            image: patient.Avatar.image,
                            current_session: current_session,
                            total_sessions: sessions.length,
                            sessions: sessions
                          });
                          resolve(1);

                        case 12:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9);
                }));

                return function (_x41) {
                  return _ref4.apply(this, arguments);
                };
              }());
            }));

          case 28:
            res.json({
              message: 'Patients found',
              patients: data
            });
            _context10.next = 35;
            break;

          case 31:
            _context10.prev = 31;
            _context10.t1 = _context10["catch"](0);
            console.log(_context10.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 35:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 31]]);
  }));
  return _indexPatients.apply(this, arguments);
}

function createPatient(_x15, _x16) {
  return _createPatient.apply(this, arguments);
}

function _createPatient() {
  _createPatient = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(req, res) {
    var _req$body2, username, password, firstname, lastname, email, phone, avatar_id, secret_question_id, secret_question_answer, Avatar, idAvatar, avatarNew, lastnameNew, emailNew, dni, phoneNew, gender, birthday, passwordEncrypt, newUser, usuarioID, userName, string, slug, newProfile, newAnswer, token;

    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _req$body2 = req.body, username = _req$body2.username, password = _req$body2.password, firstname = _req$body2.firstname, lastname = _req$body2.lastname, email = _req$body2.email, phone = _req$body2.phone, avatar_id = _req$body2.avatar_id, secret_question_id = _req$body2.secret_question_id, secret_question_answer = _req$body2.secret_question_answer;
            _context11.next = 3;
            return tbl_avatar.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 3:
            Avatar = _context11.sent;
            idAvatar = Avatar.id;
            avatarNew = avatar_id ? avatar_id : idAvatar;
            lastnameNew = lastname ? lastname : '';
            emailNew = email ? email : '';
            dni = username ? username : '';
            phoneNew = phone ? phone : '';
            gender = 'F';
            birthday = "1994-11-12";
            _context11.next = 14;
            return _bcrypt["default"].hash(password, parseInt(process.env.BCRYPT_ROUNDS));

          case 14:
            passwordEncrypt = _context11.sent;
            _context11.prev = 15;
            _context11.next = 18;
            return tbl_user.create({
              username: username,
              password: passwordEncrypt
            });

          case 18:
            newUser = _context11.sent;
            usuarioID = newUser.id;
            userName = newUser.username;
            string = "".concat(firstname, " ").concat(lastnameNew, " ").concat(newUser.id);
            slug = string.toLowerCase().replace(/\s+/g, '-');
            _context11.next = 25;
            return tbl_profile.create({
              slug: slug,
              firstname: firstname,
              'lastname': lastnameNew,
              'dni': dni,
              'email': emailNew,
              'phone': phoneNew,
              gender: gender,
              birthday: birthday,
              tbl_user_id: newUser.id,
              tbl_role_id: 1,
              tbl_avatar_id: avatarNew
            });

          case 25:
            newProfile = _context11.sent;
            _context11.next = 28;
            return tbl_user_secret_question.create({
              tbl_user_id: newUser.id,
              tbl_secret_question_id: secret_question_id,
              answer: secret_question_answer
            });

          case 28:
            newAnswer = _context11.sent;
            _context11.next = 31;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 31:
            token = _context11.sent;

            if (newProfile) {
              res.json({
                message: 'profile created',
                data: {
                  token: token
                }
              });
            }

            _context11.next = 39;
            break;

          case 35:
            _context11.prev = 35;
            _context11.t0 = _context11["catch"](15);
            console.log(_context11.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 39:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[15, 35]]);
  }));
  return _createPatient.apply(this, arguments);
}

function updateProfile(_x17, _x18) {
  return _updateProfile.apply(this, arguments);
}

function _updateProfile() {
  _updateProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(req, res) {
    var _req$body3, username, firstname, lastname, civilState, ocupation, studyNivel, id, lastnameNew, gender, birthday, usuarioID, userName, string, slug, newProfile, token;

    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _req$body3 = req.body, username = _req$body3.username, firstname = _req$body3.firstname, lastname = _req$body3.lastname, civilState = _req$body3.civilState, ocupation = _req$body3.ocupation, studyNivel = _req$body3.studyNivel;
            id = req.user.idProfile;
            lastnameNew = lastname ? lastname : '';
            gender = 'F';
            birthday = "1994-11-12";
            /*
            nombre, apellidos, estado civil, ocupacion, nivel de estudios
            */

            _context12.prev = 5;
            usuarioID = req.user.id;
            userName = req.user.username;
            string = "".concat(firstname, " ").concat(lastnameNew, " ").concat(usuarioID);
            slug = string.toLowerCase().replace(/\s+/g, '-');
            _context12.next = 12;
            return tbl_profile.update({
              slug: slug,
              firstname: firstname,
              'lastname': lastnameNew,
              gender: gender,
              birthday: birthday,
              'civil_state': civilState,
              ocupation: ocupation,
              'study_nivel': studyNivel,
              tbl_user_id: usuarioID,
              tbl_role_id: req.user.idRole
            }, {
              where: {
                id: id
              }
            });

          case 12:
            newProfile = _context12.sent;
            _context12.next = 15;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 15:
            token = _context12.sent;

            if (newProfile) {
              res.json({
                message: 'profile update',
                data: {
                  token: token
                }
              });
            }

            _context12.next = 23;
            break;

          case 19:
            _context12.prev = 19;
            _context12.t0 = _context12["catch"](5);
            console.log(_context12.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 23:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[5, 19]]);
  }));
  return _updateProfile.apply(this, arguments);
}

function updateProfileOnly(_x19, _x20) {
  return _updateProfileOnly.apply(this, arguments);
}

function _updateProfileOnly() {
  _updateProfileOnly = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(req, res) {
    var idProfile, profile;
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.prev = 0;
            idProfile = req.user.idProfile;

            if (!(idProfile != req.params.id)) {
              _context13.next = 4;
              break;
            }

            return _context13.abrupt("return", res.status(401).json({
              message: 'Access denied with provided credentials'
            }));

          case 4:
            _context13.next = 6;
            return tbl_profile.findByPk(idProfile);

          case 6:
            profile = _context13.sent;
            _context13.next = 9;
            return profile.update(req.body, {
              fields: ['firstname', 'lastname', 'dni', 'phone', 'gender', 'birthday' // 'tbl_district_id'
              ]
            });

          case 9:
            res.json({
              message: 'Profile updated'
            });
            _context13.next = 16;
            break;

          case 12:
            _context13.prev = 12;
            _context13.t0 = _context13["catch"](0);
            console.log(_context13.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13, null, [[0, 12]]);
  }));
  return _updateProfileOnly.apply(this, arguments);
}

function updateProfilePhoto(_x21, _x22) {
  return _updateProfilePhoto.apply(this, arguments);
}

function _updateProfilePhoto() {
  _updateProfilePhoto = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(req, res) {
    var idProfile, usuarioID, userName, image, photo, avatar, Avatar, token;
    return regeneratorRuntime.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.prev = 0;
            idProfile = req.user.idProfile;
            usuarioID = req.user.id;
            userName = req.user.username;
            image = req.body.image;

            if (!(idProfile != req.params.id)) {
              _context14.next = 7;
              break;
            }

            return _context14.abrupt("return", res.status(401).json({
              message: 'Access denied with provided credentials'
            }));

          case 7:
            _context14.next = 9;
            return savePhoto(req, res, image, 'uploads/profile-photo/');

          case 9:
            photo = _context14.sent;

            if (photo.success) {
              _context14.next = 12;
              break;
            }

            return _context14.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 12:
            _context14.next = 14;
            return tbl_avatar.findOne({
              include: {
                model: tbl_profile,
                as: 'Profile',
                where: {
                  id: idProfile
                },
                attributes: []
              }
            });

          case 14:
            avatar = _context14.sent;
            _context14.next = 17;
            return tbl_avatar.create({
              image: photo.data.Location,
              "default": 0
            });

          case 17:
            _context14.next = 19;
            return tbl_avatar.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 19:
            Avatar = _context14.sent;
            console.log(Avatar.id);
            console.log(idProfile);
            _context14.next = 24;
            return tbl_profile.update({
              tbl_avatar_id: Avatar.id
            }, {
              where: {
                id: idProfile
              }
            });

          case 24:
            _context14.next = 26;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 26:
            token = _context14.sent;
            res.json({
              message: 'Profile photo updated',
              data: {
                token: token
              }
            });
            _context14.next = 34;
            break;

          case 30:
            _context14.prev = 30;
            _context14.t0 = _context14["catch"](0);
            console.log(_context14.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 34:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14, null, [[0, 30]]);
  }));
  return _updateProfilePhoto.apply(this, arguments);
}

function updatePhoto(_x23, _x24) {
  return _updatePhoto.apply(this, arguments);
}

function _updatePhoto() {
  _updatePhoto = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(req, res) {
    var idProfile, image, photo, avatar;
    return regeneratorRuntime.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.prev = 0;
            idProfile = req.user.idProfile;
            image = req.body.image;
            _context15.next = 5;
            return savePhoto(req, res, image, 'uploads/profile-photo/');

          case 5:
            photo = _context15.sent;

            if (photo.success) {
              _context15.next = 8;
              break;
            }

            return _context15.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 8:
            _context15.next = 10;
            return tbl_avatar.create({
              image: photo.data.Location,
              "default": 0
            });

          case 10:
            _context15.next = 12;
            return tbl_avatar.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 12:
            avatar = _context15.sent;
            _context15.next = 15;
            return tbl_profile.update({
              tbl_avatar_id: avatar.id
            }, {
              where: {
                id: idProfile
              }
            });

          case 15:
            res.json({
              state: true,
              message: 'Profile photo updated'
            });
            _context15.next = 22;
            break;

          case 18:
            _context15.prev = 18;
            _context15.t0 = _context15["catch"](0);
            console.log(_context15.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15, null, [[0, 18]]);
  }));
  return _updatePhoto.apply(this, arguments);
}

function updateProfileAvatar(_x25, _x26) {
  return _updateProfileAvatar.apply(this, arguments);
}

function _updateProfileAvatar() {
  _updateProfileAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(req, res) {
    var idProfile, usuarioID, userName, token;
    return regeneratorRuntime.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.prev = 0;
            idProfile = req.user.idProfile;
            usuarioID = req.user.id;
            userName = req.user.username;

            if (!(idProfile != req.params.id)) {
              _context16.next = 6;
              break;
            }

            return _context16.abrupt("return", res.status(401).json({
              message: 'Access denied with provided credentials'
            }));

          case 6:
            if (!req.body.idAvatar) {
              _context16.next = 13;
              break;
            }

            _context16.next = 9;
            return tbl_profile.update({
              tbl_avatar_id: req.body.idAvatar
            }, {
              where: {
                id: idProfile
              }
            });

          case 9:
            _context16.next = 11;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 11:
            token = _context16.sent;
            return _context16.abrupt("return", res.json({
              message: 'Profile phone updated',
              data: {
                token: token
              }
            }));

          case 13:
            res.status(400).json({
              message: 'Empty data'
            });
            _context16.next = 20;
            break;

          case 16:
            _context16.prev = 16;
            _context16.t0 = _context16["catch"](0);
            console.log(_context16.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 20:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16, null, [[0, 16]]);
  }));
  return _updateProfileAvatar.apply(this, arguments);
}

function updateProfilePhone(_x27, _x28) {
  return _updateProfilePhone.apply(this, arguments);
}

function _updateProfilePhone() {
  _updateProfilePhone = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(req, res) {
    var idProfile, usuarioID, userName, token;
    return regeneratorRuntime.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            _context17.prev = 0;
            idProfile = req.user.idProfile;
            usuarioID = req.user.id;
            userName = req.user.username;

            if (!(idProfile != req.params.id)) {
              _context17.next = 6;
              break;
            }

            return _context17.abrupt("return", res.status(401).json({
              message: 'Access denied with provided credentials'
            }));

          case 6:
            if (!req.body.phone) {
              _context17.next = 13;
              break;
            }

            _context17.next = 9;
            return tbl_profile.update({
              phone: req.body.phone
            }, {
              where: {
                id: idProfile
              }
            });

          case 9:
            _context17.next = 11;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 11:
            token = _context17.sent;
            return _context17.abrupt("return", res.json({
              message: 'Profile phone updated',
              data: {
                token: token
              }
            }));

          case 13:
            res.status(400).json({
              message: 'Empty data'
            });
            _context17.next = 20;
            break;

          case 16:
            _context17.prev = 16;
            _context17.t0 = _context17["catch"](0);
            console.log(_context17.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 20:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17, null, [[0, 16]]);
  }));
  return _updateProfilePhone.apply(this, arguments);
}

function deleteProfile(_x29, _x30) {
  return _deleteProfile.apply(this, arguments);
}

function _deleteProfile() {
  _deleteProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee18(req, res) {
    var id, deleteRowCount;
    return regeneratorRuntime.wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            _context18.prev = 0;
            id = req.params.id;
            _context18.next = 4;
            return tbl_profile.destroy({
              where: {
                id: id
              }
            });

          case 4:
            deleteRowCount = _context18.sent;
            res.json({
              message: 'Profile deleted successfully',
              count: deleteRowCount
            });
            _context18.next = 12;
            break;

          case 8:
            _context18.prev = 8;
            _context18.t0 = _context18["catch"](0);
            console.log(_context18.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18, null, [[0, 8]]);
  }));
  return _deleteProfile.apply(this, arguments);
}

function getPsycology(_x31, _x32) {
  return _getPsycology.apply(this, arguments);
}

function _getPsycology() {
  _getPsycology = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee19(req, res) {
    var profile, profilespe, profileatri, attributes, data;
    return regeneratorRuntime.wrap(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            _context19.prev = 0;
            _context19.next = 3;
            return tbl_profile.findOne({
              where: {
                tbl_user_id: req.user.id
              },
              attributes: ['id', 'firstname', 'lastname', 'dni', 'gender', 'birthday', 'email'],
              include: {
                model: tbl_profile_attribute,
                attributes: [['value', 'availability']],
                where: {
                  state: 1,
                  key: 'available'
                }
              }
            });

          case 3:
            profile = _context19.sent;
            _context19.next = 6;
            return tbl_specialty.findAll({
              attributes: [//'id',
              'name', 'alias' //'duration',
              //'name',
              //'type'
              ],
              include: {
                model: tbl_profile_specialty,
                where: {
                  tbl_profile_id: profile.id,
                  state: 1
                },
                attributes: []
              }
            });

          case 6:
            profilespe = _context19.sent;
            _context19.next = 9;
            return tbl_profile_attribute.findAll({
              where: {
                tbl_profile_id: profile.id
              },
              attributes: ['key', 'value']
            });

          case 9:
            profileatri = _context19.sent;

            if (profile) {
              _context19.next = 12;
              break;
            }

            return _context19.abrupt("return", res.status(404).json({
              message: 'Training not found'
            }));

          case 12:
            attributes = {};
            profileatri.map(function (item) {
              attributes[item.key] = item.value;
            });
            data = {
              psycology: profile,
              specialties: profilespe,
              attributes: attributes
            }; //item.setDataValue('total', attributesx)

            /*.then(() => { res.json({
                // data,
                attributesx
            })*/

            res.json({
              data: data
            });
            _context19.next = 21;
            break;

          case 18:
            _context19.prev = 18;
            _context19.t0 = _context19["catch"](0);
            console.log(_context19.t0);

          case 21:
          case "end":
            return _context19.stop();
        }
      }
    }, _callee19, null, [[0, 18]]);
  }));
  return _getPsycology.apply(this, arguments);
}

function updatePsycology(_x33, _x34) {
  return _updatePsycology.apply(this, arguments);
}

function _updatePsycology() {
  _updatePsycology = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee24(req, res) {
    var idProfile, _req$body4, firstname, lastname, dni, imageDni, birthday, gender, email, phone, district_id, attributes, specialties, institutions, availabilities, saveImageDni;

    return regeneratorRuntime.wrap(function _callee24$(_context24) {
      while (1) {
        switch (_context24.prev = _context24.next) {
          case 0:
            _context24.prev = 0;
            idProfile = req.user.idProfile;
            _req$body4 = req.body, firstname = _req$body4.firstname, lastname = _req$body4.lastname, dni = _req$body4.dni, imageDni = _req$body4.imageDni, birthday = _req$body4.birthday, gender = _req$body4.gender, email = _req$body4.email, phone = _req$body4.phone, district_id = _req$body4.district_id, attributes = _req$body4.attributes, specialties = _req$body4.specialties, institutions = _req$body4.institutions, availabilities = _req$body4.availabilities; // Updated profile

            _context24.next = 5;
            return tbl_profile.update({
              firstname: firstname,
              lastname: lastname,
              dni: dni,
              birthday: birthday,
              gender: gender,
              email: email,
              phone: phone,
              district_id: district_id
            }, {
              where: {
                id: idProfile
              }
            });

          case 5:
            _context24.next = 7;
            return tbl_availability.update({
              state: 0
            }, {
              where: {
                tbl_profile_id: idProfile
              }
            });

          case 7:
            if (!(_typeof(availabilities) == 'object')) {
              _context24.next = 10;
              break;
            }

            _context24.next = 10;
            return Promise.all(availabilities.map(function (el) {
              el.hours.map(function (hour) {
                return new Promise( /*#__PURE__*/function () {
                  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee20(resolve) {
                    var _yield$tbl_availabili, _yield$tbl_availabili2, availability, created;

                    return regeneratorRuntime.wrap(function _callee20$(_context20) {
                      while (1) {
                        switch (_context20.prev = _context20.next) {
                          case 0:
                            _context20.next = 2;
                            return tbl_availability.findOrCreate({
                              where: {
                                day: String(el.day),
                                start_hour: hour.startHour
                              },
                              defaults: {
                                tbl_profile_id: idProfile,
                                day: String(el.day),
                                start_hour: hour.startHour,
                                end_hour: hour.endHour,
                                state: 1
                              }
                            });

                          case 2:
                            _yield$tbl_availabili = _context20.sent;
                            _yield$tbl_availabili2 = _slicedToArray(_yield$tbl_availabili, 2);
                            availability = _yield$tbl_availabili2[0];
                            created = _yield$tbl_availabili2[1];

                            if (created) {
                              _context20.next = 9;
                              break;
                            }

                            _context20.next = 9;
                            return availability.update({
                              state: 1
                            });

                          case 9:
                            resolve(1);

                          case 10:
                          case "end":
                            return _context20.stop();
                        }
                      }
                    }, _callee20);
                  }));

                  return function (_x42) {
                    return _ref5.apply(this, arguments);
                  };
                }());
              });
            }));

          case 10:
            _context24.next = 12;
            return tbl_profile_specialty.destroy({
              where: {
                tbl_profile_id: idProfile
              }
            });

          case 12:
            if (!(_typeof(specialties) == 'object')) {
              _context24.next = 15;
              break;
            }

            _context24.next = 15;
            return Promise.all(specialties.map(function (el) {
              return new Promise( /*#__PURE__*/function () {
                var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee21(resolve) {
                  return regeneratorRuntime.wrap(function _callee21$(_context21) {
                    while (1) {
                      switch (_context21.prev = _context21.next) {
                        case 0:
                          if (!el.value) {
                            _context21.next = 3;
                            break;
                          }

                          _context21.next = 3;
                          return tbl_profile_specialty.create({
                            tbl_profile_id: idProfile,
                            tbl_specialty_id: el.id,
                            state: 1
                          });

                        case 3:
                          resolve(1);

                        case 4:
                        case "end":
                          return _context21.stop();
                      }
                    }
                  }, _callee21);
                }));

                return function (_x43) {
                  return _ref6.apply(this, arguments);
                };
              }());
            }));

          case 15:
            if (!imageDni) {
              _context24.next = 22;
              break;
            }

            _context24.next = 18;
            return savePhoto(req, res, imageDni, 'uploads/profile-photo/');

          case 18:
            saveImageDni = _context24.sent;

            if (saveImageDni.success) {
              _context24.next = 21;
              break;
            }

            return _context24.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 21:
            attributes.push({
              key: 'imageDni',
              value: saveImageDni.data.Location
            });

          case 22:
            if (!(_typeof(attributes) == 'object')) {
              _context24.next = 25;
              break;
            }

            _context24.next = 25;
            return Promise.all(attributes.map(function (attribute) {
              return new Promise( /*#__PURE__*/function () {
                var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee22(resolve) {
                  var value, _yield$tbl_profile_at, _yield$tbl_profile_at2, profile_attribute, pa_created;

                  return regeneratorRuntime.wrap(function _callee22$(_context22) {
                    while (1) {
                      switch (_context22.prev = _context22.next) {
                        case 0:
                          value = _typeof(attribute.value) === 'object' ? JSON.stringify(attribute.value) : attribute.value;
                          _context22.next = 3;
                          return tbl_profile_attribute.findOrCreate({
                            where: {
                              tbl_profile_id: idProfile,
                              key: attribute.key
                            },
                            defaults: {
                              value: value,
                              state: 1
                            }
                          });

                        case 3:
                          _yield$tbl_profile_at = _context22.sent;
                          _yield$tbl_profile_at2 = _slicedToArray(_yield$tbl_profile_at, 2);
                          profile_attribute = _yield$tbl_profile_at2[0];
                          pa_created = _yield$tbl_profile_at2[1];

                          if (pa_created) {
                            _context22.next = 10;
                            break;
                          }

                          _context22.next = 10;
                          return profile_attribute.update({
                            value: value
                          });

                        case 10:
                          resolve(1);

                        case 11:
                        case "end":
                          return _context22.stop();
                      }
                    }
                  }, _callee22);
                }));

                return function (_x44) {
                  return _ref7.apply(this, arguments);
                };
              }());
            }));

          case 25:
            if (!(_typeof(institutions) == 'object')) {
              _context24.next = 28;
              break;
            }

            _context24.next = 28;
            return Promise.all(institutions.map(function (institution) {
              return new Promise( /*#__PURE__*/function () {
                var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee23(resolve) {
                  var _yield$tbl_profile_in, _yield$tbl_profile_in2, profile_institution, pi_created;

                  return regeneratorRuntime.wrap(function _callee23$(_context23) {
                    while (1) {
                      switch (_context23.prev = _context23.next) {
                        case 0:
                          //*por corregir*//
                          institution.id = 1;
                          _context23.next = 3;
                          return tbl_profile_institution.findOrCreate({
                            where: {
                              tbl_profile_id: idProfile,
                              tbl_institution_id: institution.id
                            },
                            defaults: {
                              state: 1,
                              academic_degree: institution.academic_degree,
                              start_date: institution.start_date,
                              ending_date: institution.ending_date,
                              document: institution.document
                            }
                          });

                        case 3:
                          _yield$tbl_profile_in = _context23.sent;
                          _yield$tbl_profile_in2 = _slicedToArray(_yield$tbl_profile_in, 2);
                          profile_institution = _yield$tbl_profile_in2[0];
                          pi_created = _yield$tbl_profile_in2[1];

                          if (pi_created) {
                            _context23.next = 10;
                            break;
                          }

                          _context23.next = 10;
                          return profile_institution.update({
                            academic_degree: institution.academic_degree,
                            start_date: institution.start_date,
                            ending_date: institution.ending_date,
                            document: institution.document
                          });

                        case 10:
                          resolve(1);

                        case 11:
                        case "end":
                          return _context23.stop();
                      }
                    }
                  }, _callee23);
                }));

                return function (_x45) {
                  return _ref8.apply(this, arguments);
                };
              }());
            }));

          case 28:
            return _context24.abrupt("return", res.json({
              message: 'Profile updated',
              data: req.body
            }));

          case 31:
            _context24.prev = 31;
            _context24.t0 = _context24["catch"](0);
            console.log(_context24.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 35:
          case "end":
            return _context24.stop();
        }
      }
    }, _callee24, null, [[0, 31]]);
  }));
  return _updatePsycology.apply(this, arguments);
}

function savePhoto(_x35, _x36, _x37, _x38) {
  return _savePhoto.apply(this, arguments);
}

function _savePhoto() {
  _savePhoto = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee25(req, res, image, path) {
    var _isValidBase64Img, isValid, buffer, ext, imageUploaded;

    return regeneratorRuntime.wrap(function _callee25$(_context25) {
      while (1) {
        switch (_context25.prev = _context25.next) {
          case 0:
            _context25.prev = 0;
            _isValidBase64Img = isValidBase64Img(image), isValid = _isValidBase64Img.isValid, buffer = _isValidBase64Img.buffer, ext = _isValidBase64Img.ext;

            if (isValid) {
              _context25.next = 4;
              break;
            }

            return _context25.abrupt("return", res.status(422).json({
              message: 'The given data was invalid.',
              errors: {
                image: ["The image must be a valid base64"]
              }
            }));

          case 4:
            _context25.next = 6;
            return s3UploadBase64Image(path, {
              buffer: buffer,
              ext: ext
            });

          case 6:
            imageUploaded = _context25.sent;
            return _context25.abrupt("return", imageUploaded);

          case 10:
            _context25.prev = 10;
            _context25.t0 = _context25["catch"](0);
            console.log('Something went error:', _context25.t0);
            return _context25.abrupt("return", null);

          case 14:
          case "end":
            return _context25.stop();
        }
      }
    }, _callee25, null, [[0, 10]]);
  }));
  return _savePhoto.apply(this, arguments);
}