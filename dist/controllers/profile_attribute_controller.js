"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListAttribute = getListAttribute;
exports.createAttribute = createAttribute;
exports.getListPsicology = getListPsicology;
exports.getProfileAttribute = getProfileAttribute;
exports.setProfileAttribute = setProfileAttribute;

var _express = require("express");

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('luxon'),
    DateTime = _require.DateTime;

var _require2 = require('../models'),
    Profile_attribute = _require2.Profile_attribute,
    tbl_profile = _require2.tbl_profile,
    tbl_avatar = _require2.tbl_avatar,
    tbl_profile_attribute = _require2.tbl_profile_attribute,
    tbl_profile_specialty = _require2.tbl_profile_specialty,
    tbl_specialty = _require2.tbl_specialty,
    tbl_blocked = _require2.tbl_blocked,
    tbl_availability = _require2.tbl_availability;

function getListAttribute(_x, _x2) {
  return _getListAttribute.apply(this, arguments);
}

function _getListAttribute() {
  _getListAttribute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _attributes;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return Profile_attribute.findAll({
              where: {
                state: 1
              }
            });

          case 3:
            _attributes = _context.sent;
            res.json({
              data: _attributes
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListAttribute.apply(this, arguments);
}

function createAttribute(_x3, _x4) {
  return _createAttribute.apply(this, arguments);
}

function _createAttribute() {
  _createAttribute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, key, value, profile_id, newAttribute;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _req$body = req.body, key = _req$body.key, value = _req$body.value, profile_id = _req$body.profile_id;
            _context2.prev = 1;
            _context2.next = 4;
            return Profile_attribute.create({
              key: key,
              value: value,
              profile_id: profile_id
            }, {
              fields: ['key', 'value', 'profile_id']
            });

          case 4:
            newAttribute = _context2.sent;

            if (newAttribute) {
              res.json({
                message: 'atribute created',
                date: attributes
              });
            }

            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 8]]);
  }));
  return _createAttribute.apply(this, arguments);
}

function getListPsicology(_x5, _x6) {
  return _getListPsicology.apply(this, arguments);
}

function _getListPsicology() {
  _getListPsicology = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var dayOfWeek, hour, gender, idRole, query, psycologys, data;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            dayOfWeek = req.query.dayOfWeek;
            hour = req.query.hour;
            gender = req.query.gender;
            idRole = req.user.idRole;
            query = {
              attributes: ['id', ['tbl_avatar_id', 'idAvatar'], 'slug', 'firstname', 'lastname', 'dni', 'phone', 'gender', 'birthday', 'email'],
              where: {
                state: 1,
                tbl_role_id: 2
              },
              include: [{
                model: tbl_profile_attribute,
                as: 'ProfileAttribute',
                attributes: [['value', 'available']],
                where: {
                  state: 1,
                  key: 'available'
                },
                required: false
              }, {
                model: tbl_availability,
                as: 'Availabilities',
                where: {
                  state: 1
                },
                required: false,
                attributes: ['id', 'day', 'start_hour', 'end_hour']
              }]
            };
            if (gender !== undefined && gender !== '') query.where.gender = gender;
            _context4.next = 9;
            return tbl_profile.findAll(query);

          case 9:
            psycologys = _context4.sent;

            /* if (profiles.length > 0) {
                const psycologys = []
                profiles.forEach(el => {
                    if (el.id )
                    psycologys
                })
            } */
            data = [];
            _context4.next = 13;
            return Promise.all(psycologys.map(function (item) {
              console.log(item.dataValues);
              return new Promise( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(resolve) {
                  var avatar, attributes, specialty, attr, specialties, blocked;
                  return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.next = 2;
                          return tbl_avatar.findOne({
                            attributes: ['id', 'image'],
                            where: {
                              id: item.dataValues.idAvatar
                            }
                          });

                        case 2:
                          avatar = _context3.sent;
                          _context3.next = 5;
                          return tbl_profile_attribute.findAll({
                            where: {
                              "tbl_profile_id": item.dataValues.id
                            }
                          });

                        case 5:
                          attributes = _context3.sent;
                          _context3.next = 8;
                          return tbl_profile_specialty.findAll({
                            where: {
                              state: 1,
                              tbl_profile_id: item.id
                            },
                            include: {
                              model: tbl_specialty,
                              as: 'Specialty',
                              where: {
                                state: 1
                              }
                            }
                          });

                        case 8:
                          specialty = _context3.sent;
                          attr = {};
                          attributes.map(function (a) {
                            attr[a.key] = a.value;
                          }); //let special = []

                          //let special = []
                          specialty.map(function (b) {
                            b.dataValues.especialidad = b.Specialty.name; // special.push(b)
                            //resolve(1)
                          });
                          item.dataValues.Specialty = specialty;
                          _context3.next = 15;
                          return tbl_specialty.findAll({
                            where: {
                              state: 1
                            },
                            attributes: ['id', 'name'],
                            include: {
                              model: tbl_profile_specialty,
                              as: 'ProfileSpecialty',
                              attributes: [],
                              where: {
                                state: 1,
                                tbl_profile_id: item.id
                              }
                            }
                          });

                        case 15:
                          specialties = _context3.sent;
                          item.dataValues.Specialties = specialties;
                          item.dataValues.Avatar = avatar;
                          if (typeof attr.available === 'undefined') attr.available = [];
                          item.dataValues.ProfileAttribute = attr;
                          _context3.next = 22;
                          return tbl_blocked.findOne({
                            where: {
                              tbl_profile_pacient_id: req.user.idProfile,
                              tbl_profile_psychologist_id: item.id,
                              state: 1
                            }
                          });

                        case 22:
                          blocked = _context3.sent;

                          if (blocked === null) {
                            data.push(item);
                          }

                          resolve(1);

                        case 25:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function (_x11) {
                  return _ref.apply(this, arguments);
                };
              }());
            }));

          case 13:
            res.json({
              data: data
            });
            _context4.next = 20;
            break;

          case 16:
            _context4.prev = 16;
            _context4.t0 = _context4["catch"](0);
            console.error('You have a error getListPsicology:', _context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 20:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 16]]);
  }));
  return _getListPsicology.apply(this, arguments);
}

function getProfileAttribute(_x7, _x8) {
  return _getProfileAttribute.apply(this, arguments);
}

function _getProfileAttribute() {
  _getProfileAttribute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var id, _attributes2;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            id = req.params.id;
            _context5.next = 4;
            return tbl_profile_attribute.findAll({
              where: {
                state: 1,
                tbl_profile_id: id
              },
              attributes: ['key', 'value']
            });

          case 4:
            _attributes2 = _context5.sent;
            res.json(_attributes2);
            _context5.next = 12;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](0);
            console.log('Something a went wrong:', _context5.t0);
            res.status(500).json({
              state: false,
              message: 'Something went wrong:'
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 8]]);
  }));
  return _getProfileAttribute.apply(this, arguments);
}

function setProfileAttribute(_x9, _x10) {
  return _setProfileAttribute.apply(this, arguments);
}

function _setProfileAttribute() {
  _setProfileAttribute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var id, _req$body2, key, value, profileAttribute, profileAttributeUpdate;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            id = req.params.id;
            _req$body2 = req.body, key = _req$body2.key, value = _req$body2.value;
            _context6.next = 5;
            return tbl_profile_attribute.findOne({
              where: {
                state: 1,
                tbl_profile_id: id,
                key: key
              }
            });

          case 5:
            profileAttribute = _context6.sent;

            if (!profileAttribute) {
              _context6.next = 12;
              break;
            }

            _context6.next = 9;
            return tbl_profile_attribute.update({
              value: value
            }, {
              where: {
                id: profileAttribute.dataValues.id
              }
            });

          case 9:
            profileAttributeUpdate = _context6.sent;
            _context6.next = 15;
            break;

          case 12:
            _context6.next = 14;
            return tbl_profile_attribute.create({
              tbl_profile_id: id,
              key: key,
              value: value
            });

          case 14:
            profileAttributeUpdate = _context6.sent;

          case 15:
            if (profileAttributeUpdate) res.status(200).json({
              state: true,
              message: 'Save attribute'
            });else res.status(200).json({
              state: false,
              message: 'Don´t save'
            });
            _context6.next = 22;
            break;

          case 18:
            _context6.prev = 18;
            _context6.t0 = _context6["catch"](0);
            console.log('You have a error:', _context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 18]]);
  }));
  return _setProfileAttribute.apply(this, arguments);
}