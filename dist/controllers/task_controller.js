'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListTask = getListTask;
exports.getListTaskByWeek = getListTaskByWeek;
exports.getTasksPatient = getTasksPatient;
exports.createSessionTask = createSessionTask;
exports.createAvatar = createAvatar;
exports.getAvatar = getAvatar;
exports.getTask = getTask;
exports.getChallenge = getChallenge;

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_task = _require.tbl_task,
    tbl_task_types = _require.tbl_task_types,
    tbl_task_question = _require.tbl_task_question,
    tbl_diary = _require.tbl_diary,
    tbl_task_media = _require.tbl_task_media,
    tbl_session_task = _require.tbl_session_task,
    tbl_session = _require.tbl_session,
    tbl_profile = _require.tbl_profile,
    tbl_medias = _require.tbl_medias,
    tbl_media_profile = _require.tbl_media_profile,
    tbl_room = _require.tbl_room,
    tbl_chat = _require.tbl_chat,
    tbl_chat_participants = _require.tbl_chat_participants;

var _require2 = require('luxon'),
    DateTime = _require2.DateTime;

var _require3 = require('sequelize'),
    Op = _require3.Op;

function getListTask(_x, _x2) {
  return _getListTask.apply(this, arguments);
}

function _getListTask() {
  _getListTask = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var tasks;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_task.findAll({
              where: {
                state: 1
              },
              attributes: ['id', 'name', 'description'],
              include: [{
                model: tbl_task_types,
                as: 'TaskTypes',
                attributes: ['id', 'name', 'description']
              }]
            });

          case 3:
            tasks = _context.sent;
            res.json({
              data: tasks
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListTask.apply(this, arguments);
}

function getListTaskByWeek(_x3, _x4) {
  return _getListTaskByWeek.apply(this, arguments);
}

function _getListTaskByWeek() {
  _getListTaskByWeek = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var week, tasks;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            week = req.params.week;
            _context2.next = 4;
            return tbl_task.findAll({
              where: {
                state: 1,
                week: week
              },
              attributes: ['id', 'name', 'description'],
              include: [{
                model: tbl_task_types,
                as: 'TaskTypes',
                attributes: ['id', 'name', 'description']
              }]
            });

          case 4:
            tasks = _context2.sent;
            res.json({
              message: 'Tasks found',
              data: tasks
            });
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _getListTaskByWeek.apply(this, arguments);
}

function getTasksPatient(_x5, _x6) {
  return _getTasksPatient.apply(this, arguments);
}

function _getTasksPatient() {
  _getTasksPatient = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$query, idPatient, week, types, tasks;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _req$query = req.query, idPatient = _req$query.idPatient, week = _req$query.week, types = _req$query.types;
            _context3.next = 4;
            return tbl_task.findAll({
              where: {
                state: 1,
                week: week,
                tbl_task_types_id: _defineProperty({}, Op["in"], types)
              },
              attributes: ['id', 'name', 'description'],
              include: [{
                model: tbl_session_task,
                as: 'SessionTask',
                where: {
                  state: 1
                },
                required: false,
                include: [{
                  model: tbl_session,
                  as: 'Session',
                  attributes: ['id'],
                  where: {
                    state: 1,
                    tbl_profile_pacient_id: idPatient
                  }
                }]
              }, {
                model: tbl_task_types,
                as: 'TaskTypes',
                attributes: ['id', 'name', 'description']
              }, {
                model: tbl_task_media,
                as: 'TaskMedia',
                attributes: ['id'],
                where: {
                  state: 1
                },
                include: [{
                  model: tbl_medias,
                  as: 'Media',
                  attributes: ['name', 'image', 'file', 'duration', 'points'],
                  where: {
                    state: 1
                  }
                }]
              }]
            });

          case 4:
            tasks = _context3.sent;
            res.json(tasks);
            _context3.next = 12;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](0);
            console.log('You have a error on getTasksPatient:', _context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 8]]);
  }));
  return _getTasksPatient.apply(this, arguments);
}

function createSessionTask(_x7, _x8) {
  return _createSessionTask.apply(this, arguments);
}

function _createSessionTask() {
  _createSessionTask = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _req$body, idSession, idTask, state, sessionTask, sessionTaskUpdate;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _req$body = req.body, idSession = _req$body.idSession, idTask = _req$body.idTask, state = _req$body.state;
            _context4.next = 4;
            return tbl_session_task.findOne({
              where: {
                tbl_session_id: idSession,
                tbl_task_id: idTask
              }
            });

          case 4:
            sessionTask = _context4.sent;
            console.log(sessionTask);

            if (!sessionTask) {
              _context4.next = 13;
              break;
            }

            console.log(sessionTask.dataValues.id);
            _context4.next = 10;
            return tbl_session_task.update({
              state: state
            }, {
              where: {
                id: sessionTask.dataValues.id
              }
            });

          case 10:
            sessionTaskUpdate = _context4.sent;
            _context4.next = 16;
            break;

          case 13:
            _context4.next = 15;
            return tbl_session_task.create({
              tbl_session_id: idSession,
              tbl_task_id: idTask,
              state: 1
            });

          case 15:
            sessionTaskUpdate = _context4.sent;

          case 16:
            res.json({
              state: true,
              message: 'Save sessionTask',
              data: sessionTaskUpdate
            });
            _context4.next = 23;
            break;

          case 19:
            _context4.prev = 19;
            _context4.t0 = _context4["catch"](0);
            console.log('You have a error on createSessionTask:', _context4.t0);
            res.status(500).json({
              message: 'Something a went error'
            });

          case 23:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 19]]);
  }));
  return _createSessionTask.apply(this, arguments);
}

function createAvatar(_x9, _x10) {
  return _createAvatar.apply(this, arguments);
}

function _createAvatar() {
  _createAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var image, newAvatar;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            image = req.body.image;
            _context5.prev = 1;
            _context5.next = 4;
            return tbl_avatar.create({
              image: image
            });

          case 4:
            newAvatar = _context5.sent;

            if (newAvatar) {
              res.json({
                message: 'avatar created',
                date: newAvatar
              });
            }

            _context5.next = 12;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 8]]);
  }));
  return _createAvatar.apply(this, arguments);
}

function getAvatar(_x11, _x12) {
  return _getAvatar.apply(this, arguments);
}

function _getAvatar() {
  _getAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var idProfile, profiles;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            idProfile = req.params.idProfile;
            _context6.next = 4;
            return tbl_profile.findByPk({
              include: {
                model: tbl_avatar,
                attributes: ['id', 'image']
              },
              where: {
                id: idProfile,
                state: 1
              }
            });

          case 4:
            profiles = _context6.sent;
            res.json({
              data: profiles
            });
            _context6.next = 11;
            break;

          case 8:
            _context6.prev = 8;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 8]]);
  }));
  return _getAvatar.apply(this, arguments);
}

function getTask(_x13, _x14) {
  return _getTask.apply(this, arguments);
}

function _getTask() {
  _getTask = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var idProfile, id, taskID, IDsession, task, taskDiary, taskMedia, date, taskRoom, data;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            idProfile = req.user.idProfile;
            id = req.params.id;
            _context8.next = 5;
            return tbl_session_task.findOne({
              where: {
                tbl_task_id: id
              }
            });

          case 5:
            taskID = _context8.sent;
            IDsession = taskID.id;
            _context8.next = 9;
            return tbl_task.findOne({
              where: {
                state: 1,
                id: id
              },
              attributes: ['tbl_method_id'],
              include: [{
                model: tbl_task_types,
                as: 'TaskTypes',
                where: {
                  state: 1
                },
                attributes: ['id', 'name', 'alias']
              }, {
                model: tbl_session_task,
                as: 'SessionTask',
                where: {
                  state: 1,
                  tbl_task_id: id,
                  id: IDsession
                },
                required: false
              }, {
                model: tbl_task_question,
                as: 'TaskQuestion',
                where: {
                  state: 1,
                  tbl_task_id: id
                },
                required: false
              }]
            });

          case 9:
            task = _context8.sent;
            _context8.next = 12;
            return tbl_diary.findAll({
              where: {
                tbl_task_id: id
              }
            });

          case 12:
            taskDiary = _context8.sent;
            task.dataValues['Diary'] = taskDiary;
            _context8.next = 16;
            return tbl_task_media.findAll({
              where: {
                tbl_task_id: id
              }
            });

          case 16:
            taskMedia = _context8.sent;
            date = new Date();
            _context8.next = 20;
            return tbl_room.findAll({
              where: {
                tbl_method_id: task.tbl_method_id,
                start: _defineProperty({}, Op.gt, date)
              }
            });

          case 20:
            taskRoom = _context8.sent;
            task.dataValues['Room'] = taskRoom;
            _context8.next = 24;
            return Promise.all(taskMedia.map( /*#__PURE__*/function () {
              var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(item, key) {
                var Medias;
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                  while (1) {
                    switch (_context7.prev = _context7.next) {
                      case 0:
                        _context7.next = 2;
                        return tbl_medias.findAll({
                          where: {
                            id: item.tbl_media_id
                          }
                        });

                      case 2:
                        Medias = _context7.sent;
                        item.setDataValue('tbl_category_media_id', Medias[0].tbl_category_media_id);
                        item.setDataValue('tbl_method_id', Medias[0].tbl_method_id);
                        item.setDataValue('name', Medias[0].name);
                        item.setDataValue('type', Medias[0].type);
                        item.setDataValue('file', Medias[0].file);
                        item.setDataValue('image', Medias[0].image);
                        item.setDataValue('duration', Medias[0].duration);
                        item.setDataValue('description', Medias[0].description);
                        item.setDataValue('points', Medias[0].points);
                        return _context7.abrupt("return", item);

                      case 13:
                      case "end":
                        return _context7.stop();
                    }
                  }
                }, _callee7);
              }));

              return function (_x17, _x18) {
                return _ref.apply(this, arguments);
              };
            }()));

          case 24:
            data = _context8.sent;
            task.dataValues['Medias'] = data;
            res.json({
              data: task
            });
            _context8.next = 33;
            break;

          case 29:
            _context8.prev = 29;
            _context8.t0 = _context8["catch"](0);
            console.error('You have a error in getTask:', _context8.t0);
            res.status(500).json({
              non_field_errors: 'Something went wrong'
            });

          case 33:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 29]]);
  }));
  return _getTask.apply(this, arguments);
}

function getChallenge(_x15, _x16) {
  return _getChallenge.apply(this, arguments);
}

function _getChallenge() {
  _getChallenge = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res) {
    var now, date, idProfile, lastSession, tasks, challenges, countMovies, countAudios, _iterator, _step, task, taskType, media_profile, countComplete, count, nametype, _taskType, rooms, _countComplete, _count, _nametype;

    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.prev = 0;
            now = DateTime.now();
            date = now.toString();
            idProfile = req.user.idProfile;
            _context9.next = 6;
            return tbl_session.findOne({
              attributes: ['id', 'date', 'week'],
              where: {
                state: 1,
                date: _defineProperty({}, Op.lt, date)
              },
              include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [['id', 'idProfilePatient']],
                where: {
                  state: 1,
                  id: idProfile
                }
              },
              order: [['date', 'DESC']]
            });

          case 6:
            lastSession = _context9.sent;
            // console.log('')
            // console.log('')
            // console.log('lastSession')
            // console.log(lastSession)
            // console.log('')
            // console.log('')
            tasks = [];
            challenges = [];

            if (!(lastSession !== null)) {
              _context9.next = 50;
              break;
            }

            _context9.next = 12;
            return tbl_session_task.findAll({
              where: {
                state: 1
              },
              include: [{
                model: tbl_session,
                as: 'Session',
                attributes: ['id', 'date', 'week'],
                where: {
                  state: 1,
                  id: lastSession.dataValues.id
                }
              }, {
                model: tbl_task,
                as: 'Task',
                attributes: ['id', 'week', 'name', 'description', ['tbl_method_id', 'idMethod']],
                where: {
                  state: 1,
                  week: lastSession.dataValues.week
                },
                include: [{
                  model: tbl_task_types,
                  as: 'TaskTypes',
                  where: _defineProperty({
                    state: 1
                  }, Op.or, [{
                    alias: 'media'
                  }, {
                    alias: 'room'
                  }]),
                  attributes: ['id', 'name', 'alias']
                }, {
                  model: tbl_task_media,
                  as: 'TaskMedia',
                  where: {
                    state: 1
                  },
                  required: false,
                  include: {
                    model: tbl_medias,
                    as: 'Media',
                    where: {
                      state: 1
                    },
                    attributes: ['type', 'points']
                  }
                }]
              }],
              attributes: ['id', 'expiration']
            });

          case 12:
            tasks = _context9.sent;
            // console.log('')
            // console.log('')
            // console.log('tasks')
            // console.log(tasks.length)
            // console.log('')
            // console.log('')
            countMovies = tasks.reduce(function (acc, el) {
              if (el.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type === 1) acc++;
              return acc;
            }, 0);
            countAudios = tasks.reduce(function (acc, el) {
              if (el.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type === 2) acc++;
              return acc;
            }, 0);
            _iterator = _createForOfIteratorHelper(tasks);
            _context9.prev = 16;

            _iterator.s();

          case 18:
            if ((_step = _iterator.n()).done) {
              _context9.next = 42;
              break;
            }

            task = _step.value;

            if (!(task.Task.dataValues.TaskTypes.dataValues.alias === 'media')) {
              _context9.next = 30;
              break;
            }

            // console.log('')
            // console.log('')
            // console.log('media')
            // console.log(task.Task.TaskMedia.Media)
            // console.log('')
            // console.log('')
            taskType = task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type;
            _context9.next = 24;
            return tbl_medias.findAll({
              where: {
                state: 1,
                id: task.Task.dataValues.TaskMedia.dataValues.tbl_media_id
              },
              include: {
                model: tbl_media_profile,
                as: 'onProfile',
                where: {
                  state: 1,
                  tbl_profile_id: idProfile,
                  done: _defineProperty({}, Op.lt, 95)
                },
                attributes: ['done']
              },
              attributes: ['id']
            });

          case 24:
            media_profile = _context9.sent;
            countComplete = media_profile.reduce(function (acc, el) {
              return acc++;
            }, 0);
            count = taskType === 1 ? countMovies - countComplete : countAudios - countComplete;
            nametype = '';

            if (taskType == 1) {
              nametype = 'Video';
            } else {
              nametype = 'Audio';
            }

            challenges.push({
              count: count,
              point: count * Number(task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.points),
              type: nametype,
              week: lastSession.dataValues.week,
              date: lastSession.dataValues.date
            });

          case 30:
            if (!(task.Task.dataValues.TaskTypes.dataValues.alias === 'room')) {
              _context9.next = 40;
              break;
            }

            _taskType = task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.type;
            _context9.next = 34;
            return tbl_room.findAll({
              where: {
                state: 1 // start: { [Op.gt]: startRoom }

              },
              include: {
                model: tbl_chat,
                as: 'Chat',
                where: {
                  state: 1
                },
                attributes: [],
                include: {
                  model: tbl_chat_participants,
                  as: 'ChatParticipant',
                  where: {
                    state: 1,
                    tbl_profile_id: idProfile
                  },
                  attributes: []
                }
              },
              attributes: ['id', 'title', 'start', 'end', 'rules', 'participants', 'image']
            });

          case 34:
            rooms = _context9.sent;
            // console.log('')
            // console.log('')
            // console.log('rooms')
            // console.log(rooms)
            // console.log('')
            // console.log('')
            _countComplete = rooms.reduce(function (acc, el) {
              return acc++;
            }, 0);
            _count = _taskType === 1 ? countMovies - _countComplete : countAudios - _countComplete;
            _nametype = '';

            if (_taskType == 1) {
              _nametype = 'Video';
            } else {
              _nametype = 'Audio';
            }

            challenges.push({
              count: _count,
              point: _count * Number(task.Task.dataValues.TaskMedia.dataValues.Media.dataValues.points),
              type: _nametype
            });

          case 40:
            _context9.next = 18;
            break;

          case 42:
            _context9.next = 47;
            break;

          case 44:
            _context9.prev = 44;
            _context9.t0 = _context9["catch"](16);

            _iterator.e(_context9.t0);

          case 47:
            _context9.prev = 47;

            _iterator.f();

            return _context9.finish(47);

          case 50:
            res.json({
              data: challenges
            });
            _context9.next = 57;
            break;

          case 53:
            _context9.prev = 53;
            _context9.t1 = _context9["catch"](0);
            console.error('You have a error in getListChangelles:', _context9.t1);
            res.status(500).json({
              non_field_errors: 'Something went wrong'
            });

          case 57:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[0, 53], [16, 44, 47, 50]]);
  }));
  return _getChallenge.apply(this, arguments);
}