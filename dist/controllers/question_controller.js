"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getQuestion = getQuestion;
exports.getListQuestion = getListQuestion;
exports.createQuestion = createQuestion;
exports.getAlternativeQuestion = getAlternativeQuestion;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_question = _require.tbl_question;

var _require2 = require('../models'),
    tbl_alternative = _require2.tbl_alternative;

function getQuestion(_x, _x2) {
  return _getQuestion.apply(this, arguments);
}

function _getQuestion() {
  _getQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var question;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_question.findAll({
              where: {
                state: 1
              }
            });

          case 3:
            question = _context.sent;
            res.json({
              data: question
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getQuestion.apply(this, arguments);
}

function getListQuestion(_x3, _x4) {
  return _getListQuestion.apply(this, arguments);
}

function _getListQuestion() {
  _getListQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var questions;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return tbl_question.findAll({
              attributes: ['id', 'question'],
              where: {
                state: 1
              }
            });

          case 3:
            questions = _context3.sent;
            _context3.next = 6;
            return Promise.all(questions.map( /*#__PURE__*/function () {
              var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(item, key) {
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return getAlternatives(item.id);

                      case 2:
                        item.dataValues.alternatives = _context2.sent;
                        return _context2.abrupt("return", item);

                      case 4:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2);
              }));

              return function (_x10, _x11) {
                return _ref.apply(this, arguments);
              };
            }())).then(function (result) {
              res.json({
                data: result
              });
            });

          case 6:
            _context3.next = 11;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 8]]);
  }));
  return _getListQuestion.apply(this, arguments);
}

function getAlternatives(_x5) {
  return _getAlternatives.apply(this, arguments);
}

function _getAlternatives() {
  _getAlternatives = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(idQuestion) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return tbl_alternative.findAll({
              attributes: ['id', 'alternative', 'score'],
              where: {
                state: 1,
                tbl_question_id: idQuestion
              }
            });

          case 2:
            return _context4.abrupt("return", _context4.sent);

          case 3:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _getAlternatives.apply(this, arguments);
}

function createQuestion(_x6, _x7) {
  return _createQuestion.apply(this, arguments);
}

function _createQuestion() {
  _createQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var question, newQuestion;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            question = req.body.question;
            _context5.prev = 1;
            _context5.next = 4;
            return tbl_question.create({
              question: question
            }, {
              fields: ['question']
            });

          case 4:
            newQuestion = _context5.sent;

            if (newQuestion) {
              res.json({
                message: 'question created',
                date: newQuestion
              });
            }

            _context5.next = 12;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 8]]);
  }));
  return _createQuestion.apply(this, arguments);
}

function getAlternativeQuestion(_x8, _x9) {
  return _getAlternativeQuestion.apply(this, arguments);
}

function _getAlternativeQuestion() {
  _getAlternativeQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var qalternative;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _context6.next = 3;
            return tbl_question.findAll({
              include: Alternative
            });

          case 3:
            qalternative = _context6.sent;
            res.json({
              data: qalternative
            });
            _context6.next = 11;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 7]]);
  }));
  return _getAlternativeQuestion.apply(this, arguments);
}