"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.putDiary = putDiary;
exports.clearDiary = clearDiary;
exports.getListDateDiary = getListDateDiary;
exports.getListDateDiaryDetails = getListDateDiaryDetails;
exports.getListDiaryViewPsychologist = getListDiaryViewPsychologist;
exports.getDetailDiary = getDetailDiary;
exports.getListDiary = getListDiary;
exports.seenDiary = seenDiary;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_diary = _require.tbl_diary;

var Sequelize = require('sequelize');

var Op = Sequelize.Op;

function putDiary(_x, _x2) {
  return _putDiary.apply(this, arguments);
}

function _putDiary() {
  _putDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var url, taskId, text, profileId, _req$body, feel, publico, date, diary, newDiaryText, Newurl, Newfeel, Newtext, Newdate, Newpublico, DiaryText, newDiary;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = req.file ? req.file.location : null;
            taskId = req.body.taskId ? req.body.taskId : null;
            text = req.body.text ? req.body.text : null;
            profileId = req.user.idProfile;
            _req$body = req.body, feel = _req$body.feel, publico = _req$body.publico, date = _req$body.date;

            if (!taskId) {
              _context.next = 28;
              break;
            }

            _context.next = 9;
            return tbl_diary.findOne({
              where: {
                tbl_profile_id: profileId,
                tbl_task_id: taskId
              }
            });

          case 9:
            diary = _context.sent;

            if (diary) {
              _context.next = 17;
              break;
            }

            _context.next = 13;
            return tbl_diary.create({
              tbl_profile_id: profileId,
              tbl_task_id: taskId,
              text: text,
              audio: url,
              feel: feel,
              date: date,
              "public": publico
            });

          case 13:
            newDiaryText = _context.sent;
            return _context.abrupt("return", res.json({
              message: 'Diary Created',
              data: newDiaryText
            }));

          case 17:
            Newurl = req.file ? req.file.location : diary.url;
            Newfeel = req.body.taskId ? req.body.taskId : diary.feel;
            Newtext = req.body.text ? req.body.text : diary.text;
            Newdate = req.body.date ? req.body.date : diary.date;
            Newpublico = req.body.publico ? req.body.publico : diary.publico;
            _context.next = 24;
            return tbl_diary.update({
              text: Newtext,
              audio: Newurl,
              feel: Newfeel,
              date: Newdate,
              "public": Newpublico
            }, {
              where: {
                tbl_profile_id: profileId,
                tbl_task_id: taskId
              }
            });

          case 24:
            DiaryText = _context.sent;
            return _context.abrupt("return", res.json({
              message: 'Diary Update',
              data: DiaryText
            }));

          case 26:
            _context.next = 32;
            break;

          case 28:
            _context.next = 30;
            return tbl_diary.create({
              tbl_profile_id: profileId,
              tbl_task_id: null,
              text: text,
              audio: url,
              feel: feel,
              date: date,
              "public": publico
            });

          case 30:
            newDiary = _context.sent;
            return _context.abrupt("return", res.json({
              message: 'Diary Created',
              data: newDiary
            }));

          case 32:
            _context.next = 38;
            break;

          case 34:
            _context.prev = 34;
            _context.t0 = _context["catch"](0);
            console.error(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 38:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 34]]);
  }));
  return _putDiary.apply(this, arguments);
}

function clearDiary(_x3, _x4) {
  return _clearDiary.apply(this, arguments);
}

function _clearDiary() {
  _clearDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var idProfile, area, id, body, diary;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            idProfile = req.user.idProfile;
            area = req.body.area;
            id = req.params.id;
            body = {};
            if (area === 'text') body = {
              text: null
            };
            if (area === 'audio') body = {
              audio: null
            };
            _context2.next = 9;
            return tbl_diary.update(body, {
              where: {
                id: id,
                tbl_profile_id: idProfile
              }
            });

          case 9:
            _context2.next = 11;
            return tbl_diary.findOne({
              where: {
                id: id
              }
            });

          case 11:
            diary = _context2.sent;

            if (!(diary && !diary.text && !diary.audio)) {
              _context2.next = 15;
              break;
            }

            _context2.next = 15;
            return tbl_diary.destroy({
              where: {
                id: id
              }
            });

          case 15:
            return _context2.abrupt("return", res.json({
              message: "Update ".concat(area, " of Diary")
            }));

          case 18:
            _context2.prev = 18;
            _context2.t0 = _context2["catch"](0);
            console.error(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 18]]);
  }));
  return _clearDiary.apply(this, arguments);
}

function getListDateDiary(_x5, _x6) {
  return _getListDateDiary.apply(this, arguments);
}

function _getListDateDiary() {
  _getListDateDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var idProfile, month, year, dates;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            idProfile = req.user.idProfile;
            month = req.query.month;
            year = req.query.year;
            _context3.next = 6;
            return tbl_diary.findAll({
              where: [Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM\')'), _defineProperty({}, Op.like, year + "-" + month)), Sequelize.where(Sequelize.literal('tbl_profile_id'), _defineProperty({}, Op.eq, idProfile))],
              attributes: [// specify an array where the first element is the SQL function and the second is the alias
              [Sequelize.fn('DISTINCT', Sequelize.literal('to_char("date", \'YYYY-MM-DD\')')), 'date'] // specify any additional columns, e.g. country_code
              // 'country_code'
              ]
            });

          case 6:
            dates = _context3.sent;
            return _context3.abrupt("return", res.json({
              message: "Dates Availabilitys Diary",
              data: dates
            }));

          case 10:
            _context3.prev = 10;
            _context3.t0 = _context3["catch"](0);
            console.error(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 10]]);
  }));
  return _getListDateDiary.apply(this, arguments);
}

function getListDateDiaryDetails(_x7, _x8) {
  return _getListDateDiaryDetails.apply(this, arguments);
}

function _getListDateDiaryDetails() {
  _getListDateDiaryDetails = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var idProfile, date, diarys;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            idProfile = req.user.idProfile;
            date = req.query.date;
            _context4.next = 5;
            return tbl_diary.findAll({
              where: [Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM-DD\')'), _defineProperty({}, Op.like, date)), Sequelize.where(Sequelize.literal('tbl_profile_id'), _defineProperty({}, Op.eq, idProfile))]
            });

          case 5:
            diarys = _context4.sent;
            return _context4.abrupt("return", res.json({
              message: "Dates Availabilitys Diary",
              data: diarys
            }));

          case 9:
            _context4.prev = 9;
            _context4.t0 = _context4["catch"](0);
            console.error(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 9]]);
  }));
  return _getListDateDiaryDetails.apply(this, arguments);
}

function getListDiaryViewPsychologist(_x9, _x10) {
  return _getListDiaryViewPsychologist.apply(this, arguments);
}

function _getListDiaryViewPsychologist() {
  _getListDiaryViewPsychologist = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _req$query, isPatient, month, year, where, dates, lista;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _req$query = req.query, isPatient = _req$query.isPatient, month = _req$query.month, year = _req$query.year;
            where = [Sequelize.where(Sequelize.literal('state'), _defineProperty({}, Op.eq, 1)), Sequelize.where(Sequelize.literal('tbl_profile_id'), _defineProperty({}, Op.eq, isPatient))];
            if (month && year) where.push(Sequelize.where(Sequelize.literal('to_char("date", \'YYYY-MM\')'), _defineProperty({}, Op.like, year + '-' + month)));
            _context5.next = 6;
            return tbl_diary.findAll({
              where: where
            });

          case 6:
            dates = _context5.sent;
            lista = [];
            dates.map(function (value) {
              var date = new Date(value.dataValues.date);
              var day = date.toJSON().substr(0, 4);
              var month = date.toJSON().substr(5, 2);
              var year = date.toJSON().substr(8, 2);
              var index = lista.findIndex(function (el) {
                return el.date === "".concat(day, "-").concat(month, "-").concat(year); // or el.nombre=='T NORTE';
              });

              if (index === -1) {
                lista.push({
                  date: "".concat(day, "-").concat(month, "-").concat(year),
                  diaries: []
                });
                lista[lista.length - 1].diaries.push(value);
              } else {
                lista[index].diaries.push(value);
              }
            });
            return _context5.abrupt("return", res.json({
              message: "Dates Availabilitys Diary",
              data: lista
            }));

          case 12:
            _context5.prev = 12;
            _context5.t0 = _context5["catch"](0);
            console.error(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 12]]);
  }));
  return _getListDiaryViewPsychologist.apply(this, arguments);
}

function getDetailDiary(_x11, _x12) {
  return _getDetailDiary.apply(this, arguments);
}

function _getDetailDiary() {
  _getDetailDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var idProfile, id, detailDiary;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            idProfile = req.user.idProfile;
            id = req.params.id; //const month = req.query.month

            console.log(idProfile);
            _context6.next = 6;
            return tbl_diary.findOne({
              where: {
                tbl_profile_id: idProfile,
                id: id
              }
            });

          case 6:
            detailDiary = _context6.sent;
            return _context6.abrupt("return", res.json({
              message: "Diary found",
              data: detailDiary
            }));

          case 10:
            _context6.prev = 10;
            _context6.t0 = _context6["catch"](0);
            console.error(_context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 10]]);
  }));
  return _getDetailDiary.apply(this, arguments);
}

function getListDiary(_x13, _x14) {
  return _getListDiary.apply(this, arguments);
}

function _getListDiary() {
  _getListDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var idProfile, Diary;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            idProfile = req.user.idProfile; //const month = req.query.month

            console.log(idProfile);
            _context7.next = 5;
            return tbl_diary.findAll({
              where: {
                tbl_profile_id: idProfile
              },
              order: [['date', 'DESC']]
            });

          case 5:
            Diary = _context7.sent;
            return _context7.abrupt("return", res.json({
              message: "Diary List",
              data: Diary
            }));

          case 9:
            _context7.prev = 9;
            _context7.t0 = _context7["catch"](0);
            console.error(_context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 9]]);
  }));
  return _getListDiary.apply(this, arguments);
}

function seenDiary(_x15, _x16) {
  return _seenDiary.apply(this, arguments);
}

function _seenDiary() {
  _seenDiary = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var id, updateDiary, diary;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            id = req.params.id;
            _context8.next = 4;
            return tbl_diary.update({
              seen: 1
            }, {
              where: {
                id: id
              }
            });

          case 4:
            updateDiary = _context8.sent;

            if (!updateDiary) {
              _context8.next = 12;
              break;
            }

            _context8.next = 8;
            return tbl_diary.findOne({
              where: {
                id: id
              }
            });

          case 8:
            diary = _context8.sent;
            return _context8.abrupt("return", res.json({
              message: "diary updated",
              data: diary
            }));

          case 12:
            res.status(404).json({
              message: 'Diary not found'
            });

          case 13:
            _context8.next = 19;
            break;

          case 15:
            _context8.prev = 15;
            _context8.t0 = _context8["catch"](0);
            console.error(_context8.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 19:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 15]]);
  }));
  return _seenDiary.apply(this, arguments);
}