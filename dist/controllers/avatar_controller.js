"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListAvatar = getListAvatar;
exports.createAvatar = createAvatar;
exports.getAvatar = getAvatar;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_avatar = _require.tbl_avatar;

function getListAvatar(_x, _x2) {
  return _getListAvatar.apply(this, arguments);
}

function _getListAvatar() {
  _getListAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var avatars;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_avatar.findAll({
              where: {
                "default": 1
              }
            });

          case 3:
            avatars = _context.sent;
            res.json({
              data: avatars
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListAvatar.apply(this, arguments);
}

function createAvatar(_x3, _x4) {
  return _createAvatar.apply(this, arguments);
}

function _createAvatar() {
  _createAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var image, newAvatar;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            image = req.body.image;
            _context2.prev = 1;
            _context2.next = 4;
            return tbl_avatar.create({
              image: image
            });

          case 4:
            newAvatar = _context2.sent;

            if (newAvatar) {
              res.json({
                message: 'avatar created',
                date: newAvatar
              });
            }

            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 8]]);
  }));
  return _createAvatar.apply(this, arguments);
}

function getAvatar(_x5, _x6) {
  return _getAvatar.apply(this, arguments);
}

function _getAvatar() {
  _getAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var idProfile, profiles;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            idProfile = req.params.idProfile;
            _context3.next = 4;
            return tbl_profile.findByPk({
              include: {
                model: tbl_avatar,
                attributes: ['id', 'image']
              },
              where: {
                id: idProfile,
                state: 1
              }
            });

          case 4:
            profiles = _context3.sent;
            res.json({
              data: profiles
            });
            _context3.next = 12;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 8]]);
  }));
  return _getAvatar.apply(this, arguments);
}