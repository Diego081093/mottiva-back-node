"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.postChatMessage = postChatMessage;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_chat = _require.tbl_chat,
    tbl_chat_participants = _require.tbl_chat_participants,
    tbl_chat_participants_message = _require.tbl_chat_participants_message,
    tbl_profile = _require.tbl_profile;

var Sequelize = require('sequelize');

function postChatMessage(_x, _x2) {
  return _postChatMessage.apply(this, arguments);
}

function _postChatMessage() {
  _postChatMessage = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _req$body, id, message, chatMessage;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _req$body = req.body, id = _req$body.id, message = _req$body.message;
            _context.next = 4;
            return tbl_chat_participants_message.create({
              tbl_chat_participants_id: id,
              message: message
            });

          case 4:
            chatMessage = _context.sent;
            res.json({
              message: 'message save succesful',
              data: chatMessage
            });
            _context.next = 12;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 8]]);
  }));
  return _postChatMessage.apply(this, arguments);
}