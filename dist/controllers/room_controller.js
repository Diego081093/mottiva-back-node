"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListRoomByProfile = getListRoomByProfile;
exports.getRoomDetails = getRoomDetails;
exports.getEmotionRooms = getEmotionRooms;
exports.showEmotionRoom = showEmotionRoom;
exports.twilioListRooms = twilioListRooms;
exports.twilioCreateRoom = twilioCreateRoom;
exports.twilioUpdateRoom = twilioUpdateRoom;
exports.postRoom = postRoom;
exports.postEnterRoom = postEnterRoom;
exports.postSignupRoom = postSignupRoom;
exports.deleteUnsubscribeRoom = deleteUnsubscribeRoom;
exports.closedRoom = closedRoom;
exports.twilioAccessToken = twilioAccessToken;
exports.getParticipants = getParticipants;
exports.addRoomNote = addRoomNote;
exports.putEmotionRooms = putEmotionRooms;

var _Chat = _interopRequireDefault(require("twilio/lib/rest/Chat"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var AccessToken = require('twilio').jwt.AccessToken;

var VideoGrant = AccessToken.VideoGrant;

var Sequelize = require('sequelize');

var _require = require('./../helpers'),
    isValidBase64Img = _require.isValidBase64Img,
    s3UploadBase64Image = _require.s3UploadBase64Image;

var _require2 = require('../models'),
    tbl_profile = _require2.tbl_profile,
    tbl_room = _require2.tbl_room,
    tbl_chat = _require2.tbl_chat,
    tbl_method = _require2.tbl_method,
    tbl_thematic = _require2.tbl_thematic,
    tbl_chat_participants = _require2.tbl_chat_participants,
    tbl_room_participants = _require2.tbl_room_participants,
    tbl_room_note = _require2.tbl_room_note,
    tbl_avatar = _require2.tbl_avatar;

var _require3 = require('sequelize'),
    Op = _require3.Op;

var _require4 = require('luxon'),
    DateTime = _require4.DateTime;

var accountSid = process.env.TWILIO_ACCOUNT_SID;
var authToken = process.env.TWILIO_AUTH_TOKEN;

var client = require('twilio')(accountSid, authToken);

function getListRoomByProfile(_x, _x2) {
  return _getListRoomByProfile.apply(this, arguments);
}

function _getListRoomByProfile() {
  _getListRoomByProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var idRole, whereQuery, now, date, room, data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            idRole = req.user.idRole;
            whereQuery = {
              state: 1
            };

            if (req.query.search) {
              whereQuery.title = _defineProperty({}, Op.iLike, '%' + req.query.search + '%');
            }

            if (req.query.idThematic) {
              whereQuery.tbl_thematic_id = req.query.idThematic;
            }

            if (req.query.idMethod) {
              whereQuery.tbl_method_id = req.query.idMethod;
            }

            if (idRole === 1) {
              now = DateTime.fromISO(DateTime.now(), {
                locale: 'es-ES',
                zone: 'UTC'
              });
              date = now.plus({
                hour: -5,
                minutes: -50
              }).toString();
              whereQuery.start = _defineProperty({}, Op.gt, date);
            }

            _context2.next = 9;
            return tbl_room.findAll({
              where: whereQuery,
              order: [['start', 'ASC']],
              include: [{
                model: tbl_chat,
                as: 'Chat',
                attributes: ['id'],
                where: {
                  state: 1
                },
                include: {
                  model: tbl_chat_participants,
                  as: 'ChatParticipant',
                  attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                  where: {
                    'tbl_profile_id': req.user.idProfile,
                    'state': 1
                  },
                  required: false
                }
              }, {
                model: tbl_method,
                as: 'Method',
                attributes: ['id', 'name']
              }, {
                model: tbl_thematic,
                as: 'Thematic',
                attributes: ['id', 'name']
              }],
              attributes: ['id', 'title', 'start', 'end', 'rules', 'participants', 'maxParticipants', 'inscription', 'image', 'twilio_uniquename', 'created_at']
            });

          case 9:
            room = _context2.sent;
            _context2.next = 12;
            return Promise.all(room.map( /*#__PURE__*/function () {
              var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(item, key) {
                var n_participants, psychologist, participants;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return tbl_room_participants.count({
                          where: {
                            tbl_room_id: item.id
                          },
                          include: [{
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {
                              tbl_role_id: 1
                            },
                            attributes: [],
                            required: true
                          }]
                        });

                      case 2:
                        n_participants = _context.sent;
                        _context.next = 5;
                        return tbl_room_participants.findAll({
                          where: {
                            tbl_room_id: item.id
                          },
                          include: [{
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {
                              tbl_role_id: 2
                            },
                            attributes: []
                          }],
                          attributes: [[Sequelize.literal('"Profile_participant"."firstname"'), 'name'], [Sequelize.literal('"Profile_participant"."id"'), 'id']]
                        });

                      case 5:
                        psychologist = _context.sent;
                        _context.next = 8;
                        return tbl_room_participants.findAll({
                          where: {
                            tbl_room_id: item.id
                          },
                          include: [{
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {
                              tbl_role_id: 1
                            },
                            attributes: []
                          }],
                          attributes: [[Sequelize.literal('"Profile_participant"."firstname"'), 'name'], [Sequelize.literal('"Profile_participant"."id"'), 'id']]
                        });

                      case 8:
                        participants = _context.sent;
                        item.setDataValue('participants', n_participants);
                        item.setDataValue('Participants', participants);
                        item.setDataValue('psychologist', psychologist);
                        item.setDataValue('status', 'error');
                        return _context.abrupt("return", item);

                      case 14:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x36, _x37) {
                return _ref.apply(this, arguments);
              };
            }()));

          case 12:
            data = _context2.sent;
            res.json({
              message: 'Rooms found',
              data: data
            });
            _context2.next = 20;
            break;

          case 16:
            _context2.prev = 16;
            _context2.t0 = _context2["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 16]]);
  }));
  return _getListRoomByProfile.apply(this, arguments);
}

function getRoomDetails(_x3, _x4) {
  return _getRoomDetails.apply(this, arguments);
}

function _getRoomDetails() {
  _getRoomDetails = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var roomId, participants, roomDetails;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            roomId = req.params.id;
            _context3.next = 4;
            return tbl_room_participants.count({
              where: {
                tbl_room_id: roomId
              }
            });

          case 4:
            participants = _context3.sent;
            _context3.next = 7;
            return tbl_room.findOne({
              attributes: ['id', 'title', 'twilio_uniquename', 'start', 'rules', 'image'],
              where: {
                id: roomId,
                state: 1
              }
            });

          case 7:
            roomDetails = _context3.sent;
            roomDetails['participants'] = participants;
            res.json({
              data: roomDetails
            });
            _context3.next = 16;
            break;

          case 12:
            _context3.prev = 12;
            _context3.t0 = _context3["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 12]]);
  }));
  return _getRoomDetails.apply(this, arguments);
}

function getEmotionRooms(_x5, _x6) {
  return _getEmotionRooms.apply(this, arguments);
}

function _getEmotionRooms() {
  _getEmotionRooms = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _start, _start2, _start3, _start4, _start5;

    var to, search, whereQuery, now, weekday, quarter, emotion_rooms;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            to = req.query.to, search = req.query.search;
            // Today, 00:00:00
            now = DateTime.fromObject({
              day: DateTime.now().day
            });
            _context4.t0 = to;
            _context4.next = _context4.t0 === 'yesterday' ? 6 : _context4.t0 === 'week' ? 8 : _context4.t0 === 'month' ? 11 : _context4.t0 === 'quarter' ? 13 : _context4.t0 === 'custom' ? 16 : 18;
            break;

          case 6:
            whereQuery = {
              start: (_start = {}, _defineProperty(_start, Op.gte, now.minus({
                days: 1
              }).toISO()), _defineProperty(_start, Op.lt, now.toISO()), _start)
            };
            return _context4.abrupt("break", 20);

          case 8:
            weekday = now.weekday;
            whereQuery = {
              start: (_start2 = {}, _defineProperty(_start2, Op.gte, now.plus({
                days: 1 - weekday
              }).toISO()), _defineProperty(_start2, Op.lt, now.plus({
                days: 8 - weekday
              }).toISO()), _start2)
            };
            return _context4.abrupt("break", 20);

          case 11:
            whereQuery = {
              start: (_start3 = {}, _defineProperty(_start3, Op.gte, DateTime.fromObject({
                day: 1
              }).toISO()), _defineProperty(_start3, Op.lt, DateTime.fromObject({
                day: 1,
                month: now.month + 1
              }).toISO()), _start3)
            };
            return _context4.abrupt("break", 20);

          case 13:
            quarter = Math.ceil(now.month / 3);
            whereQuery = {
              start: (_start4 = {}, _defineProperty(_start4, Op.gte, DateTime.fromObject({
                day: 1,
                month: 3 * quarter - 2
              }).toISO()), _defineProperty(_start4, Op.lt, DateTime.fromObject({
                day: 1,
                month: 3 * quarter
              }).plus({
                months: 1
              }).toISO()), _start4)
            };
            return _context4.abrupt("break", 20);

          case 16:
            whereQuery = {
              start: (_start5 = {}, _defineProperty(_start5, Op.gte, DateTime.fromISO(req.query.c_from).toISO()), _defineProperty(_start5, Op.lt, DateTime.fromISO(req.query.c_to).plus({
                days: 1
              }).toISO()), _start5)
            };
            return _context4.abrupt("break", 20);

          case 18:
            whereQuery = {};
            return _context4.abrupt("break", 20);

          case 20:
            if (search) {
              whereQuery.title = _defineProperty({}, Op.iLike, '%' + search + '%');
            }

            _context4.next = 23;
            return tbl_room.findAll({
              where: whereQuery,
              include: {
                model: tbl_room_participants,
                where: {
                  tbl_profile_participant_id: req.user.idProfile,
                  state: 1
                },
                attributes: []
              },
              attributes: ['id', 'title', 'start', 'end', 'image']
            });

          case 23:
            emotion_rooms = _context4.sent;
            return _context4.abrupt("return", res.json({
              message: 'Rooms found',
              rooms: emotion_rooms
            }));

          case 27:
            _context4.prev = 27;
            _context4.t1 = _context4["catch"](0);
            console.log(_context4.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 31:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 27]]);
  }));
  return _getEmotionRooms.apply(this, arguments);
}

function showEmotionRoom(_x7, _x8) {
  return _showEmotionRoom.apply(this, arguments);
}

function _showEmotionRoom() {
  _showEmotionRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var emotion_room, profileAttributes, p_patients, p_psychologists, p_note, _yield$Promise$all, _yield$Promise$all2, patients, psychologists, note;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return tbl_room.findByPk(req.params.id, {
              include: [{
                model: tbl_room_participants,
                where: {
                  tbl_profile_participant_id: req.user.idProfile
                },
                attributes: []
              }, {
                model: tbl_chat,
                as: 'Chat',
                attributes: ['id']
              }],
              attributes: ['id', 'title', 'start', 'end', 'image', 'twilio_uniquename', 'state']
            });

          case 3:
            emotion_room = _context5.sent;

            if (!emotion_room) {
              _context5.next = 17;
              break;
            }

            profileAttributes = ['id', 'firstname', 'lastname'];
            p_patients = tbl_profile.findAll({
              where: {
                tbl_role_id: 1
              },
              include: {
                model: tbl_room_participants,
                as: 'RoomParticipants',
                where: {
                  tbl_room_id: emotion_room.id
                },
                attributes: []
              },
              attributes: profileAttributes
            });
            p_psychologists = tbl_profile.findAll({
              where: {
                tbl_role_id: 2
              },
              include: {
                model: tbl_room_participants,
                as: 'RoomParticipants',
                where: {
                  tbl_room_id: emotion_room.id
                },
                attributes: []
              },
              attributes: profileAttributes
            });
            p_note = tbl_room_note.findOne({
              where: {
                tbl_room_id: emotion_room.id,
                tbl_profile_psychologist_id: req.user.idProfile
              }
            });
            _context5.next = 11;
            return Promise.all([p_patients, p_psychologists, p_note]);

          case 11:
            _yield$Promise$all = _context5.sent;
            _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 3);
            patients = _yield$Promise$all2[0];
            psychologists = _yield$Promise$all2[1];
            note = _yield$Promise$all2[2];
            return _context5.abrupt("return", res.json({
              message: 'Emotion room found',
              emotion_room: emotion_room,
              patients: patients,
              psychologists: psychologists,
              note: note
            }));

          case 17:
            res.status(404).json({
              message: 'Emotion room not found'
            });
            _context5.next = 24;
            break;

          case 20:
            _context5.prev = 20;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 24:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 20]]);
  }));
  return _showEmotionRoom.apply(this, arguments);
}

function twilioListRooms(_x9, _x10) {
  return _twilioListRooms.apply(this, arguments);
}

function _twilioListRooms() {
  _twilioListRooms = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var rooms;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _context6.next = 3;
            return client.video.rooms.list({});

          case 3:
            rooms = _context6.sent;
            res.json({
              message: 'Rooms found',
              rooms: rooms
            });
            _context6.next = 11;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](0);
            console.error('You have a error in twilioListRooms:', _context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 7]]);
  }));
  return _twilioListRooms.apply(this, arguments);
}

function twilioCreateRoom(_x11, _x12, _x13, _x14, _x15) {
  return _twilioCreateRoom.apply(this, arguments);
}

function _twilioCreateRoom() {
  _twilioCreateRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res, type, nameRoom, participants) {
    var room_type, room;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            room_type = type;
            _context7.t0 = room_type;
            _context7.next = _context7.t0 === 'group' ? 5 : _context7.t0 === 'go' ? 5 : 6;
            break;

          case 5:
            return _context7.abrupt("break", 8);

          case 6:
            room_type = 'go';
            return _context7.abrupt("break", 8);

          case 8:
            _context7.next = 10;
            return client.video.rooms.create({
              type: room_type,
              uniqueName: nameRoom,
              maxParticipants: participants
            });

          case 10:
            room = _context7.sent;
            return _context7.abrupt("return", room);

          case 14:
            _context7.prev = 14;
            _context7.t1 = _context7["catch"](0);
            console.error('You have a error in twilioCreateRoom:', _context7.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 18:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 14]]);
  }));
  return _twilioCreateRoom.apply(this, arguments);
}

function twilioUpdateRoom(_x16, _x17) {
  return _twilioUpdateRoom.apply(this, arguments);
}

function _twilioUpdateRoom() {
  _twilioUpdateRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var room;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            _context8.next = 3;
            return client.video.rooms(req.params.sid).update({
              status: 'completed'
            });

          case 3:
            room = _context8.sent;
            res.json({
              message: 'Rooms updated',
              room: room
            });
            _context8.next = 11;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](0);
            console.error('You have a error in twilioUpdateRoom:', _context8.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 7]]);
  }));
  return _twilioUpdateRoom.apply(this, arguments);
}

function postRoom(_x18, _x19) {
  return _postRoom.apply(this, arguments);
}

function _postRoom() {
  _postRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res) {
    var _isValidBase64Img, isValid, buffer, ext, imageUploaded, image, participants, sessionId, idSession, nameRoom, room, chat, type, twilio;

    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.prev = 0;
            _isValidBase64Img = isValidBase64Img(req.body.image), isValid = _isValidBase64Img.isValid, buffer = _isValidBase64Img.buffer, ext = _isValidBase64Img.ext;

            if (isValid) {
              _context9.next = 4;
              break;
            }

            return _context9.abrupt("return", res.status(422).json({
              message: 'The given data was invalid.',
              errors: {
                image: ["The image must be a valid base64"]
              }
            }));

          case 4:
            req.body.image = {
              buffer: buffer,
              ext: ext
            };
            _context9.next = 7;
            return s3UploadBase64Image('uploads', req.body.image);

          case 7:
            imageUploaded = _context9.sent;
            console.log('');
            console.log('');
            console.log(imageUploaded.data.location);
            console.log('');
            console.log('');
            image = imageUploaded.data.Location;
            participants = req.body.participants;
            _context9.next = 17;
            return tbl_room.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 17:
            sessionId = _context9.sent;
            idSession = sessionId.id + 1;
            nameRoom = 'room-r-' + idSession;
            _context9.next = 22;
            return tbl_room.create({
              id: idSession,
              title: req.body.tittle,
              tbl_thematic_id: req.body.idThematic,
              tbl_method_id: req.body.idMethod,
              start: req.body.start,
              end: req.body.end,
              rules: req.body.rules,
              participants: participants,
              maxParticipants: participants,
              image: image,
              twilio_uniquename: nameRoom
            });

          case 22:
            room = _context9.sent;
            _context9.next = 25;
            return tbl_room_participants.create({
              tbl_room_id: idSession,
              tbl_profile_participant_id: req.body.idPsychologist1
            });

          case 25:
            _context9.next = 27;
            return tbl_room_participants.create({
              tbl_room_id: idSession,
              tbl_profile_participant_id: req.body.idPsychologist2
            });

          case 27:
            _context9.next = 29;
            return tbl_chat.create({
              tbl_room_id: idSession,
              start: req.body.start,
              helpdesk: 0
            });

          case 29:
            chat = _context9.sent;
            _context9.next = 32;
            return tbl_chat_participants.create({
              tbl_chat_id: chat.id,
              tbl_profile_id: req.body.idPsychologist1
            });

          case 32:
            _context9.next = 34;
            return tbl_chat_participants.create({
              tbl_chat_id: chat.id,
              tbl_profile_id: req.body.idPsychologist2
            });

          case 34:
            type = 'group';
            _context9.next = 37;
            return twilioCreateRoom(req, res, type, nameRoom, participants);

          case 37:
            twilio = _context9.sent;
            res.json({
              data: room,
              twilio: twilio
            });
            _context9.next = 45;
            break;

          case 41:
            _context9.prev = 41;
            _context9.t0 = _context9["catch"](0);
            console.error('You have a error in:', _context9.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 45:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[0, 41]]);
  }));
  return _postRoom.apply(this, arguments);
}

function postEnterRoom(_x20, _x21) {
  return _postEnterRoom.apply(this, arguments);
}

function _postEnterRoom() {
  _postEnterRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res) {
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            try {
              res.json({
                message: 'You entered the room'
              });
            } catch (error) {
              console.error('You have a error in:', error);
              res.status(500).json({
                message: 'Something went wrong'
              });
            }

          case 1:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _postEnterRoom.apply(this, arguments);
}

function postSignupRoom(_x22, _x23) {
  return _postSignupRoom.apply(this, arguments);
}

function _postSignupRoom() {
  _postSignupRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(req, res) {
    var room_id, idProfile, room, _yield$tbl_chat$findO, _yield$tbl_chat$findO2, chat, chat_created, chat_participant;

    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;
            room_id = req.params.id;
            idProfile = req.user.idProfile;
            _context11.next = 5;
            return tbl_room.findByPk(room_id);

          case 5:
            room = _context11.sent;

            if (!room) {
              _context11.next = 22;
              break;
            }

            _context11.next = 9;
            return tbl_chat.findOrCreate({
              where: {
                tbl_room_id: room_id
              }
            });

          case 9:
            _yield$tbl_chat$findO = _context11.sent;
            _yield$tbl_chat$findO2 = _slicedToArray(_yield$tbl_chat$findO, 2);
            chat = _yield$tbl_chat$findO2[0];
            chat_created = _yield$tbl_chat$findO2[1];
            _context11.next = 15;
            return tbl_chat_participants.findOne({
              where: {
                tbl_chat_id: chat.id,
                tbl_profile_id: req.user.idProfile
              }
            });

          case 15:
            chat_participant = _context11.sent;

            if (chat_participant) {
              _context11.next = 21;
              break;
            }

            _context11.next = 19;
            return tbl_chat_participants.create({
              tbl_chat_id: chat.id,
              tbl_profile_id: req.user.idProfile
            });

          case 19:
            _context11.next = 21;
            return tbl_room_participants.create({
              tbl_room_id: room_id,
              tbl_profile_participant_id: req.user.idProfile
            });

          case 21:
            return _context11.abrupt("return", res.json({
              message: 'Subscribed to room'
            }));

          case 22:
            res.status(404).json({
              message: 'Room not found'
            });
            _context11.next = 29;
            break;

          case 25:
            _context11.prev = 25;
            _context11.t0 = _context11["catch"](0);
            console.error('You have a error in:', _context11.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 29:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[0, 25]]);
  }));
  return _postSignupRoom.apply(this, arguments);
}

function deleteUnsubscribeRoom(_x24, _x25) {
  return _deleteUnsubscribeRoom.apply(this, arguments);
}

function _deleteUnsubscribeRoom() {
  _deleteUnsubscribeRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(req, res) {
    var room_id, room, chat;
    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.prev = 0;
            room_id = req.params.id;
            _context12.next = 4;
            return tbl_room.findByPk(room_id);

          case 4:
            room = _context12.sent;

            if (!room) {
              _context12.next = 15;
              break;
            }

            _context12.next = 8;
            return tbl_chat.findOne({
              where: {
                tbl_room_id: room_id
              }
            });

          case 8:
            chat = _context12.sent;

            if (!chat) {
              _context12.next = 14;
              break;
            }

            _context12.next = 12;
            return tbl_chat_participants.destroy({
              where: {
                tbl_chat_id: chat.id,
                tbl_profile_id: req.user.idProfile
              }
            });

          case 12:
            _context12.next = 14;
            return tbl_room_participants.destroy({
              where: {
                tbl_room_id: room_id,
                tbl_profile_participant_id: req.user.idProfile
              }
            });

          case 14:
            return _context12.abrupt("return", res.json({
              message: 'Unsubscribed from room'
            }));

          case 15:
            res.status(404).json({
              message: 'Room not found'
            });
            _context12.next = 22;
            break;

          case 18:
            _context12.prev = 18;
            _context12.t0 = _context12["catch"](0);
            console.error('You have a error in:', _context12.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 22:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[0, 18]]);
  }));
  return _deleteUnsubscribeRoom.apply(this, arguments);
}

function closedRoom(_x26, _x27) {
  return _closedRoom.apply(this, arguments);
}

function _closedRoom() {
  _closedRoom = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(req, res) {
    var _closedRoom2;

    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.prev = 0;
            _context13.next = 3;
            return tbl_room.update({
              state: req.body.state
            }, {
              where: {
                id: req.body.idRoom
              }
            });

          case 3:
            _closedRoom2 = _context13.sent;
            _context13.next = 6;
            return tbl_room_participants.update({
              state: 0
            }, {
              where: {
                id: req.body.idRoom,
                tbl_profile_participant_id: req.user.idProfile
              }
            });

          case 6:
            res.json({
              message: "Room Closed!!"
            });
            _context13.next = 13;
            break;

          case 9:
            _context13.prev = 9;
            _context13.t0 = _context13["catch"](0);
            console.error('You have a error in:', _context13.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13, null, [[0, 9]]);
  }));
  return _closedRoom.apply(this, arguments);
}

function twilioAccessToken(_x28, _x29) {
  return _twilioAccessToken.apply(this, arguments);
}

function _twilioAccessToken() {
  _twilioAccessToken = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(req, res) {
    var config, identity, videoGrant, token;
    return regeneratorRuntime.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            try {
              config = require('../config/twilio.js');
              identity = req.user.firstname;

              if (req.body.name) {
                identity = req.body.name;
              }

              videoGrant = new VideoGrant({
                room: req.body.room
              });
              token = new AccessToken(config.twilioAccountSid, config.twilioApiKey, config.twilioApiSecret, {
                identity: identity
              });
              token.addGrant(videoGrant);
              res.json({
                message: "Token created",
                data: {
                  token: token.toJwt()
                }
              });
            } catch (error) {
              console.error('You have a error in:', error);
              res.status(500).json({
                message: 'Something went wrong'
              });
            }

          case 1:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14);
  }));
  return _twilioAccessToken.apply(this, arguments);
}

function getParticipants(_x30, _x31) {
  return _getParticipants.apply(this, arguments);
}

function _getParticipants() {
  _getParticipants = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(req, res) {
    var profileAttributes, participants;
    return regeneratorRuntime.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.prev = 0;
            profileAttributes = ['id', 'firstname', 'lastname'];
            _context15.next = 4;
            return tbl_profile.findAll({
              where: {
                tbl_role_id: 1
              },
              include: [{
                model: tbl_room_participants,
                as: 'RoomParticipants',
                where: {
                  tbl_room_id: req.params.id
                },
                attributes: []
              }, {
                model: tbl_avatar,
                as: 'Avatar',
                attributes: ['image']
              }],
              attributes: profileAttributes
            });

          case 4:
            participants = _context15.sent;
            return _context15.abrupt("return", res.json({
              message: 'Emotion room found',
              data: participants
            }));

          case 8:
            _context15.prev = 8;
            _context15.t0 = _context15["catch"](0);
            console.error('You have a error in:', _context15.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15, null, [[0, 8]]);
  }));
  return _getParticipants.apply(this, arguments);
}

function addRoomNote(_x32, _x33) {
  return _addRoomNote.apply(this, arguments);
}

function _addRoomNote() {
  _addRoomNote = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(req, res) {
    var _req$body, note, priority, priorityVal, currentNote;

    return regeneratorRuntime.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.prev = 0;
            _req$body = req.body, note = _req$body.note, priority = _req$body.priority;

            if (!note) {
              _context16.next = 19;
              break;
            }

            priorityVal = 0;
            if (priority) priorityVal = priority;
            console.log("aki llega: " + req.user.idProfile);
            _context16.next = 8;
            return tbl_room_note.findOne({
              where: {
                tbl_room_id: req.params.id,
                tbl_profile_psychologist_id: req.user.idProfile
              }
            });

          case 8:
            currentNote = _context16.sent;

            if (!currentNote) {
              _context16.next = 14;
              break;
            }

            _context16.next = 12;
            return tbl_room_note.update({
              notes: note,
              priority: priorityVal
            }, {
              where: {
                id: currentNote.id
              }
            });

          case 12:
            _context16.next = 16;
            break;

          case 14:
            _context16.next = 16;
            return tbl_room_note.create({
              tbl_room_id: req.params.id,
              tbl_profile_psychologist_id: req.user.idProfile,
              notes: note,
              priority: priorityVal,
              state: 1
            });

          case 16:
            res.status(200).json({
              message: 'Note registered'
            });
            _context16.next = 20;
            break;

          case 19:
            res.status(200).json({
              message: 'Nothing to do'
            });

          case 20:
            _context16.next = 26;
            break;

          case 22:
            _context16.prev = 22;
            _context16.t0 = _context16["catch"](0);
            console.error('You have a error in:', _context16.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 26:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16, null, [[0, 22]]);
  }));
  return _addRoomNote.apply(this, arguments);
}

function putEmotionRooms(_x34, _x35) {
  return _putEmotionRooms.apply(this, arguments);
}

function _putEmotionRooms() {
  _putEmotionRooms = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(req, res) {
    return regeneratorRuntime.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            try {
              console.log(req.params.id);
              res.status(200).json({
                message: 'updated'
              });
            } catch (error) {
              res.status(500).json({
                message: 'Something went wrong'
              });
            }

          case 1:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17);
  }));
  return _putEmotionRooms.apply(this, arguments);
}