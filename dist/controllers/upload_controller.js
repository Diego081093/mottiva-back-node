"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storeFiles = storeFiles;
exports.storeFile = storeFile;
exports.uploadFileS3 = uploadFileS3;
exports.uploadBase64File = uploadBase64File;
exports.uploadAvatar = uploadAvatar;
exports.uploadFile = exports.uploadFiles = void 0;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import Role from '../models/role_model'
var _require = require('../models'),
    tbl_profile_attribute = _require.tbl_profile_attribute,
    tbl_avatar = _require.tbl_avatar;

var _require2 = require('./../helpers'),
    isValidBase64Img = _require2.isValidBase64Img,
    s3UploadBase64Image = _require2.s3UploadBase64Image;

var multer = require('multer');

var _require3 = require('luxon'),
    DateTime = _require3.DateTime;

var slugify = require('slugify');

var qwe = [];
var fileType;
var storage = multer.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function filename(req, file, cb) {
    var name = DateTime.now().toFormat('yyyyMMddHHmmss') + '_' + slugify(file.originalname);
    qwe.push(name);
    cb(null, name);
  }
});

var fileFilter = function fileFilter(req, file, cb) {
  var validMimetypes = ['image/jpeg', 'image/png', 'application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];

  if (validMimetypes.includes(file.mimetype)) {
    fileType = 'document';

    if (['image/jpeg', 'image/png'].includes(file.mimetype)) {
      fileType = 'image';
    }

    cb(null, true);
  }

  cb(null, false);
};

var limits = {
  // 1 KB = 1024 bytes
  fileSize: 1024 * 600
};
var upload = multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: limits
}); // export async function getListRole(req, res){
//     try {
//         const roles = await tbl_role.findAll();
//         res.json({
//             data: roles
//         });
//     } catch (error) {
//         console.log(error);
//         res.status(500).json({
//             message: 'Something went wrong'
//         });
//     }
// }

var uploadFiles = upload.array('photos', 3);
exports.uploadFiles = uploadFiles;
var uploadFile = upload.single('file');
exports.uploadFile = uploadFile;

function storeFiles(_x, _x2) {
  return _storeFiles.apply(this, arguments);
}

function _storeFiles() {
  _storeFiles = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            try {
              res.json({
                message: 'Se guardaron las imágenes satisfactoriamente',
                files: qwe
              });
            } catch (error) {
              console.log(error);
              res.status(500).json({
                message: 'Something went wrong'
              });
            }

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _storeFiles.apply(this, arguments);
}

function storeFile(_x3, _x4) {
  return _storeFile.apply(this, arguments);
}

function _storeFile() {
  _storeFile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var user, profile_attribute;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            user = req.user;
            _context2.next = 4;
            return tbl_profile_attribute.create({
              tbl_profile_id: req.user.idProfile,
              key: fileType,
              value: 'uploads/' + qwe[0],
              state: 1
            });

          case 4:
            profile_attribute = _context2.sent;
            res.json({
              message: 'Se guardó la imagen satisfactoriamente',
              files: '/uploads/' + qwe[0]
            });
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _storeFile.apply(this, arguments);
}

function uploadFileS3(_x5, _x6) {
  return _uploadFileS.apply(this, arguments);
}

function _uploadFileS() {
  _uploadFileS = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var url;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            console.log('req.file');
            console.log(req.file);
            console.log(req.file.location);
            console.log('');
            console.log('');
            url = req.file.location;
            return _context3.abrupt("return", url);

          case 10:
            _context3.prev = 10;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 10]]);
  }));
  return _uploadFileS.apply(this, arguments);
}

function uploadBase64File(_x7, _x8) {
  return _uploadBase64File.apply(this, arguments);
}

function _uploadBase64File() {
  _uploadBase64File = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _isValidBase64Img, isValid, buffer, ext, imageUploaded, url, newAvatar;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _isValidBase64Img = isValidBase64Img(req.body.image), isValid = _isValidBase64Img.isValid, buffer = _isValidBase64Img.buffer, ext = _isValidBase64Img.ext;

            if (isValid) {
              _context4.next = 3;
              break;
            }

            return _context4.abrupt("return", res.status(422).json({
              message: 'The given data was invalid.',
              errors: {
                image: ["The image must be a valid base64"]
              }
            }));

          case 3:
            req.body.image = {
              buffer: buffer,
              ext: ext
            };
            _context4.next = 6;
            return s3UploadBase64Image('uploads/', req.body.image);

          case 6:
            imageUploaded = _context4.sent;
            url = imageUploaded.data.Location;
            _context4.next = 10;
            return tbl_avatar.create({
              image: url,
              "default": 0
            });

          case 10:
            newAvatar = _context4.sent;

            if (imageUploaded.success) {
              _context4.next = 13;
              break;
            }

            return _context4.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 13:
            res.json({
              message: 'File uploaded',
              data: newAvatar
            });

          case 14:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _uploadBase64File.apply(this, arguments);
}

function uploadAvatar(_x9, _x10) {
  return _uploadAvatar.apply(this, arguments);
}

function _uploadAvatar() {
  _uploadAvatar = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var url, newAvatar;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            console.log('req.file');
            console.log(req.file);
            console.log(req.file.location);
            console.log('');
            console.log('');
            url = req.file.location;
            _context5.next = 9;
            return tbl_avatar.create({
              image: url,
              "default": 0
            });

          case 9:
            newAvatar = _context5.sent;
            res.json({
              newAvatar: newAvatar
            });
            _context5.next = 17;
            break;

          case 13:
            _context5.prev = 13;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 13]]);
  }));
  return _uploadAvatar.apply(this, arguments);
}