"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMedia = getMedia;
exports.getListMedias = getListMedias;
exports.watchLaterMedia = watchLaterMedia;
exports.favoriteMedia = favoriteMedia;
exports.postFavoriteMedia = postFavoriteMedia;
exports.postDesactivateFavoriteMedia = postDesactivateFavoriteMedia;
exports.ListCategoryMedia = ListCategoryMedia;
exports.postDoneMedia = postDoneMedia;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import Role from '../models/role_model'
var _require = require('../models'),
    tbl_medias = _require.tbl_medias,
    tbl_media_profile = _require.tbl_media_profile,
    tbl_media_role = _require.tbl_media_role,
    tbl_category_medias = _require.tbl_category_medias;

var Sequelize = require('sequelize');

var Op = Sequelize.Op;

function getMedia(_x, _x2) {
  return _getMedia.apply(this, arguments);
}

function _getMedia() {
  _getMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var id, whereRole, media;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            id = req.params.id;
            whereRole = {};

            if (req.query.role) {
              whereRole.tbl_role_id = req.query.role;
            } else {
              whereRole.tbl_role_id = req.user.idRole;
            }

            _context.next = 6;
            return tbl_medias.findOne({
              where: {
                id: id
              },
              attributes: ['id', ['tbl_category_media_id', 'idCategory'], ['tbl_method_id', 'idMethod'], 'name', 'type', 'file', 'image', 'duration', 'description', 'points', 'state'],
              include: [{
                model: tbl_media_role,
                as: 'Role',
                where: whereRole,
                attributes: ['id']
              }, {
                model: tbl_media_profile,
                as: 'onProfile',
                where: {
                  tbl_profile_id: req.user.idProfile
                },
                attributes: ['id', 'done', 'favorite'],
                required: false
              }]
            });

          case 6:
            media = _context.sent;
            res.json({
              data: media
            });
            _context.next = 14;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 10]]);
  }));
  return _getMedia.apply(this, arguments);
}

function getListMedias(_x3, _x4) {
  return _getListMedias.apply(this, arguments);
}

function _getListMedias() {
  _getListMedias = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$query, withMethod, type, ref, method, category, role, history, favorite, archived, marked, where, whereRole, whereMediaProfile, requiredDone, medias;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _req$query = req.query, withMethod = _req$query.withMethod, type = _req$query.type, ref = _req$query.ref, method = _req$query.method, category = _req$query.category, role = _req$query.role, history = _req$query.history, favorite = _req$query.favorite, archived = _req$query.archived, marked = _req$query.marked;
            console.log('method', withMethod);
            where = {
              tbl_method_id: withMethod == 0 ? _defineProperty({}, Op.is, null) : _defineProperty({}, Op.not, null)
            };

            if (type) {
              where.type = type === "video" ? "1" : "2";
            }

            if (ref) {
              where.name = _defineProperty({}, Op.iLike, '%' + ref + '%');

              if (method) {
                where.tbl_method_id = _defineProperty({}, Op["in"], method.split(','));
              }

              if (category) {
                where.tbl_category_media_id = _defineProperty({}, Op["in"], category.split(','));
              }
            }

            whereRole = {};

            if (role && role.length > 0) {
              whereRole.tbl_role_id = role;
            } else {
              whereRole.tbl_role_id = req.user.idRole;
            }

            whereMediaProfile = {
              tbl_profile_id: req.user.idProfile,
              state: 1
            };
            requiredDone = false;

            if (history == 1) {
              whereMediaProfile.done = _defineProperty({}, Op.gte, 1);
              requiredDone = true;
            }

            if (favorite == 1) {
              whereMediaProfile.favorite = 1;
              requiredDone = true;
            }

            if (archived) {
              whereMediaProfile.state = 0;
              requiredDone = true;
            }

            if (marked) {
              whereMediaProfile.watch_later = true;
              requiredDone = true;
            }

            _context2.next = 16;
            return tbl_medias.findAll({
              attributes: ['id', ['tbl_category_media_id', 'idCategory'], ['tbl_method_id', 'idMethod'], 'name', 'type', 'file', 'image', 'duration', 'description', 'points', 'state'],
              where: where,
              include: [{
                model: tbl_media_role,
                where: whereRole,
                as: 'Role',
                attributes: [['tbl_role_id', 'id']]
              }, {
                model: tbl_media_profile,
                as: 'onProfile',
                where: whereMediaProfile,
                attributes: ['id', 'done', 'favorite'],
                required: requiredDone
              }]
            });

          case 16:
            medias = _context2.sent;
            res.json({
              data: medias
            });
            _context2.next = 24;
            break;

          case 20:
            _context2.prev = 20;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 24:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 20]]);
  }));
  return _getListMedias.apply(this, arguments);
}

function watchLaterMedia(_x5, _x6) {
  return _watchLaterMedia.apply(this, arguments);
}

function _watchLaterMedia() {
  _watchLaterMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var validRoles, media, _yield$tbl_media_prof, _yield$tbl_media_prof2, media_profile, created;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            validRoles = [1, 2];

            if (validRoles.includes(req.user.idRole)) {
              _context3.next = 4;
              break;
            }

            return _context3.abrupt("return", res.status(400).json({
              message: 'Access denied with provided credentials'
            }));

          case 4:
            _context3.next = 6;
            return tbl_medias.findByPk(req.params.id, {
              attributes: ['id']
            });

          case 6:
            media = _context3.sent;

            if (media) {
              _context3.next = 9;
              break;
            }

            return _context3.abrupt("return", res.status(404).json({
              message: 'Media not found'
            }));

          case 9:
            _context3.next = 11;
            return tbl_media_profile.findOrCreate({
              where: {
                tbl_media_id: req.params.id,
                tbl_profile_id: req.user.idProfile
              },
              defaults: {
                done: 0,
                favorite: 0,
                state: 1,
                watch_later: 1
              }
            });

          case 11:
            _yield$tbl_media_prof = _context3.sent;
            _yield$tbl_media_prof2 = _slicedToArray(_yield$tbl_media_prof, 2);
            media_profile = _yield$tbl_media_prof2[0];
            created = _yield$tbl_media_prof2[1];

            if (created) {
              _context3.next = 18;
              break;
            }

            _context3.next = 18;
            return media_profile.update({
              watch_later: 1
            });

          case 18:
            return _context3.abrupt("return", res.json({
              message: 'Item saved successfully'
            }));

          case 21:
            _context3.prev = 21;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 25:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 21]]);
  }));
  return _watchLaterMedia.apply(this, arguments);
}

function favoriteMedia(_x7, _x8) {
  return _favoriteMedia.apply(this, arguments);
}

function _favoriteMedia() {
  _favoriteMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var validRoles, media, _yield$tbl_media_prof3, _yield$tbl_media_prof4, media_profile, created;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            validRoles = [1, 2];

            if (validRoles.includes(req.user.idRole)) {
              _context4.next = 4;
              break;
            }

            return _context4.abrupt("return", res.status(400).json({
              message: 'Access denied with provided credentials'
            }));

          case 4:
            _context4.next = 6;
            return tbl_medias.findByPk(req.params.id, {
              attributes: ['id']
            });

          case 6:
            media = _context4.sent;

            if (media) {
              _context4.next = 9;
              break;
            }

            return _context4.abrupt("return", res.status(404).json({
              message: 'Media not found'
            }));

          case 9:
            _context4.next = 11;
            return tbl_media_profile.findOrCreate({
              where: {
                tbl_media_id: req.params.id,
                tbl_profile_id: req.user.idProfile
              },
              defaults: {
                done: 0,
                favorite: 1,
                state: 1,
                watch_later: 0
              }
            });

          case 11:
            _yield$tbl_media_prof3 = _context4.sent;
            _yield$tbl_media_prof4 = _slicedToArray(_yield$tbl_media_prof3, 2);
            media_profile = _yield$tbl_media_prof4[0];
            created = _yield$tbl_media_prof4[1];

            if (created) {
              _context4.next = 18;
              break;
            }

            _context4.next = 18;
            return media_profile.update({
              favorite: 1
            });

          case 18:
            return _context4.abrupt("return", res.json({
              message: 'Item saved as favorite successfully'
            }));

          case 21:
            _context4.prev = 21;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 25:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 21]]);
  }));
  return _favoriteMedia.apply(this, arguments);
}

function postFavoriteMedia(_x9, _x10) {
  return _postFavoriteMedia.apply(this, arguments);
}

function _postFavoriteMedia() {
  _postFavoriteMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var IDprofile, id, media_favorite, favorites;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            IDprofile = req.user.idProfile;
            id = req.body.id;
            _context5.next = 5;
            return tbl_media_profile.findOne({
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id
              }
            });

          case 5:
            media_favorite = _context5.sent;

            if (!media_favorite) {
              _context5.next = 12;
              break;
            }

            _context5.next = 9;
            return tbl_media_profile.update({
              favorite: 1
            }, {
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id
              }
            });

          case 9:
            _context5.t0 = _context5.sent;
            _context5.next = 15;
            break;

          case 12:
            _context5.next = 14;
            return tbl_media_profile.create({
              tbl_profile_id: IDprofile,
              tbl_media_id: id,
              favorite: 1
            });

          case 14:
            _context5.t0 = _context5.sent;

          case 15:
            favorites = _context5.t0;
            res.json({
              message: "Se guardó el favorito satisfactoriamente"
            });
            _context5.next = 23;
            break;

          case 19:
            _context5.prev = 19;
            _context5.t1 = _context5["catch"](0);
            console.log(_context5.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 23:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 19]]);
  }));
  return _postFavoriteMedia.apply(this, arguments);
}

function postDesactivateFavoriteMedia(_x11, _x12) {
  return _postDesactivateFavoriteMedia.apply(this, arguments);
}

function _postDesactivateFavoriteMedia() {
  _postDesactivateFavoriteMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var IDprofile, id, media_favorite, favorites;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            IDprofile = req.user.idProfile;
            id = req.body.id;
            _context6.next = 5;
            return tbl_media_profile.findOne({
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id,
                favorite: 1,
                state: 1
              }
            });

          case 5:
            media_favorite = _context6.sent;

            if (!media_favorite) {
              _context6.next = 12;
              break;
            }

            _context6.next = 9;
            return tbl_media_profile.update({
              favorite: 0
            }, {
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id
              }
            });

          case 9:
            _context6.t0 = _context6.sent;
            _context6.next = 15;
            break;

          case 12:
            _context6.next = 14;
            return tbl_media_profile.create({
              tbl_profile_id: IDprofile,
              tbl_media_id: id,
              favorite: 0
            });

          case 14:
            _context6.t0 = _context6.sent;

          case 15:
            favorites = _context6.t0;
            res.json({
              message: "Se desactivó el favorito satisfactoriamente"
            });
            _context6.next = 23;
            break;

          case 19:
            _context6.prev = 19;
            _context6.t1 = _context6["catch"](0);
            console.log(_context6.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 23:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 19]]);
  }));
  return _postDesactivateFavoriteMedia.apply(this, arguments);
}

function ListCategoryMedia(_x13, _x14) {
  return _ListCategoryMedia.apply(this, arguments);
}

function _ListCategoryMedia() {
  _ListCategoryMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var list_category;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            _context7.next = 3;
            return tbl_category_medias.findAll({
              attributes: ['id', 'name'],
              where: {
                state: 1
              }
            });

          case 3:
            list_category = _context7.sent;
            res.json({
              data: list_category
            });
            _context7.next = 11;
            break;

          case 7:
            _context7.prev = 7;
            _context7.t0 = _context7["catch"](0);
            console.log(_context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 7]]);
  }));
  return _ListCategoryMedia.apply(this, arguments);
}

function postDoneMedia(_x15, _x16) {
  return _postDoneMedia.apply(this, arguments);
}

function _postDoneMedia() {
  _postDoneMedia = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var IDprofile, id, done, media_done, insertDone;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            IDprofile = req.user.idProfile;
            id = req.body.id;
            done = req.body.done;
            _context8.next = 6;
            return tbl_media_profile.findOne({
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id,
                state: 1
              }
            });

          case 6:
            media_done = _context8.sent;

            if (!media_done) {
              _context8.next = 13;
              break;
            }

            _context8.next = 10;
            return tbl_media_profile.update({
              done: done
            }, {
              where: {
                tbl_profile_id: IDprofile,
                tbl_media_id: id
              }
            });

          case 10:
            _context8.t0 = _context8.sent;
            _context8.next = 16;
            break;

          case 13:
            _context8.next = 15;
            return tbl_media_profile.create({
              tbl_profile_id: IDprofile,
              tbl_media_id: id,
              done: done
            });

          case 15:
            _context8.t0 = _context8.sent;

          case 16:
            insertDone = _context8.t0;
            res.json({
              message: "Se guardó el done satisfactoriamente"
            });
            _context8.next = 24;
            break;

          case 20:
            _context8.prev = 20;
            _context8.t1 = _context8["catch"](0);
            console.log(_context8.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 24:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 20]]);
  }));
  return _postDoneMedia.apply(this, arguments);
}