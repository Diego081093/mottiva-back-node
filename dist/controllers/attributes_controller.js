"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListAttributes = getListAttributes;
exports.createAlternative = createAlternative;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_attributes = _require.tbl_attributes;

function getListAttributes(_x, _x2) {
  return _getListAttributes.apply(this, arguments);
}

function _getListAttributes() {
  _getListAttributes = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _req$query, section, key, value, type, attributes;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _req$query = req.query, section = _req$query.section, key = _req$query.key, value = _req$query.value, type = _req$query.type;
            _context.next = 4;
            return tbl_attributes.findOne({
              where: {
                section: section,
                type: type
              }
            });

          case 4:
            attributes = _context.sent;
            res.json({
              data: attributes
            });
            _context.next = 12;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 8]]);
  }));
  return _getListAttributes.apply(this, arguments);
}

function createAlternative(_x3, _x4) {
  return _createAlternative.apply(this, arguments);
}

function _createAlternative() {
  _createAlternative = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, alternative, score, question_id, newAlternative;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _req$body = req.body, alternative = _req$body.alternative, score = _req$body.score, question_id = _req$body.question_id;
            _context2.prev = 1;
            _context2.next = 4;
            return tbl_alternative.create({
              alternative: alternative,
              score: score,
              question_id: question_id
            });

          case 4:
            newAlternative = _context2.sent;

            if (newAlternative) {
              res.json({
                message: 'alternative created',
                date: newAlternative
              });
            }

            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 8]]);
  }));
  return _createAlternative.apply(this, arguments);
}