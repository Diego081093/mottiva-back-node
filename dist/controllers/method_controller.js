"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListMethod = getListMethod;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import Role from '../models/role_model'
var _require = require('../models'),
    tbl_method = _require.tbl_method;

var Sequelize = require('sequelize');

function getListMethod(_x, _x2) {
  return _getListMethod.apply(this, arguments);
}

function _getListMethod() {
  _getListMethod = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var list_method;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_method.findAll({
              attributes: ['id', 'name', 'image'],
              where: {
                state: 1
              }
            });

          case 3:
            list_method = _context.sent;
            res.json({
              data: list_method
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListMethod.apply(this, arguments);
}