"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createBlocked = createBlocked;
exports.getListBlocked = getListBlocked;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require("sequelize"),
    Op = _require.Op;

var _require2 = require('../models'),
    tbl_blocked = _require2.tbl_blocked,
    tbl_profile = _require2.tbl_profile,
    tbl_reason = _require2.tbl_reason,
    tbl_session = _require2.tbl_session;

function createBlocked(_x, _x2) {
  return _createBlocked.apply(this, arguments);
}

function _createBlocked() {
  _createBlocked = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var idProfile, idPsychologist, _req$body, reasonId, comment, blocked, newBlocked, updateSession;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            idProfile = req.user.idProfile;
            idPsychologist = req.params.id;
            _req$body = req.body, reasonId = _req$body.reasonId, comment = _req$body.comment;
            _context.prev = 3;
            _context.next = 6;
            return tbl_blocked.findOne({
              where: {
                tbl_profile_psychologist_id: idPsychologist,
                tbl_profile_pacient_id: idProfile,
                state: 1
              }
            });

          case 6:
            blocked = _context.sent;

            if (!(blocked === null)) {
              _context.next = 21;
              break;
            }

            _context.next = 10;
            return tbl_blocked.create({
              tbl_profile_pacient_id: idProfile,
              tbl_profile_psychologist_id: idPsychologist,
              tbl_reason_id: reasonId,
              comment: comment
            });

          case 10:
            newBlocked = _context.sent;

            if (!newBlocked) {
              _context.next = 18;
              break;
            }

            _context.next = 14;
            return tbl_session.update({
              state: 0
            }, {
              where: {
                tbl_profile_psychologist_id: idPsychologist,
                tbl_profile_pacient_id: idProfile
              }
            });

          case 14:
            updateSession = _context.sent;
            if (updateSession) res.status(200).json({
              message: 'Psychologist blocked',
              data: newBlocked
            });else res.status(500).json({
              message: 'Something went wrong'
            });
            _context.next = 19;
            break;

          case 18:
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 19:
            _context.next = 24;
            break;

          case 21:
            _context.next = 23;
            return tbl_session.update({
              state: 0
            }, {
              where: {
                tbl_profile_psychologist_id: idPsychologist,
                tbl_profile_pacient_id: idProfile
              }
            });

          case 23:
            res.status(200).json({
              message: 'User already blocked'
            });

          case 24:
            _context.next = 30;
            break;

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](3);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 30:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[3, 26]]);
  }));
  return _createBlocked.apply(this, arguments);
}

function getListBlocked(_x3, _x4) {
  return _getListBlocked.apply(this, arguments);
}

function _getListBlocked() {
  _getListBlocked = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var listBlocked;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return tbl_blocked.findAll({
              include: [{
                model: tbl_profile,
                as: 'Psychologist',
                attributes: ['id', 'lastname', 'firstname']
              }, {
                model: tbl_profile,
                as: 'Patient',
                attributes: ['id', 'lastname', 'firstname']
              }, {
                model: tbl_reason,
                as: 'Reason'
              }]
            });

          case 3:
            listBlocked = _context2.sent;
            res.json({
              message: 'Blocked Found',
              data: listBlocked
            });
            _context2.next = 11;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 7]]);
  }));
  return _getListBlocked.apply(this, arguments);
}