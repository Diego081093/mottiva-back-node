"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListCompanyBenefits = getListCompanyBenefits;
exports.getListBenefits = getListBenefits;
exports.createBenefit = createBenefit;
exports.exchangeBenefits = exchangeBenefits;
exports.getBenefit = getBenefit;
exports.getBenefitsProfile = getBenefitsProfile;
exports.getHistoryBenefits = getHistoryBenefits;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_benefit = _require.tbl_benefit,
    tbl_company_benefit = _require.tbl_company_benefit,
    tbl_profile = _require.tbl_profile,
    tbl_benefit_profile = _require.tbl_benefit_profile;

var _require2 = require('luxon'),
    DateTime = _require2.DateTime;

var Sequelize = require('sequelize');

var url = require('url');

var _require3 = require('./../helpers'),
    isValidBase64Img = _require3.isValidBase64Img,
    s3UploadBase64Image = _require3.s3UploadBase64Image,
    MottivaConstants = _require3.MottivaConstants;

var Op = Sequelize.Op;

function getListCompanyBenefits(_x, _x2) {
  return _getListCompanyBenefits.apply(this, arguments);
}

function _getListCompanyBenefits() {
  _getListCompanyBenefits = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var companys;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_company_benefit.findAll();

          case 3:
            companys = _context.sent;
            res.json({
              data: companys
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListCompanyBenefits.apply(this, arguments);
}

function getListBenefits(_x3, _x4) {
  return _getListBenefits.apply(this, arguments);
}

function _getListBenefits() {
  _getListBenefits = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var where, limit, offset, benefits, _yield$tbl_benefit$fi, count, sessions, urlPath, previousPageUrl, nextPageUrl, p_query_offset;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            where = {};

            if (req.query.search) {
              where.description = _defineProperty({}, Op.like, '%' + req.query.search + '%');
            }

            limit = 20, offset = 0;

            if (req.query.limit) {
              limit = parseInt(req.query.limit);
            }

            if (req.query.offset) {
              offset = parseInt(req.query.offset);
            }

            _context2.next = 8;
            return tbl_benefit.findAll({
              where: where,
              attributes: ['id', 'name', 'image', 'description', 'point'],
              include: [{
                model: tbl_company_benefit,
                as: 'CompanyBenefit',
                attributes: ['id', 'name', 'image']
              }]
            });

          case 8:
            benefits = _context2.sent;
            _context2.next = 11;
            return tbl_benefit.findAndCountAll({
              where: where,
              attributes: ['id', 'name', 'image', 'description', 'point'],
              include: [{
                model: tbl_company_benefit,
                as: 'CompanyBenefit',
                attributes: ['id', 'name', 'image']
              }],
              limit: limit,
              offset: offset,
              distinct: true
            });

          case 11:
            _yield$tbl_benefit$fi = _context2.sent;
            count = _yield$tbl_benefit$fi.count;
            sessions = _yield$tbl_benefit$fi.rows;
            console.log(count);
            urlPath = req.baseUrl + req.path;

            if (urlPath.substring(urlPath.length - 1) == '/') {
              urlPath = urlPath.substring(0, urlPath.length - 1);
            }

            previousPageUrl = null, nextPageUrl = null;

            if (offset > 0) {
              p_query_offset = offset - limit;

              if (offset - limit < 0) {
                p_query_offset = 0;
              }

              previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: p_query_offset,
                  limit: limit
                }
              });
            }

            if (offset + limit < count) {
              nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: offset + limit,
                  limit: limit
                }
              });
            }

            res.json({
              data: {
                count: count,
                previous: previousPageUrl,
                next: nextPageUrl,
                limit: limit,
                offset: offset,
                benefits: benefits
              }
            });
            _context2.next = 27;
            break;

          case 23:
            _context2.prev = 23;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 27:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 23]]);
  }));
  return _getListBenefits.apply(this, arguments);
}

function createBenefit(_x5, _x6) {
  return _createBenefit.apply(this, arguments);
}

function _createBenefit() {
  _createBenefit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, company_id, name, description, point, _isValidBase64Img, isValid, buffer, ext, imageUploaded;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _req$body = req.body, company_id = _req$body.company_id, name = _req$body.name, description = _req$body.description, point = _req$body.point;
            _isValidBase64Img = isValidBase64Img(req.body.image), isValid = _isValidBase64Img.isValid, buffer = _isValidBase64Img.buffer, ext = _isValidBase64Img.ext;

            if (isValid) {
              _context3.next = 5;
              break;
            }

            return _context3.abrupt("return", res.status(422).json({
              message: 'The given data was invalid.',
              errors: {
                image: ["The image must be a valid base64"]
              }
            }));

          case 5:
            req.body.image = {
              buffer: buffer,
              ext: ext
            };
            _context3.next = 8;
            return s3UploadBase64Image('uploads/profile-photo/', req.body.image);

          case 8:
            imageUploaded = _context3.sent;
            console.log('');
            console.log('');
            console.log(imageUploaded);
            console.log('');
            console.log('');

            if (imageUploaded.success) {
              _context3.next = 16;
              break;
            }

            return _context3.abrupt("return", res.status(500).json({
              message: 'Something went wrong uploading the image'
            }));

          case 16:
            _context3.next = 18;
            return tbl_benefit.create({
              tbl_company_benefit_id: company_id,
              name: name,
              image: imageUploaded.data.Location,
              description: description,
              point: point
            });

          case 18:
            res.json({
              message: 'Benefit created'
            });
            _context3.next = 25;
            break;

          case 21:
            _context3.prev = 21;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 25:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 21]]);
  }));
  return _createBenefit.apply(this, arguments);
}

function exchangeBenefits(_x7, _x8) {
  return _exchangeBenefits.apply(this, arguments);
}

function _exchangeBenefits() {
  _exchangeBenefits = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;

            if (!req.body.benefit_id) {
              _context4.next = 5;
              break;
            }

            _context4.next = 4;
            return tbl_benefit_profile.create({
              tbl_benefit_id: req.body.benefit_id,
              tbl_profile_id: req.user.idProfile
            });

          case 4:
            return _context4.abrupt("return", res.json({
              message: 'Benefit exchanged'
            }));

          case 5:
            res.status(400).json({
              message: 'Empty data'
            });
            _context4.next = 12;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));
  return _exchangeBenefits.apply(this, arguments);
}

function getBenefit(_x9, _x10) {
  return _getBenefit.apply(this, arguments);
}

function _getBenefit() {
  _getBenefit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var id, benefit;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            id = req.params.id;
            _context5.next = 4;
            return tbl_benefit.findOne({
              where: {
                state: 1,
                id: id
              },
              attributes: ['id', 'name', 'image', 'description', 'point'],
              include: [{
                model: tbl_company_benefit,
                as: 'CompanyBenefit',
                attributes: ['id', 'name', 'image']
              }]
            });

          case 4:
            benefit = _context5.sent;
            res.json({
              data: benefit
            });
            _context5.next = 12;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 8]]);
  }));
  return _getBenefit.apply(this, arguments);
}

function getBenefitsProfile(_x11, _x12) {
  return _getBenefitsProfile.apply(this, arguments);
}

function _getBenefitsProfile() {
  _getBenefitsProfile = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var benefits;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;

            if (!(req.user.idRole == MottivaConstants.HELPDESK_ROLE)) {
              _context6.next = 6;
              break;
            }

            _context6.next = 4;
            return tbl_benefit_profile.findAll({
              include: [{
                model: tbl_profile,
                as: 'Profile',
                attributes: ['firstname', 'lastname', 'phone', 'points']
              }, {
                model: tbl_benefit,
                as: 'Benefit',
                include: {
                  model: tbl_company_benefit,
                  as: 'CompanyBenefit',
                  attributes: ['name', 'image', 'state']
                },
                attributes: ['name', 'image', 'description', 'point', 'state']
              }],
              attributes: ['id', 'state']
            });

          case 4:
            benefits = _context6.sent;
            return _context6.abrupt("return", res.json({
              message: 'Benefits profile found',
              data: benefits
            }));

          case 6:
            res.status(401).json({
              message: 'Access denied with provided credentials'
            });
            _context6.next = 13;
            break;

          case 9:
            _context6.prev = 9;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 9]]);
  }));
  return _getBenefitsProfile.apply(this, arguments);
}

function getHistoryBenefits(_x13, _x14) {
  return _getHistoryBenefits.apply(this, arguments);
}

function _getHistoryBenefits() {
  _getHistoryBenefits = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var idProfile, now, benefit;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            idProfile = req.user.idProfile;
            now = DateTime.now();
            _context7.next = 5;
            return tbl_benefit.findAll({
              where: {
                state: 1,
                created_at: _defineProperty({}, Op.lt, now.toString())
              },
              attributes: ['id', 'name', 'image', 'description', 'point'],
              include: [{
                model: tbl_company_benefit,
                as: 'CompanyBenefit',
                attributes: ['id', 'name', 'image']
              }, {
                model: tbl_benefit_profile,
                as: 'BenefitProfile',
                where: {
                  state: 1,
                  tbl_profile_id: idProfile
                },
                attributes: ['id']
              }]
            });

          case 5:
            benefit = _context7.sent;
            res.json({
              data: benefit
            });
            _context7.next = 13;
            break;

          case 9:
            _context7.prev = 9;
            _context7.t0 = _context7["catch"](0);
            console.log(_context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 9]]);
  }));
  return _getHistoryBenefits.apply(this, arguments);
}