"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createToken = createToken;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_profile = _require.tbl_profile,
    tbl_user = _require.tbl_user,
    tbl_avatar = _require.tbl_avatar;

function createToken(_x, _x2, _x3, _x4) {
  return _createToken.apply(this, arguments);
}

function _createToken() {
  _createToken = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, usuarioID, username) {
    var profile, avatar, list, body, token;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_profile.findOne({
              where: {
                tbl_user_id: usuarioID,
                state: 1
              },
              include: [{
                model: tbl_avatar,
                as: 'Avatar',
                attributes: ['id', 'image']
              }, {
                model: tbl_user,
                as: 'User',
                attributes: ['id', 'username', 'tutorial']
              }]
            });

          case 3:
            profile = _context.sent;
            _context.next = 6;
            return tbl_avatar.findOne({
              where: {
                id: profile.tbl_avatar_id,
                state: 1
              }
            });

          case 6:
            avatar = _context.sent;
            list = [];

            if (!(profile.tbl_role_id === 1)) {
              _context.next = 12;
              break;
            }

            _context.next = 11;
            return tbl_profile.findAll({
              attributes: ['id', ['firstname', 'name']],
              include: [{
                model: tbl_avatar,
                as: 'Avatar',
                attributes: ['id', 'image']
              }, {
                model: tbl_user,
                as: 'User',
                attributes: ['id', 'username']
              }],
              where: {
                phone: profile.phone,
                state: 1
              }
            });

          case 11:
            list = _context.sent;

          case 12:
            body = {
              id: usuarioID,
              idRole: profile.tbl_role_id,
              idProfile: profile.id,
              username: username,
              slug: profile.slug,
              firstname: profile.firstname,
              lastname: profile.lastname,
              dni: profile.dni,
              email: profile.email,
              phone: profile.phone,
              gender: profile.gender,
              birthday: profile.birthday,
              points: profile.points,
              avatar: avatar.image,
              tutorial: profile.User.tutorial,

              /*civilState: profile.civil_state,
              ocupation: profile.ocupation,
              studyNivel: profile.study_nivel,*/
              profiles: list //id_role: user.role_id

            };
            _context.next = 15;
            return _jsonwebtoken["default"].sign({
              user: body
            }, 'globoazul_token');

          case 15:
            token = _context.sent;
            return _context.abrupt("return", token);

          case 19:
            _context.prev = 19;
            _context.t0 = _context["catch"](0);
            res.status(500).json({
              message: 'Something went error'
            });

          case 22:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 19]]);
  }));
  return _createToken.apply(this, arguments);
}