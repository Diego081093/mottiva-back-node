'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListSchedule = getListSchedule;
exports.getListHistorySchedule = getListHistorySchedule;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_task = _require.tbl_task,
    tbl_task_types = _require.tbl_task_types,
    tbl_session_task = _require.tbl_session_task,
    tbl_session = _require.tbl_session,
    tbl_room_participants = _require.tbl_room_participants,
    tbl_profile = _require.tbl_profile,
    tbl_profile_attribute = _require.tbl_profile_attribute,
    tbl_avatar = _require.tbl_avatar,
    tbl_profile_specialty = _require.tbl_profile_specialty,
    tbl_specialty = _require.tbl_specialty,
    tbl_room = _require.tbl_room,
    tbl_chat = _require.tbl_chat,
    tbl_chat_participants = _require.tbl_chat_participants,
    tbl_notification = _require.tbl_notification;

var _require2 = require('luxon'),
    DateTime = _require2.DateTime;

var _require3 = require('sequelize'),
    Op = _require3.Op;

function getListSchedule(_x, _x2) {
  return _getListSchedule.apply(this, arguments);
}

function _getListSchedule() {
  _getListSchedule = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var now, date, idProfile, lastSession, tasks, nextSession, specialties, rowAttributes, attributes, startRoom, nextRooms, data, countNotification, schedule;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            now = DateTime.fromISO(DateTime.now(), {
              locale: 'es-ES',
              zone: 'UTC'
            });
            date = now.plus({
              hour: -5
            }).toString();
            idProfile = req.user.idProfile;
            _context2.next = 6;
            return tbl_session.findOne({
              attributes: ['id', 'date', 'week'],
              where: {
                state: 1,
                date: _defineProperty({}, Op.lt, date)
              },
              include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [['id', 'idProfilePatient']],
                where: {
                  state: 1,
                  id: idProfile
                }
              },
              order: [['date', 'DESC']]
            });

          case 6:
            lastSession = _context2.sent;
            tasks = [];

            if (!(lastSession !== null)) {
              _context2.next = 12;
              break;
            }

            _context2.next = 11;
            return tbl_session_task.findAll({
              where: {
                state: 1
              },
              include: [{
                model: tbl_session,
                as: 'Session',
                attributes: ['id', 'date', 'week'],
                where: {
                  state: 1,
                  id: lastSession.dataValues.id
                }
              }, {
                model: tbl_task,
                as: 'Task',
                attributes: ['id', 'week', 'name', 'description', ['tbl_method_id', 'idMethod']],
                where: {
                  state: 1,
                  week: lastSession.dataValues.week
                },
                include: {
                  model: tbl_task_types,
                  as: 'TaskTypes',
                  where: {
                    state: 1
                  },
                  attributes: ['id', 'name', 'alias']
                }
              }],
              attributes: ['id', 'expiration']
            });

          case 11:
            tasks = _context2.sent;

          case 12:
            _context2.next = 14;
            return tbl_session.findOne({
              attributes: ['id', 'date'],
              // where: { state: 1, patient_state: 1, date:{[Op.gte]: date}},
              where: {
                state: 1,
                patient_state: 1
              },
              include: [{
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [],
                where: {
                  state: 1,
                  id: idProfile
                }
              }, {
                model: tbl_profile,
                as: 'ProfilePsychologist',
                attributes: ['id', 'firstname', 'lastname', 'birthday'],
                where: {
                  state: 1
                },
                include: [{
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image'],
                  required: false
                }]
              }],
              order: [['id', 'DESC']]
            });

          case 14:
            nextSession = _context2.sent;

            if (!(nextSession !== null)) {
              _context2.next = 26;
              break;
            }

            _context2.next = 18;
            return tbl_specialty.findAll({
              where: {
                state: 1
              },
              attributes: ['id', 'name'],
              include: {
                model: tbl_profile_specialty,
                as: 'ProfileSpecialty',
                attributes: [],
                where: {
                  state: 1,
                  tbl_profile_id: nextSession.dataValues.ProfilePsychologist.dataValues.id
                }
              }
            });

          case 18:
            specialties = _context2.sent;
            nextSession.dataValues.ProfilePsychologist.dataValues.Specialties = specialties;
            _context2.next = 22;
            return tbl_profile_attribute.findAll({
              where: {
                state: 1,
                tbl_profile_id: nextSession.ProfilePsychologist.id
              },
              attributes: ['id', 'key', 'value']
            });

          case 22:
            rowAttributes = _context2.sent;
            attributes = {};
            rowAttributes.map(function (attribute) {
              attributes[attribute.key] = attribute.value;
            });
            nextSession.dataValues.ProfilePsychologist.dataValues.ProfileAttribute = attributes; // Details of Psychologist

          case 26:
            startRoom = now.plus({
              hour: -5,
              minutes: -50
            }).toString();
            _context2.next = 29;
            return tbl_room.findAll({
              where: {
                state: 1,
                start: _defineProperty({}, Op.gt, startRoom)
              },
              include: {
                model: tbl_chat,
                as: 'Chat',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_chat_participants,
                  as: 'ChatParticipant',
                  where: {
                    state: 1,
                    tbl_profile_id: idProfile
                  },
                  attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                  required: true
                }
              },
              attributes: ['id', 'title', 'start', 'end', 'rules', 'participants', 'image', 'maxParticipants', 'inscription', 'image', 'twilio_uniquename', 'created_at'],
              order: [['start', 'ASC']]
            });

          case 29:
            nextRooms = _context2.sent;
            _context2.next = 32;
            return Promise.all(nextRooms.map( /*#__PURE__*/function () {
              var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(item, key) {
                var n_participants;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return tbl_room_participants.count({
                          where: {
                            tbl_room_id: item.id
                          },
                          include: [{
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {
                              tbl_role_id: 1
                            },
                            attributes: [],
                            required: true
                          }]
                        });

                      case 2:
                        n_participants = _context.sent;
                        item.setDataValue('participants', n_participants);
                        return _context.abrupt("return", item);

                      case 5:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x5, _x6) {
                return _ref.apply(this, arguments);
              };
            }()));

          case 32:
            data = _context2.sent;
            _context2.next = 35;
            return tbl_notification.count({
              where: {
                tbl_profile_id: req.user.idProfile,
                done: 0
              }
            });

          case 35:
            countNotification = _context2.sent;
            schedule = {
              data: {
                tasks: tasks,
                nextSession: nextSession,
                nextRooms: nextRooms,
                countNotification: countNotification
              }
            };
            res.json(schedule);
            _context2.next = 44;
            break;

          case 40:
            _context2.prev = 40;
            _context2.t0 = _context2["catch"](0);
            console.error('You have a error in getListSchedule:', _context2.t0);
            res.status(500).json({
              non_field_errors: 'Something went wrong'
            });

          case 44:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 40]]);
  }));
  return _getListSchedule.apply(this, arguments);
}

function getListHistorySchedule(_x3, _x4) {
  return _getListHistorySchedule.apply(this, arguments);
}

function _getListHistorySchedule() {
  _getListHistorySchedule = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var now, date, idProfile, _req$query, from, to, psychologist, where, wherePsychologist, fromDate, toDate, toDateNew, toNew, toDateFinal, prevSessions, prevRooms, schedule;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            now = DateTime.now();
            date = now.toString();
            idProfile = req.user.idProfile;
            _req$query = req.query, from = _req$query.from, to = _req$query.to, psychologist = _req$query.psychologist;
            where = {
              state: 1,
              patient_state: 1
            };
            wherePsychologist = {
              state: 1
            };

            if (psychologist) {
              wherePsychologist = _defineProperty({}, Op.or, [{
                firstname: _defineProperty({}, Op.like, '%' + psychologist + '%')
              }, {
                lastname: _defineProperty({}, Op.like, '%' + psychologist + '%')
              }]); // wherePsychologist.lastname = { [Op.like]: '%' + psychologist + '%' }
            }

            if (typeof from !== 'undefined' && typeof to !== 'undefined') {
              fromDate = DateTime.fromSQL(from).toString();
              toDate = DateTime.fromSQL(to).toString();
              toDateNew = new Date(toDate);
              toNew = toDateNew.setDate(toDateNew.getDate() + 1);
              toDateFinal = new Date(toNew);
              where.date = _defineProperty({}, Op.between, [fromDate, toDateFinal]);
            } else {
              where.date = _defineProperty({}, Op.lt, date);
            }

            _context3.next = 11;
            return tbl_session.findAll({
              attributes: ['date', 'week'],
              where: where,
              include: [{
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [],
                where: {
                  state: 1,
                  id: idProfile
                }
              }, {
                model: tbl_profile,
                as: 'ProfilePsychologist',
                attributes: ['id', 'firstname', 'lastname', 'birthday'],
                where: wherePsychologist,
                include: [{
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image']
                }, {
                  model: tbl_profile_attribute,
                  as: 'ProfileAttribute',
                  attributes: [['value', 'shortDescription']],
                  where: {
                    state: 1,
                    key: 'short_description'
                  }
                }]
              }],
              order: [['date', 'ASC']]
            });

          case 11:
            prevSessions = _context3.sent;
            _context3.next = 14;
            return tbl_room.findAll({
              where: {
                state: 1,
                end: _defineProperty({}, Op.lt, date)
              },
              include: {
                model: tbl_chat,
                as: 'Chat',
                where: {
                  state: 1
                },
                attributes: [],
                include: {
                  model: tbl_chat_participants,
                  as: 'ChatParticipant',
                  where: {
                    state: 1,
                    tbl_profile_id: idProfile
                  },
                  attributes: []
                }
              },
              attributes: ['id', 'title', 'start', 'end', 'rules', 'participants', 'image']
            });

          case 14:
            prevRooms = _context3.sent;
            schedule = {
              data: {
                prevSessions: prevSessions,
                prevRooms: prevRooms
              }
            };
            res.json(schedule);
            _context3.next = 23;
            break;

          case 19:
            _context3.prev = 19;
            _context3.t0 = _context3["catch"](0);
            console.error('You have a error in getListHistorySchedule:', _context3.t0);
            res.status(500).json({
              non_field_errors: 'Something went wrong'
            });

          case 23:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 19]]);
  }));
  return _getListHistorySchedule.apply(this, arguments);
}