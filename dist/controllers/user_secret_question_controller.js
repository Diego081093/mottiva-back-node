"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUserSecretQuestion = getUserSecretQuestion;
exports.createUserSecretQuestion = createUserSecretQuestion;
exports.validateUserSecretQuestion = validateUserSecretQuestion;
exports.updateUserSecretAnswer = updateUserSecretAnswer;
exports.deleteUserSecretAnswer = deleteUserSecretAnswer;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_user_secret_question = _require.tbl_user_secret_question;

function getUserSecretQuestion(_x, _x2) {
  return _getUserSecretQuestion.apply(this, arguments);
}

function _getUserSecretQuestion() {
  _getUserSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var answer;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_user_secret_question.findAll();

          case 3:
            answer = _context.sent;
            res.json({
              data: answer
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getUserSecretQuestion.apply(this, arguments);
}

function createUserSecretQuestion(_x3, _x4) {
  return _createUserSecretQuestion.apply(this, arguments);
}

function _createUserSecretQuestion() {
  _createUserSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, user_id, secret_question_id, answer, state, newUserSecretAnswer;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _req$body = req.body, user_id = _req$body.user_id, secret_question_id = _req$body.secret_question_id, answer = _req$body.answer, state = _req$body.state;
            _context2.prev = 1;
            _context2.next = 4;
            return tbl_user_secret_question.create({
              user_id: user_id,
              secret_question_id: secret_question_id,
              answer: answer,
              state: state
            });

          case 4:
            newUserSecretAnswer = _context2.sent;

            if (newUserSecretAnswer) {
              res.json({
                message: 'User secret answer created',
                date: newUserSecretAnswer
              });
            }

            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 8]]);
  }));
  return _createUserSecretQuestion.apply(this, arguments);
}

function validateUserSecretQuestion(_x5, _x6) {
  return _validateUserSecretQuestion.apply(this, arguments);
}

function _validateUserSecretQuestion() {
  _validateUserSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var validated;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return tbl_user_secret_question.findOne({
              where: {
                tbl_user_id: req.user.id,
                answer: req.body.answer,
                state: 1
              }
            });

          case 3:
            validated = _context3.sent;

            if (!validated) {
              _context3.next = 6;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'User secret question validated'
            }));

          case 6:
            return _context3.abrupt("return", res.status(400).json({
              message: 'Incorrect answer'
            }));

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _validateUserSecretQuestion.apply(this, arguments);
}

function updateUserSecretAnswer(_x7, _x8) {
  return _updateUserSecretAnswer.apply(this, arguments);
}

function _updateUserSecretAnswer() {
  _updateUserSecretAnswer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var answer, userSecretAnswer;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            answer = req.body.answer;
            _context4.prev = 1;
            _context4.next = 4;
            return tbl_user_secret_question.findByPk(req.params.id);

          case 4:
            userSecretAnswer = _context4.sent;

            if (!userSecretAnswer) {
              _context4.next = 9;
              break;
            }

            _context4.next = 8;
            return userSecretAnswer.update({
              answer: answer
            });

          case 8:
            return _context4.abrupt("return", res.json({
              message: 'User secret answer updated',
              data: userSecretAnswer
            }));

          case 9:
            return _context4.abrupt("return", res.status(404).json({
              message: 'User secret answer not found'
            }));

          case 12:
            _context4.prev = 12;
            _context4.t0 = _context4["catch"](1);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[1, 12]]);
  }));
  return _updateUserSecretAnswer.apply(this, arguments);
}

function deleteUserSecretAnswer(_x9, _x10) {
  return _deleteUserSecretAnswer.apply(this, arguments);
}

function _deleteUserSecretAnswer() {
  _deleteUserSecretAnswer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var deleteRowCount;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return tbl_user_secret_question.destroy({
              where: {
                id: req.params.id
              }
            });

          case 3:
            deleteRowCount = _context5.sent;
            res.json({
              message: 'User secret question deleted successfully',
              count: deleteRowCount
            });
            _context5.next = 11;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 7]]);
  }));
  return _deleteUserSecretAnswer.apply(this, arguments);
}