"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListSession = getListSession;
exports.getSessions = getSessions;
exports.getSessionsPsychologist = getSessionsPsychologist;
exports.getSessionsHelpdesk = getSessionsHelpdesk;
exports.getSession = getSession;
exports.getNotesList = getNotesList;
exports.disableNote = disableNote;
exports.getPatientsSession = getPatientsSession;
exports.postSession = postSession;
exports.postNextSession = postNextSession;
exports.addSessionNote = addSessionNote;
exports.completeSession = completeSession;
exports.closedSession = closedSession;
exports.rescheduleSession = rescheduleSession;
exports.rescheduleReplaceSession = rescheduleReplaceSession;
exports.cancelSession = cancelSession;
exports.getSessionDetails = getSessionDetails;
exports.getSessionCases = getSessionCases;
exports.getListSessionCases = getListSessionCases;
exports.getSessionCaseDetails = getSessionCaseDetails;
exports.assignSession = assignSession;
exports.postCaseSession = postCaseSession;

var _sequelize = require("sequelize");

var _notification_controller = require("../controllers/notification_controller");

var _token_controller = require("./token_controller");

var _room_controller = require("./room_controller");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_user = _require.tbl_user,
    tbl_session = _require.tbl_session,
    tbl_profile = _require.tbl_profile,
    tbl_profile_attribute = _require.tbl_profile_attribute,
    tbl_session_attribute = _require.tbl_session_attribute,
    tbl_avatar = _require.tbl_avatar,
    tbl_session_note = _require.tbl_session_note,
    tbl_room = _require.tbl_room,
    tbl_room_note = _require.tbl_room_note,
    tbl_chat_participants = _require.tbl_chat_participants,
    tbl_session_cases = _require.tbl_session_cases,
    tbl_chat = _require.tbl_chat,
    tbl_room_participants = _require.tbl_room_participants,
    tbl_session_task = _require.tbl_session_task,
    tbl_blocked = _require.tbl_blocked;

var _require2 = require('./../helpers'),
    MottivaConstants = _require2.MottivaConstants;

var _require3 = require('sequelize'),
    Op = _require3.Op,
    json = _require3.json,
    Sequelize = _require3.Sequelize;

var _require4 = require('luxon'),
    DateTime = _require4.DateTime;

var url = require('url');

function getListSession(_x, _x2) {
  return _getListSession.apply(this, arguments);
}

function _getListSession() {
  _getListSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var sessions;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_session.findAll();

          case 3:
            sessions = _context.sent;
            res.json({
              sessions: sessions
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getListSession.apply(this, arguments);
}

function getSessions(_x3, _x4) {
  return _getSessions.apply(this, arguments);
}

function _getSessions() {
  _getSessions = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!(req.user.idRole == 2)) {
              _context2.next = 4;
              break;
            }

            getSessionsPsychologist(req, res);
            _context2.next = 9;
            break;

          case 4:
            if (!(req.user.idRole == 3)) {
              _context2.next = 8;
              break;
            }

            getSessionsHelpdesk(req, res);
            _context2.next = 9;
            break;

          case 8:
            return _context2.abrupt("return", res.status(401).send('Unauthorized'));

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getSessions.apply(this, arguments);
}

function getSessionsPsychologist(_x5, _x6) {
  return _getSessionsPsychologist.apply(this, arguments);
}

function _getSessionsPsychologist() {
  _getSessionsPsychologist = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _date, _date2, _date3, _date4, _start, _start2, _start3, _start4;

    var to, offset, limit, schedule, search, whereQuery, searchWhereQuery, data, now, weekday, _yield$tbl_session$fi, count, sessions, morning, afternoon, urlPath, previousPageUrl, nextPageUrl, p_query_offset, whereQueryRoom, _weekday, rooms;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            to = req.query.to, offset = 0, limit = 20, schedule = req.query.schedule, search = req.query.search, searchWhereQuery = {}, data = [];

            if (req.query.offset) {
              offset = parseInt(req.query.offset);
            }

            if (req.query.limit) {
              limit = parseInt(req.query.limit);
            } // Today, 00:00:00
            // const now = DateTime.fromObject({day: DateTime.now().day})


            now = (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS');
            _context4.t0 = to;
            _context4.next = _context4.t0 === 'today' ? 8 : _context4.t0 === 'week' ? 10 : _context4.t0 === 'month' ? 13 : _context4.t0 === 'year' ? 15 : 17;
            break;

          case 8:
            whereQuery = {
              date: (_date = {}, _defineProperty(_date, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-DD 00:00:00')), _defineProperty(_date, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'days').format('YYYY-MM-DD 00:00:00')), _date),
              state: 1
            };
            return _context4.abrupt("break", 19);

          case 10:
            weekday = (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('d');
            whereQuery = {
              date: (_date2 = {}, _defineProperty(_date2, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1 - weekday, 'days').format('YYYY-MM-DD 00:00:00')), _defineProperty(_date2, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(3, 'days').format('YYYY-MM-DD 00:00:00')), _date2),
              state: 1
            };
            return _context4.abrupt("break", 19);

          case 13:
            whereQuery = {
              date: (_date3 = {}, _defineProperty(_date3, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-01 00:00:00')), _defineProperty(_date3, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'months').format('YYYY-MM-01 00:00:00')), _date3),
              state: 1
            };
            return _context4.abrupt("break", 19);

          case 15:
            whereQuery = {
              date: (_date4 = {}, _defineProperty(_date4, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-01-01 00:00:00')), _defineProperty(_date4, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'years').format('YYYY-01-01 00:00:00')), _date4),
              state: 1
            };
            return _context4.abrupt("break", 19);

          case 17:
            whereQuery = {
              state: 1
            };
            return _context4.abrupt("break", 19);

          case 19:
            console.log(whereQuery);

            if (search) {
              searchWhereQuery = _defineProperty({}, Op.or, [{
                firstname: _defineProperty({}, Op.iLike, '%' + search + '%')
              }, {
                lastname: _defineProperty({}, Op.iLike, '%' + search + '%')
              }]);
            }

            _context4.next = 23;
            return tbl_session.findAndCountAll({
              where: whereQuery,
              include: [{
                model: tbl_profile,
                as: 'Psychologist',
                where: {
                  tbl_user_id: req.user.id,
                  state: 1
                },
                attributes: []
              }, {
                model: tbl_profile,
                as: 'Patient',
                where: searchWhereQuery,
                attributes: ['firstname', 'lastname' //'slug'
                ],
                include: {
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image']
                }
              }],
              attributes: ['id', ['tbl_profile_pacient_id', 'patient_id'], ['tbl_profile_psychologist_id', 'psychologist_id'], 'date', 'state', 'order', 'week'],
              limit: limit,
              offset: offset,
              distinct: true
            });

          case 23:
            _yield$tbl_session$fi = _context4.sent;
            count = _yield$tbl_session$fi.count;
            sessions = _yield$tbl_session$fi.rows;
            // if( sessions.length == 0 ){
            //     return res.status(404).json({
            //         message: 'No sessions were found'
            //     })
            // }
            morning = [], afternoon = [];
            _context4.next = 29;
            return Promise.all(sessions.map(function (session) {
              return new Promise( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(resolve) {
                  var total, sessionDate;
                  return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.next = 2;
                          return tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: session.dataValues.patient_id,
                              tbl_profile_psychologist_id: session.dataValues.psychologist_id
                            }
                          });

                        case 2:
                          total = _context3.sent;
                          session.setDataValue('total', total);
                          data.push(session);

                          if (schedule) {
                            sessionDate = DateTime.fromJSDate(session.date);

                            if (sessionDate.hour < 12) {
                              morning.push(session);
                            } else {
                              afternoon.push(session);
                            }
                          }

                          resolve(1);

                        case 7:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function (_x45) {
                  return _ref.apply(this, arguments);
                };
              }());
            }));

          case 29:
            urlPath = req.baseUrl + req.path;

            if (urlPath.substring(urlPath.length - 1) == '/') {
              urlPath = urlPath.substring(0, urlPath.length - 1);
            }

            previousPageUrl = null, nextPageUrl = null;

            if (offset > 0) {
              p_query_offset = offset - limit;

              if (offset - limit < 0) {
                p_query_offset = 0;
              }

              previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: p_query_offset,
                  limit: limit
                }
              });
            }

            if (offset + limit < count) {
              nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: offset + limit,
                  limit: limit
                }
              });
            }

            whereQueryRoom = {};
            _context4.t1 = to;
            _context4.next = _context4.t1 === 'today' ? 38 : _context4.t1 === 'week' ? 40 : _context4.t1 === 'month' ? 43 : _context4.t1 === 'year' ? 45 : 47;
            break;

          case 38:
            whereQueryRoom = {
              start: (_start = {}, _defineProperty(_start, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-DD 00:00:00')), _defineProperty(_start, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'days').format('YYYY-MM-DD 00:00:00')), _start),
              state: 1
            };
            return _context4.abrupt("break", 49);

          case 40:
            _weekday = now.weekday;
            whereQueryRoom = {
              start: (_start2 = {}, _defineProperty(_start2, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1 - _weekday, 'days').format('YYYY-MM-DD 00:00:00')), _defineProperty(_start2, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(3, 'days').format('YYYY-MM-DD 00:00:00')), _start2),
              state: 1
            };
            return _context4.abrupt("break", 49);

          case 43:
            whereQueryRoom = {
              start: (_start3 = {}, _defineProperty(_start3, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-01 00:00:00')), _defineProperty(_start3, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'months').format('YYYY-MM-01 00:00:00')), _start3),
              state: 1
            };
            return _context4.abrupt("break", 49);

          case 45:
            whereQueryRoom = {
              start: (_start4 = {}, _defineProperty(_start4, Op.gte, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-01-01 00:00:00')), _defineProperty(_start4, Op.lt, (0, _momentTimezone["default"])(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'years').format('YYYY-01-01 00:00:00')), _start4),
              state: 1
            };
            return _context4.abrupt("break", 49);

          case 47:
            whereQueryRoom = {
              state: 1
            };
            return _context4.abrupt("break", 49);

          case 49:
            _context4.next = 51;
            return tbl_room.findAll({
              where: whereQueryRoom,
              attributes: ['id', 'title', 'image', ['start', 'date']],
              include: [{
                model: tbl_room_participants,
                where: {
                  tbl_profile_participant_id: req.user.idProfile
                }
              }],
              order: [['start', 'ASC']]
            });

          case 51:
            rooms = _context4.sent;
            res.json({
              message: 'Sessions found',
              count: count,
              previous: previousPageUrl,
              next: nextPageUrl,
              limit: limit,
              offset: offset,
              data: data,
              rooms: rooms
            });
            _context4.next = 59;
            break;

          case 55:
            _context4.prev = 55;
            _context4.t2 = _context4["catch"](0);
            console.log(_context4.t2);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 59:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 55]]);
  }));
  return _getSessionsPsychologist.apply(this, arguments);
}

function getSessionsHelpdesk(_x7, _x8) {
  return _getSessionsHelpdesk.apply(this, arguments);
}

function _getSessionsHelpdesk() {
  _getSessionsHelpdesk = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _date5;

    var to, limit, offset, search, whereQuery, now, session_attributes, profile_attributes, _yield$tbl_session$fi2, count, sessions, arr_sessions_id, searchWhereQuery, p_search_psy, p_search_pat, _yield$Promise$all, _yield$Promise$all2, _yield$Promise$all2$, s_psy_count, search_psy, _yield$Promise$all2$2, s_pat_count, search_pat, count_sum, urlPath, previousPageUrl, nextPageUrl, p_query_offset;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            to = req.query.to, limit = 20, offset = 0, search = req.query.search;

            if (req.query.limit) {
              limit = parseInt(req.query.limit);
            }

            if (req.query.offset) {
              offset = parseInt(req.query.offset);
            }

            // Today, 00:00:00
            now = DateTime.fromObject({
              day: DateTime.now().plus({
                hour: -5
              }).day
            });
            _context6.t0 = to;
            _context6.next = _context6.t0 === 'today' ? 8 : _context6.t0 === 'pending' ? 10 : _context6.t0 === 'draft' ? 12 : 14;
            break;

          case 8:
            whereQuery = {
              date: (_date5 = {}, _defineProperty(_date5, Op.gte, now.toISO()), _defineProperty(_date5, Op.lt, now.plus({
                hour: -5,
                days: 1
              }).toISO()), _date5)
            };
            return _context6.abrupt("break", 16);

          case 10:
            whereQuery = {
              patient_state: 2
            };
            return _context6.abrupt("break", 16);

          case 12:
            whereQuery = {
              patient_state: 3
            };
            return _context6.abrupt("break", 16);

          case 14:
            whereQuery = {};
            return _context6.abrupt("break", 16);

          case 16:
            session_attributes = ['id', 'date', 'state', 'patient_state', 'psychologist_state', 'order', 'week'];
            profile_attributes = ['id', 'firstname', 'lastname', 'birthday']; // Obtain array sessions id

            _context6.next = 20;
            return tbl_session.findAndCountAll({
              where: whereQuery,
              attributes: ['id', 'date'],
              limit: limit,
              offset: offset,
              distinct: true,
              order: [['date', 'ASC']],
              include: [{
                model: tbl_profile,
                as: 'Helpdesk',
                attributes: ['id']
              }],
              group: ['"tbl_session.id"', '"Helpdesk.id"'],
              having: Sequelize.where(Sequelize.literal('"Helpdesk"."id"'), _defineProperty({}, Op.or, [_defineProperty({}, Op.eq, null), _defineProperty({}, Op.eq, req.user.idProfile)]))
            });

          case 20:
            _yield$tbl_session$fi2 = _context6.sent;
            count = _yield$tbl_session$fi2.count;
            sessions = _yield$tbl_session$fi2.rows;
            // if( sessions.length == 0 ){
            //     return res.status(404).json({
            //         message: 'No sessions were found'
            //     })
            // }
            arr_sessions_id = sessions.map(function (session) {
              return session.id;
            }); // console.log('')
            // console.log('')
            // console.log('arr_sessions_id')
            // console.log(arr_sessions_id)
            // console.log('')
            // console.log('')

            if (!search) {
              _context6.next = 43;
              break;
            }

            searchWhereQuery = _defineProperty({}, Op.or, [{
              firstname: _defineProperty({}, Op.iLike, '%' + search + '%')
            }, {
              lastname: _defineProperty({}, Op.iLike, '%' + search + '%')
            }]);
            p_search_psy = tbl_session.findAndCountAll({
              where: {
                id: _defineProperty({}, Op.or, arr_sessions_id)
              },
              include: [{
                model: tbl_profile,
                as: 'Psychologist',
                where: searchWhereQuery,
                attributes: profile_attributes
              }, {
                model: tbl_profile,
                as: 'Patient',
                attributes: profile_attributes
              }],
              attributes: session_attributes
            });
            p_search_pat = tbl_session.findAndCountAll({
              where: {
                id: _defineProperty({}, Op.or, arr_sessions_id)
              },
              include: [{
                model: tbl_profile,
                as: 'Psychologist',
                attributes: profile_attributes
              }, {
                model: tbl_profile,
                as: 'Patient',
                where: searchWhereQuery,
                attributes: profile_attributes
              }],
              attributes: session_attributes
            });
            _context6.next = 30;
            return Promise.all([p_search_psy, p_search_pat]);

          case 30:
            _yield$Promise$all = _context6.sent;
            _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 2);
            _yield$Promise$all2$ = _yield$Promise$all2[0];
            s_psy_count = _yield$Promise$all2$.count;
            search_psy = _yield$Promise$all2$.rows;
            _yield$Promise$all2$2 = _yield$Promise$all2[1];
            s_pat_count = _yield$Promise$all2$2.count;
            search_pat = _yield$Promise$all2$2.rows;
            sessions = search_psy.concat(search_pat);
            count_sum = s_psy_count + s_pat_count;

            if (count_sum < sessions_count) {
              sessions_count = count_sum;
            }

            _context6.next = 46;
            break;

          case 43:
            _context6.next = 45;
            return tbl_session.findAll({
              where: {
                id: _defineProperty({}, Op.or, arr_sessions_id)
              },
              include: [{
                model: tbl_profile,
                as: 'Psychologist',
                attributes: profile_attributes
              }, {
                model: tbl_profile,
                as: 'Patient',
                attributes: profile_attributes
              }, {
                model: tbl_profile,
                as: 'Helpdesk',
                attributes: ['id']
              }],
              attributes: session_attributes,
              order: [['date', 'ASC']],
              group: ['"tbl_session.id"', '"Psychologist.id"', '"Patient.id"', '"Helpdesk.id"'],
              having: Sequelize.where(Sequelize.literal('"Helpdesk"."id"'), _defineProperty({}, Op.or, [_defineProperty({}, Op.eq, null), _defineProperty({}, Op.eq, req.user.idProfile)]))
            });

          case 45:
            sessions = _context6.sent;

          case 46:
            _context6.next = 48;
            return Promise.all(sessions.map(function (session) {
              return new Promise( /*#__PURE__*/function () {
                var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(resolve) {
                  var p_concluded, p_total, notification, fechaActual, year, birthdayPatient, newBirthdayPatient, yearPatient, agePatient, birthdayPsychologist, newBirthdayPsychologist, yearPsychologist, agePsychologist, personal, _yield$Promise$all3, _yield$Promise$all4, concluded, total, nnn, hhh, mmm, timeLeft, countCase;

                  return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          p_concluded = tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: session.Patient.id,
                              tbl_profile_psychologist_id: session.Psychologist.id,
                              date: _defineProperty({}, Op.lt, now.toISO())
                            }
                          });
                          p_total = tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: session.Patient.id,
                              tbl_profile_psychologist_id: session.Psychologist.id
                            }
                          });
                          notification = {
                            state: 'error',
                            name: 'Reagendar'
                          };
                          fechaActual = new Date();
                          year = fechaActual.getFullYear();
                          birthdayPatient = session.Patient.birthday;
                          newBirthdayPatient = new Date(birthdayPatient);
                          yearPatient = newBirthdayPatient.getFullYear();
                          agePatient = year - yearPatient;
                          birthdayPsychologist = session.Psychologist.birthday;
                          newBirthdayPsychologist = new Date(birthdayPsychologist);
                          yearPsychologist = newBirthdayPsychologist.getFullYear();
                          agePsychologist = year - yearPsychologist;
                          personal = {
                            "patient": "error",
                            "psychologist": "error"
                          };
                          _context5.next = 16;
                          return Promise.all([p_concluded, p_total]);

                        case 16:
                          _yield$Promise$all3 = _context5.sent;
                          _yield$Promise$all4 = _slicedToArray(_yield$Promise$all3, 2);
                          concluded = _yield$Promise$all4[0];
                          total = _yield$Promise$all4[1];
                          nnn = new Date(session.date);
                          hhh = fechaActual.getHours() - nnn.getHours();
                          mmm = fechaActual.getMinutes() - nnn.getMinutes();
                          timeLeft = hhh + ':' + Math.abs(mmm);
                          session.setDataValue('timeLeft', timeLeft);
                          session.Patient.setDataValue('age', agePatient);
                          session.Psychologist.setDataValue('age', agePsychologist);
                          session.setDataValue('total', total);
                          session.setDataValue('reasign', concluded);
                          session.setDataValue('personal', personal);
                          session.setDataValue('notification', notification);
                          _context5.next = 33;
                          return tbl_session_cases.count({
                            where: {
                              tbl_session_id: session.id
                            }
                          });

                        case 33:
                          countCase = _context5.sent;
                          console.log(countCase);
                          session.setDataValue('countCase', countCase);
                          resolve(session);

                        case 37:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5);
                }));

                return function (_x46) {
                  return _ref6.apply(this, arguments);
                };
              }());
            }));

          case 48:
            sessions = _context6.sent;
            urlPath = req.baseUrl + req.path;

            if (urlPath.substring(urlPath.length - 1) == '/') {
              urlPath = urlPath.substring(0, urlPath.length - 1);
            }

            previousPageUrl = null, nextPageUrl = null;

            if (offset > 0) {
              p_query_offset = offset - limit;

              if (offset - limit < 0) {
                p_query_offset = 0;
              }

              previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: p_query_offset,
                  limit: limit
                }
              });
            }

            if (offset + limit < count) {
              nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: offset + limit,
                  limit: limit
                }
              });
            }

            return _context6.abrupt("return", res.json({
              message: 'Sessions found',
              count: count.length,
              previous: previousPageUrl,
              next: nextPageUrl,
              limit: limit,
              offset: offset,
              data: sessions
            }));

          case 57:
            _context6.prev = 57;
            _context6.t1 = _context6["catch"](0);
            console.log(_context6.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 61:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 57]]);
  }));
  return _getSessionsHelpdesk.apply(this, arguments);
}

function getSession(_x9, _x10) {
  return _getSession.apply(this, arguments);
}

function _getSession() {
  _getSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var id, sessions, Patient, chat_pacients, chat_psychologists, idChat, chat, _chat, chatParticipantPsychologist, chatParticipantPatient;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            id = req.params.id;
            _context8.next = 4;
            return tbl_session.findAll({
              where: {
                tbl_profile_pacient_id: id,
                state: 1
              },
              include: [{
                model: tbl_profile,
                as: 'Psychologist'
              }, {
                model: tbl_session_note,
                as: 'Notes'
              }],
              order: [['week', 'DESC']]
            });

          case 4:
            sessions = _context8.sent;
            _context8.next = 7;
            return tbl_profile.findByPk(id, {
              where: {
                state: 1
              },
              include: [{
                model: tbl_avatar,
                as: 'Avatar'
              }]
            });

          case 7:
            Patient = _context8.sent;
            _context8.next = 10;
            return tbl_profile_attribute.findAll({
              where: {
                tbl_profile_id: id
              },
              attributes: ['key', 'value']
            });

          case 10:
            Patient.dataValues.attributes = _context8.sent;
            _context8.next = 13;
            return tbl_chat_participants.findAll({
              where: {
                tbl_profile_id: id
              },
              attributes: [['tbl_chat_id', 'idChat']]
            });

          case 13:
            chat_pacients = _context8.sent;
            _context8.next = 16;
            return tbl_chat_participants.findAll({
              where: {
                tbl_profile_id: sessions[sessions.length - 1].Psychologist.dataValues.id
              },
              attributes: [['tbl_chat_id', 'idChat']]
            });

          case 16:
            chat_psychologists = _context8.sent;
            idChat = null;
            _context8.next = 20;
            return tbl_chat.findOne({
              attributes: ['id'],
              where: {
                helpdesk: 0,
                tbl_room_id: null
              },
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: id
                }
              }, {
                model: tbl_chat_participants,
                as: 'ChatParticipantPsychologist',
                attributes: [],
                where: {
                  tbl_profile_id: req.user.idProfile
                }
              }]
            });

          case 20:
            chat = _context8.sent;
            console.log(sessions[sessions.length - 1].Psychologist.dataValues.id);

            if (!(chat === null)) {
              _context8.next = 35;
              break;
            }

            _context8.next = 25;
            return tbl_chat.create({
              start: Date.now(),
              helpdesk: 0
            });

          case 25:
            _chat = _context8.sent;
            _context8.next = 28;
            return tbl_chat_participants.create({
              tbl_chat_id: _chat.id,
              tbl_profile_id: id
            });

          case 28:
            chatParticipantPsychologist = _context8.sent;
            _context8.next = 31;
            return tbl_chat_participants.create({
              tbl_chat_id: _chat.id,
              tbl_profile_id: req.user.idProfile
            });

          case 31:
            chatParticipantPatient = _context8.sent;
            idChat = _chat.id;
            _context8.next = 36;
            break;

          case 35:
            idChat = chat.id;

          case 36:
            _context8.next = 38;
            return Promise.all(sessions.map( /*#__PURE__*/function () {
              var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(item, key) {
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                  while (1) {
                    switch (_context7.prev = _context7.next) {
                      case 0:
                        _context7.next = 2;
                        return tbl_session_attribute.findAll({
                          where: {
                            state: 1,
                            tbl_session_id: item.dataValues.id
                          },
                          attributes: ['key', 'value'],
                          order: [['id', 'ASC']]
                        });

                      case 2:
                        item.dataValues.attributes = _context7.sent;
                        return _context7.abrupt("return", item);

                      case 4:
                      case "end":
                        return _context7.stop();
                    }
                  }
                }, _callee7);
              }));

              return function (_x47, _x48) {
                return _ref7.apply(this, arguments);
              };
            }())).then(function (sessions) {
              res.json({
                Patient: Patient,
                idChat: idChat,
                sessions: sessions
              });
            });

          case 38:
            _context8.next = 44;
            break;

          case 40:
            _context8.prev = 40;
            _context8.t0 = _context8["catch"](0);
            console.log(_context8.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 44:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 40]]);
  }));
  return _getSession.apply(this, arguments);
}

function getNotesList(_x11, _x12) {
  return _getNotesList.apply(this, arguments);
}

function _getNotesList() {
  _getNotesList = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res) {
    var type, offset, limit, data, count, _yield$tbl_room_note$, count_room, rows_room, _yield$tbl_session_no, count_session, rows_session, urlPath, previousPageUrl, nextPageUrl, p_query_offset;

    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            type = req.query.type, offset = 0, limit = 20;

            if (req.query.offset) {
              offset = parseInt(req.query.offset);
            }

            if (req.query.limit) {
              limit = parseInt(req.query.limit);
            }

            _context10.t0 = type;
            _context10.next = _context10.t0 === 'room' ? 7 : 15;
            break;

          case 7:
            _context10.next = 9;
            return tbl_room_note.findAndCountAll({
              where: {
                tbl_profile_psychologist_id: req.user.idProfile,
                state: 1
              },
              attributes: ['id', ['notes', 'text'], 'state', 'created_at' //'name',
              //'type'
              ],
              include: [{
                model: tbl_profile,
                as: 'Patient',
                attributes: ['firstname', 'lastname'],
                include: {
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image']
                }
              }, {
                model: tbl_room,
                as: 'Room',
                attributes: ['id', 'title', 'start', 'end', 'rules', 'image']
              }],
              limit: limit,
              offset: offset,
              distinct: true
            });

          case 9:
            _yield$tbl_room_note$ = _context10.sent;
            count_room = _yield$tbl_room_note$.count;
            rows_room = _yield$tbl_room_note$.rows;
            count = count_room;
            data = rows_room;
            return _context10.abrupt("break", 26);

          case 15:
            type = 'session';
            _context10.next = 18;
            return tbl_session_note.findAndCountAll({
              where: {
                state: 1
              },
              include: {
                model: tbl_session,
                as: 'Session',
                attributes: ['date', 'order', 'tbl_profile_pacient_id', 'tbl_profile_psychologist_id'],
                required: true,
                include: [{
                  model: tbl_profile,
                  as: 'Psychologist',
                  where: {
                    tbl_user_id: req.user.id,
                    state: 1
                  },
                  attributes: []
                }, {
                  model: tbl_profile,
                  as: 'Patient',
                  attributes: ['firstname', 'lastname'],
                  include: {
                    model: tbl_avatar,
                    as: 'Avatar',
                    attributes: ['image']
                  }
                }, {
                  model: tbl_session_attribute,
                  as: 'SessionAttributes',
                  attributes: ['key', 'value']
                }]
              },
              attributes: ['id', ['note', 'text'], 'state', 'created_at' //'key',
              //'value'
              ],
              limit: limit,
              offset: offset,
              distinct: true
            });

          case 18:
            _yield$tbl_session_no = _context10.sent;
            count_session = _yield$tbl_session_no.count;
            rows_session = _yield$tbl_session_no.rows;
            _context10.next = 23;
            return Promise.all(rows_session.map(function (note) {
              return new Promise( /*#__PURE__*/function () {
                var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(resolve) {
                  var total;
                  return regeneratorRuntime.wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          _context9.next = 2;
                          return tbl_session.count({
                            where: {
                              tbl_profile_pacient_id: note.dataValues.Session.tbl_profile_pacient_id,
                              tbl_profile_psychologist_id: note.dataValues.Session.tbl_profile_psychologist_id
                            }
                          });

                        case 2:
                          total = _context9.sent;
                          note.Session.setDataValue('total', total);
                          resolve(note);

                        case 5:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9);
                }));

                return function (_x49) {
                  return _ref8.apply(this, arguments);
                };
              }());
            }));

          case 23:
            data = _context10.sent;
            count = count_session;
            return _context10.abrupt("break", 26);

          case 26:
            urlPath = req.baseUrl + req.path;

            if (urlPath.substring(urlPath.length - 1) == '/') {
              urlPath = urlPath.substring(0, urlPath.length - 1);
            }

            previousPageUrl = null, nextPageUrl = null;

            if (offset > 0) {
              p_query_offset = offset - limit;

              if (offset - limit < 0) {
                p_query_offset = 0;
              }

              previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: p_query_offset,
                  limit: limit
                }
              });
            }

            if (offset + limit < count) {
              nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: offset + limit,
                  limit: limit
                }
              });
            } // Capitalize


            type = type[0].toUpperCase() + type.slice(1);
            res.json({
              message: type + ' notes found',
              count: count,
              previous: previousPageUrl,
              next: nextPageUrl,
              limit: limit,
              offset: offset,
              data: data
            });
            _context10.next = 39;
            break;

          case 35:
            _context10.prev = 35;
            _context10.t1 = _context10["catch"](0);
            console.error('You have a error in:', _context10.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 39:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 35]]);
  }));
  return _getNotesList.apply(this, arguments);
}

function disableNote(_x13, _x14) {
  return _disableNote.apply(this, arguments);
}

function _disableNote() {
  _disableNote = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(req, res) {
    var note_type, note, not_found, note_id;
    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;
            note_type = req.body.type, note_id = req.params.id;
            _context11.t0 = note_type;
            _context11.next = _context11.t0 === 'room' ? 5 : 15;
            break;

          case 5:
            _context11.next = 7;
            return tbl_room_note.findOne({
              where: {
                id: note_id,
                tbl_profile_psychologist_id: req.user.idProfile
              }
            });

          case 7:
            note = _context11.sent;

            if (!note) {
              _context11.next = 13;
              break;
            }

            _context11.next = 11;
            return note.update({
              state: 0
            });

          case 11:
            _context11.next = 14;
            break;

          case 13:
            not_found = true;

          case 14:
            return _context11.abrupt("break", 25);

          case 15:
            _context11.next = 17;
            return tbl_session_note.findOne({
              where: {
                id: note_id
              },
              include: {
                model: tbl_session,
                as: 'Session',
                where: {
                  tbl_profile_psychologist_id: req.user.idProfile
                }
              }
            });

          case 17:
            note = _context11.sent;

            if (!note) {
              _context11.next = 23;
              break;
            }

            _context11.next = 21;
            return note.update({
              state: 0
            });

          case 21:
            _context11.next = 24;
            break;

          case 23:
            not_found = true;

          case 24:
            return _context11.abrupt("break", 25);

          case 25:
            if (!not_found) {
              _context11.next = 27;
              break;
            }

            return _context11.abrupt("return", res.status(404).json({
              message: 'Note not found'
            }));

          case 27:
            res.json({
              message: 'Note disabled'
            });
            _context11.next = 34;
            break;

          case 30:
            _context11.prev = 30;
            _context11.t1 = _context11["catch"](0);
            console.error('You have a error in:', _context11.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 34:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[0, 30]]);
  }));
  return _disableNote.apply(this, arguments);
}

function getPatientsSession(_x15, _x16) {
  return _getPatientsSession.apply(this, arguments);
}

function _getPatientsSession() {
  _getPatientsSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(req, res) {
    var psycologyId, patients, _iterator, _step, patient, sessions;

    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.prev = 0;
            _context12.next = 3;
            return tbl_profile.findOne({
              attributes: ['id'],
              where: {
                tbl_user_id: req.user.id
              }
            });

          case 3:
            psycologyId = _context12.sent;
            _context12.next = 6;
            return tbl_session.findAll({
              where: {
                state: 1,
                tbl_profile_psychologist_id: psycologyId.id
              },
              include: [{
                model: tbl_profile,
                as: 'Patient',
                attributes: ['id', 'firstname', 'lastname'],
                where: {
                  state: 1
                },
                include: {
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image']
                }
              }, {
                model: tbl_session_attribute,
                as: 'SessionAttributes',
                attributes: [['key', 'type'], 'value'],
                where: {
                  state: 1,
                  key: 'type'
                }
              }]
            });

          case 6:
            patients = _context12.sent;
            _iterator = _createForOfIteratorHelper(patients);
            _context12.prev = 8;

            _iterator.s();

          case 10:
            if ((_step = _iterator.n()).done) {
              _context12.next = 17;
              break;
            }

            patient = _step.value;
            _context12.next = 14;
            return tbl_session.findAll({
              attributes: [['id', 'idSession'], 'date'],
              where: {
                tbl_profile_pacient_id: patient.tbl_profile_pacient_id
              }
            });

          case 14:
            sessions = _context12.sent;

          case 15:
            _context12.next = 10;
            break;

          case 17:
            _context12.next = 22;
            break;

          case 19:
            _context12.prev = 19;
            _context12.t0 = _context12["catch"](8);

            _iterator.e(_context12.t0);

          case 22:
            _context12.prev = 22;

            _iterator.f();

            return _context12.finish(22);

          case 25:
            res.json({
              data: patients
            });
            _context12.next = 32;
            break;

          case 28:
            _context12.prev = 28;
            _context12.t1 = _context12["catch"](0);
            console.error('You have a error in:', _context12.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 32:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[0, 28], [8, 19, 22, 25]]);
  }));
  return _getPatientsSession.apply(this, arguments);
}

function postSession(_x17, _x18) {
  return _postSession.apply(this, arguments);
}

function _postSession() {
  _postSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(req, res) {
    var idPatient, idPsychologist, date, week, sessionId, idSession, nameRoom, patientSession;
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.prev = 0;
            idPatient = req.user.idProfile;
            idPsychologist = req.body.idPsychologist;
            date = req.body.date;
            week = req.body.week;
            _context13.next = 7;
            return tbl_session.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 7:
            sessionId = _context13.sent;
            idSession = 1;

            if (sessionId) {
              idSession = sessionId.id + 1;
            }

            nameRoom = 'room-s-' + idSession;
            _context13.next = 13;
            return tbl_session.create({
              tbl_profile_pacient_id: idPatient,
              patient_state: 1,
              tbl_profile_psychologist_id: idPsychologist,
              psychologist_state: 1,
              date: date,
              week: week,
              twilio_uniquename: nameRoom,
              order: 1
            });

          case 13:
            patientSession = _context13.sent;
            //const type ='go'
            //await twilioCreateRoom(req,res,type,nameRoom)
            res.json({
              message: 'Session Created',
              data: patientSession
            });
            _context13.next = 21;
            break;

          case 17:
            _context13.prev = 17;
            _context13.t0 = _context13["catch"](0);
            console.error('You have a error in:', _context13.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 21:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13, null, [[0, 17]]);
  }));
  return _postSession.apply(this, arguments);
}

function postNextSession(_x19, _x20) {
  return _postNextSession.apply(this, arguments);
}

function _postNextSession() {
  _postNextSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(req, res) {
    var _req$body, idPatient, idPsychologist, date, week, now, newDate, sessionId, idSession, nameRoom, sessionUpdate, sameSession, patientSession, idNewSession, session, pacient, formatDate, notification, data, status;

    return regeneratorRuntime.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.prev = 0;
            _req$body = req.body, idPatient = _req$body.idPatient, idPsychologist = _req$body.idPsychologist, date = _req$body.date, week = _req$body.week;
            now = DateTime.fromISO(new Date(date).toJSON(), {
              locale: 'es-ES',
              zone: 'UTC'
            });
            newDate = now;
            _context14.next = 6;
            return tbl_session.findOne({
              attributes: ['id'],
              order: [['id', 'DESC']]
            });

          case 6:
            sessionId = _context14.sent;
            idSession = sessionId.id + 1;
            nameRoom = 'room-s-' + idSession;
            _context14.next = 11;
            return tbl_session.update({
              completed: true
            }, {
              where: {
                tbl_profile_pacient_id: idPatient,
                tbl_profile_psychologist_id: idPsychologist,
                week: _defineProperty({}, Op.lt, week),
                state: 1
              }
            });

          case 11:
            sessionUpdate = _context14.sent;

            if (!sessionUpdate) {
              _context14.next = 53;
              break;
            }

            _context14.next = 15;
            return tbl_session.findOne({
              where: {
                tbl_profile_pacient_id: idPatient,
                tbl_profile_psychologist_id: idPsychologist,
                week: week,
                state: 1
              }
            });

          case 15:
            sameSession = _context14.sent;
            patientSession = null;

            if (!(sameSession === null)) {
              _context14.next = 23;
              break;
            }

            _context14.next = 20;
            return tbl_session.create({
              tbl_profile_pacient_id: idPatient,
              patient_state: 1,
              tbl_profile_psychologist_id: idPsychologist,
              psychologist_state: 1,
              date: newDate,
              week: week,
              twilio_uniquename: nameRoom,
              order: 1
            });

          case 20:
            patientSession = _context14.sent;
            _context14.next = 26;
            break;

          case 23:
            _context14.next = 25;
            return tbl_session.update({
              tbl_profile_pacient_id: idPatient,
              patient_state: 1,
              tbl_profile_psychologist_id: idPsychologist,
              psychologist_state: 1,
              date: newDate,
              week: week,
              twilio_uniquename: nameRoom,
              order: 1
            }, {
              where: {
                id: sameSession.id
              }
            });

          case 25:
            patientSession = _context14.sent;

          case 26:
            if (!patientSession) {
              _context14.next = 49;
              break;
            }

            idNewSession = sameSession === null ? patientSession.dataValues.id : sameSession.id;
            _context14.next = 30;
            return tbl_session_task.update({
              state: 1
            }, {
              where: {
                tbl_session_id: idNewSession
              }
            });

          case 30:
            _context14.next = 32;
            return tbl_session.findOne({
              where: {
                id: idNewSession,
                state: 1
              },
              include: [{
                model: tbl_profile,
                as: 'Psychologist'
              }, {
                model: tbl_session_note,
                as: 'Notes'
              }]
            });

          case 32:
            session = _context14.sent;
            _context14.next = 35;
            return tbl_session_attribute.findAll({
              where: {
                state: 1,
                tbl_session_id: idNewSession
              },
              attributes: ['key', 'value']
            });

          case 35:
            session.dataValues.attributes = _context14.sent;
            _context14.next = 38;
            return tbl_profile.findByPk(idPatient, {
              attributes: ['id'],
              include: {
                model: tbl_user,
                as: 'User',
                where: {
                  state: 1
                },
                attributes: ['id', 'token_device']
              }
            });

          case 38:
            pacient = _context14.sent;

            if (!pacient) {
              _context14.next = 47;
              break;
            }

            formatDate = (0, _momentTimezone["default"])(new Date(date).toJSON(), 'YYYY-MM-DD HH:mm:ss').locale("es").format('dddd DD') + ' a las ' + (0, _momentTimezone["default"])(new Date(date).toJSON(), 'YYYY-MM-DD HH:mm:ss').format('hh:mm a');
            notification = {
              title: 'Siguiente sesión',
              body: 'Tu sesión ha sido agendada exitosamente para el ' + formatDate
            };
            data = {
              idProfile: idPatient,
              type: 'session'
            };
            _context14.next = 45;
            return (0, _notification_controller.notify)(notification, data, pacient.dataValues.User.dataValues.token_device);

          case 45:
            status = _context14.sent;

            if (status) {
              res.json({
                message: 'Session Created',
                data: session
              });
            } else {
              res.json({
                message: 'Session Created but with errors',
                data: session
              });
            }

          case 47:
            _context14.next = 51;
            break;

          case 49:
            console.error('You have a error in save next session:', error);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 51:
            _context14.next = 55;
            break;

          case 53:
            console.error('You have a error in update current session:', error);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 55:
            _context14.next = 61;
            break;

          case 57:
            _context14.prev = 57;
            _context14.t0 = _context14["catch"](0);
            console.error('You have a error in:', _context14.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 61:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14, null, [[0, 57]]);
  }));
  return _postNextSession.apply(this, arguments);
}

function addSessionNote(_x21, _x22) {
  return _addSessionNote.apply(this, arguments);
}

function _addSessionNote() {
  _addSessionNote = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(req, res) {
    var _req$body2, note, priority, priorityVal, session, currentNote;

    return regeneratorRuntime.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.prev = 0;
            _req$body2 = req.body, note = _req$body2.note, priority = _req$body2.priority;

            if (!note) {
              _context15.next = 21;
              break;
            }

            priorityVal = 0;
            if (priority) priorityVal = priority;
            _context15.next = 7;
            return tbl_session.findOne({
              where: {
                id: req.params.id,
                tbl_profile_psychologist_id: req.user.idProfile
              }
            });

          case 7:
            session = _context15.sent;
            _context15.next = 10;
            return tbl_session_note.findOne({
              where: {
                tbl_session_id: session.id
              }
            });

          case 10:
            currentNote = _context15.sent;

            if (!currentNote) {
              _context15.next = 16;
              break;
            }

            _context15.next = 14;
            return tbl_session_note.update({
              note: note,
              priority: priorityVal
            }, {
              where: {
                id: currentNote.id
              }
            });

          case 14:
            _context15.next = 18;
            break;

          case 16:
            _context15.next = 18;
            return tbl_session_note.create({
              tbl_session_id: session.id,
              note: note,
              priority: priorityVal
            });

          case 18:
            res.status(200).json({
              message: 'Note registered'
            });
            _context15.next = 22;
            break;

          case 21:
            res.status(200).json({
              message: 'Nothing to do'
            });

          case 22:
            _context15.next = 28;
            break;

          case 24:
            _context15.prev = 24;
            _context15.t0 = _context15["catch"](0);
            console.error('You have a error in:', _context15.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 28:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15, null, [[0, 24]]);
  }));
  return _addSessionNote.apply(this, arguments);
}

function completeSession(_x23, _x24) {
  return _completeSession.apply(this, arguments);
}

function _completeSession() {
  _completeSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(req, res) {
    var session, patient, token;
    return regeneratorRuntime.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.prev = 0;
            _context16.next = 3;
            return tbl_session.findOne({
              where: {
                id: req.params.id,
                completed: false
              }
            });

          case 3:
            session = _context16.sent;

            if (!session) {
              _context16.next = 18;
              break;
            }

            _context16.next = 7;
            return session.update({
              completed: true
            });

          case 7:
            _context16.next = 9;
            return tbl_profile.findByPk(session.tbl_profile_pacient_id);

          case 9:
            patient = _context16.sent;

            if (!patient) {
              _context16.next = 17;
              break;
            }

            _context16.next = 13;
            return patient.update({
              potins: patient.points + MottivaConstants.SESSION_POINTS
            });

          case 13:
            _context16.next = 15;
            return (0, _token_controller.createToken)(req, res, req.user.id, req.user.username);

          case 15:
            token = _context16.sent;
            return _context16.abrupt("return", res.json({
              message: 'Session completed',
              data: {
                token: token
              }
            }));

          case 17:
            return _context16.abrupt("return", res.status(404).json({
              message: 'Patient not found'
            }));

          case 18:
            return _context16.abrupt("return", res.status(404).json({
              message: 'Session not found'
            }));

          case 21:
            _context16.prev = 21;
            _context16.t0 = _context16["catch"](0);
            console.error('You have a error in:', _context16.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 25:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16, null, [[0, 21]]);
  }));
  return _completeSession.apply(this, arguments);
}

function closedSession(_x25, _x26) {
  return _closedSession.apply(this, arguments);
}

function _closedSession() {
  _closedSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(req, res) {
    var state, idSession, _closedSession2;

    return regeneratorRuntime.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            _context17.prev = 0;
            state = req.body.state;
            idSession = req.body.idSession;
            _context17.next = 5;
            return tbl_session.update({
              state: state
            }, {
              where: {
                id: idSession
              }
            });

          case 5:
            _closedSession2 = _context17.sent;
            res.json({
              message: 'Session Closed!!'
            });
            _context17.next = 13;
            break;

          case 9:
            _context17.prev = 9;
            _context17.t0 = _context17["catch"](0);
            console.error('You have a error in:', _context17.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17, null, [[0, 9]]);
  }));
  return _closedSession.apply(this, arguments);
}

function rescheduleSession(_x27, _x28) {
  return _rescheduleSession.apply(this, arguments);
}

function _rescheduleSession() {
  _rescheduleSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee18(req, res) {
    return regeneratorRuntime.wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            _context18.prev = 0;
            console.log(req.body.date);
            _context18.next = 4;
            return tbl_session.update({
              date: req.body.date
            }, {
              where: {
                id: req.params.id
              }
            });

          case 4:
            res.json({
              message: 'Session updated'
            });
            _context18.next = 11;
            break;

          case 7:
            _context18.prev = 7;
            _context18.t0 = _context18["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context18.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18, null, [[0, 7]]);
  }));
  return _rescheduleSession.apply(this, arguments);
}

function rescheduleReplaceSession(_x29, _x30) {
  return _rescheduleReplaceSession.apply(this, arguments);
}

function _rescheduleReplaceSession() {
  _rescheduleReplaceSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee19(req, res) {
    var dateNow;
    return regeneratorRuntime.wrap(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            _context19.prev = 0;
            dateNow = new Date();
            _context19.next = 4;
            return tbl_session_cases.create({
              tbl_session_id: req.params.id,
              tbl_profile_id: req.user.idProfile,
              action: req.body.action,
              theme: req.body.theme,
              reason: req.body.subject,
              emotion: req.body.emotion,
              date: dateNow
            });

          case 4:
            _context19.next = 6;
            return tbl_session.update({
              tbl_profile_psychologist_id: req.body.psychologist,
              date: req.body.date
            }, {
              where: {
                id: req.params.id
              }
            });

          case 6:
            res.json({
              message: 'Session updated'
            });
            _context19.next = 13;
            break;

          case 9:
            _context19.prev = 9;
            _context19.t0 = _context19["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context19.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context19.stop();
        }
      }
    }, _callee19, null, [[0, 9]]);
  }));
  return _rescheduleReplaceSession.apply(this, arguments);
}

function cancelSession(_x31, _x32) {
  return _cancelSession.apply(this, arguments);
}

function _cancelSession() {
  _cancelSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee20(req, res) {
    var dateNow, session;
    return regeneratorRuntime.wrap(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            _context20.prev = 0;
            dateNow = new Date();
            _context20.next = 4;
            return tbl_session_cases.create({
              tbl_session_id: req.params.id,
              tbl_profile_id: req.user.idProfile,
              action: req.body.action,
              theme: req.body.theme,
              reason: req.body.subject,
              emotion: req.body.emotion,
              date: dateNow
            });

          case 4:
            _context20.next = 6;
            return tbl_session.findOne({
              where: {
                id: req.params.id
              }
            });

          case 6:
            session = _context20.sent;
            _context20.next = 9;
            return tbl_session.update({
              state: 0
            }, {
              where: {
                tbl_profile_pacient_id: session.tbl_profile_pacient_id,
                tbl_profile_psychologist_id: session.tbl_profile_psychologist_id
              }
            });

          case 9:
            _context20.next = 11;
            return tbl_blocked.create({
              tbl_profile_pacient_id: session.tbl_profile_pacient_id,
              tbl_profile_psychologist_id: session.tbl_profile_psychologist_id,
              tbl_reason_id: null,
              comment: req.body.subject
            });

          case 11:
            res.json({
              message: 'Session updated'
            });
            _context20.next = 18;
            break;

          case 14:
            _context20.prev = 14;
            _context20.t0 = _context20["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context20.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 18:
          case "end":
            return _context20.stop();
        }
      }
    }, _callee20, null, [[0, 14]]);
  }));
  return _cancelSession.apply(this, arguments);
}

function getSessionDetails(_x33, _x34) {
  return _getSessionDetails.apply(this, arguments);
}

function _getSessionDetails() {
  _getSessionDetails = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee21(req, res) {
    var sessionId, sessionDetails;
    return regeneratorRuntime.wrap(function _callee21$(_context21) {
      while (1) {
        switch (_context21.prev = _context21.next) {
          case 0:
            _context21.prev = 0;
            sessionId = req.query.sessionId;
            _context21.next = 4;
            return tbl_session.findOne({
              attributes: ['id', 'title', 'start', 'rules', 'participants', 'image'],
              where: {
                id: sessionId,
                state: 1
              }
            });

          case 4:
            sessionDetails = _context21.sent;
            res.json({
              data: sessionDetails
            });
            _context21.next = 12;
            break;

          case 8:
            _context21.prev = 8;
            _context21.t0 = _context21["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context21.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context21.stop();
        }
      }
    }, _callee21, null, [[0, 8]]);
  }));
  return _getSessionDetails.apply(this, arguments);
}

function getSessionCases(_x35, _x36) {
  return _getSessionCases.apply(this, arguments);
}

function _getSessionCases() {
  _getSessionCases = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee22(req, res) {
    var sessionCases;
    return regeneratorRuntime.wrap(function _callee22$(_context22) {
      while (1) {
        switch (_context22.prev = _context22.next) {
          case 0:
            _context22.prev = 0;
            _context22.next = 3;
            return tbl_session_cases.findAll({
              where: {
                tbl_session_id: req.params.id
              }
            });

          case 3:
            sessionCases = _context22.sent;
            res.json({
              data: sessionCases
            });
            _context22.next = 11;
            break;

          case 7:
            _context22.prev = 7;
            _context22.t0 = _context22["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context22.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context22.stop();
        }
      }
    }, _callee22, null, [[0, 7]]);
  }));
  return _getSessionCases.apply(this, arguments);
}

function getListSessionCases(_x37, _x38) {
  return _getListSessionCases.apply(this, arguments);
}

function _getListSessionCases() {
  _getListSessionCases = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee23(req, res) {
    var sessionCases;
    return regeneratorRuntime.wrap(function _callee23$(_context23) {
      while (1) {
        switch (_context23.prev = _context23.next) {
          case 0:
            _context23.prev = 0;
            _context23.next = 3;
            return tbl_session_cases.findAll({
              include: [{
                model: tbl_profile,
                as: 'Profile',
                attributes: ['id', 'firstname', 'lastname']
              }, {
                model: tbl_session,
                as: 'Session',
                include: {
                  model: tbl_profile,
                  as: 'Patient',
                  attributes: ['id', 'firstname', 'lastname']
                }
              }]
            });

          case 3:
            sessionCases = _context23.sent;
            res.json({
              data: sessionCases
            });
            _context23.next = 11;
            break;

          case 7:
            _context23.prev = 7;
            _context23.t0 = _context23["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context23.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context23.stop();
        }
      }
    }, _callee23, null, [[0, 7]]);
  }));
  return _getListSessionCases.apply(this, arguments);
}

function getSessionCaseDetails(_x39, _x40) {
  return _getSessionCaseDetails.apply(this, arguments);
}

function _getSessionCaseDetails() {
  _getSessionCaseDetails = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee24(req, res) {
    var id, sessionCase;
    return regeneratorRuntime.wrap(function _callee24$(_context24) {
      while (1) {
        switch (_context24.prev = _context24.next) {
          case 0:
            _context24.prev = 0;
            id = req.params.id;
            _context24.next = 4;
            return tbl_session_cases.findOne({
              where: {
                id: id
              }
            });

          case 4:
            sessionCase = _context24.sent;
            res.json({
              data: sessionCase
            });
            _context24.next = 12;
            break;

          case 8:
            _context24.prev = 8;
            _context24.t0 = _context24["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context24.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context24.stop();
        }
      }
    }, _callee24, null, [[0, 8]]);
  }));
  return _getSessionCaseDetails.apply(this, arguments);
}

function assignSession(_x41, _x42) {
  return _assignSession.apply(this, arguments);
}

function _assignSession() {
  _assignSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee25(req, res) {
    return regeneratorRuntime.wrap(function _callee25$(_context25) {
      while (1) {
        switch (_context25.prev = _context25.next) {
          case 0:
            _context25.prev = 0;
            _context25.next = 3;
            return tbl_session.update({
              tbl_profile_helpdesk_id: req.user.idProfile
            }, {
              where: {
                id: req.params.id
              }
            });

          case 3:
            res.json({
              message: 'Session updated'
            });
            _context25.next = 10;
            break;

          case 6:
            _context25.prev = 6;
            _context25.t0 = _context25["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context25.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 10:
          case "end":
            return _context25.stop();
        }
      }
    }, _callee25, null, [[0, 6]]);
  }));
  return _assignSession.apply(this, arguments);
}

function postCaseSession(_x43, _x44) {
  return _postCaseSession.apply(this, arguments);
}

function _postCaseSession() {
  _postCaseSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee26(req, res) {
    var _req$body3, action, theme, subject, emotion, date, idSession, idRoom, dateNow;

    return regeneratorRuntime.wrap(function _callee26$(_context26) {
      while (1) {
        switch (_context26.prev = _context26.next) {
          case 0:
            _context26.prev = 0;
            _req$body3 = req.body, action = _req$body3.action, theme = _req$body3.theme, subject = _req$body3.subject, emotion = _req$body3.emotion, date = _req$body3.date;
            idSession = req.body.idRoom ? null : req.body.idSession;
            idRoom = req.body.idRoom ? req.body.idRoom : null;
            dateNow = new Date();
            _context26.next = 7;
            return tbl_session_cases.create({
              tbl_session_id: idSession,
              tbl_profile_id: req.user.idProfile,
              action: action,
              theme: theme,
              reason: subject,
              emotion: emotion,
              date: dateNow,
              tbl_room_id: idRoom
            });

          case 7:
            if (!(action === 'reschedule')) {
              _context26.next = 10;
              break;
            }

            _context26.next = 10;
            return tbl_session.update({
              date: date
            }, {
              where: {
                id: idSession
              }
            });

          case 10:
            res.json({
              message: 'Session case created'
            });
            _context26.next = 17;
            break;

          case 13:
            _context26.prev = 13;
            _context26.t0 = _context26["catch"](0);
            console.error('You have a error in getListRoomByProfile:', _context26.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context26.stop();
        }
      }
    }, _callee26, null, [[0, 13]]);
  }));
  return _postCaseSession.apply(this, arguments);
}