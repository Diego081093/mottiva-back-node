"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterAvailability = filterAvailability;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require("sequelize"),
    Op = _require.Op;

var _require2 = require('../models'),
    tbl_availability = _require2.tbl_availability,
    tbl_profile = _require2.tbl_profile,
    tbl_avatar = _require2.tbl_avatar;

function filterAvailability(_x, _x2) {
  return _filterAvailability.apply(this, arguments);
}

function _filterAvailability() {
  _filterAvailability = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_availability.findAll({
              where: {
                state: 1,
                day: req.query.day,
                start_hour: _defineProperty({}, Op.lte, req.query.hour),
                end_hour: _defineProperty({}, Op.gt, req.query.hour)
              },
              include: {
                model: tbl_profile,
                as: 'Profile',
                where: {
                  gender: req.query.gender
                },
                include: {
                  model: tbl_avatar,
                  as: 'Avatar',
                  attributes: ['image']
                },
                attributes: ['id', 'slug', 'firstname', 'lastname', 'email', 'phone', 'gender', 'state']
              },
              attributes: ['id', 'day', 'start_hour', 'end_hour']
            });

          case 3:
            data = _context.sent;
            res.json({
              message: 'Therapies found',
              data: data
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _filterAvailability.apply(this, arguments);
}