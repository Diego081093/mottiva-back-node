"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNotifications = getNotifications;
exports.setTempoSession = setTempoSession;
exports.sendPushNotificationUser = sendPushNotificationUser;
exports.sendPushNotificationUsers = sendPushNotificationUsers;
exports.sendPushNotificationSessionStart = sendPushNotificationSessionStart;
exports.sendPushNotificationRoomStart = sendPushNotificationRoomStart;
exports.sendPushNotificationBenefits = sendPushNotificationBenefits;
exports.sendPushNotificationMedias = sendPushNotificationMedias;
exports.notify = notify;

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var fetch = require('node-fetch');

var _require = require('../models'),
    tbl_notification = _require.tbl_notification,
    tbl_notification_attributes = _require.tbl_notification_attributes,
    tbl_session = _require.tbl_session,
    tbl_profile = _require.tbl_profile,
    tbl_user = _require.tbl_user,
    tbl_room = _require.tbl_room,
    tbl_chat = _require.tbl_chat,
    tbl_chat_participants = _require.tbl_chat_participants,
    tbl_benefit_profile = _require.tbl_benefit_profile,
    tbl_benefit = _require.tbl_benefit,
    tbl_medias = _require.tbl_medias,
    tbl_media_profile = _require.tbl_media_profile;

var _require2 = require('sequelize'),
    Op = _require2.Op;

var _require3 = require('luxon'),
    DateTime = _require3.DateTime;

function getNotifications(_x, _x2) {
  return _getNotifications.apply(this, arguments);
}

function _getNotifications() {
  _getNotifications = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var key_filter, whereQuery, notifications, resUpdate;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            key_filter = req.query.filter;
            whereQuery = {
              state: 1
            };

            if (key_filter) {
              whereQuery = {
                key: key_filter,
                state: 1
              };
            }

            _context.next = 6;
            return tbl_notification.findAll({
              where: {
                tbl_profile_id: req.user.idProfile,
                state: 1
              },
              include: {
                model: tbl_notification_attributes,
                as: 'NotificationAttributes',
                where: whereQuery,
                attributes: ['key', 'value'],
                required: false
              },
              attributes: ['id', 'type', 'title', 'description', 'state', 'created_at', 'done'],
              order: [['created_at', 'DESC']]
            });

          case 6:
            notifications = _context.sent;
            _context.next = 9;
            return tbl_notification.update({
              done: 1
            }, {
              where: {
                tbl_profile_id: req.user.idProfile,
                state: 1,
                done: 0
              }
            });

          case 9:
            resUpdate = _context.sent;
            res.json({
              message: 'Notifications found',
              data: notifications
            });
            _context.next = 17;
            break;

          case 13:
            _context.prev = 13;
            _context.t0 = _context["catch"](0);
            console.error('You have a error on setTempoSession: ', _context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 13]]);
  }));
  return _getNotifications.apply(this, arguments);
}

function setTempoSession(_x3, _x4) {
  return _setTempoSession.apply(this, arguments);
}

function _setTempoSession() {
  _setTempoSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _date, _date2, _where, now, startMedium, endMedium, startQuarter, endQuarter, listPatients, _iterator, _step, session, end, start, differ, minutesp, minutes, body, save;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            now = DateTime.fromISO(DateTime.now(), {
              locale: 'es-ES',
              zone: 'UTC'
            });
            startMedium = now.plus({
              hour: -5,
              minutes: 28
            }).toString();
            endMedium = now.plus({
              hour: -5,
              minutes: 32
            }).toString();
            startQuarter = now.plus({
              hour: -5,
              minutes: 13
            }).toString();
            endQuarter = now.plus({
              hour: -5,
              minutes: 17
            }).toString();
            _context2.next = 8;
            return tbl_session.findAll({
              where: (_where = {}, _defineProperty(_where, Op.or, [{
                date: (_date = {}, _defineProperty(_date, Op.gte, startMedium), _defineProperty(_date, Op.lte, endMedium), _date)
              }, {
                date: (_date2 = {}, _defineProperty(_date2, Op.gte, startQuarter), _defineProperty(_date2, Op.lte, endQuarter), _date2)
              }]), _defineProperty(_where, "state", 1), _where),
              include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_user,
                  as: 'User',
                  where: {
                    state: 1
                  },
                  attributes: ['id', 'token_device']
                }
              },
              attributes: ['id', 'date', ['tbl_profile_psychologist_id', 'idProfilePsychologist']]
            });

          case 8:
            listPatients = _context2.sent;
            _iterator = _createForOfIteratorHelper(listPatients);
            _context2.prev = 10;

            _iterator.s();

          case 12:
            if ((_step = _iterator.n()).done) {
              _context2.next = 27;
              break;
            }

            session = _step.value;

            if (!(session.ProfilePatient.User.dataValues.token_device != null || session.ProfilePatient.User.dataValues.token_device != '')) {
              _context2.next = 25;
              break;
            }

            end = new Date(session.dataValues.date);
            start = new Date();
            differ = Math.abs(end.getTime() - start.getTime());
            minutesp = Math.ceil(differ / (1000 * 60));
            minutes = minutesp > 27 ? 30 : 15;
            body = {
              notification: {
                title: "Mottiva",
                body: "Tu sessión comienza en " + minutes + " minutos."
              },
              data: {
                id: session.dataValues.id,
                date: session.dataValues.date,
                idProfilePsychologist: session.dataValues.idProfilePsychologist,
                points: 5
              },
              to: session.ProfilePatient.User.dataValues.token_device,
              direct_boot_ok: true
            };
            _context2.next = 23;
            return tbl_notification.create({
              tbl_profile_id: session.ProfilePatient.id,
              type: 'session',
              title: "Tu sessión comienza en " + minutes + " minutos.",
              description: '',
              state: 1,
              created_by: 'root',
              createdAt: now.plus({
                hour: -5
              }).toString()
            });

          case 23:
            save = _context2.sent;
            fetch('https://fcm.googleapis.com/fcm/send', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
              },
              body: JSON.stringify(body)
            }).then(function (json) {
              console.log(json.status);
            });

          case 25:
            _context2.next = 12;
            break;

          case 27:
            _context2.next = 32;
            break;

          case 29:
            _context2.prev = 29;
            _context2.t0 = _context2["catch"](10);

            _iterator.e(_context2.t0);

          case 32:
            _context2.prev = 32;

            _iterator.f();

            return _context2.finish(32);

          case 35:
            res.json({
              message: 'Process successfully',
              session: listPatients
            });
            _context2.next = 42;
            break;

          case 38:
            _context2.prev = 38;
            _context2.t1 = _context2["catch"](0);
            console.error('You have a error on setTempoSession: ', _context2.t1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 42:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 38], [10, 29, 32, 35]]);
  }));
  return _setTempoSession.apply(this, arguments);
}

function sendPushNotificationUser(_x5, _x6) {
  return _sendPushNotificationUser.apply(this, arguments);
}

function _sendPushNotificationUser() {
  _sendPushNotificationUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var idRole, id, onePatient, body;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            idRole = req.user.idRole;

            if (!(idRole == 2)) {
              _context3.next = 11;
              break;
            }

            id = req.params.id;
            _context3.next = 6;
            return tbl_session.findOne({
              where: {
                state: 1,
                id: id
              },
              include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_user,
                  as: 'User',
                  where: {
                    state: 1
                  },
                  attributes: ['id', 'token_device']
                }
              },
              attributes: ['id', 'date', 'twilio_uniquename', ['tbl_profile_psychologist_id', 'idProfilePsychologist']]
            });

          case 6:
            onePatient = _context3.sent;

            if (onePatient.dataValues.ProfilePatient.User.dataValues.token_device != null || onePatient.dataValues.ProfilePatient.User.dataValues.token_device != '') {
              body = {
                notification: {
                  title: "Mottiva",
                  body: "Tu sessión ha comenzado"
                },
                data: {
                  id: onePatient.dataValues.id,
                  date: onePatient.dataValues.date,
                  idProfilePsychologist: onePatient.dataValues.idProfilePsychologist,
                  type: 'start_sessionRoom',
                  twilio_uniquename: onePatient.dataValues.twilio_uniquename,
                  points: 5
                },
                to: onePatient.dataValues.ProfilePatient.User.dataValues.token_device,
                direct_boot_ok: true
              };
              fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                },
                body: JSON.stringify(body)
              }).then(function (json) {
                console.log(json.status);
              });
            }

            res.status(200).json({
              message: 'Successfully'
            });
            _context3.next = 12;
            break;

          case 11:
            res.status(400).json({
              non_field_errors: 'Unable to register with provided credentials.'
            });

          case 12:
            _context3.next = 18;
            break;

          case 14:
            _context3.prev = 14;
            _context3.t0 = _context3["catch"](0);
            console.error('Something went error on sendPushNotificationUser:', _context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 18:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 14]]);
  }));
  return _sendPushNotificationUser.apply(this, arguments);
}

function sendPushNotificationUsers(_x7, _x8) {
  return _sendPushNotificationUsers.apply(this, arguments);
}

function _sendPushNotificationUsers() {
  _sendPushNotificationUsers = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var idRole, id, room, fullPatients, _iterator2, _step2, patient, body;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            idRole = req.user.idRole;

            if (!(idRole == 2)) {
              _context4.next = 17;
              break;
            }

            id = req.params.id;
            _context4.next = 6;
            return tbl_room.findOne({
              where: {
                state: 1,
                id: id
              },
              include: {
                model: tbl_chat,
                where: {
                  state: 1
                },
                attributes: ['id', 'start']
              },
              attributes: ['id', 'twilio_uniquename']
            });

          case 6:
            room = _context4.sent;
            fullPatients = null;

            if (!(room !== null)) {
              _context4.next = 14;
              break;
            }

            _context4.next = 11;
            return tbl_chat_participants.findAll({
              where: {
                state: 1,
                tbl_chat_id: room.tbl_chat.dataValues.id
              },
              include: {
                model: tbl_profile,
                as: 'Profile',
                where: {
                  state: 1,
                  tbl_role_id: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_user,
                  as: 'User',
                  where: {
                    state: 1
                  },
                  attributes: ['id', 'token_device']
                }
              },
              attributes: ['id']
            });

          case 11:
            fullPatients = _context4.sent;
            _iterator2 = _createForOfIteratorHelper(fullPatients);

            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                patient = _step2.value;

                if (patient.dataValues.Profile.User.dataValues.token_device != null || patient.dataValues.Profile.User.dataValues.token_device != '') {
                  body = {
                    notification: {
                      title: "Mottiva",
                      body: "Tu sessión ha comenzado"
                    },
                    data: {
                      id: room.dataValues.id,
                      date: room.tbl_chat.dataValues.start,
                      type: 'start_sessionRoom',
                      twilio_uniquename: room.dataValues.twilio_uniquename,
                      points: 5
                    },
                    to: patient.dataValues.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                  };
                  fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                    },
                    body: JSON.stringify(body)
                  }).then(function (json) {
                    console.log(json.status);
                  });
                }
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }

          case 14:
            res.status(200).json({
              message: 'Successfully'
            });
            _context4.next = 18;
            break;

          case 17:
            res.status(400).json({
              non_field_errors: 'Unable to register with provided credentials.'
            });

          case 18:
            _context4.next = 24;
            break;

          case 20:
            _context4.prev = 20;
            _context4.t0 = _context4["catch"](0);
            console.error('Something went error on sendPushNotificationUsers:', _context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 24:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 20]]);
  }));
  return _sendPushNotificationUsers.apply(this, arguments);
}

function sendPushNotificationSessionStart(_x9, _x10) {
  return _sendPushNotificationSessionStart.apply(this, arguments);
}

function _sendPushNotificationSessionStart() {
  _sendPushNotificationSessionStart = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _date3, _idProfile, now, start, end, listPsychologists, _iterator3, _step3, session, body;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _idProfile = req.user.idProfile;
            now = DateTime.now();
            start = now.plus({
              hour: -5,
              minutes: -2
            }).toString();
            end = now.plus({
              hour: -5,
              minutes: 2
            }).toString();
            _context5.next = 7;
            return tbl_session.findAll({
              where: {
                date: (_date3 = {}, _defineProperty(_date3, Op.gte, start), _defineProperty(_date3, Op.lte, end), _date3),
                state: 1
              },
              include: {
                model: tbl_profile,
                as: 'ProfilePsychologist',
                where: {
                  state: 1,
                  id: _idProfile
                },
                attributes: ['id'],
                include: {
                  model: tbl_user,
                  as: 'User',
                  where: {
                    state: 1
                  },
                  attributes: ['id', 'token_device']
                }
              },
              attributes: ['id', 'date', 'twilio_uniquename', ['tbl_profile_psychologist_id', 'idProfilePsychologist']]
            });

          case 7:
            listPsychologists = _context5.sent;
            _iterator3 = _createForOfIteratorHelper(listPsychologists);

            try {
              for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                session = _step3.value;

                if (session.ProfilePsychologist.User.dataValues.token_device != null || session.ProfilePsychologist.User.dataValues.token_device != '') {
                  body = {
                    notification: {
                      title: "Mottiva",
                      body: "Tu sessión debe ser iniciada en este momento"
                    },
                    data: {
                      id: session.dataValues.id,
                      date: session.dataValues.date,
                      type: 'start_session',
                      twilio_uniquename: session.dataValues.twilio_uniquename
                    },
                    to: session.ProfilePsychologist.User.dataValues.token_device,
                    direct_boot_ok: true
                  };
                  fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                    },
                    body: JSON.stringify(body)
                  }).then(function (json) {
                    console.log(json.status);
                  });
                }
              }
            } catch (err) {
              _iterator3.e(err);
            } finally {
              _iterator3.f();
            }

            res.json({
              message: 'Process successfully',
              session: listPsychologists
            });
            _context5.next = 17;
            break;

          case 13:
            _context5.prev = 13;
            _context5.t0 = _context5["catch"](0);
            console.error('You have a error on setTempoSession: ', _context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 13]]);
  }));
  return _sendPushNotificationSessionStart.apply(this, arguments);
}

function sendPushNotificationRoomStart(_x11, _x12) {
  return _sendPushNotificationRoomStart.apply(this, arguments);
}

function _sendPushNotificationRoomStart() {
  _sendPushNotificationRoomStart = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _start, _idProfile2, now, start, end, listPsychologists, _iterator4, _step4, session, body;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _idProfile2 = req.user.idProfile;
            now = DateTime.now();
            start = now.plus({
              hour: -5,
              minutes: -2
            }).toString();
            end = now.plus({
              hour: -5,
              minutes: 2
            }).toString();
            _context6.next = 7;
            return tbl_room.findAll({
              where: {
                start: (_start = {}, _defineProperty(_start, Op.gte, start), _defineProperty(_start, Op.lte, end), _start),
                state: 1
              },
              include: {
                model: tbl_chat,
                as: 'Chat',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_chat_participants,
                  as: 'ChatParticipant',
                  where: {
                    state: 1
                  },
                  attributes: ['id'],
                  include: {
                    model: tbl_profile,
                    as: 'Profile',
                    where: {
                      state: 1,
                      id: _idProfile2
                    },
                    //where: { state: 1, tbl_role_id: 2 },
                    attributes: ['id'],
                    include: {
                      model: tbl_user,
                      as: 'User',
                      where: {
                        state: 1
                      },
                      attributes: ['id', 'token_device']
                    }
                  }
                }
              },
              attributes: ['id', 'title', 'start', 'rules', 'participants', 'twilio_uniquename']
            });

          case 7:
            listPsychologists = _context6.sent;
            _iterator4 = _createForOfIteratorHelper(listPsychologists);

            try {
              for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                session = _step4.value;

                if (session.Chat.ChatParticipant.Profile.User.dataValues.token_device != null || session.Chat.ChatParticipant.Profile.User.dataValues.token_device != '') {
                  body = {
                    notification: {
                      title: "Mottiva",
                      body: "La Sala de emociones debe ser iniciada en este momento"
                    },
                    data: {
                      id: session.dataValues.id,
                      date: session.dataValues.date,
                      type: 'start_session',
                      twilio_uniquename: session.dataValues.twilio_uniquename
                    },
                    to: session.Chat.ChatParticipant.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                  };
                  fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                    },
                    body: JSON.stringify(body)
                  }).then(function (json) {
                    console.log(json.status);
                  });
                }
              }
            } catch (err) {
              _iterator4.e(err);
            } finally {
              _iterator4.f();
            }

            res.json({
              message: 'Process successfully',
              session: listPsychologists
            });
            _context6.next = 17;
            break;

          case 13:
            _context6.prev = 13;
            _context6.t0 = _context6["catch"](0);
            console.error('You have a error on setTempoSession: ', _context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 13]]);
  }));
  return _sendPushNotificationRoomStart.apply(this, arguments);
}

function sendPushNotificationBenefits(_x13, _x14) {
  return _sendPushNotificationBenefits.apply(this, arguments);
}

function _sendPushNotificationBenefits() {
  _sendPushNotificationBenefits = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var _idProfile3, now, start, end, listBenefits, _iterator5, _step5, benefit, body;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            _idProfile3 = req.user.idProfile;
            now = DateTime.now();
            start = now.plus({
              hour: -5,
              minutes: -2
            }).toString();
            end = now.plus({
              hour: -5,
              minutes: 2
            }).toString();
            _context7.next = 7;
            return tbl_benefit.findAll({
              where: {
                state: 1
              },
              include: {
                model: tbl_benefit_profile,
                as: 'BenefitProfile',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_profile,
                  as: 'Profile',
                  where: {
                    state: 1,
                    id: _idProfile3
                  },
                  //where: { state: 1, tbl_role_id: 2 },
                  attributes: ['id'],
                  include: {
                    model: tbl_user,
                    as: 'User',
                    where: {
                      state: 1
                    },
                    attributes: ['id', 'token_device']
                  }
                }
              },
              attributes: ['id', 'point']
            });

          case 7:
            listBenefits = _context7.sent;
            _iterator5 = _createForOfIteratorHelper(listBenefits);

            try {
              for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                benefit = _step5.value;

                if (benefit.BenefitProfile.Profile.User.dataValues.token_device != null || benefit.BenefitProfile.Profile.User.dataValues.token_device != '') {
                  body = {
                    notification: {
                      title: "Tienes nuevos retos esta semana",
                      body: "Puedes ganar hasta 5000 puntos"
                    },
                    data: {
                      id: _idProfile3
                    },
                    to: benefit.BenefitProfile.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                  };
                  fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                    },
                    body: JSON.stringify(body)
                  }).then(function (json) {
                    console.log(json.status);
                  });
                }
              }
            } catch (err) {
              _iterator5.e(err);
            } finally {
              _iterator5.f();
            }

            res.json({
              message: 'Process successfully',
              benefit: listBenefits
            });
            _context7.next = 17;
            break;

          case 13:
            _context7.prev = 13;
            _context7.t0 = _context7["catch"](0);
            console.error('You have a error on setTempoSession: ', _context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 13]]);
  }));
  return _sendPushNotificationBenefits.apply(this, arguments);
}

function sendPushNotificationMedias(_x15, _x16) {
  return _sendPushNotificationMedias.apply(this, arguments);
}

function _sendPushNotificationMedias() {
  _sendPushNotificationMedias = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var _idProfile4, now, start, end, media, videos, totalVideos, body;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            _idProfile4 = req.user.idProfile;
            now = DateTime.now();
            start = now.plus({
              hour: -5,
              minutes: -2
            }).toString();
            end = now.plus({
              hour: -5,
              minutes: 2
            }).toString();
            _context8.next = 7;
            return tbl_medias.findOne({
              where: {
                state: 1
              },
              include: {
                model: tbl_media_profile,
                as: 'onProfile',
                where: {
                  state: 1
                },
                attributes: ['id'],
                include: {
                  model: tbl_profile,
                  as: 'Profile',
                  where: {
                    state: 1,
                    id: _idProfile4
                  },
                  //where: { state: 1, tbl_role_id: 2 },
                  attributes: ['id'],
                  include: {
                    model: tbl_user,
                    as: 'User',
                    where: {
                      state: 1
                    },
                    attributes: ['id', 'token_device']
                  }
                }
              },
              attributes: ['id']
            });

          case 7:
            media = _context8.sent;
            _context8.next = 10;
            return tbl_media_profile.findAndCountAll({
              where: {
                tbl_profile_id: _idProfile4
              }
            });

          case 10:
            videos = _context8.sent;
            totalVideos = videos.count;

            if (media.onProfile.Profile.User.token_device != null || media.onProfile.Profile.User.token_device != '') {
              body = {
                notification: {
                  title: "Tienes " + totalVideos + " vídeos nuevos en tu reto semanal",
                  body: "Puedes ganar hasta 2500 puntos"
                },
                data: {
                  id: _idProfile4
                },
                to: media.onProfile.Profile.User.dataValues.token_device,
                direct_boot_ok: true
              };
              fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
                },
                body: JSON.stringify(body)
              }).then(function (json) {
                console.log(json.status);
              });
            }

            res.json({
              message: 'Process successfully',
              media: media
            });
            _context8.next = 20;
            break;

          case 16:
            _context8.prev = 16;
            _context8.t0 = _context8["catch"](0);
            console.error('You have a error on setTempoSession: ', _context8.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 20:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 16]]);
  }));
  return _sendPushNotificationMedias.apply(this, arguments);
}

function notify() {
  return _notify.apply(this, arguments);
}

function _notify() {
  _notify = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
    var notification,
        data,
        to,
        now,
        body,
        notificationCreate,
        status,
        _args9 = arguments;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            notification = _args9.length > 0 && _args9[0] !== undefined ? _args9[0] : {
              title: '',
              body: ''
            };
            data = _args9.length > 1 && _args9[1] !== undefined ? _args9[1] : {
              idProfile: idProfile,
              type: type
            };
            to = _args9.length > 2 ? _args9[2] : undefined;
            _context9.prev = 3;
            now = DateTime.fromISO(DateTime.now(), {
              locale: 'es-ES',
              zone: 'UTC'
            });
            body = {
              notification: notification,
              data: data,
              to: to,
              direct_boot_ok: true
            };
            _context9.next = 8;
            return tbl_notification.create({
              tbl_profile_id: data.idProfile,
              type: data.type,
              title: notification.title,
              description: notification.body,
              state: 1,
              created_by: 'JJohan',
              createdAt: now.plus({
                hour: -5
              }).toString()
            });

          case 8:
            notificationCreate = _context9.sent;

            if (!notificationCreate) {
              _context9.next = 16;
              break;
            }

            _context9.next = 12;
            return fetch('https://fcm.googleapis.com/fcm/send', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN'
              },
              body: JSON.stringify(body)
            }).then(function (json) {
              return json.statusText === 'OK' ? true : false;
            });

          case 12:
            status = _context9.sent;
            return _context9.abrupt("return", status);

          case 16:
            console.error('You have a error on notify');
            return _context9.abrupt("return", false);

          case 18:
            _context9.next = 24;
            break;

          case 20:
            _context9.prev = 20;
            _context9.t0 = _context9["catch"](3);
            console.log('Something went error notify:', _context9.t0);
            return _context9.abrupt("return", false);

          case 24:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[3, 20]]);
  }));
  return _notify.apply(this, arguments);
}