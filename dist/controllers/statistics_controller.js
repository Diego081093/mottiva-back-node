"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAll = getAll;

var _sequelize = require("sequelize");

var _luxon = require("luxon");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_profile = _require.tbl_profile,
    tbl_session = _require.tbl_session,
    tbl_room = _require.tbl_room,
    tbl_room_participants = _require.tbl_room_participants,
    tbl_diary = _require.tbl_diary;

function getAll(_x, _x2) {
  return _getAll.apply(this, arguments);
}

function _getAll() {
  _getAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var countPatient, countPsychologist, countHelpdesk, countSession, countProgram, countRoom, countRoomParticipant, now, countRoomCompleted, countRoomCompletedParticipant, countDiary;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_profile.count({
              where: {
                tbl_role_id: 1
              }
            });

          case 3:
            countPatient = _context.sent;
            _context.next = 6;
            return tbl_profile.count({
              where: {
                tbl_role_id: 2
              }
            });

          case 6:
            countPsychologist = _context.sent;
            _context.next = 9;
            return tbl_profile.count({
              where: {
                tbl_role_id: 3
              }
            });

          case 9:
            countHelpdesk = _context.sent;
            _context.next = 12;
            return tbl_session.count({
              where: {
                completed: true
              }
            });

          case 12:
            countSession = _context.sent;
            _context.next = 15;
            return tbl_session.count({
              where: {
                completed: true,
                week: 6
              }
            });

          case 15:
            countProgram = _context.sent;
            _context.next = 18;
            return tbl_room.count({
              where: {
                state: 1
              }
            });

          case 18:
            countRoom = _context.sent;
            _context.next = 21;
            return tbl_room_participants.count({
              where: {
                state: 1
              }
            });

          case 21:
            countRoomParticipant = _context.sent;
            now = _luxon.DateTime.fromISO(_luxon.DateTime.now(), {
              locale: 'es-ES',
              zone: 'UTC'
            });
            _context.next = 25;
            return tbl_room.count({
              where: {
                end: _defineProperty({}, _sequelize.Op.lt, now.plus({
                  hour: -5
                }).toString())
              }
            });

          case 25:
            countRoomCompleted = _context.sent;
            _context.next = 28;
            return tbl_room_participants.count({
              where: {
                state: 1
              },
              include: {
                model: tbl_room,
                as: 'Room',
                where: {
                  end: _defineProperty({}, _sequelize.Op.lt, now.plus({
                    hour: -5
                  }).toString())
                }
              }
            });

          case 28:
            countRoomCompletedParticipant = _context.sent;
            _context.next = 31;
            return tbl_diary.count();

          case 31:
            countDiary = _context.sent;
            res.json({
              data: {
                patient: countPatient,
                psychologist: countPsychologist,
                helpdesk: countHelpdesk,
                session: countSession,
                program: countProgram,
                room: countRoom,
                roomParticipant: countRoomParticipant,
                roomCompleted: countRoomCompleted,
                roomCompletedParticipant: countRoomCompletedParticipant,
                diary: countDiary
              }
            });
            _context.next = 39;
            break;

          case 35:
            _context.prev = 35;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 39:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 35]]);
  }));
  return _getAll.apply(this, arguments);
}