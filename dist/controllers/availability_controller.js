"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listAvailability = listAvailability;
exports.listPsycologistAvailability = listPsycologistAvailability;
exports.storeAvailability = storeAvailability;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_availability = _require.tbl_availability,
    tbl_profile = _require.tbl_profile;

var _require2 = require('luxon'),
    DateTime = _require2.DateTime;

var pushAvilabilityTime = function pushAvilabilityTime(day_array, availability) {
  day_array.push(DateTime.fromFormat(availability.start_hour, 'HH:mm:ss').toFormat('HH:mm'));
};

var getEndHour = function getEndHour(start_hour) {
  return DateTime.fromFormat(start_hour, 'HH:mm').plus({
    hours: 1
  }).toFormat('HH:mm:ss');
};

function listAvailability(_x, _x2) {
  return _listAvailability.apply(this, arguments);
}

function _listAvailability() {
  _listAvailability = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var data, lunes_availability, martes_availability, miercoles_availability, jueves_availability, viernes_availability, sabado_availability, domingo_availability;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return tbl_availability.findAll({
              where: {
                tbl_profile_id: req.user.idProfile
              }
            });

          case 3:
            data = _context.sent;
            lunes_availability = [], martes_availability = [], miercoles_availability = [], jueves_availability = [], viernes_availability = [], sabado_availability = [], domingo_availability = [];
            data.filter(function (availability) {
              // Lunes
              switch (availability.day) {
                case 'Lunes':
                  pushAvilabilityTime(lunes_availability, availability);
                  break;

                case 'Martes':
                  pushAvilabilityTime(martes_availability, availability);
                  break;

                case 'Miercoles':
                  pushAvilabilityTime(miercoles_availability, availability);
                  break;

                case 'Jueves':
                  pushAvilabilityTime(jueves_availability, availability);
                  break;

                case 'Viernes':
                  pushAvilabilityTime(viernes_availability, availability);
                  break;

                case 'Sabado':
                  pushAvilabilityTime(sabado_availability, availability);
                  break;

                case 'Domingo':
                  pushAvilabilityTime(domingo_availability, availability);
                  break;
              }
            });
            res.json({
              data: {
                Lunes: lunes_availability,
                Martes: martes_availability,
                Miercoles: miercoles_availability,
                Jueves: jueves_availability,
                Viernes: viernes_availability,
                Sabado: sabado_availability,
                Domingo: domingo_availability
              }
            });
            _context.next = 13;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 9]]);
  }));
  return _listAvailability.apply(this, arguments);
}

function listPsycologistAvailability(_x3, _x4) {
  return _listPsycologistAvailability.apply(this, arguments);
}

function _listPsycologistAvailability() {
  _listPsycologistAvailability = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return tbl_profile.findAll({
              where: {
                tbl_role_id: 2
              },
              include: {
                model: tbl_availability,
                as: 'Availability'
              }
            });

          case 3:
            data = _context2.sent;
            res.json({
              data: {
                data: data
              }
            });
            _context2.next = 11;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 7]]);
  }));
  return _listPsycologistAvailability.apply(this, arguments);
}

function storeAvailability(_x5, _x6) {
  return _storeAvailability.apply(this, arguments);
} // export async function getAvatar(req,res){
//     try {
//         const {idProfile} = req.params;
//         const profiles = await tbl_profile.findByPk({
//             include: {
//                 model: tbl_avatar,
//                 attributes: [
//                     'id',
//                     'image'
//                 ]
//             },
//             where: {
//                 id: idProfile,
//                 state:1
//             }
//         });
//         res.json({
//             data: profiles
//         });
//     } catch (error) {
//         console.log(error);
//     }
// }


function _storeAvailability() {
  _storeAvailability = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var availability, idProfile, data, lunes_availability, martes_availability, miercoles_availability, jueves_availability, viernes_availability, sabado_availability, domingo_availability, availability_delete_lu, availability_add_lu, p_availability_delete_lu, p_availability_add_lu, availability_delete_ma, availability_add_ma, p_availability_delete_ma, p_availability_add_ma, availability_delete_mi, availability_add_mi, p_availability_delete_mi, p_availability_add_mi, availability_delete_ju, availability_add_ju, p_availability_delete_ju, p_availability_add_ju, availability_delete_vi, availability_add_vi, p_availability_delete_vi, p_availability_add_vi, availability_delete_sa, availability_add_sa, p_availability_delete_sa, p_availability_add_sa, availability_delete_do, availability_add_do, p_availability_delete_do, p_availability_add_do;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            availability = req.body.availability;
            idProfile = req.user.idProfile;
            _context3.next = 5;
            return tbl_availability.findAll({
              where: {
                tbl_profile_id: idProfile
              }
            });

          case 5:
            data = _context3.sent;
            lunes_availability = [], martes_availability = [], miercoles_availability = [], jueves_availability = [], viernes_availability = [], sabado_availability = [], domingo_availability = [];
            data.filter(function (availability) {
              // Lunes
              switch (availability.day) {
                case 'Lunes':
                  pushAvilabilityTime(lunes_availability, availability);
                  break;

                case 'Martes':
                  pushAvilabilityTime(martes_availability, availability);
                  break;

                case 'Miercoles':
                  pushAvilabilityTime(miercoles_availability, availability);
                  break;

                case 'Jueves':
                  pushAvilabilityTime(jueves_availability, availability);
                  break;

                case 'Viernes':
                  pushAvilabilityTime(viernes_availability, availability);
                  break;

                case 'Sabado':
                  pushAvilabilityTime(sabado_availability, availability);
                  break;

                case 'Domingo':
                  pushAvilabilityTime(domingo_availability, availability);
                  break;
              }
            });

            if (!availability) {
              _context3.next = 40;
              break;
            }

            // Lunes
            //      Delete
            availability_delete_lu = lunes_availability.filter(function (e) {
              return !availability.Lunes.includes(e);
            }); //      Add

            availability_add_lu = availability.Lunes.filter(function (e) {
              return !lunes_availability.includes(e);
            });
            p_availability_delete_lu = availability_delete_lu.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Lunes',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_lu = availability_add_lu.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Lunes',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Martes
            //      Delete

            availability_delete_ma = martes_availability.filter(function (e) {
              return !availability.Martes.includes(e);
            }); //      Add

            availability_add_ma = availability.Martes.filter(function (e) {
              return !martes_availability.includes(e);
            });
            p_availability_delete_ma = availability_delete_ma.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Martes',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_ma = availability_add_ma.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Martes',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Miercoles
            //      Delete

            availability_delete_mi = miercoles_availability.filter(function (e) {
              return !availability.Miercoles.includes(e);
            }); //      Add

            availability_add_mi = availability.Miercoles.filter(function (e) {
              return !miercoles_availability.includes(e);
            });
            p_availability_delete_mi = availability_delete_mi.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Miercoles',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_mi = availability_add_mi.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Miercoles',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Jueves
            //      Delete

            availability_delete_ju = jueves_availability.filter(function (e) {
              return !availability.Jueves.includes(e);
            }); //      Add

            availability_add_ju = availability.Jueves.filter(function (e) {
              return !jueves_availability.includes(e);
            });
            p_availability_delete_ju = availability_delete_ju.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Jueves',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_ju = availability_add_ju.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Jueves',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Viernes
            //      Delete

            availability_delete_vi = viernes_availability.filter(function (e) {
              return !availability.Viernes.includes(e);
            }); //      Add

            availability_add_vi = availability.Viernes.filter(function (e) {
              return !viernes_availability.includes(e);
            });
            p_availability_delete_vi = availability_delete_vi.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Viernes',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_vi = availability_add_vi.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Viernes',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Sabado
            //      Delete

            availability_delete_sa = sabado_availability.filter(function (e) {
              return !availability.Sabado.includes(e);
            }); //      Add

            availability_add_sa = availability.Sabado.filter(function (e) {
              return !sabado_availability.includes(e);
            });
            p_availability_delete_sa = availability_delete_sa.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Sabado',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_sa = availability_add_sa.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Sabado',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            }); // Domingo
            //      Delete

            availability_delete_do = domingo_availability.filter(function (e) {
              return !availability.Domingo.includes(e);
            }); //      Add

            availability_add_do = availability.Domingo.filter(function (e) {
              return !domingo_availability.includes(e);
            });
            p_availability_delete_do = availability_delete_do.map(function (start_hour) {
              return tbl_availability.destroy({
                where: {
                  tbl_profile_id: idProfile,
                  day: 'Domingo',
                  start_hour: start_hour
                }
              });
            });
            p_availability_add_do = availability_add_do.map(function (start_hour) {
              return tbl_availability.create({
                tbl_profile_id: idProfile,
                day: 'Domingo',
                start_hour: start_hour,
                end_hour: getEndHour(start_hour)
              });
            });
            _context3.next = 39;
            return Promise.all([p_availability_delete_lu, p_availability_add_lu, p_availability_delete_ma, p_availability_add_ma, p_availability_delete_mi, p_availability_add_mi, p_availability_delete_ju, p_availability_add_ju, p_availability_delete_vi, p_availability_add_vi, p_availability_delete_sa, p_availability_add_sa, p_availability_delete_do, p_availability_add_do]);

          case 39:
            return _context3.abrupt("return", res.json({
              message: 'Availability stored'
            }));

          case 40:
            res.status(400).json({
              message: 'Empty data'
            });
            _context3.next = 47;
            break;

          case 43:
            _context3.prev = 43;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 47:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 43]]);
  }));
  return _storeAvailability.apply(this, arguments);
}