"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTrainings = getTrainings;
exports.getTrainingsFilters = getTrainingsFilters;
exports.getTraining = getTraining;
exports.createTraining = createTraining;
exports.createTrainingVideo = createTrainingVideo;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_media_profile = _require.tbl_media_profile,
    tbl_profile = _require.tbl_profile,
    tbl_category_medias = _require.tbl_category_medias,
    tbl_medias = _require.tbl_medias,
    tbl_method = _require.tbl_method,
    tbl_media_role = _require.tbl_media_role;

var _require2 = require('sequelize'),
    Op = _require2.Op;

var url = require('url');

function getTrainings(_x, _x2) {
  return _getTrainings.apply(this, arguments);
}

function _getTrainings() {
  _getTrainings = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var whereQuery, mediaRoleWhereQuery, offset, limit, role, type, category, type_cat_media, method, search, mediaWhereQuery, categoryMediaWhereQuery, favorite, archived, requiredProfile, _yield$tbl_media_prof, count, trainings, urlPath, previousPageUrl, nextPageUrl, p_query_offset;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            whereQuery = {}, mediaRoleWhereQuery = {}, offset = 0, limit = 20, role = parseInt(req.query.role), type = parseInt(req.query.type), category = parseInt(req.query.category), type_cat_media = parseInt(req.query.type_cat_media), method = parseInt(req.query.method), search = req.query.search, mediaWhereQuery = {
              state: 1
            }, categoryMediaWhereQuery = {
              state: 1
            }, favorite = req.query.favorite == true, archived = req.query.archived == true, requiredProfile = true; // if( req.user.idRole == 2 ){
            //     profileWhereQuery.tbl_user_id = req.user.id
            // } else if( req.user.idRole != 3 ){
            //     return res.status(401).send('Unauthorized')
            // }

            if (role) {
              if (role == 1) {
                mediaRoleWhereQuery = {
                  tbl_role_id: 1
                };
              } else if (role == 2) {
                mediaRoleWhereQuery = {
                  tbl_role_id: 2
                };
              } else {
                mediaRoleWhereQuery = {
                  tbl_role_id: 3
                };
              }
            }

            if (req.query.offset) {
              offset = parseInt(req.query.offset);
            }

            if (req.query.limit) {
              limit = parseInt(req.query.limit);
            } // audio or video


            if (type) {
              mediaWhereQuery.type = type;
            }

            if (type_cat_media) {
              categoryMediaWhereQuery.type = type_cat_media;
            }

            if (category) {
              mediaWhereQuery.tbl_category_media_id = category;
            }

            if (method) {
              mediaWhereQuery.tbl_method_id = method;
            }

            if (search) {
              mediaWhereQuery.name = _defineProperty({}, Op.iLike, '%' + search + '%');
            }

            if (favorite) {
              whereQuery = {
                favorite: 1
              };
            }

            if (archived) {
              mediaWhereQuery.state = 0;
            }

            _context.next = 14;
            return tbl_media_profile.findAndCountAll({
              //where: whereQuery,
              attributes: ['id', 'favorite', 'watch_later', 'done'],
              include: [{
                model: tbl_media_role,
                as: 'MediaRole',
                where: mediaRoleWhereQuery,
                attributes: []
              }, {
                model: tbl_profile,
                as: 'Profile',
                where: {
                  tbl_user_id: req.user.id,
                  state: 1
                },
                attributes: [],
                required: requiredProfile
              }, {
                model: tbl_medias,
                as: 'Media',
                where: mediaWhereQuery,
                attributes: ['id', 'file', 'image', 'duration', 'name', 'type', 'description'],
                include: [{
                  model: tbl_category_medias,
                  as: 'category_media',
                  where: categoryMediaWhereQuery,
                  attributes: ['name', 'type', 'state']
                }, {
                  model: tbl_method,
                  as: 'method',
                  attributes: ['name', 'image', 'state']
                }]
              }],
              limit: limit,
              offset: offset,
              distinct: true
            });

          case 14:
            _yield$tbl_media_prof = _context.sent;
            count = _yield$tbl_media_prof.count;
            trainings = _yield$tbl_media_prof.rows;

            if (!(trainings.length == 0)) {
              _context.next = 19;
              break;
            }

            return _context.abrupt("return", res.status(200).json({
              message: 'No trainings were found',
              data: []
            }));

          case 19:
            urlPath = req.baseUrl + req.path;

            if (urlPath.substring(urlPath.length - 1) == '/') {
              urlPath = urlPath.substring(0, urlPath.length - 1);
            }

            previousPageUrl = null, nextPageUrl = null;

            if (offset > 0) {
              p_query_offset = offset - limit;

              if (offset - limit < 0) {
                p_query_offset = 0;
              }

              previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: p_query_offset,
                  limit: limit
                }
              });
            }

            if (offset + limit < count) {
              nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                  offset: offset + limit,
                  limit: limit
                }
              });
            }

            res.json({
              message: 'Trainings found',
              count: count,
              previous: previousPageUrl,
              next: nextPageUrl,
              limit: limit,
              offset: offset,
              data: trainings
            });
            _context.next = 31;
            break;

          case 27:
            _context.prev = 27;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 31:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 27]]);
  }));
  return _getTrainings.apply(this, arguments);
}

function getTrainingsFilters(_x3, _x4) {
  return _getTrainingsFilters.apply(this, arguments);
}

function _getTrainingsFilters() {
  _getTrainingsFilters = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var options, p_cat_medias, p_methods, media_types, category_media_types, _yield$Promise$all, _yield$Promise$all2, cat_medias, methods;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            options = {
              where: {
                state: 1
              },
              attributes: ['id', 'name']
            };
            p_cat_medias = tbl_category_medias.findAll(options);
            p_methods = tbl_method.findAll(options);
            media_types = [{
              id: 1,
              name: 'video'
            }, {
              id: 2,
              name: 'audio'
            }];
            category_media_types = [{
              id: 1,
              name: 'capacitaciones'
            }, {
              id: 2,
              name: 'libre'
            }];
            _context2.next = 8;
            return Promise.all([p_cat_medias, p_methods]);

          case 8:
            _yield$Promise$all = _context2.sent;
            _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 2);
            cat_medias = _yield$Promise$all2[0];
            methods = _yield$Promise$all2[1];
            res.json({
              message: 'Filters found',
              filters: {
                categories: cat_medias,
                methods: methods,
                media_types: media_types,
                category_media_types: category_media_types
              }
            });
            _context2.next = 19;
            break;

          case 15:
            _context2.prev = 15;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 19:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 15]]);
  }));
  return _getTrainingsFilters.apply(this, arguments);
}

function getTraining(_x5, _x6) {
  return _getTraining.apply(this, arguments);
}

function _getTraining() {
  _getTraining = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var training;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return tbl_media_profile.findByPk(req.params.id, {
              attributes: ['done'],
              include: [{
                model: tbl_profile,
                as: 'Profile',
                where: {
                  tbl_user_id: req.user.id,
                  state: 1
                },
                attributes: []
              }, {
                model: tbl_medias,
                as: 'Media',
                attributes: [//'id',
                'id', ['tbl_category_media_id', 'idCategory'], ['tbl_method_id', 'idMethod'], 'name', 'type', 'file', 'image', 'duration', 'description', 'points', 'state' //'type'
                ],
                include: [{
                  model: tbl_category_medias,
                  attributes: ['name']
                }, {
                  model: tbl_media_role,
                  as: 'Role',
                  attributes: [['tbl_role_id', 'id']]
                }, {
                  model: tbl_method,
                  as: 'method',
                  attributes: ['name']
                }]
              }]
            });

          case 3:
            training = _context3.sent;

            if (training) {
              _context3.next = 6;
              break;
            }

            return _context3.abrupt("return", res.status(404).json({
              message: 'Training not found'
            }));

          case 6:
            res.json({
              data: training
            });
            _context3.next = 13;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _getTraining.apply(this, arguments);
}

function createTraining(_x7, _x8) {
  return _createTraining.apply(this, arguments);
}

function _createTraining() {
  _createTraining = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _req$body, category_media_id, method_id, name, duration, description, points, role_id, media;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _req$body = req.body, category_media_id = _req$body.category_media_id, method_id = _req$body.method_id, name = _req$body.name, duration = _req$body.duration, description = _req$body.description, points = _req$body.points, role_id = _req$body.role_id;
            _context4.next = 4;
            return tbl_medias.create({
              tbl_category_media_id: category_media_id,
              tbl_method_id: method_id,
              name: name,
              type: 2,
              file: req.files.file[0].location,
              image: req.files.image[0].location,
              duration: duration,
              description: description,
              points: points
            });

          case 4:
            media = _context4.sent;

            if (!media) {
              _context4.next = 11;
              break;
            }

            _context4.next = 8;
            return tbl_media_role.create({
              tbl_media_id: media.dataValues.id,
              tbl_role_id: role_id,
              state: 1
            });

          case 8:
            res.json({
              message: 'Training created'
            });
            _context4.next = 13;
            break;

          case 11:
            console.log(error);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
            _context4.next = 19;
            break;

          case 15:
            _context4.prev = 15;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 19:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 15]]);
  }));
  return _createTraining.apply(this, arguments);
}

function createTrainingVideo(_x9, _x10) {
  return _createTrainingVideo.apply(this, arguments);
}
/*
SELECT "tbl_media_profile"."id", "tbl_media_profile"."done", "tbl_medium"."id" AS "tbl_medium.id", "tbl_medium"."duration" AS "tbl_medium.duration", "tbl_medium"."name" AS "tbl_medium.name", "tbl_medium"."link" AS "tbl_medium.link", "tbl_medium"."type" AS "tbl_medium.type"
FROM "tbl_media_profiles" AS "tbl_media_profile"
INNER JOIN "tbl_profiles" AS "tbl_profile" ON "tbl_media_profile"."tbl_profile_id" = "tbl_profile"."id" AND "tbl_profile"."tbl_user_id" = 1
LEFT OUTER JOIN "tbl_media" AS "tbl_medium" ON "tbl_media_profile"."tbl_media_id" = "tbl_medium"."id";
*/


function _createTrainingVideo() {
  _createTrainingVideo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _req$body2, category_media_id, method_id, name, file, image, duration, description, points, role_id, media;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _req$body2 = req.body, category_media_id = _req$body2.category_media_id, method_id = _req$body2.method_id, name = _req$body2.name, file = _req$body2.file, image = _req$body2.image, duration = _req$body2.duration, description = _req$body2.description, points = _req$body2.points, role_id = _req$body2.role_id;
            _context5.next = 4;
            return tbl_medias.create({
              tbl_category_media_id: category_media_id,
              tbl_method_id: method_id,
              name: name,
              type: 1,
              file: file,
              image: image,
              duration: duration,
              description: description,
              points: points
            });

          case 4:
            media = _context5.sent;

            if (!media) {
              _context5.next = 11;
              break;
            }

            _context5.next = 8;
            return tbl_media_role.create({
              tbl_media_id: media.dataValues.id,
              tbl_role_id: role_id,
              state: 1
            });

          case 8:
            res.json({
              message: 'Training created'
            });
            _context5.next = 13;
            break;

          case 11:
            console.log(error);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 13:
            _context5.next = 19;
            break;

          case 15:
            _context5.prev = 15;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 19:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 15]]);
  }));
  return _createTrainingVideo.apply(this, arguments);
}