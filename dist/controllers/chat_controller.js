"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getChat = getChat;
exports.getChatSession = getChatSession;
exports.joinHelpdesk = joinHelpdesk;
exports.getHelpdesk = getHelpdesk;
exports.getAllHelpdesk = getAllHelpdesk;
exports.getChatPrivate = getChatPrivate;

var _psychologist_routes = _interopRequireDefault(require("../routes/psychologist_routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_chat = _require.tbl_chat,
    tbl_chat_participants = _require.tbl_chat_participants,
    tbl_chat_participants_message = _require.tbl_chat_participants_message,
    tbl_profile = _require.tbl_profile,
    tbl_session = _require.tbl_session,
    tbl_avatar = _require.tbl_avatar,
    tbl_room = _require.tbl_room,
    tbl_room_participants = _require.tbl_room_participants;

var Sequelize = require('sequelize');

var Op = Sequelize.Op;

function getChatHelpdesk(_x) {
  return _getChatHelpdesk.apply(this, arguments);
}

function _getChatHelpdesk() {
  _getChatHelpdesk = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(idProfile) {
    var chat, messages;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return tbl_chat.findOne({
              attributes: ['id'],
              where: {
                helpdesk: 1
              },
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: idProfile
                }
              }]
            });

          case 2:
            chat = _context.sent;

            if (!(chat != null)) {
              _context.next = 10;
              break;
            }

            _context.next = 6;
            return tbl_chat_participants_message.findAll({
              attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                attributes: [],
                include: [{
                  model: tbl_chat,
                  as: 'Chat',
                  where: {
                    id: chat.id
                  }
                }, {
                  model: tbl_profile,
                  as: 'Profile'
                }],
                where: {
                  tbl_chat_id: chat.id
                }
              }],
              order: [['id', 'ASC']]
            });

          case 6:
            messages = _context.sent;
            return _context.abrupt("return", {
              id: chat.id,
              ChatParticipant: chat.ChatParticipant,
              messages: messages
            });

          case 10:
            return _context.abrupt("return", null);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getChatHelpdesk.apply(this, arguments);
}

function getChat(_x2, _x3) {
  return _getChat.apply(this, arguments);
}

function _getChat() {
  _getChat = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var idProfile, idChat, chat, messages, datos;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            idProfile = req.user.idProfile;
            idChat = req.params.id;
            _context2.next = 5;
            return tbl_chat.findOne({
              attributes: ['id'],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: idProfile
                }
              }],
              where: {
                id: idChat
              }
            });

          case 5:
            chat = _context2.sent;
            _context2.next = 8;
            return tbl_chat_participants_message.findAll({
              attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                include: [{
                  model: tbl_chat,
                  as: 'Chat',
                  where: {
                    id: idChat
                  },
                  attributes: []
                }, {
                  model: tbl_profile,
                  as: 'Profile',
                  attributes: []
                }],
                where: {
                  tbl_chat_id: idChat
                },
                attributes: []
              }],
              order: [['id', 'ASC']]
            });

          case 8:
            messages = _context2.sent;
            datos = {
              id: idChat,
              ChatParticipant: chat.ChatParticipant,
              messages: messages
            };
            res.json({
              data: datos
            });
            _context2.next = 17;
            break;

          case 13:
            _context2.prev = 13;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 13]]);
  }));
  return _getChat.apply(this, arguments);
}

function getChatSession(_x4, _x5) {
  return _getChatSession.apply(this, arguments);
}

function _getChatSession() {
  _getChatSession = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var idProfile, idSession, session, chat, messages, _chat, chatParticipant, chatParticipantPsychologist;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            idProfile = req.user.idProfile;
            idSession = req.params.id;
            _context3.next = 5;
            return tbl_session.findOne({
              where: {
                id: idSession
              }
            });

          case 5:
            session = _context3.sent;
            _context3.next = 8;
            return tbl_chat.findOne({
              attributes: ['id'],
              where: {
                helpdesk: 0,
                tbl_room_id: null
              },
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: idProfile
                }
              }, {
                model: tbl_chat_participants,
                as: 'ChatParticipantPsychologist',
                attributes: [],
                where: {
                  tbl_profile_id: session.tbl_profile_psychologist_id
                }
              }]
            });

          case 8:
            chat = _context3.sent;
            messages = [];

            if (!(chat === null)) {
              _context3.next = 22;
              break;
            }

            _context3.next = 13;
            return tbl_chat.create({
              start: Date.now(),
              helpdesk: 0
            });

          case 13:
            _chat = _context3.sent;
            _context3.next = 16;
            return tbl_chat_participants.create({
              tbl_chat_id: _chat.id,
              tbl_profile_id: idProfile
            });

          case 16:
            chatParticipant = _context3.sent;
            _context3.next = 19;
            return tbl_chat_participants.create({
              tbl_chat_id: _chat.id,
              tbl_profile_id: session.tbl_profile_psychologist_id
            });

          case 19:
            chatParticipantPsychologist = _context3.sent;
            _context3.next = 25;
            break;

          case 22:
            _context3.next = 24;
            return tbl_chat_participants_message.findAll({
              attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type']],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                attributes: [],
                include: [{
                  model: tbl_chat,
                  as: 'Chat',
                  where: {
                    id: chat.id
                  }
                }, {
                  model: tbl_profile,
                  as: 'Profile'
                }],
                where: {
                  tbl_chat_id: chat.id
                }
              }]
            });

          case 24:
            messages = _context3.sent;

          case 25:
            res.json({
              data: {
                id: chat.id,
                ChatParticipant: chat.ChatParticipant,
                messages: messages
              }
            });
            _context3.next = 32;
            break;

          case 28:
            _context3.prev = 28;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 32:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 28]]);
  }));
  return _getChatSession.apply(this, arguments);
}

function joinHelpdesk(_x6, _x7) {
  return _joinHelpdesk.apply(this, arguments);
}

function _joinHelpdesk() {
  _joinHelpdesk = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var id, chatParticipantHelpdesk, chat, messages;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            id = req.body.id;
            _context4.next = 3;
            return tbl_chat_participants.create({
              tbl_chat_id: id,
              tbl_profile_id: req.user.idProfile
            });

          case 3:
            chatParticipantHelpdesk = _context4.sent;
            _context4.next = 6;
            return tbl_chat.findOne({
              attributes: ['id'],
              where: {
                id: id
              },
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: req.user.idProfile
                }
              }]
            });

          case 6:
            chat = _context4.sent;
            _context4.next = 9;
            return tbl_chat_participants_message.findAll({
              attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chat.ChatParticipant.id + ' THEN \'in\' ELSE \'out\' END'), 'type']],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                attributes: [],
                include: [{
                  model: tbl_chat,
                  as: 'Chat',
                  where: {
                    id: chat.id
                  }
                }, {
                  model: tbl_profile,
                  as: 'Profile'
                }],
                where: {
                  tbl_chat_id: chat.id
                }
              }]
            });

          case 9:
            messages = _context4.sent;
            res.json({
              data: {
                id: chat.id,
                ChatParticipant: chat.ChatParticipant,
                messages: messages
              }
            });

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _joinHelpdesk.apply(this, arguments);
}

function getHelpdesk(_x8, _x9) {
  return _getHelpdesk.apply(this, arguments);
}

function _getHelpdesk() {
  _getHelpdesk = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var data, chat, chatParticipant;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return getChatHelpdesk(req.user.idProfile);

          case 3:
            data = _context5.sent;

            if (!(data == null)) {
              _context5.next = 14;
              break;
            }

            _context5.next = 7;
            return tbl_chat.create({
              start: Date.now(),
              helpdesk: 1
            });

          case 7:
            chat = _context5.sent;
            _context5.next = 10;
            return tbl_chat_participants.create({
              tbl_chat_id: chat.id,
              tbl_profile_id: req.user.idProfile
            });

          case 10:
            chatParticipant = _context5.sent;
            _context5.next = 13;
            return getChatHelpdesk(req.user.idProfile);

          case 13:
            data = _context5.sent;

          case 14:
            res.json({
              data: data
            });
            _context5.next = 21;
            break;

          case 17:
            _context5.prev = 17;
            _context5.t0 = _context5["catch"](0);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 21:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 17]]);
  }));
  return _getHelpdesk.apply(this, arguments);
}

function getAllHelpdesk(_x10, _x11) {
  return _getAllHelpdesk.apply(this, arguments);
}

function _getAllHelpdesk() {
  _getAllHelpdesk = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var where, chats;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            where = {
              helpdesk: 1
            };

            if (req.query.reference !== '') {
              where = [Sequelize.where(Sequelize.literal('"ChatParticipant->Profile"."firstname"'), _defineProperty({}, Op.iLike, '%' + req.query.reference + '%')), Sequelize.where(Sequelize.literal('"tbl_chat"."helpdesk"'), _defineProperty({}, Op.eq, 1))];
            }

            _context7.next = 5;
            return tbl_chat.findAll({
              attributes: ['id', 'start', [Sequelize.literal('"ChatParticipantHelpdesk->Profile"."id"'), 'helpdesker']],
              where: where,
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id'],
                include: {
                  model: tbl_profile,
                  as: 'Profile',
                  attributes: ['id', 'tbl_role_id', 'firstname'],
                  where: {
                    tbl_role_id: _defineProperty({}, Op["in"], [1, 2])
                  },
                  include: {
                    model: tbl_avatar,
                    as: 'Avatar',
                    attributes: ['image']
                  }
                }
              }, {
                model: tbl_chat_participants,
                as: 'ChatParticipantHelpdesk',
                attributes: ['id'],
                include: {
                  model: tbl_profile,
                  as: 'Profile',
                  attributes: [],
                  where: {
                    tbl_role_id: _defineProperty({}, Op.notIn, [1, 2])
                  }
                },
                required: false
              }],
              group: ['"tbl_chat"."id"', '"ChatParticipantHelpdesk->Profile"."id"', '"ChatParticipant"."id"', '"ChatParticipant->Profile"."id"', '"ChatParticipant->Profile->Avatar"."id"', '"ChatParticipantHelpdesk"."id"'],
              having: Sequelize.where(Sequelize.literal('"ChatParticipantHelpdesk->Profile"."id"'), _defineProperty({}, Op.or, [_defineProperty({}, Op.eq, null), _defineProperty({}, Op.eq, req.user.idProfile)]))
            });

          case 5:
            chats = _context7.sent;
            _context7.next = 8;
            return Promise.all(chats.map( /*#__PURE__*/function () {
              var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(item, key) {
                var messages;
                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                  while (1) {
                    switch (_context6.prev = _context6.next) {
                      case 0:
                        _context6.next = 2;
                        return tbl_chat_participants_message.findOne({
                          attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', 'created_at', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + item.dataValues.ChatParticipant.id + ' THEN \'in\' ELSE \'out\' END'), 'type']],
                          include: [{
                            model: tbl_chat_participants,
                            as: 'ChatPaticipants',
                            attributes: [],
                            include: [{
                              model: tbl_chat,
                              as: 'Chat',
                              where: {
                                id: item.dataValues.id
                              }
                            }, {
                              model: tbl_profile,
                              as: 'Profile'
                            }],
                            where: {
                              tbl_chat_id: item.dataValues.id
                            }
                          }],
                          order: [['id', 'DESC']]
                        });

                      case 2:
                        messages = _context6.sent;
                        item.dataValues.lastMessage = {};

                        if (messages !== null) {
                          item.dataValues.lastMessage = messages;
                          item.dataValues.updated = messages.dataValues.created_at;
                        }

                        return _context6.abrupt("return", item);

                      case 6:
                      case "end":
                        return _context6.stop();
                    }
                  }
                }, _callee6);
              }));

              return function (_x14, _x15) {
                return _ref3.apply(this, arguments);
              };
            }())).then(function (chats) {
              chats.sort(compare);
              res.json({
                data: chats
              });
            });

          case 8:
            _context7.next = 14;
            break;

          case 10:
            _context7.prev = 10;
            _context7.t0 = _context7["catch"](0);
            console.log(_context7.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 14:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 10]]);
  }));
  return _getAllHelpdesk.apply(this, arguments);
}

function getChatPrivate(_x12, _x13) {
  return _getChatPrivate.apply(this, arguments);
}

function _getChatPrivate() {
  _getChatPrivate = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res) {
    var room, include, chat, chatParticipant, data, chatResponse, messages;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            if (!(req.query.idRoom > 0 && req.query.idPatient > 0)) {
              _context9.next = 39;
              break;
            }

            _context9.prev = 1;
            _context9.next = 4;
            return tbl_room.findOne({
              where: {
                id: req.query.idRoom
              },
              include: {
                model: tbl_room_participants,
                attributes: ['id', ['tbl_profile_participant_id', 'idProfile']],
                include: {
                  model: tbl_profile,
                  as: 'Profile_participant',
                  where: {
                    'tbl_role_id': 2
                  },
                  attributes: []
                }
              },
              attributes: ['id', 'title']
            });

          case 4:
            room = _context9.sent;

            if (!(room != null)) {
              _context9.next = 31;
              break;
            }

            include = [{
              model: tbl_chat_participants,
              as: 'ChatParticipant',
              attributes: ['id'],
              where: {
                tbl_profile_id: req.query.idPatient
              }
            }];

            if (room.tbl_room_participants[0]) {
              include.push({
                model: tbl_chat_participants,
                as: 'ChatParticipantPsychologist',
                attributes: ['id'],
                where: {
                  tbl_profile_id: room.tbl_room_participants[0].dataValues.idProfile
                }
              });
            }

            if (room.tbl_room_participants[1]) {
              include.push({
                model: tbl_chat_participants,
                as: 'ChatParticipantPsychologistAssistant',
                attributes: ['id'],
                where: {
                  tbl_profile_id: room.tbl_room_participants[1].dataValues.idProfile
                }
              });
            }

            _context9.next = 11;
            return tbl_chat.findOne({
              attributes: ['id'],
              where: {
                helpdesk: 0,
                tbl_room_id: null,
                state: 1
              },
              include: include
            });

          case 11:
            chat = _context9.sent;

            if (!(chat == null)) {
              _context9.next = 22;
              break;
            }

            _context9.next = 15;
            return tbl_chat.create({
              start: Date.now(),
              helpdesk: 0
            });

          case 15:
            chat = _context9.sent;
            _context9.next = 18;
            return tbl_chat_participants.create({
              tbl_chat_id: chat.id,
              tbl_profile_id: req.query.idPatient
            });

          case 18:
            chatParticipant = _context9.sent;
            _context9.next = 21;
            return Promise.all(room.tbl_room_participants.map( /*#__PURE__*/function () {
              var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(item, key) {
                var chatParticipant;
                return regeneratorRuntime.wrap(function _callee8$(_context8) {
                  while (1) {
                    switch (_context8.prev = _context8.next) {
                      case 0:
                        _context8.next = 2;
                        return tbl_chat_participants.create({
                          tbl_chat_id: chat.id,
                          tbl_profile_id: item.dataValues.idProfile
                        });

                      case 2:
                        chatParticipant = _context8.sent;
                        return _context8.abrupt("return", item);

                      case 4:
                      case "end":
                        return _context8.stop();
                    }
                  }
                }, _callee8);
              }));

              return function (_x16, _x17) {
                return _ref4.apply(this, arguments);
              };
            }()));

          case 21:
            data = _context9.sent;

          case 22:
            _context9.next = 24;
            return tbl_chat.findOne({
              attributes: ['id'],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatParticipant',
                attributes: ['id', ['tbl_chat_id', 'idChat'], ['tbl_profile_id', 'idProfile']],
                where: {
                  tbl_profile_id: req.query.idPatient
                }
              }],
              where: {
                id: chat.id
              }
            });

          case 24:
            chatResponse = _context9.sent;
            _context9.next = 27;
            return tbl_chat_participants_message.findAll({
              attributes: ['id', ['tbl_chat_participants_id', 'idChatParticipant'], 'message', [Sequelize.literal('"ChatPaticipants->Profile"."firstname"'), 'author'], [Sequelize.literal('CASE WHEN "tbl_chat_participants_message"."tbl_chat_participants_id" = ' + chatResponse.ChatParticipant.id + ' THEN \'out\' ELSE \'in\' END'), 'type'], 'created_at'],
              include: [{
                model: tbl_chat_participants,
                as: 'ChatPaticipants',
                include: [{
                  model: tbl_chat,
                  as: 'Chat',
                  where: {
                    id: chat.id
                  },
                  attributes: []
                }, {
                  model: tbl_profile,
                  as: 'Profile',
                  attributes: []
                }],
                where: {
                  tbl_chat_id: chat.id
                },
                attributes: []
              }],
              order: [['id', 'ASC']]
            });

          case 27:
            messages = _context9.sent;
            res.json({
              data: {
                id: chatResponse.id,
                ChatParticipant: chatResponse.ChatParticipant,
                messages: messages
              }
            });
            _context9.next = 32;
            break;

          case 31:
            res.status(404).json({
              message: 'Room not found'
            });

          case 32:
            _context9.next = 37;
            break;

          case 34:
            _context9.prev = 34;
            _context9.t0 = _context9["catch"](1);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 37:
            _context9.next = 40;
            break;

          case 39:
            res.status(500).json({
              message: 'Nothing to search...'
            });

          case 40:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[1, 34]]);
  }));
  return _getChatPrivate.apply(this, arguments);
}

function compare(a, b) {
  if (a.dataValues.updated < b.dataValues.updated) {
    return 1;
  }

  if (a.dataValues.updated > b.dataValues.updated) {
    return -1;
  }

  return 0;
}