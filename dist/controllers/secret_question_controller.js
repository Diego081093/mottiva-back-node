"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSecretQuestion = getSecretQuestion;
exports.postValidateSecretQuestion = postValidateSecretQuestion;
exports.getListSecretQuestion = getListSecretQuestion;
exports.createSecretQuestion = createSecretQuestion;
exports.updateSecretQuestion = updateSecretQuestion;
exports.deleteSecretQuestion = deleteSecretQuestion;

var _token_controller = require("./token_controller");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models'),
    tbl_secret_question = _require.tbl_secret_question,
    tbl_user = _require.tbl_user,
    tbl_user_secret_question = _require.tbl_user_secret_question,
    tbl_profile = _require.tbl_profile,
    tbl_avatar = _require.tbl_avatar;

function getSecretQuestion(_x, _x2) {
  return _getSecretQuestion.apply(this, arguments);
}

function _getSecretQuestion() {
  _getSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var dni, user, user_id, user_secret_question, secret_question_id, secretquestion, question;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            dni = req.params.dni;
            _context.next = 4;
            return tbl_user.findOne({
              where: {
                username: dni,
                state: 1
              }
            });

          case 4:
            user = _context.sent;
            user_id = user.id;
            _context.next = 8;
            return tbl_user_secret_question.findOne({
              where: {
                tbl_user_id: user_id,
                state: 1
              }
            });

          case 8:
            user_secret_question = _context.sent;
            secret_question_id = user_secret_question.tbl_secret_question_id;
            _context.next = 12;
            return tbl_secret_question.findOne({
              where: {
                id: secret_question_id,
                state: 1
              }
            });

          case 12:
            secretquestion = _context.sent;
            question = secretquestion.question;
            res.json({
              data: question
            });
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _getSecretQuestion.apply(this, arguments);
}

function postValidateSecretQuestion(_x3, _x4) {
  return _postValidateSecretQuestion.apply(this, arguments);
}

function _postValidateSecretQuestion() {
  _postValidateSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, dni, answer, user, usuarioID, user_secret_question, secret_question_id, secretquestion, userName, token;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _req$body = req.body, dni = _req$body.dni, answer = _req$body.answer;
            _context2.next = 4;
            return tbl_user.findOne({
              where: {
                username: dni
              }
            });

          case 4:
            user = _context2.sent;
            usuarioID = user.id;
            _context2.next = 8;
            return tbl_user_secret_question.findOne({
              where: {
                tbl_user_id: usuarioID,
                state: 1
              }
            });

          case 8:
            user_secret_question = _context2.sent;
            secret_question_id = user_secret_question.tbl_secret_question_id;
            _context2.next = 12;
            return tbl_user_secret_question.findOne({
              where: {
                tbl_secret_question_id: secret_question_id,
                answer: answer,
                state: 1
              }
            });

          case 12:
            secretquestion = _context2.sent;
            userName = user.username;
            console.log(usuarioID);

            if (!secretquestion) {
              _context2.next = 20;
              break;
            }

            _context2.next = 18;
            return (0, _token_controller.createToken)(req, res, usuarioID, userName);

          case 18:
            token = _context2.sent;
            return _context2.abrupt("return", res.json({
              message: "Answer Correct!",
              data: {
                token: token
              }
            }));

          case 20:
            return _context2.abrupt("return", res.status(400).json({
              message: "Answer Incorrect!"
            }));

          case 23:
            _context2.prev = 23;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 27:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 23]]);
  }));
  return _postValidateSecretQuestion.apply(this, arguments);
}

function getListSecretQuestion(_x5, _x6) {
  return _getListSecretQuestion.apply(this, arguments);
}

function _getListSecretQuestion() {
  _getListSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var questions;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return tbl_secret_question.findAll({
              where: {
                state: 1
              }
            });

          case 3:
            questions = _context3.sent;
            res.json({
              data: questions
            });
            _context3.next = 11;
            break;

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 7]]);
  }));
  return _getListSecretQuestion.apply(this, arguments);
}

function createSecretQuestion(_x7, _x8) {
  return _createSecretQuestion.apply(this, arguments);
}

function _createSecretQuestion() {
  _createSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var question, newSecretQuestion;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            question = req.body.question;
            _context4.prev = 1;
            _context4.next = 4;
            return tbl_secret_question.create({
              question: question
            });

          case 4:
            newSecretQuestion = _context4.sent;

            if (newSecretQuestion) {
              res.json({
                message: 'User secret question created',
                data: newSecretQuestion
              });
            }

            _context4.next = 12;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](1);
            console.log(_context4.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[1, 8]]);
  }));
  return _createSecretQuestion.apply(this, arguments);
}

function updateSecretQuestion(_x9, _x10) {
  return _updateSecretQuestion.apply(this, arguments);
}

function _updateSecretQuestion() {
  _updateSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _req$body2, question, state, SecretQuestion;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _req$body2 = req.body, question = _req$body2.question, state = _req$body2.state;
            _context5.prev = 1;
            _context5.next = 4;
            return tbl_secret_question.findByPk(req.params.id);

          case 4:
            SecretQuestion = _context5.sent;

            if (!SecretQuestion) {
              _context5.next = 9;
              break;
            }

            _context5.next = 8;
            return SecretQuestion.update({
              question: question,
              state: state
            });

          case 8:
            return _context5.abrupt("return", res.json({
              message: 'User secret question updated',
              data: SecretQuestion
            }));

          case 9:
            return _context5.abrupt("return", res.status(404).json({
              message: 'User secret question not found'
            }));

          case 12:
            _context5.prev = 12;
            _context5.t0 = _context5["catch"](1);
            console.log(_context5.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 16:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 12]]);
  }));
  return _updateSecretQuestion.apply(this, arguments);
}

function deleteSecretQuestion(_x11, _x12) {
  return _deleteSecretQuestion.apply(this, arguments);
}

function _deleteSecretQuestion() {
  _deleteSecretQuestion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _deleteSecretQuestion2;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _context6.next = 3;
            return tbl_secret_question.destroy({
              where: {
                id: req.params.id
              }
            });

          case 3:
            _deleteSecretQuestion2 = _context6.sent;
            res.json({
              message: 'User secret question deleted successfully',
              count: _deleteSecretQuestion2
            });
            _context6.next = 11;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);
            res.status(500).json({
              message: 'Something went wrong'
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 7]]);
  }));
  return _deleteSecretQuestion.apply(this, arguments);
}