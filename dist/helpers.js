"use strict";

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('./config/aws'),
    s3 = _require.s3,
    Bucket = _require.Bucket;

var slugify = require('slugify');

var multer = require('multer');

var multerS3 = require('multer-s3');

var _require2 = require('luxon'),
    DateTime = _require2.DateTime;

var _require3 = require('uuid'),
    uuidv4 = _require3.v4;

exports.MottivaConstants = {
  PATIENT_ROLE: 1,
  PSYCHOLOGIST_ROLE: 2,
  HELPDESK_ROLE: 3,
  SESSION_POINTS: 300
};

exports.isValidPassword = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(password, passwordEncrypt) {
    var compare;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _bcrypt["default"].compare(password, passwordEncrypt);

          case 3:
            compare = _context.sent;
            return _context.abrupt("return", compare);

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.error('Error isValidPassword: ', _context.t0);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.isValidBase64Img = function (base64Img) {
  var validMimes = ['jpeg', 'png', 'pdf'];
  var splitArray = base64Img.split(',');

  if (splitArray.length == 2) {
    var _splitArray = _slicedToArray(splitArray, 2),
        dataToReplace = _splitArray[0],
        base64 = _splitArray[1];

    var mime = dataToReplace.replace('data:image/', '').replace('data:application/', '').replace(';base64', '');

    if (validMimes.includes(mime)) {
      return {
        isValid: true,
        buffer: Buffer.from(base64, 'base64'),
        ext: mime
      };
    }
  }

  return {
    isValid: false
  };
};

exports.s3UploadBase64Image = function (prefix, _ref2) {
  var Body = _ref2.buffer,
      ext = _ref2.ext;
  var now = DateTime.utc().toISODate();
  var fileName = uuidv4() + '-' + now + '.' + ext;
  var Key = prefix + fileName;
  return new Promise(function (resolve, reject) {
    var ContentType = ext === 'pdf' ? 'application/pdf' : 'image/' + ext;
    var uploadParams = {
      Bucket: Bucket,
      Key: Key,
      Body: Body,
      ContentType: ContentType,
      ContentEncoding: 'base64'
    };
    s3.upload(uploadParams, function (err, data) {
      if (err) {
        resolve({
          success: false
        });
      } else {
        resolve({
          success: true,
          data: data,
          fileName: fileName
        });
      }
    });
  });
};

exports.s3Upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: Bucket,
    key: function key(req, file, cb) {
      // console.log('')
      // console.log('')
      // console.log('file')
      // console.log(file)
      // console.log('')
      // console.log('')
      cb(null, Date.now().toString() + '_' + slugify(file.originalname));
    },
    contentType: function contentType(req, file, cb) {
      cb(null, file.mimetype);
    }
  })
});

exports.s3UploadWithDest = function (dest) {
  return multer({
    storage: multerS3({
      s3: s3,
      bucket: Bucket,
      key: function key(req, file, cb) {
        var regex = /(?:\.([^.]+))?$/;
        var ext = regex.exec(file.originalname)[1];

        if (!ext) {
          var noExtError = new Error('¡Ups! No extension was found');
          return cb(noExtError);
        }

        var now = DateTime.utc().toISODate();
        var fileName = uuidv4() + '-' + now + '.' + ext;
        cb(null, dest + fileName);
      },
      contentType: function contentType(req, file, cb) {
        cb(null, file.mimetype);
      }
    })
  });
};

exports.s3UploadImage = function (prefix, _ref3) {
  var Body = _ref3.buffer,
      ext = _ref3.ext;
  var now = DateTime.utc().toISODate();
  var fileName = uuidv4() + '-' + now + '.' + ext;
  Key = prefix + fileName;
  return new Promise(function (resolve, reject) {
    var uploadParams = {
      Bucket: Bucket,
      Key: Key,
      Body: Body,
      ContentType: 'image/' + ext,
      ContentEncoding: 'base64'
    };
    s3.upload(uploadParams, function (err, data) {
      if (err) {
        resolve({
          success: false
        });
      } else {
        resolve({
          success: true,
          data: data,
          fileName: fileName
        });
      }
    });
  });
};