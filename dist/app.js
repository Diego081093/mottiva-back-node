"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireWildcard(require("express"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _morgan = _interopRequireDefault(require("morgan"));

var _user_routes = _interopRequireDefault(require("./routes/user_routes"));

var _profile_routes = _interopRequireDefault(require("./routes/profile_routes"));

var _profile_attribute_routes = _interopRequireDefault(require("./routes/profile_attribute_routes"));

var _role_routes = _interopRequireDefault(require("./routes/role_routes"));

var _secret_question_routes = _interopRequireDefault(require("./routes/secret_question_routes"));

var _user_secret_question_routes = _interopRequireDefault(require("./routes/user_secret_question_routes"));

var _avatar_routes = _interopRequireDefault(require("./routes/avatar_routes"));

var _availability_routes = _interopRequireDefault(require("./routes/availability_routes"));

var _password_routes = _interopRequireDefault(require("./routes/password_routes"));

var _question_routes = _interopRequireDefault(require("./routes/question_routes"));

var _session_routes = _interopRequireDefault(require("./routes/session_routes"));

var _session_attribute_routes = _interopRequireDefault(require("./routes/session_attribute_routes"));

var _training_routes = _interopRequireDefault(require("./routes/training_routes"));

var _emotion_room_routes = _interopRequireDefault(require("./routes/emotion_room_routes"));

var _verification_routes = _interopRequireDefault(require("./routes/verification_routes"));

var _login_routes = _interopRequireDefault(require("./routes/login_routes"));

var _patient_routes = _interopRequireDefault(require("./routes/patient_routes"));

var _patient_by_routes = _interopRequireDefault(require("./routes/patient_by_routes"));

var _dni_routes = _interopRequireDefault(require("./routes/dni_routes"));

var _medias_routes = _interopRequireDefault(require("./routes/medias_routes"));

var _watch_later_routes = _interopRequireDefault(require("./routes/watch_later_routes"));

var _favorite_routes = _interopRequireDefault(require("./routes/favorite_routes"));

var _test_routes = _interopRequireDefault(require("./routes/test_routes"));

var _upload_routes = _interopRequireDefault(require("./routes/upload_routes"));

var _update_phone_routes = _interopRequireDefault(require("./routes/update_phone_routes"));

var _psycology_routes = _interopRequireDefault(require("./routes/psycology_routes"));

var _psychologist_routes = _interopRequireDefault(require("./routes/psychologist_routes"));

var _therapy_routes = _interopRequireDefault(require("./routes/therapy_routes"));

var _notes_routes = _interopRequireDefault(require("./routes/notes_routes"));

var _room_routes = _interopRequireDefault(require("./routes/room_routes"));

var _thematic_routes = _interopRequireDefault(require("./routes/thematic_routes"));

var _twilio_routes = _interopRequireDefault(require("./routes/twilio_routes"));

var _category_routes = _interopRequireDefault(require("./routes/category_routes"));

var _method_routes = _interopRequireDefault(require("./routes/method_routes"));

var _poll_routes = _interopRequireDefault(require("./routes/poll_routes"));

var _benefit_routes = _interopRequireDefault(require("./routes/benefit_routes"));

var _schedule_routes = _interopRequireDefault(require("./routes/schedule_routes"));

var _notification_routes = _interopRequireDefault(require("./routes/notification_routes"));

var _task_routes = _interopRequireDefault(require("./routes/task_routes"));

var _token_device_routes = _interopRequireDefault(require("./routes/token_device_routes"));

var _blocked_routes = _interopRequireDefault(require("./routes/blocked_routes"));

var _diary_routes = _interopRequireDefault(require("./routes/diary_routes"));

var _patients_routes = _interopRequireDefault(require("./routes/patients_routes"));

var _session_cases_routes = _interopRequireDefault(require("./routes/session_cases_routes"));

var _chat_routes = _interopRequireDefault(require("./routes/chat_routes"));

var _statistics_routes = _interopRequireDefault(require("./routes/statistics_routes"));

var _institution_routes = _interopRequireDefault(require("./routes/institution_routes"));

var _attributes_routes = _interopRequireDefault(require("./routes/attributes_routes"));

var _protocol_routes = _interopRequireDefault(require("./routes/protocol_routes"));

var _protocol_process_routes = _interopRequireDefault(require("./routes/protocol_process_routes"));

var _specialty_routes = _interopRequireDefault(require("./routes/specialty_routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

//importing routes
require('./middleware/auth');

var cors = require('cors'); //initializacion


var app = (0, _express["default"])();
app.use(cors());
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
}); //middlewares

app.use((0, _morgan["default"])('dev')); //app.use(json());

app.use(_bodyParser["default"].json({
  limit: '50mb'
}));
app.use(_bodyParser["default"].urlencoded({
  limit: '50mb',
  extended: true
}));
app.use(_express["default"].json()); //app.use(express.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}))

app.use('/uploads', _express["default"]["static"]('uploads'));
app.use('/user', _user_routes["default"]);
app.use('/profile', _profile_routes["default"]);
app.use('/profileAttribute', _profile_attribute_routes["default"]);
app.use('/role', _role_routes["default"]); //1

app.use('/verification', _verification_routes["default"]);
app.use('/dni', _dni_routes["default"]);
app.use('/password', _password_routes["default"]);
app.use('/avatar', _avatar_routes["default"]); //2

app.use('/availability', _availability_routes["default"]);
app.use('/secretQuestion', _secret_question_routes["default"]);
app.use('/patient', _patient_routes["default"]);
app.use('/patient-by', _patient_by_routes["default"]);
app.use('/question', _question_routes["default"]);
app.use('/login', _login_routes["default"]);
app.use('/userSecretQuestion', _user_secret_question_routes["default"]);
app.use('/patients', _patients_routes["default"]);
app.use('/sessions', _session_routes["default"]);
app.use('/sessionsAttribute', _session_attribute_routes["default"]);
app.use('/trainings', _training_routes["default"]);
app.use('/emotions-room', _emotion_room_routes["default"]);
app.use('/media', _medias_routes["default"]);
app.use('/watch-later', _watch_later_routes["default"]);
app.use('/favorite', _favorite_routes["default"]);
app.use('/test', _test_routes["default"]);
app.use('/send-image', _upload_routes["default"]);
app.use('/update-phone', _update_phone_routes["default"]);
app.use('/psycology', _psycology_routes["default"]);
app.use('/psychologist', _psychologist_routes["default"]);
app.use('/therapies', _therapy_routes["default"]);
app.use('/notes', _notes_routes["default"]);
app.use('/room', _room_routes["default"]);
app.use('/thematic', _thematic_routes["default"]);
app.use('/twilio', _twilio_routes["default"]);
app.use('/category', _category_routes["default"]);
app.use('/method', _method_routes["default"]);
app.use('/poll', _poll_routes["default"]);
app.use('/benefits', _benefit_routes["default"]);
app.use('/task', _task_routes["default"]);
app.use('/schedule', _schedule_routes["default"]);
app.use('/notification', _notification_routes["default"]);
app.use('/tokenDevice', _token_device_routes["default"]);
app.use('/blocked', _blocked_routes["default"]);
app.use('/diary', _diary_routes["default"]);
app.use('/sessionCases', _session_cases_routes["default"]);
app.use('/statistics', _statistics_routes["default"]);
app.use('/institution', _institution_routes["default"]);
app.use('/attributes', _attributes_routes["default"]);
app.use('/protocols-processes', _protocol_process_routes["default"]);
app.use('/protocols', _protocol_routes["default"]);
app.use('/chat', _chat_routes["default"]);
app.use('/specialties', _specialty_routes["default"]);
var _default = app;
exports["default"] = _default;