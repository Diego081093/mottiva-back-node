# verification

Service exist for validation of identity's document

**URL** : `/verification/`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
  "number": "[valid phone number]"
}
```

**Data example**

```json
{
  "number": "934575282"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "message": "Code verification is sent!",
  "phonenumber": "934575282",
  "code": "6578"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
