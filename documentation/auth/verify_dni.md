# Register

Service exist for validation of identity's document

**URL** : `/dni/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "dni": "[valid identity's document]",
  "issue":"[date of issue]"
}
```

**Data example**

```json
{
    "dni": "44022592",
    "issue":"2019-12-15"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Dni & date correct!",
    "data": {
        "dni": "44022592",
        "issue":"2019-12-15"
    }
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to dni with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
