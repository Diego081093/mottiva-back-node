# Secret Question

Service exist to get secret question

**URL** : `/SecretQuestion/:dni`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
  "question": "[text question]"

}
```

**Data example**

```json
{
   
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": "question secret"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : 

**Content** :

```json
{
  
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
