# psycologies List

Service to get list psycologies 

**URL** : `/psycology/list`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```
dayOfWeek = "string"
hour = "H:i"
gender = "M | F"
```

**Data example**

```json
{
 
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "data": [
    {
      "id": "integer(id)",
      "idAvatar": "integer(id)",
      "slug": "String",
      "firstname": "String",
      "lastname": "String",
      "dni": "Number",
      "phone": "Number",
      "gender": "M | F",
      "birthday": "date",
      "email": "String",
      "tbl_profile_attribute": {
        "available": "Json",
        "description": "string",
        "short_description": "string"
      },
      "avatar": [
        {
          "id": "integer(id)",
          "image": "url"
        }
      ],
    },
    
  ]
}
    
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```text
Unauthorized
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```