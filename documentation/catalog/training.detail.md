# Training Detail

Service to get avatar and image

**URL** : `/training/{training_id}`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{

}
```

**Data example**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
 {
        "done": "number",// "status"
       "tbl_media": {
        "file": "url",
        "image": "url",
        "duration": "time",
        "name": "String",
            }
        }
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Unauthorized"]
}
```

---


```