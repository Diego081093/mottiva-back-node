# Update Psychologist

Service to create session note

**URL** : `/sessions/:id/notes`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "note": "Lorem ipsum", // Contenido de la nota
    "priority": "Urgente", // Prioridad de la nota
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Session note created"
}
```

## Error Response

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Session not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```