# Avatar

Service to get avatar and image

**URL** : `/avatar`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
  "image": "[urlImage]"

}
```

**Data example**

```json
{
    "id" :  "integer(id)",
  "image": "url",
  "state": "integer(1-0)"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id" :  1,
  "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png",
  "state": 1
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : 

**Content** :

```json
{
  
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```