# Created Poll

service to save or update a diary

**URL** : `/diary`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "taskId":"6",
    "text":"Hola Mundo",
    "audio":"file[]",
    "feel":"integer(1-3)"
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Diary Created",
    "data": {
        "state": 1,
        "id": 6,
        "tbl_profile_id": 2,
        "tbl_task_id": 2,
        "text": "lihiljio",
        "audio": "https://mottiva.s3.sa-east-1.amazonaws.com/1621374213479_yt1s.com-Nino-oxxo-Video-completo.mp3",
        "feel": 2,
        "updatedAt": "2021-05-18T21:43:35.667Z",
        "createdAt": "2021-05-18T21:43:35.667Z",
        "date": null,
        "created_by": null,
        "edited_by": null
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```