# Update Psychologist

Service to update psychologist data

**URL** : `/psycology`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "firstname": "Prueba",
    "lastname": "Tres",
    "dni": "12872177",
    "phone": "945502075",
    "gender": "F",
    "birthday": "2021-05-03",
    "attributes": [ // Array de atributos a actualizar
        {
            "key": "children",
            "value": 5
        }
    ],
    "specialties": [1, 2, 3, 4] // Array de especialidades que tendra el perfil
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile updated"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```