# Update Psychologist

Service to update psychologist data

**URL** : `/emotions-room`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
  /**
   * In To have:
   * yesterday
   * week
   * month
   * quarter
   * custom
   */
  "to": "[String validation filter]",

  /**
   * Optional
   *
   * c_from
   * c_to
   * Only required when 'to' == 'custom'
   *
   * In To have:
   * YYYY-MM-DD
   */
  "c_from": "[String validation filter]", // Custom date range from
  "c_to": "[String validation filter]", // Custom date range to
  "search": "some title to search"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Rooms found",
    "rooms": [
        {
            "id": 3,
            "title": "Human Optimization Director",
            "start": "2021-09-18T21:29:13.787Z", // Broadcasted start timestamp
            "end": "2021-09-29T12:35:49.272Z", // Broadcasted end timestamp
            "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png"
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```