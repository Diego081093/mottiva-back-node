# Get Points Used

Service to get points used

**URL** : `/profile/points-used`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Benefits found",
    "benefits": [
        {
            "id": 1,
            "state": 1,
            "Benefit": {
                "id": 1,
                "name": "Confianza",
                "image": "https://picsum.photos/100",
                "description": "Quis et tempore accusamus. Et deleniti qui. Consectetur est iure animi aut. Sed dolores ad nemo ex perferendis aut qui sunt. Reiciendis placeat neque quis earum quia maxime at.\n \rQuo minima aliquid ipsa ducimus omnis facilis. Assumenda soluta illo natus ex voluptatum mollitia atque ad. Est est nostrum dolorem. Commodi et omnis similique labore.\n \rEos ut itaque doloremque perferendis omnis minus. Quia quas quidem modi rerum quo error accusamus. Exercitationem est minima in occaecati voluptas qui quidem eligendi. Aperiam vero in ipsum voluptatibus error harum fugit aut.",
                "point": 40,
                "CompanyBenefit": {
                    "name": "Depor",
                    "image": "https://picsum.photos/100"
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```