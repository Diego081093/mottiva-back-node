# Send Image

Service to get avatar and image

**URL** : `/send-image`

**Method** : `POST`

**Auth required** : no

**Data constraints**

```json
{
  "file": 'SINGLE_FILE' // File to upload
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Se guardó la imagen satisfactoriamente",
    "file": "name_of_file"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
