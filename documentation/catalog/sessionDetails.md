# Session Detail

It is used to receive a list of all active sessions to which a psychologist belongs.

**URL** : `/session/:id`

**Method** : `GET`

**Auth required** : TOKEN

**Data constraints** : NO

## Success Response

**Code** : `200 OK`

**Content example**
// actual session
 // total sesions
```json
  {
    "id": "Integer(id)",
    "date": "Date('Y-m-d H:i:s')",
    "firstnamePsychologist": "String",
    "lastnamePsychologist": "String",
    "idChat": "Integer",
    "point": "Integer"
  }
```
**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
