# List patients

Service to list patients

**URL** : `/patients/list`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "city": "1", // City ID
    "institution": "3", // Institution ID
    "age": "21", // Patient age, must NOT be used with age_range_min or age_range_max
    "age_range_min": "18", // Patient age min
    "age_range_max": "26", // Patient age max
    "sessions": "5", // Patient sessions
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Patients found",
    "data": [
        {
            "id": 9,
            "tbl_role_id": 1,
            "tbl_user_id": 9,
            "tbl_avatar_id": 9,
            "tbl_district_id": 9,
            "slug": "eligendi-unde-esse",
            "firstname": "Mathias",
            "lastname": "Nolan",
            "dni": "34860609",
            "photo_dni": null,
            "email": "Marlene51@yahoo.com",
            "phone": "988529452",
            "gender": "F",
            "birthday": "2019-06-19",
            "state": 1,
            "points": 0,
            "req_sent_at": null,
            "req_state": false,
            "req_approved_at": null,
            "civil_state": null,
            "ocupation": null,
            "study_nivel": null,
            "created_by": "root",
            "createdAt": "2021-06-19T00:39:18.656Z",
            "edited_by": "root",
            "updatedAt": "2021-06-19T00:39:18.656Z",
            "District": {
                "name": "Comas",
                "City": {
                    "id": 1,
                    "name": "Lima"
                }
            },
            "ProfileInstitutions": [
                {
                    "tbl_institution_id": 2,
                    "Institution": {
                        "name": "Arequipa"
                    }
                }
            ],
            "sessions": 0,
            "program": null,
            "age": 2
        },
        {
            "id": 10,
            "tbl_role_id": 1,
            "tbl_user_id": 10,
            "tbl_avatar_id": 10,
            "tbl_district_id": 10,
            "slug": "dolor-ipsa-quis",
            "firstname": "Estel",
            "lastname": "Krajcik",
            "dni": "50961269",
            "photo_dni": null,
            "email": "Raul_Macejkovic@gmail.com",
            "phone": "990374923",
            "gender": "M",
            "birthday": "2014-06-19",
            "state": 1,
            "points": 0,
            "req_sent_at": null,
            "req_state": false,
            "req_approved_at": null,
            "civil_state": null,
            "ocupation": null,
            "study_nivel": null,
            "created_by": "root",
            "createdAt": "2021-06-19T00:39:18.656Z",
            "edited_by": "root",
            "updatedAt": "2021-06-19T00:39:18.656Z",
            "District": {
                "name": "San Martin de Porres",
                "City": {
                    "id": 1,
                    "name": "Lima"
                }
            },
            "ProfileInstitutions": [
                {
                    "tbl_institution_id": 4,
                    "Institution": {
                        "name": "Huancayo"
                    }
                }
            ],
            "sessions": 0,
            "program": null,
            "age": 7
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```