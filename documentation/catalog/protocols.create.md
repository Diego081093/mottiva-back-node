# Create Protocol

Service to create protocol

**URL** : `/protocols`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "code": "00034",
    "goal": "Lorem ipsum dolor sit amet",
    "responsibleId": "2",
    "processId": "1",
    "introduction": "Lorem ipusm...",
    "procedure": "Lorem ipusm..."
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Protocol created"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```