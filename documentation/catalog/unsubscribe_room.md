# Subscribe Room

Service to unsubscribe in the emotion room.

**URL**: `/room/:id/signup`

**Method** : `DELETE`

**Auth required** : YES

**Data Constraints**

```json
{
    "idRoom": 1
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
"data": {
    "message": "Unsubscribe to room"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
