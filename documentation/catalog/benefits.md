# Benefits

Service to list benefits

**URL** : `/benefits`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
  

}
```

**Data example**

```json
{
   
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
        {
    "data": [
        {
            "id": 1,
            "name": "50 GB",
            "image": "https://picsum.photos/100",
            "description": "redes sociales gratis,spotify,waze ilimitado",
            "point": 10,
            "CompanyBenefit": {
                "id": 1,
                "name": "Claro",
                "image": "https://picsum.photos/100"
            }
        },
        {
            "id": 2,
            "name": "500 MB",
            "image": "https://picsum.photos/100",
            "description": "redes sociales gratis,spotify,waze ilimitado",
            "point": 20,
            "CompanyBenefit": {
                "id": 2,
                "name": "Movistar",
                "image": "https://picsum.photos/100"
            }
        },
        {
            "id": 3,
            "name": "20 GB",
            "image": "https://picsum.photos/100",
            "description": "redes sociales gratis,spotify,waze ilimitado",
            "point": 20,
            "CompanyBenefit": {
                "id": 3,
                "name": "Entel",
                "image": "https://picsum.photos/100"
            }
        },
        {
            "id": 4,
            "name": "100 GB",
            "image": "https://picsum.photos/100",
            "description": "redes sociales gratis,spotify,waze ilimitado",
            "point": 50,
            "CompanyBenefit": {
                "id": 4,
                "name": "Bitel",
                "image": "https://picsum.photos/100"
            }
        },
        {
            "id": 5,
            "name": "750 MB",
            "image": "https://picsum.photos/100",
            "description": "redes sociales gratis,spotify,waze ilimitado",
            "point": 30,
            "CompanyBenefit": {
                "id": 5,
                "name": "Twenti",
                "image": "https://picsum.photos/100"
            }
        }
    ]
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : 

**Content** :

```json
{
  
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```