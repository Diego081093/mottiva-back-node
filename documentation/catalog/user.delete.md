# Delete user

Service to delete user

**URL** : `/user/:id`

**Method** : `DELETE`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "User deleted"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Unauthorized"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```