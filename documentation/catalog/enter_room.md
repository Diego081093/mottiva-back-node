# Enter Room

Service for when you enter a room.

**URL**: `/room/enter`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "idRoom": 1,
    "nickname": "Pepe",
    "avatar": "https://apple.com.pe/image/43819587439807584.jpg"
}
```
## Success Response
**Code** : `200 OK`

**Not code example**

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
