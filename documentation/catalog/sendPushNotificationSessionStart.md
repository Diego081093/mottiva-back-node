# Send Push Notification for start session

Notify the psychologist of the start of your session

**URL** : `/notification/sendPushNotificationSessionStart`

**Method** : `GET`

**Auth required** : SI

**Data constraints** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Process successfully",
    "session": [
        {
            "id": "Integer(id)",
            "date": "date('Y-m-d H:i:sTZ')",
            "twilio_uniquename": "jd3zc0lw8bjzwdrnss5lrj7n7...",
            "idProfilePsychologist": "Integer(id)",
            "ProfilePsychologist": {
                "id": "Integer(id)",
                "User": {
                    "id": "Integer(id)",
                    "token_device": "cAFzam9VQSqz1z3b6s8adC-..."
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```