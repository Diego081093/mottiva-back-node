# Create benefit

Service to create benefit

**URL** : `/benefits`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "company_id": "11",
    "name": "Lorem ipsum",
    "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQA...",
    "description": "Some description",
    "point": "50"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Benefit created"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```