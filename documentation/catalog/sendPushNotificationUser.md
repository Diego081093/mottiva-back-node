# Send Push Notification User

Notify session participants after meeting starts

**URL** : `/notification/sendPushNotificationUser/:id`

**Method** : `GET`

**Auth required** : SI

**Data constraints** :

```text
id = Integer(idSession)
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Successfully"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```