# Thematics

Theme list service for the emotions room.

**URL**: `/thematic`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "null": "null"
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
"data": [
    {
        "id": 1,
        "name": "Tematica 1"
    },
    {
        "ids": 2,
        "name": "Tematica 2"
    }
]
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
