# Session List

It is used to receive a list of all active sessions to which a psychologist belongs.

**URL** : `/sessions`

**Method** : `GET`

**Auth required** : TOKEN

**Data constraints**

- **to**: Rangos de fechas predefinidos

  Psychologist:
  - today
  - week
  - month
  - year

  Helpdesk:
  - today
  - pending
  - draft

- **schedule**: Horario de las sesiones( SOLO Psychologist )

  - morning: Antes de las 12pm
  - afternoon: Desde las 12pm

- **limit**: Limita los resultados a el valor deseado

- **offset**: Cuantos resultados debe ignorar

- **search**: Termino a buscar

**Data example**

```json
{
  "to": "month",
  "schedule": "morning",
  "limit": 5,
  "offset": 10,
  "search": "maria"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

### Psychologist data

// actual session
 // total sesions
```json
{
    "message": "Sessions found",
    "count": 10, // Numero total de sesiones
    "previous": "http://localhost:3000/session?offset=0&limit=2",
    "next": "http://localhost:3000/session?offset=4&limit=2",
    "limit": 2,
    "offset": 2,
    "data": [
        {
            "patient_id": 4,
            "psychologist_id": 2,
            "date": "2021-04-08T15:03:19.188Z", // Fecha de la cita
            "state": 1, // Estado de la sesion
            "order": 1, // Numero de sesion
            "patient": {
                "firstname": "Prince",
                "lastname": "Vandervort",
                "Avatar": {
                    "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_4.png"
                }
            },
            "total": 2 // Numero de sessiones por paciente
        },
        {
            "patient_id": 10,
            "psychologist_id": 2,
            "date": "2021-04-08T15:03:19.188Z",
            "state": 1,
            "order": 1,
            "patient": {
                "firstname": "Carole",
                "lastname": "Schmitt",
                "Avatar": {
                    "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_10.png"
                }
            },
            "total": 2
        }
    ]
}
```

### Helpdesk data

```json
{
    "message": "Sessions found",
    "count": 10, // Numero total de sesiones
    "previous": "http://localhost:3000/session?offset=0&limit=2",
    "next": "http://localhost:3000/session?offset=4&limit=2",
    "limit": 2,
    "offset": 2,
    "data": [
        {
            "id": 3,
            "date": "2021-04-08T15:03:19.188Z", // Fecha de la cita
            "state": 1, // Estado de la sesion
            "patient_state": 0, // Estado de conexion del paciente
            "psychologist_state": 1, // Estado de conexion del psicologo
            "order": 1, // Numero de la sesion
            "psychologist": {
                "id": 2,
                "firstname": "Raphael",
                "lastname": "Anderson",
                "birthday": "2021-04-08"
            },
            "patient": {
                "id": 4,
                "firstname": "Prince",
                "lastname": "Vandervort",
                "birthday": "2021-04-08"
            },
            "total": 2, // Numero total de sesiones
            "concluded": 2 // Numero de sesiones concluidas
        },
        {
            "id": 4,
            "date": "2021-04-08T15:03:19.188Z",
            "state": 1,
            "patient_state": 1,
            "psychologist_state": 1,
            "order": 1,
            "psychologist": {
                "id": 2,
                "firstname": "Raphael",
                "lastname": "Anderson",
                "birthday": "2021-04-08"
            },
            "patient": {
                "id": 10,
                "firstname": "Carole",
                "lastname": "Schmitt",
                "birthday": "2021-04-08"
            },
            "total": 2,
            "concluded": 2
        }
    ]
}
```

## Error Response

**Condition** : If no sessions are found as helpdesk

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "message": "No sessions were found"
}
```


**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
