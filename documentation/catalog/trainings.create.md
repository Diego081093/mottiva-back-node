# Create training

Service to create training

**URL** : `/trainings`

**Method** : `POST`

**Auth required** : yes

**Data constraints**

```json
{
    "file": "mi-file",  // form-data
    "image": "mi-image", // form-data
    "category_media_id": 1,
    "method_id": 2,
    "name": "Ipsum",
    "type":2,
    "duration": "01:30:00",
    "description": "myDes",
    "points": 10
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Training created"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```