# Update Profile Photo Only

Service to update profile photo only

**URL** : `/profile/:id/photo`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQ..."
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile photo updated"
}
```

## Error Response

**Condition** : If the provided credentials are incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `422 Invalid data`

**Content** :

```json
{
    "message": "The given data was invalid."
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```