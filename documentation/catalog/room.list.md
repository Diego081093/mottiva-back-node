# Thematics

Endpoint to list or filter emotion rooms.

**URL**: `/room?idThematic={value}&idMethod={value}`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
"data": [
    {
        "id": 4,
        "method": {
            "id": 3,
            "name": "Autoconocerme"
        },
        "title": "Room title",
        "start": "2021-04-05:12:37",
        "end": "2021-14-05:12:37",
        "rules": "Regla1, Regla2",
        "thematic": "Tematica 1",
        "participants": 12,
        "maxParticipants": 20,
        "inscription": "¿!?",
        "image": "httpsasdajn.com",
        "twilio_uniquename": "asdlsjkdang"
    },
    {...}
]
```

## Success Response
**Code** : `200 OK`

**Content example**

```json
"data": {
    "message": "success"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
