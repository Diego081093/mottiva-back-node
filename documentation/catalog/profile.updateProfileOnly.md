# Update Profile Data Only

Service to update profile data only

**URL** : `/profile/:id`

**Method** : `PUT`

**Auth required** : YES

**Data constraints**

```json
{
    "firstname": "Prueba",
    "lastname": "Tres",
    "dni": "12872177",
    "phone": "945502075",
    "gender": "F",
    "birthday": "2021-05-03"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Profile updated"
}
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "message": "Access denied with provided credentials"
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```