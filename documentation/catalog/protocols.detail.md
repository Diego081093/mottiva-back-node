# Find protocol by ID

Service to find protocol by ID

**URL** : `/protocols/:protocolID`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Protocol found",
    "data": {
        "id": 1,
        "code": "00001",
        "goal": "Consequatur aut eius temporibus.",
        "introduction": "Quam tenetur distinctio eos iure minus quia dolore. At maxime consequatur aut voluptatem.",
        "procedure": "Enim delectus natus in. Deserunt corporis tenetur dolorum illo. Est consectetur nesciunt blanditiis et nam sequi. Aperiam cum nihil non velit possimus magnam. Cum voluptatum facere laudantium cum sunt quis. Ut non voluptates tenetur quia iusto.",
        "state": 1,
        "createdAt": "2021-06-17T22:32:06.858Z",
        "ProtocolProcess": {
            "id": 1,
            "name": "Botón escuchame"
        },
        "Responsible": {
            "id": 2,
            "slug": "dolor-dolorum-a",
            "firstname": "Cielo",
            "lastname": "Wiza",
            "state": 1
        },
        "Approver": {
            "id": 2,
            "slug": "dolor-dolorum-a",
            "firstname": "Cielo",
            "lastname": "Wiza",
            "state": 1
        }
    }
}
```

## Error Response

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Protocol not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```