# Session completed

Service to complete a session

**URL** : `/sessions/:id/completed`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "patientId": 7
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Session completed"
}
```

## Error Response

**Condition** : Reason

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "message": "Unauthorized"
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Session not found"] // Or patient
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```