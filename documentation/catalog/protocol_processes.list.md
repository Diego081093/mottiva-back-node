# List protocol processes

Service to list protocol processes

**URL** : `/protocol-processes`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{

}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Protocol processes found",
    "data": [
        {
            "id": 1,
            "name": "Botón escuchame"
        },
        {
            "id": 2,
            "name": "Recepción de llamada"
        },
        {
            "id": 3,
            "name": "Lorem"
        },
        {
            "id": 4,
            "name": "Ipsum"
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```