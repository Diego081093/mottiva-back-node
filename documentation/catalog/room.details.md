# Room Emotions

Service to get details of room

**URL** : `/room/details`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

```json
{
 
}
```

**Data example**

```json
{
 ```
roomId=2

```
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": {
        "id": 2,
        "title": "Chief Division Administrator",
        "start": "2022-01-25T19:27:16.721Z",
        "rules": "eligendi-quae-consequatur",
        "participants": 4,
        "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png"
    }
}
    
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to number with provided credentials."]
}
```

---

**Condition** : If the information provided is incomplete or incorrect.

**Code** : `401 Unauthorized`

**Content** :

```text
Unauthorized
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["Training not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```
