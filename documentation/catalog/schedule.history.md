# My Schedule

This endpoint shows the past agendas of each patient

**URL** : `/schedule/history`

**Method** : `GET`

**Auth required** : SI

**Data constraints** : OPTIONAL

```text
    from="String(date)"
    to="String(date)"
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": {
        "prevSessions": [
            {
                "date": "Date",
                "week": "Integer",
                "ProfilePsychologist": {
                    "id": 2,
                    "firstname": "String",
                    "lastname": "String",
                    "birthday": "Date(Y-m-d)",
                    "Avatar": {
                        "image": "String(url)"
                    },
                    "ProfileAttribute": {
                        "shortDescription": "String"
                    }
                }
            }, {...}
        ],
        "prevRooms": [
            {
                "id": "Integer(id)",
                "title": "String",
                "start": "Date",
                "end": "Date",
                "rules": "String",
                "participants": "Integer",
                "image": "String(url)"
            }, {...}
        ]
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "non_field_errors": ["Unable to register with provided credentials."]
}
```

**Code** : `401 UNAUTHORIZED`

**Content** :

```txt
Unauthorized
```

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```