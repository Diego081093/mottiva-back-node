# Push Notification

Service to list the all session

**URL** : `/notification/sessions`

**Method** : `GET`

**Auth required** : NO

**Data constraints** : NO
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Process successfuly",
    "session": [
        {
            "id": "Integer(id)",
            "date": "String",
            "idProfilePsychologist": 2,
            "ProfilePatient": {
                "id": "Integer(id)",
                "User": {
                    "id": "Integer(id)",
                    "token_device": "String"
                }
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```