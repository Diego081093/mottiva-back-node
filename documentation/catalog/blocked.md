# Created Poll

service to save a blocked

**URL** : `/blocked`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{

    "psychologistId":"2",
    "reasonId":"2",
    "comment":"comentario del blocked"

}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Blocked Created",
    "date": {
        "state": 1,
        "id": 2,
        "tbl_profile_pacient_id": 2,
        "tbl_profile_psychologist_id": 2,
        "tbl_reason_id": 2,
        "comment": "comentario del blocked",
        "created_by": "root",
        "edited_by": "root",
        "updatedAt": "2021-04-29T15:54:54.591Z",
        "createdAt": "2021-04-29T15:54:54.591Z"
    }
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```