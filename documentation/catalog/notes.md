# Notes

Service to get list notes of the session and room emotions

**URL** : `/notes`

**Method** : `GET`

**Auth required** : YES TOKEN

**Data constraints**

- **type**: Tipo de notas

  - session: Default
  - room

- **limit**: Limita los resultados a el valor deseado

- **offset**: Cuantos resultados debe ignorar

**Data example**

```json
{
    "type": "session"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Session notes found",
    "count": 10,
    "previous": null,
    "next": "http://localhost:3000/notes?offset=2&limit=2",
    "limit": 2,
    "offset": 0,
    "data": [
        {
            "id": 1,
            "text": "Lorem ipsum",
            "state": 1,
            "Session": {
                "date": "2021-01-02T00:40:14.260Z", // Fecha y hora
                "order": 1, // Numero de sesion actual
                "tbl_profile_pacient_id": 1, // No se puede quitar, se rompe
                "tbl_profile_psychologist_id": 2, // No se puede quitar, se rompe
                "Patient": {
                    "firstname": "Gennaro",
                    "lastname": "Quigley",
                    "Avatar": {
                        "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png"
                    }
                },
                "SessionAttributes": [ // Atributos de la sesion, aca esta el tipo
                    {
                        "key": "state",
                        "value": "Lorem"
                    },
                    {
                        "key": "type",
                        "value": "Urgente"
                    }
                ],
                "total": 6 // Total de sesiones
            }
        },
        {
            "id": 2,
            "text": "Dolor sit",
            "state": 1,
            "Session": {
                "date": "2020-09-07T11:43:34.746Z",
                "order": 1,
                "tbl_profile_pacient_id": 1,
                "tbl_profile_psychologist_id": 2,
                "Patient": {
                    "firstname": "Gennaro",
                    "lastname": "Quigley",
                    "Avatar": {
                        "image": "https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png"
                    }
                },
                "SessionAttributes": [
                    {
                        "key": "state",
                        "value": "Lorem"
                    },
                    {
                        "key": "type",
                        "value": "Moderado"
                    }
                ],
                "total": 6
            }
        }
    ]
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}