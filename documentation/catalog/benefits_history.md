# Benefit history

Service to history benefit

**URL** : `/benefits/history`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

**Data example**

## Success Response

**Code** : `200 OK`

**Content example**

```json
    {
        "data": [
          {
            "id": "Integer(id)",
            "name": "String",
            "image": "String(url)",
            "description": "String",
            "point": "integer",
            "CompanyBenefit": {
                "id": "Integer(id)",
                "name": "String",
                "image": "String(url)"
            }
          },
          {...}
        ]
    }
```

## Error Response

**Condition** : If the information provided is incomplete or incorrect.

**Code** : 

**Content** :

```json
{
  
}
```

---

**Condition** : If route no exist or no conection with server

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "non_field_errors": ["EndPoint not found"]
}
```

---

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```