# Desactivate Favorite Media

Service for desactivate the favorite option in the media table

**URL** : `/media/desactivate_favorite`

**Method** : `POST`

**Auth required** : si

**Data constraints**

```json
{
  "id": tbl_media_id
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "message": "Se desactivó el favorito satisfactoriamente"
}
```

## Error Response

**Condition** : Servers are not working as expected. The request is probably valid but needs to be requested again later.

**Code** : `500 INTERNAL SERVER ERROR`

**Content** :

```json
{
  "non_field_errors": ["Something went wrong"]
}
```