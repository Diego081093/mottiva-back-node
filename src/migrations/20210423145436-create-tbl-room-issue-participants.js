'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_room_issue_participants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_room_issue_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_room_issues',
          key: 'id'
        }
      },
      tbl_chat_participants_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_chat_participants',
          key: 'id'
        }
      },
      answered: {
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_room_issue_participants');
  }
};