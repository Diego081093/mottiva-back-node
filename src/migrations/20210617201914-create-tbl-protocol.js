'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_protocols', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING
      },
      tbl_prot_process_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_protocol_processes',
          key: 'id'
        }
      },
      goal: {
        type: Sequelize.STRING
      },
      tbl_profile_responsible_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      tbl_profile_approver_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      state: {
        type: Sequelize.INTEGER
      },
      introduction: {
        type: Sequelize.TEXT
      },
      procedure: {
        type: Sequelize.TEXT
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_protocols');
  }
};