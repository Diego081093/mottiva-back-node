'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_profile_institutions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_profile_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      tbl_institution_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_institutions',
          key: 'id'
        }
      },
      academic_degree: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.INTEGER
      },
      start_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      ending_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      document: {
        type: Sequelize.STRING
      },
      created_by: {
        type: Sequelize.STRING
      },
      edited_by: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      edited_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_profile_institutions');
  }
};