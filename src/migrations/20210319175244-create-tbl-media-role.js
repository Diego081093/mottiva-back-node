'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_media_roles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_media_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_medias',
          key: 'id'
        }
      },
      tbl_role_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_roles',
          key: 'id'
        }
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_media_roles');
  }
};