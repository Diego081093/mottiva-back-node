'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_media_profiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_media_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_medias',
          key: 'id'
        }
      },
      tbl_profile_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      done: {
        type: Sequelize.INTEGER
      },
      favorite: {
        type: Sequelize.INTEGER
      },
      watch_later: {
        type: Sequelize.BOOLEAN
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_media_profiles');
  }
};