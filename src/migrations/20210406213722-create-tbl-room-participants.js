'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_room_participants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_room_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_rooms',
          key: 'id'
        }
      },
      tbl_profile_participant_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      state: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      edited_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_room_participants');
  }
};