'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_task_questions', {
      id: {
      allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_task_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_tasks',
          key: 'id'
        }
      },
      type: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      question: {
        type: Sequelize.TEXT
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_task_questions');
  }
};