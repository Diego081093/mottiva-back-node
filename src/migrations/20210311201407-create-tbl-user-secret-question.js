'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_user_secret_questions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_users',
          key: 'id'
        }
      },
      tbl_secret_question_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_secret_questions',
          key: 'id'
        }
      },
      answer: {
        type: Sequelize.STRING(200)
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_user_secret_questions');
  }
};