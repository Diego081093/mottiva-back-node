'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_task_media', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_task_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_tasks',
          key: 'id'
        }
      },
      tbl_media_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_medias',
          key: 'id'
        }
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: false,
        type: Sequelize.STRING
      },
      edited_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_task_media');
  }
};