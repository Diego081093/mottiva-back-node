'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_session_cases', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_session_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_sessions',
          key: 'id'
        }
      },
      tbl_profile_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      action: {
        type: Sequelize.STRING
      },
      theme: {
        type: Sequelize.STRING
      },
      reason: {
        type: Sequelize.TEXT
      },
      emotion: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.DATE
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_session_cases');
  }
};