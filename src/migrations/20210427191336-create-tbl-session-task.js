'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_session_tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_session_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_sessions',
          key: 'id'
        }
      },
      tbl_task_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_tasks',
          key: 'id'
        }
      },
      expiration: {
        type: Sequelize.DATE
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_session_tasks')
  }
};
