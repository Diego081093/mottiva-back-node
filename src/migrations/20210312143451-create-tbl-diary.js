'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_diaries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_profile_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      tbl_task_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_tasks',
          key: 'id'
        }
      },
      text: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      audio: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      feel: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE
      },
      public: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_diaries');
  }
};