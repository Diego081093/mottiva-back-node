'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_room_issues', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_room_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_rooms',
          key: 'id'
        }
      },
      situation: {
        type: Sequelize.TEXT
      },
      actions: {
        type: Sequelize.TEXT
      },
      resolution: {
        type: Sequelize.TEXT
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_room_issues');
  }
};