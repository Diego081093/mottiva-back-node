'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_districts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_city_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_cities',
          key: 'id'
        }
      },
      name: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_districts');
  }
};