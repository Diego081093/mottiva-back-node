'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_availabilities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_profile_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_profiles',
          key: 'id'
        }
      },
      day: {
        type: Sequelize.STRING
      },
      start_hour: {
        type: Sequelize.TIME
      },
      end_hour: {
        type: Sequelize.TIME
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        type: Sequelize.STRING
      },
      edited_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      edited_by: {
        type: Sequelize.STRING
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_availabilities');
  }
};