'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_benefits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_company_benefit_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_company_benefits',
          key: 'id'
        }
      },
      name: {
        type: Sequelize.STRING(250)
      },
      image: {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      point: {
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_benefits');
  }
};