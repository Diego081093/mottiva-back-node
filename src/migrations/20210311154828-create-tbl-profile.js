'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_profiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_role_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_roles',
          key: 'id'
        }
      },
      tbl_user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_users',
          key: 'id'
        }
      },
      tbl_avatar_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_avatars',
          key: 'id'
        }
      },
      tbl_district_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_districts',
          key: 'id'
        }
      },
      slug: {
        type: Sequelize.STRING(250)
      },
      firstname: {
        type: Sequelize.STRING(100)
      },
      lastname: {
        type: Sequelize.STRING(100)
      },
      dni: {
        type: Sequelize.STRING(8)
      },
      photo_dni: {
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING(45)
      },
      phone: {
        allowNull: true,
        type: Sequelize.STRING(10)
      },
      gender: {
        allowNull: true,
        type: Sequelize.STRING(1)
      },
      birthday: {
        allowNull: true,
        type: Sequelize.DATEONLY
      },
      state: {
        type: Sequelize.INTEGER
      },
      points: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      req_sent_at: {
        type: Sequelize.DATE
      },
      req_state: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      req_approved_at: {
        type: Sequelize.DATE
      },
      civil_state: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ocupation: {
        allowNull: true,
        type: Sequelize.STRING
      },
      study_nivel: {
        allowNull: true,
        type: Sequelize.STRING
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_profiles');
  }
};