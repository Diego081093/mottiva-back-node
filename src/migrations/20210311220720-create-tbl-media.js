'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_medias', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_category_media_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_category_medias',
          key: 'id'
        }
      },
      tbl_method_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_methods',
          key: 'id'
        }
      },
      name: {
        type: Sequelize.STRING(100)
      },
      type: {
        type: Sequelize.INTEGER
      },
      file: {
        type: Sequelize.TEXT
      },
      image: {
        type: Sequelize.TEXT
      },
      duration: {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      points: {
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_medias');
  }
};