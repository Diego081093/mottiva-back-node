'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tbl_tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tbl_method_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_methods',
          key: 'id'
        }
      },
      tbl_task_types_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'tbl_task_types',
          key: 'id'
        }
      },
      week: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(250)
      },
      description: {
        type: Sequelize.STRING(250)
      },
      state: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.STRING(100)
      },
      created_at: {
        type: Sequelize.DATE
      },
      edited_by: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      edited_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tbl_tasks');
  }
};