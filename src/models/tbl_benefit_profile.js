'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_benefit_profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      /*this.belongsTo(models.tbl_benefit, {
        foreignKey: 'tbl_benefit_id',
        as: 'Profile'
      })*/
      this.belongsTo(models.tbl_benefit, {
        foreignKey: 'tbl_benefit_id',
        as: 'Benefit'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_id',
        as: 'Profile'
      })
    }
  };
  tbl_benefit_profile.init({
    tbl_benefit_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_benefit_profile',
  });
  return tbl_benefit_profile;
};