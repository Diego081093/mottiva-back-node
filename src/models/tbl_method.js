'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_method extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.tbl_medias, { //le brindo a este modelo
        foreignKey: 'tbl_method_id',
        as:'Media'                       //este campo
      })
      this.hasOne(models.tbl_task, { //le brindo a este modelo
        foreignKey: 'tbl_method_id',
        as:'Task'                       //este campo
      })
    }
  };
  tbl_method.init({
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    state: DataTypes.INTEGER,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    created_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE
    },
    edited_by: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'tbl_method',
  });
  return tbl_method;
};