'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_avatar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.hasOne(models.tbl_profile, {
        as:'Profile',//le brindo a este modelo
        foreignKey: 'tbl_avatar_id'                       //este campo
      })
    }
  };
  tbl_avatar.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    image: DataTypes.TEXT,
    default: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_avatar',
  });
  return tbl_avatar;
};