'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_task_types extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.tbl_task, { //le brindo a este modelo
        foreignKey: 'tbl_task_types_id',
        as:'TaskTypes'                       //este campo
      })
    }
  };
  tbl_task_types.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    alias: DataTypes.STRING,
    description: DataTypes.STRING,
    state:{
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
      }
  }, {
    sequelize,
    modelName: 'tbl_task_types',
  });
  return tbl_task_types;
};