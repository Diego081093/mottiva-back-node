'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_medias extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_category_medias, {  //necesito de este modelo
        foreignKey: 'tbl_category_media_id'               //este campo
      })
      this.belongsTo(models.tbl_category_medias, {  //necesito de este modelo
        foreignKey: 'tbl_category_media_id',               //este campo
        as: 'category_media'
      })

      this.belongsTo(models.tbl_method, {  //necesito de este modelo
        foreignKey: 'tbl_method_id',               //este campo
        as: 'method'
      })

      this.hasOne(models.tbl_media_profile, { //le brindo mi ID a este modelo
        as: 'onProfile',
        foreignKey: 'tbl_media_id'                       //para este campo
      })
      this.hasOne(models.tbl_media_role, { //le brindo mi ID a este modelo
        as: 'Role',
        foreignKey: 'tbl_media_id'                       //para este campo
      })
      this.hasMany(models.tbl_task_media, { //le brindo mi ID a este modelo
        as: 'TaskMedia',
        foreignKey: 'tbl_media_id'                       //para este campo
      })
    }
  };
  tbl_medias.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_category_media_id: DataTypes.INTEGER,
    tbl_method_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    type: DataTypes.INTEGER,
    file: DataTypes.TEXT,
    image: DataTypes.TEXT,
    duration: DataTypes.TEXT,
    description: DataTypes.TEXT,
    points: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_medias',
  });
  return tbl_medias;
};