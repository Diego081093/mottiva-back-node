'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_specialty extends Model {
    static associate(models) {
      // define association here
      this.hasOne(models.tbl_profile_specialty, { //le brindo a este modelo
        foreignKey: 'tbl_specialty_id',
        as:'ProfileSpecialty'                       //este campo
      })

    }
  };
 tbl_specialty.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    icon: DataTypes.STRING,
    alias: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_specialty',
    tableName: 'tbl_specialties'
  });
  return tbl_specialty;
};