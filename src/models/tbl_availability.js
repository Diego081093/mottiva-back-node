'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_availability extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as: 'Profile'               //este campo
      })
    }
  };
  tbl_availability.init({
    tbl_profile_id:DataTypes.INTEGER,
    day: DataTypes.STRING,
    start_hour: DataTypes.TIME,
    end_hour: DataTypes.TIME,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_availability',
  });
  return tbl_availability;
};