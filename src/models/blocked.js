'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_blocked extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {  
        foreignKey: 'tbl_profile_pacient_id',
        as:'Patient'               
      }),
      this.belongsTo(models.tbl_profile, {  
        foreignKey: 'tbl_profile_psychologist_id',
        as:'Psychologist'               
      }),
      this.belongsTo(models.tbl_reason, {  
        foreignKey: 'tbl_reason_id',
        as:'Reason'               
      })

    }
  };
  tbl_blocked.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_profile_pacient_id: DataTypes.INTEGER,
    tbl_profile_psychologist_id: DataTypes.INTEGER,
    tbl_reason_id: DataTypes.INTEGER,
    comment: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
      }, {
    sequelize,
    modelName: 'tbl_blocked',
  });
  return tbl_blocked;
};