'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_room_issue_participants extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_room_issue, {
        foreignKey: 'tbl_room_issue_id',
        as:'RoomIssue'
      })
      this.belongsTo(models.tbl_chat_participants, {
        foreignKey: 'tbl_chat_participants_id',
        as:'ChatParticipant'
      })
      
    }
  };
  tbl_room_issue_participants.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_room_issue_id: DataTypes.INTEGER,
    tbl_chat_participants_id: DataTypes.INTEGER,
    answered: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_room_issue_participants',
  });
  return tbl_room_issue_participants;
};