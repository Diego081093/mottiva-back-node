'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_district extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_city, {
        foreignKey: 'tbl_city_id',
        as: 'City'
      })
      this.hasOne(models.tbl_profile, { //le brindo a este modelo
        foreignKey: 'tbl_district_id',
        as:'Profile'                       //este campo
      })
    }
  };
  tbl_district.init({
    tbl_city_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_district',
  });
  return tbl_district;
};