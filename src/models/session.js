'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_session extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_psychologist_id',
        as: 'ProfilePsychologist'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_pacient_id',
        as: 'ProfilePatient'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_psychologist_id',
        as: 'Psychologist'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_pacient_id',
        as: 'Patient'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_helpdesk_id',
        as: 'Helpdesk'
      })

      this.hasMany(models.tbl_session_attribute, { //le brindo a este modelo
        foreignKey: 'tbl_session_id',                       //este campo
        as: 'SessionAttributes'
      })
      this.hasMany(models.tbl_session_note, { //le brindo a este modelo
        foreignKey: 'tbl_session_id' ,                      //este campo
        as: 'Notes'
      })
      this.hasOne(models.tbl_poll, { //le brindo a este modelo
        foreignKey: 'tbl_session_id',
        as:'Poll'                       //este campo
      })
      this.hasOne(models.tbl_session_task, {
        foreignKey: 'tbl_session_id'
      })
    }
  };
  tbl_session.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_profile_pacient_id: DataTypes.INTEGER,
    patient_state: DataTypes.INTEGER,
    tbl_profile_psychologist_id: DataTypes.INTEGER,
    tbl_profile_helpdesk_id: DataTypes.INTEGER,
    psychologist_state: DataTypes.INTEGER,
    week: DataTypes.INTEGER,
    date: DataTypes.DATE,
    twilio_uniquename: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    order: DataTypes.INTEGER,
    completed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_session',
  });
  return tbl_session;
};