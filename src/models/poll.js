'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_poll extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_session, {  //necesito de este modelo
        foreignKey: 'tbl_session_id',
        as: 'Session'               //este campo
      })
    }
  };
  tbl_poll.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_session_id: DataTypes.INTEGER,
    experience: DataTypes.INTEGER,
    qualification: DataTypes.INTEGER,
    comment: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
      created_by: DataTypes.STRING,
      createdAt: {
        field: 'created_at',
        type: DataTypes.DATE,
      },
      edited_by: DataTypes.STRING,
      updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
      }
  }, {
    sequelize,
    modelName: 'tbl_poll',
  });
  return tbl_poll;
};