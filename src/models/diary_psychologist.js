'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_diary_psychologist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_diary, {  //necesito de este modelo
        foreignKey: 'tbl_diary_id',
        as: 'Diary'               //este campo
      })
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'               //este campo
      })
    }
  };
  tbl_diary_psychologist.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_diary_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_diary_psychologist',
  });
  return tbl_diary_psychologist;
};