'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_profile_specialty extends Model {
    static associate(models) {
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'               //este campo
      })

      this.belongsTo(models.tbl_specialty, {
        as: 'Specialty',
        foreignKey: 'tbl_specialty_id'
      })
    }
  };
  tbl_profile_specialty.init({
    tbl_profile_id: DataTypes.INTEGER,
    tbl_specialty_id: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_profile_specialty',
  });
  return tbl_profile_specialty;
};