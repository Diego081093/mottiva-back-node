'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_company_benefit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
        this.hasOne(models.tbl_benefit, { //le brindo a este modelo
        foreignKey: 'tbl_company_benefit_id',
        as:'Benefit'                       //este campo
      })
    }
  };
  tbl_company_benefit.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    image: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_company_benefit',
  });
  return tbl_company_benefit;
};