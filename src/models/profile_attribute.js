'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_profile_attribute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_profile, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'                       //este campo
      })
    }
  };
  tbl_profile_attribute.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_profile_id: DataTypes.INTEGER,
    key : DataTypes.STRING,
    value : {
      type: DataTypes.TEXT,
      get() {
        try {

          const isJson = JSON.parse(this.getDataValue('value'))

          return isJson

        } catch (e) {

          return this.getDataValue('value')
        }
      }
    },
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_profile_attribute',
  });
  return tbl_profile_attribute;
};