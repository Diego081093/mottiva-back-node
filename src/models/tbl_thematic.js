'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_thematic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tbl_thematic.init({
    state: DataTypes.INTEGER,
    name: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'tbl_thematic',
  });
  return tbl_thematic;
};