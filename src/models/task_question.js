'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_task_question extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_task, {  //necesito de este modelo
        foreignKey: 'tbl_task_id',
        as:'Task'               //este campo
      })

      this.hasOne(models.tbl_task_answer, { //le brindo a este modelo
        foreignKey: 'tbl_task_question_id',
        as:'TaskAnswer'                       //este campo
      })
    }
  };
  tbl_task_question.init({
    tbl_task_id: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    question: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_task_question',
  });
  return tbl_task_question;
};