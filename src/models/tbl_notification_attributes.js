'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_notification_attributes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tbl_notification_attributes.init({
    tbl_notification_id: DataTypes.INTEGER,
    key: DataTypes.STRING,
    value: DataTypes.STRING,
    state: DataTypes.INTEGER,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    created_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE
    },
    edited_by: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tbl_notification_attributes',
  });
  return tbl_notification_attributes;
};