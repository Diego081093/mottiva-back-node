'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_city extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_countrie, {
        foreignKey: 'tbl_country_id',
        as: 'Country'
      })
      this.hasOne(models.tbl_district, { //le brindo a este modelo
        foreignKey: 'tbl_city_id',
        as:'District'                       //este campo
      })

    }
  };
  tbl_city.init({
    tbl_country_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_city',
  });
  return tbl_city;
};