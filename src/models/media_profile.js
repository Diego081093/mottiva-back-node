'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_media_profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_medias, {  //necesito de este modelo
        foreignKey: 'tbl_media_id',               //este campo
        as: 'Media'
      })

      this.belongsTo(models.tbl_profile, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'                       //este campo
      })

      this.hasOne(models.tbl_media_role, { //le brindo a este modelo
        foreignKey: 'tbl_media_id',
        as:'MediaRole'                       //este campo
      })
    }
  };
  tbl_media_profile.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_media_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    done: DataTypes.INTEGER,
    favorite: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    watch_later: DataTypes.BOOLEAN,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_media_profile',
  });
  return tbl_media_profile;
};