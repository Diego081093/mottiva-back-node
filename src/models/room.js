'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_room extends Model {
    static associate(models) {

      this.hasOne(models.tbl_room_note, { //le brindo a este modelo
        foreignKey: 'tbl_room_id',
        as:'RoomNote'                       //este campo
      })
      this.hasOne(models.tbl_chat, {
        as: 'Chat',
        foreignKey: 'tbl_room_id'
      })
      this.hasMany(models.tbl_room_participants, {
        foreignKey: 'tbl_room_id'
      })
      this.belongsTo(models.tbl_method, {
        foreignKey: 'tbl_method_id'
      })
      this.belongsTo(models.tbl_method, {
        foreignKey: 'tbl_method_id',
        as: 'Method'
      })
      this.belongsTo(models.tbl_thematic, {
        foreignKey: 'tbl_thematic_id',
        as: 'Thematic'
      })
    }
  };
  tbl_room.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_method_id: DataTypes.INTEGER,
    tbl_thematic_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    rules: DataTypes.TEXT,
    participants: DataTypes.INTEGER,
    maxParticipants: DataTypes.INTEGER,
    inscription: DataTypes.STRING,
    twilio_uniquename: DataTypes.STRING,
    image: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_room',
  });
  return tbl_room;
};