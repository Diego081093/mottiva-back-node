'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_user_secret_question extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_user, {  //necesito de este modelo
        foreignKey: 'tbl_user_id', 
        as:'User'              //este campo
      })

      this.belongsTo(models.tbl_secret_question, { //le brindo a este modelo
        foreignKey: 'tbl_secret_question_id',
        as:'SecretQuestion'                       //este campo
      })
    }
  };
  tbl_user_secret_question.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_user_id: DataTypes.INTEGER,
    tbl_secret_question_id:DataTypes.INTEGER,
    answer: DataTypes.STRING,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
    
  }, {
    sequelize,
    modelName: 'tbl_user_secret_question',
  });
  return tbl_user_secret_question;
};