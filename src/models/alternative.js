'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_alternatives extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_question, {  //necesito de este modelo
        foreignKey: 'tbl_question_id', 
        as:'Question'              //este campo
      })

      this.hasOne(models.tbl_profile_alternative, { //le brindo a este modelo
        foreignKey: 'tbl_altenative_id',               //este campo
        as: 'ProfileAlternative'                       //este campo
      })
      
    }
  };
  tbl_alternatives.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_question_id: DataTypes.INTEGER,
    alternative: DataTypes.TEXT,
    score: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_alternative',
  });
  return tbl_alternatives;
};