'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_media_role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_medias, {
        foreignKey: 'tbl_media_id',
        as:'Media'

      })

      this.belongsTo(models.tbl_role, {
        foreignKey: 'tbl_role_id',
        as:'Role'
      })
    }
  };
  tbl_media_role.init({
    tbl_media_id: DataTypes.INTEGER,
    tbl_role_id: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_media_role',
  });
  return tbl_media_role;
};