'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_diary extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as: 'Profile'              //este campo
      })
      this.belongsTo(models.tbl_task, {  //necesito de este modelo
        foreignKey: 'tbl_task_id',
        as: 'Task'              //este campo
      })

      this.hasOne(models.tbl_diary_psychologist, { //le brindo a este modelo
        foreignKey: 'tbl_diary_id',
        as:'DiaryPsychologist'                       //este campo
      })
    }
  };
  tbl_diary.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_profile_id: DataTypes.INTEGER,
    tbl_task_id: DataTypes.INTEGER,
    text: DataTypes.TEXT,
    audio: DataTypes.TEXT,
    feel: DataTypes.INTEGER,
    seen:{
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    date: DataTypes.DATE,
    public: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_diary',
  });
  return tbl_diary;
};