'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_task_media extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_medias, { //le brindo mi ID a este modelo
        as: 'Media',
        foreignKey: 'tbl_media_id'                       //para este campo
      })
    }
  };
  tbl_task_media.init({
    tbl_task_id: DataTypes.INTEGER,
    tbl_media_id: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  },
   {
    sequelize,
    modelName: 'tbl_task_media',
  });
  return tbl_task_media;
};