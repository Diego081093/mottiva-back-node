'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_session_note extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_session, {  //necesito de este modelo
        foreignKey: 'tbl_session_id',
        as:'Session'               //este campo

      })
    }
  };
  tbl_session_note.init({
    tbl_session_id: DataTypes.INTEGER,
    note: DataTypes.STRING,
    priority: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_session_note',
  });
  return tbl_session_note;
};