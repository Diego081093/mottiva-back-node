'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_profile_institution extends Model {
    /**
    * Helper method for defining associations.
    * This method is not a part of Sequelize lifecycle.
    * The `models/index` file will call this method automatically.
    */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_institution, {  //necesito de este modelo
        foreignKey: 'tbl_institution_id',
        as: 'Institution'               //este campo
      })
    }
  };
  tbl_profile_institution.init({
    tbl_profile_id: DataTypes.INTEGER,
    tbl_institution_id: DataTypes.INTEGER,
    academic_degree: DataTypes.STRING,
    state: DataTypes.INTEGER,
    start_date: DataTypes.DATE,
    ending_date: DataTypes.DATE,
    document: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_profile_institution',
  });
  return tbl_profile_institution;
};