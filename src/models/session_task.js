'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_session_task extends Model {
    static associate(models) {
      this.belongsTo(models.tbl_session, {
        as: 'Session',
        foreignKey: 'tbl_session_id'
      })
      this.belongsTo(models.tbl_task, {
        as: 'Task',
        foreignKey: 'tbl_task_id'
      })
    }
  };
  tbl_session_task.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_session_id: DataTypes.INTEGER,
    tbl_task_id: DataTypes.INTEGER,
    expiration: DataTypes.DATE,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_session_task',
  });
  return tbl_session_task;
};