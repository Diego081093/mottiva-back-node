'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_session_cases extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_session, {  //necesito de este modelo
        foreignKey: 'tbl_session_id',
        as:'Session'               //este campo

      })
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'               //este campo

      })
    }
  };
  tbl_session_cases.init({
    tbl_session_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    action: DataTypes.STRING,
    theme: DataTypes.STRING,
    reason: DataTypes.TEXT,
    emotion: DataTypes.STRING,
    date: DataTypes.DATE,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    },
    tbl_room_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'tbl_session_cases',
  });
  return tbl_session_cases;
};