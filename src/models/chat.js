'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_chat extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_room, {  //necesito de este modelo
        foreignKey: 'tbl_room_id',
        as:'Room'               //este campo
      })

      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_chat_id',
        as:'ChatParticipant'                      //este campo,
      })

      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_chat_id',
        as:'ChatParticipantPsychologist'                      //este campo,
      })

      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_chat_id',
        as:'ChatParticipantPsychologistAssistant'                      //este campo,
      })

      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_chat_id',
        as:'ChatParticipantHelpdesk'                      //este campo,
      })

    }
  };
  tbl_chat.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_room_id: DataTypes.INTEGER,
    start: DataTypes.DATE,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    helpdesk: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_chat',
  });
  return tbl_chat;
};