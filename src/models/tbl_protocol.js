'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_protocol extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.tbl_protocol_process, {  //necesito de este modelo
        foreignKey: 'tbl_prot_process_id',
        as: 'ProtocolProcess'               //este campo
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_responsible_id',
        as: 'Responsible'
      })
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_approver_id',
        as: 'Approver'
      })
    }
  };
  tbl_protocol.init({
    code: DataTypes.STRING,
    tbl_prot_process_id: DataTypes.INTEGER,
    goal: DataTypes.STRING,
    tbl_profile_responsible_id: DataTypes.INTEGER,
    tbl_profile_approver_id: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    introduction: DataTypes.STRING,
    procedure: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_protocol',
  });
  return tbl_protocol;
};