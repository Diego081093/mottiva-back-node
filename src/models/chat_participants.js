'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_chat_participants extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_chat, {  //necesito de este modelo
        foreignKey: 'tbl_chat_id',
        as:'Chat'               //este campo
      })
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',               //este campo
        as: 'Profile'
      })
      this.hasOne(models.tbl_chat_participants_message, { //le brindo a este modelo
        foreignKey: 'tbl_chat_participants_id',
        as:'ChatParticipantMessage'                       //este campo
      })
      this.hasOne(models.tbl_chat_participants_message, { //le brindo a este modelo
        foreignKey: 'tbl_chat_participants_id',
        as:'ChatParticipantLastMessage'                       //este campo
      })
    }
  };
  tbl_chat_participants.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_chat_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_chat_participants',
  });
  return tbl_chat_participants;
};