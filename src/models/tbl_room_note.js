'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_room_note extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_psychologist_id',
        as: 'Psychologist'
      })
      this.belongsTo(models.tbl_room, {  //necesito de este modelo
        foreignKey: 'tbl_room_id',
        as:'Room'               //este campo
      })
      this.belongsTo(models.tbl_room, {  //necesito de este modelo
        foreignKey: 'tbl_room_id'               //este campo
      })
    }
  };
  tbl_room_note.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_profile_psychologist_id: DataTypes.INTEGER,
    tbl_room_id: DataTypes.INTEGER,
    notes: DataTypes.STRING,
    priority: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_room_note',
  });
  return tbl_room_note;
};