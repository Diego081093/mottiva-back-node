'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {
        foreignKey: 'tbl_profile_id',
        as: 'Profile'
      })
      this.hasMany(models.tbl_notification_attributes, { //le brindo a este modelo
        foreignKey: 'tbl_notification_id',
        as:'NotificationAttributes'                //este campo
      })
    }
  };
  tbl_notification.init({
    tbl_profile_id: DataTypes.INTEGER,
    type: DataTypes.STRING,
    title: DataTypes.TEXT,
    description: DataTypes.TEXT,
    state: DataTypes.INTEGER,
    done: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    created_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE
    },
    edited_by: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tbl_notification',
  });
  return tbl_notification;
};