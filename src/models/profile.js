'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_role, {  //necesito de este modelo
        foreignKey: 'tbl_role_id',
        as:'Role'
       // as:Role               //este campo
      })
      this.belongsTo(models.tbl_user, {  //necesito de este modelo
        foreignKey: 'tbl_user_id',
        as: 'User'               //este campo
      })
      this.belongsTo(models.tbl_avatar, {  //necesito de este modelo
        foreignKey: 'tbl_avatar_id',
        as: 'Avatar'               //este campo
      })
      this.belongsTo(models.tbl_district, {  //necesito de este modelo
        foreignKey: 'tbl_district_id',
        as: 'District'               //este campo
      })

      this.hasOne(models.tbl_profile_alternative, { //le brindo a este modelo
        foreignKey: 'profile_id',
        as:'ProfileAlternative'                          //este campo
      })
      this.hasOne(models.tbl_profile_attribute, {
        foreignKey: 'tbl_profile_id',
        as:'ProfileAttribute'
      })
      this.hasMany(models.tbl_profile_attribute, {
        foreignKey: 'tbl_profile_id',
        as:'Agenda'
      })
      this.hasMany(models.tbl_profile_institution, {
        foreignKey: 'tbl_profile_id',
        as:'ProfileInstitutions'
      })
      this.belongsToMany(models.tbl_specialty, {
        through: models.tbl_profile_specialty,
        foreignKey: 'tbl_profile_id',
        otherKey: 'tbl_specialty_id',
        // as: 'Specialties'
        as: {
          plural: 'Specialties',
          singular: 'Specialty'
        }
      })
      this.hasOne(models.tbl_blocked, { //le brindo a este modelo
        foreignKey: 'tbl_profile_pacient_id' ,
        as:'BlockedPacient'                         //este campo
      })
      this.hasOne(models.tbl_blocked, { //le brindo a este modelo
        foreignKey: 'tbl_profile_psychologist_id' ,
        as:'BlockedPsychologist'                         //este campo
      })
      this.hasOne(models.tbl_diary ,{
        foreignKey: 'tbl_profile_id' ,
        as:'Diary'                      //este campo
      })
      this.hasOne(models.tbl_diary_psychologist ,{
        foreignKey: 'tbl_profile_id',
        as:'DiaryPsychologist '                       //este campo
      })
      // Was causing issues
      // this.hasOne(models.tbl_session ,{
      //   foreignKey: 'tbl_profile_id' ,
      //   as:'Session'                      //este campo
      // })
      this.hasOne(models.tbl_profile_specialty, {
        as: 'ProfileSpecialty',
        foreignKey: 'tbl_profile_id'
      })
      this.hasOne(models.tbl_chat_participants, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'ChatParticipant'                          //este campo
      })
      this.hasOne(models.tbl_media_profile, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'MediaProfile'                        //este campo
      })
      this.hasOne(models.tbl_task_answer, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'TaskAnswer'                          //este campo
      })
      this.hasOne(models.tbl_benefit_profile, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'BenefitProfile'                          //este campo
      })
      this.hasOne(models.tbl_availability, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'Availability'                          //este campo
      })
      this.hasMany(models.tbl_availability, { //le brindo a este modelo
        foreignKey: 'tbl_profile_id',
        as:'Availabilities'                          //este campo
      })

      this.hasMany(models.tbl_room_participants, {
        foreignKey: 'tbl_profile_participant_id',
        as:'RoomParticipants'
      })
    }
  };
  tbl_profile.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_role_id: DataTypes.INTEGER,
    tbl_user_id: DataTypes.INTEGER,
    tbl_avatar_id: DataTypes.INTEGER,
    tbl_district_id: DataTypes.INTEGER,
    slug: DataTypes.STRING,
    firstname:DataTypes.STRING,
    lastname: DataTypes.STRING,
    dni: DataTypes.STRING,
    photo_dni: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    gender: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    points: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    req_sent_at: DataTypes.DATE,
    req_state: DataTypes.BOOLEAN,
    req_approved_at: DataTypes.DATE,
    civil_state: DataTypes.STRING,
    ocupation: DataTypes.STRING,
    study_nivel: DataTypes.STRING,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_profile',
  });
  return tbl_profile;
};