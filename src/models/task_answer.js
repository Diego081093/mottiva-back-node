'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_task_answer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_profile, {  //necesito de este modelo
        foreignKey: 'tbl_profile_id',
        as:'Profile'               //este campo
      })

      this.belongsTo(models.tbl_task_question, { //le brindo a este modelo
        foreignKey: 'tbl_task_question_id',
        as:'TaskQuestion'                       //este campo
      })
      
    }
  };
  tbl_task_answer.init({
    tbl_task_question_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    tbl_profile_id: DataTypes.INTEGER,
    sort: DataTypes.INTEGER,
    answer: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_task_answer',
  });
  return tbl_task_answer;
};