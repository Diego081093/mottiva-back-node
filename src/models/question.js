'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_question extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    

      this.hasOne(models.tbl_alternative, { //le brindo a este modelo
        foreignKey: 'tbl_question_id' ,
        as:'Alternative'                      //este campo
      })
    }
  };
  tbl_question.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    question: DataTypes.TEXT,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
      },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
    field: 'edited_at',
    type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_question',
  });
  return tbl_question;
};