'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_room_issue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_room, {
        foreignKey: 'tbl_room_id',
        as:'room'
      })
      this.hasOne(models.tbl_room_issue_participants, { //le brindo a este modelo
        foreignKey: 'tbl_room_issue_id',
        as:'RoomIssueParticipants'                       //este campo
      })
    }
  };
  tbl_room_issue.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_room_id: DataTypes.INTEGER,
    situation: DataTypes.TEXT,
    actions: DataTypes.TEXT,
    resolution: DataTypes.TEXT,
    state: DataTypes.INTEGER,
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_room_issue',
  });
  return tbl_room_issue;
};