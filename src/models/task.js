'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.tbl_task_types, {  //necesito de este modelo
        as:'TaskTypes',
        foreignKey: 'tbl_task_types_id'               //este campo
      })
      this.belongsTo(models.tbl_method, {  //necesito de este modelo
        foreignKey: 'tbl_method_id',
        as:'Method'               //este campo
      })
      this.hasOne(models.tbl_task_question, { //le brindo a este modelo
        foreignKey: 'tbl_task_id',
        as: 'TaskQuestion'                       //este campo
      })
      this.hasOne(models.tbl_diary, { //le brindo a este modelo
        foreignKey: 'tbl_task_id',
        as: 'Diary'                       //este campo
      })
      this.hasOne(models.tbl_session_task, { //le brindo a este modelo
        foreignKey: 'tbl_task_id',
        as:'SessionTask'                       //este campo
      })
      this.hasOne(models.tbl_task_media, { //le brindo a este modelo
        foreignKey: 'tbl_task_id',
        as:'TaskMedia'                      //este campo
      })
    }
  };
  tbl_task.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_method_id:DataTypes.INTEGER,
    tbl_task_types_id:DataTypes.INTEGER,
    week: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    expiration: DataTypes.DATE,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_task',
  });
  return tbl_task;
};