'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_benefit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_company_benefit, {  //necesito de este modelo
        foreignKey: 'tbl_company_benefit_id' ,
        as:'CompanyBenefit'              //este campo
      })
      this.hasOne(models.tbl_benefit_profile, { //le brindo a este modelo
        foreignKey: 'tbl_benefit_id' ,
        as:'BenefitProfile'                         //este campo
      })

      // define association here
    }
  };
  tbl_benefit.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tbl_company_benefit_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    image: DataTypes.TEXT,
    description: DataTypes.TEXT,
    point: DataTypes.INTEGER,
    state: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    created_by: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    edited_by: DataTypes.STRING,
    updatedAt: {
      field: 'edited_at',
      type: DataTypes.DATE,
    }
  }, {
    sequelize,
    modelName: 'tbl_benefit',
  });
  return tbl_benefit;
};