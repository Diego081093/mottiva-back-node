const {tbl_attributes} = require('../models')

export async function getListAttributes(req, res){
    try {
        const {section,key,value,type} = req.query
        const attributes = await tbl_attributes.findOne({
            where:{
                section,type 
            }
        });
        res.json({
            data: attributes
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createAlternative(req, res){
    const {alternative,score,question_id }=req.body;

    try{
        let newAlternative = await tbl_alternative.create({
            alternative,
            score,
            question_id
        });
        if(newAlternative){

            res.json({
                message: 'alternative created',
                date: newAlternative
            });
        }


    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}