const { tbl_thematic } = require('../models')

export async function getListThematic(req, res){
    try {
        const thematics = await tbl_thematic.findAll({
            where: {
                state: 1
            },
            attributes: [
                'id',
                'name'
            ],
            order: [['id', 'ASC']]
        });

        res.json({
            message: 'Thematics found',
            data: thematics
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}