const { tbl_specialty } = require('../models')

export async function list (req, res) {
    try {
        const specialties = await tbl_specialty.findAll({
            where: { state: 1 }
        })
        res.json(specialties)
    } catch (error) {
        console.log('Something went error on list:', error)
        res.status(500).json({
            message: 'Something went wrong',
        })
    }
}