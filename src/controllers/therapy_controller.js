const { Op } = require("sequelize");

const { tbl_availability, tbl_profile, tbl_avatar } = require('../models')

export async function filterAvailability(req, res){
    try {

        const data = await tbl_availability.findAll({
            where: {
                state: 1,
                day: req.query.day,
                start_hour: {
                    [Op.lte]: req.query.hour
                },
                end_hour: {
                    [Op.gt]: req.query.hour
                }
            },
            include: {
                model: tbl_profile,
                as: 'Profile',
                where: {
                    gender: req.query.gender
                },
                include: {
                    model: tbl_avatar,
                    as: 'Avatar',
                    attributes: ['image']
                },
                attributes: [
                    'id',
                    'slug',
                    'firstname',
                    'lastname',
                    'email',
                    'phone',
                    'gender',
                    'state'
                ],
            },
            attributes: [
                'id',
                'day',
                'start_hour',
                'end_hour',
            ]
        })

        res.json({
            message: 'Therapies found',
            data: data
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}