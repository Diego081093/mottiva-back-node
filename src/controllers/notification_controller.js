const fetch = require('node-fetch')
const {
    tbl_notification,
    tbl_notification_attributes,
    tbl_session,
    // tbl_session_attribute,
    tbl_profile,
    tbl_user,
    tbl_room,
    tbl_chat,
    tbl_chat_participants,
    tbl_benefit_profile,
    tbl_benefit,
    tbl_medias,
    tbl_media_profile
} = require('../models')
const { Op } = require('sequelize')


const { DateTime } = require('luxon');

export async function getNotifications(req, res){

    try{

        const key_filter = req.query.filter

        let whereQuery = {
            state: 1
        }

        if( key_filter ){

            whereQuery = {
                key: key_filter,
                state: 1
            }
        }

        const notifications = await tbl_notification.findAll({
            where: {
                tbl_profile_id: req.user.idProfile,
                state: 1
            },
            include: {
                model: tbl_notification_attributes,
                as: 'NotificationAttributes',
                where: whereQuery,
                attributes: [
                    'key',
                    'value'
                ],
                required: false
            },
            attributes: [
                'id',
                'type',
                'title',
                'description',
                'state',
                'created_at',
                'done'
            ],
            order: [
                ['created_at', 'DESC']
            ]
        })

        const resUpdate = await tbl_notification.update({
            done: 1
        }, {
            where: {
                tbl_profile_id: req.user.idProfile,
                state: 1,
                done: 0
            }
        })

        res.json({
            message: 'Notifications found',
            data: notifications
        })

    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function setTempoSession(req, res) {
    try {
        const now = DateTime.fromISO(DateTime.now(), { locale: 'es-ES', zone: 'UTC' })
        const startMedium = now.plus({hour: -5, minutes: 28}).toString();
        const endMedium = now.plus({hour: -5,minutes: 32}).toString();
        const startQuarter = now.plus({hour: -5,minutes: 13}).toString();
        const endQuarter = now.plus({hour: -5,minutes: 17}).toString();

        const listPatients = await tbl_session.findAll({
            where: {
                [Op.or] : [
                    {
                        date: {
                            [Op.gte] : startMedium,
                            [Op.lte] : endMedium
                        },
                    },
                    {
                        date: {
                            [Op.gte] : startQuarter,
                            [Op.lte] : endQuarter
                        }
                    }
                ],
                state : 1
            },
            include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                where: { state: 1 },
                attributes: [ 'id' ],
                include: {
                    model: tbl_user,
                    as: 'User',
                    where: { state: 1 },
                    attributes: [ 'id', 'token_device' ]
                }
            },
            attributes: [ 'id', 'date', ['tbl_profile_psychologist_id', 'idProfilePsychologist'] ]
        })
        for(const session of listPatients) {
            if (session.ProfilePatient.User.dataValues.token_device != null || session.ProfilePatient.User.dataValues.token_device != '') {
                var end = new Date(session.dataValues.date);
                var start = new Date();

                var differ = Math.abs(end.getTime() - start.getTime())
                var minutesp = Math.ceil(differ / (1000 * 60));
                let minutes = (minutesp > 27) ? 30 : 15
                const body = {
                    notification: {
                        title: "Mottiva",
                        body: "Tu sessión comienza en " + minutes + " minutos."
                    },
                    data: {
                        id: session.dataValues.id,
                        date: session.dataValues.date,
                        idProfilePsychologist: session.dataValues.idProfilePsychologist,
                        points: 5
                    },
                    to: session.ProfilePatient.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                const save = await tbl_notification.create({
                    tbl_profile_id: session.ProfilePatient.id,
                    type: 'session',
                    title: "Tu sessión comienza en " + minutes + " minutos.",
                    description: '',
                    state: 1,
                    created_by: 'root',
                    createdAt: now.plus({hour: -5}).toString()
                })
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }
        }
        res.json({ message: 'Process successfully', session: listPatients})
    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationUser(req, res) {
    try {
        const idRole = req.user.idRole
        if (idRole == 2) {
            const { id } = req.params
            const onePatient = await tbl_session.findOne({
                where: { state: 1, id },
                include: {
                    model: tbl_profile,
                    as: 'ProfilePatient',
                    where: { state: 1 },
                    attributes: [ 'id' ],
                    include: {
                        model: tbl_user,
                        as: 'User',
                        where: { state: 1 },
                        attributes: [ 'id', 'token_device' ]
                    },
                },
                attributes: [ 'id', 'date', 'twilio_uniquename', ['tbl_profile_psychologist_id', 'idProfilePsychologist'] ]
            })
            if (onePatient.dataValues.ProfilePatient.User.dataValues.token_device != null || onePatient.dataValues.ProfilePatient.User.dataValues.token_device != '') {
                const body = {
                    notification: {
                        title: "Mottiva",
                        body: "Tu sessión ha comenzado"
                    },
                    data: {
                        id: onePatient.dataValues.id,
                        date: onePatient.dataValues.date,
                        idProfilePsychologist: onePatient.dataValues.idProfilePsychologist,
                        type: 'start_sessionRoom',
                        twilio_uniquename: onePatient.dataValues.twilio_uniquename,
                        points: 5
                    },
                    to: onePatient.dataValues.ProfilePatient.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }
            res.status(200).json({
                message: 'Successfully'
            })
        } else {
            res.status(400).json({
                non_field_errors: 'Unable to register with provided credentials.'
            })
        }
    } catch(err) {
        console.error('Something went error on sendPushNotificationUser:', err)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationUsers(req, res) {
    try {
        const { idRole } = req.user
        if (idRole == 2) {
            const { id } = req.params
            const room = await tbl_room.findOne({
                where: { state: 1, id },
                include: {
                    model: tbl_chat,
                    where: { state: 1 },
                    attributes: [ 'id', 'start' ]
                },
                attributes: [ 'id', 'twilio_uniquename' ]
            })
            let fullPatients = null
            if (room !== null) {
                fullPatients = await tbl_chat_participants.findAll({
                    where: { state: 1, tbl_chat_id: room.tbl_chat.dataValues.id },
                    include: {
                        model: tbl_profile,
                        as: 'Profile',
                        where: { state: 1, tbl_role_id: 1 },
                        attributes: [ 'id' ],
                        include: {
                            model: tbl_user,
                            as: 'User',
                            where: { state: 1 },
                            attributes: [ 'id', 'token_device' ]
                        },
                    },
                    attributes: [ 'id' ]
                })
                for(let patient of fullPatients) {
                    if (patient.dataValues.Profile.User.dataValues.token_device != null || patient.dataValues.Profile.User.dataValues.token_device != '') {
                        const body = {
                            notification: {
                                title: "Mottiva",
                                body: "Tu sessión ha comenzado"
                            },
                            data: {
                                id: room.dataValues.id,
                                date: room.tbl_chat.dataValues.start,
                                type: 'start_sessionRoom',
                                twilio_uniquename: room.dataValues.twilio_uniquename,
                                points: 5
                            },
                            to: patient.dataValues.Profile.User.dataValues.token_device,
                            direct_boot_ok: true
                        }
                        fetch('https://fcm.googleapis.com/fcm/send', {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                            body : JSON.stringify(body)
                        })
                        .then(json => {
                            console.log(json.status);
                        });
                    }
                }
            }
            res.status(200).json({
                message: 'Successfully'
            })
        } else {
            res.status(400).json({
                non_field_errors: 'Unable to register with provided credentials.'
            })
        }
    } catch(err) {
        console.error('Something went error on sendPushNotificationUsers:', err)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationSessionStart(req, res) {
    try {
        const { idProfile } = req.user
        const now = DateTime.now()
        const start = now.plus({hour: -5, minutes: -2}).toString();
        const end = now.plus({hour: -5, minutes: 2}).toString();

        const listPsychologists = await tbl_session.findAll({
            where: {
                date: {
                    [Op.gte] : start,
                    [Op.lte] : end
                },
                state : 1
            },
            include: {
                model: tbl_profile,
                as: 'ProfilePsychologist',
                where: { state: 1, id: idProfile },
                attributes: [ 'id' ],
                include: {
                    model: tbl_user,
                    as: 'User',
                    where: { state: 1 },
                    attributes: [ 'id', 'token_device' ]
                }
            },
            attributes: [ 'id', 'date', 'twilio_uniquename', ['tbl_profile_psychologist_id', 'idProfilePsychologist'] ]
        })
        for(const session of listPsychologists) {
            if (session.ProfilePsychologist.User.dataValues.token_device != null || session.ProfilePsychologist.User.dataValues.token_device != '') {

                const body = {
                    notification: {
                        title: "Mottiva",
                        body: "Tu sessión debe ser iniciada en este momento"
                    },
                    data: {
                        id: session.dataValues.id,
                        date: session.dataValues.date,
                        type: 'start_session',
                        twilio_uniquename: session.dataValues.twilio_uniquename
                    },
                    to: session.ProfilePsychologist.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }
        }
        res.json({ message: 'Process successfully', session: listPsychologists})
    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationRoomStart(req, res) {
    try {
        const { idProfile } = req.user
        const now = DateTime.now()
        const start = now.plus({hour: -5, minutes: -2}).toString();
        const end = now.plus({hour: -5, minutes: 2}).toString();

        const listPsychologists = await tbl_room.findAll({
            where: {
                start: {
                    [Op.gte] : start,
                    [Op.lte] : end
                },
                state : 1
            },
            include: {
                model: tbl_chat,
                as: 'Chat',
                where: { state: 1 },
                attributes: [ 'id' ],
                include: {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    where: { state: 1 },
                    attributes: [ 'id' ],
                    include: {
                        model: tbl_profile,
                        as: 'Profile',
                        where: { state: 1, id: idProfile },
                        //where: { state: 1, tbl_role_id: 2 },
                        attributes: [ 'id' ],
                        include: {
                            model: tbl_user,
                            as: 'User',
                            where: { state: 1 },
                            attributes: [ 'id', 'token_device' ]
                        }
                    }
                }
            },
            attributes: [ 'id', 'title', 'start', 'rules', 'participants', 'twilio_uniquename' ]
        })
        for(const session of listPsychologists) {
            if (session.Chat.ChatParticipant.Profile.User.dataValues.token_device != null || session.Chat.ChatParticipant.Profile.User.dataValues.token_device != '') {

                const body = {
                    notification: {
                        title: "Mottiva",
                        body: "La Sala de emociones debe ser iniciada en este momento"
                    },
                    data: {
                        id: session.dataValues.id,
                        date: session.dataValues.date,
                        type: 'start_session',
                        twilio_uniquename: session.dataValues.twilio_uniquename
                    },
                    to: session.Chat.ChatParticipant.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }
        }
        res.json({ message: 'Process successfully', session: listPsychologists})
    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationBenefits(req, res) {
    try {
        const { idProfile } = req.user
        const now = DateTime.now()
        const start = now.plus({hour: -5, minutes: -2}).toString();
        const end = now.plus({hour: -5, minutes: 2}).toString();

        const listBenefits = await tbl_benefit.findAll({
            where: {

                state : 1
            },
            include: {
                model: tbl_benefit_profile,
                as: 'BenefitProfile',
                where: { state: 1 },
                attributes: [ 'id' ],
                include: {
                    model: tbl_profile,
                    as: 'Profile',
                    where: { state: 1, id: idProfile },
                    //where: { state: 1, tbl_role_id: 2 },
                    attributes: [ 'id' ],
                    include: {
                        model: tbl_user,
                        as: 'User',
                        where: { state: 1 },
                        attributes: [ 'id', 'token_device' ]
                    }
                }
            },
            attributes: [ 'id', 'point']
        })
        for(const benefit of listBenefits) {
            if (benefit.BenefitProfile.Profile.User.dataValues.token_device != null || benefit.BenefitProfile.Profile.User.dataValues.token_device != '') {

                const body = {
                    notification: {
                        title: "Tienes nuevos retos esta semana",
                        body: "Puedes ganar hasta 5000 puntos"
                    },
                    data: {
                        id: idProfile
                    },
                    to: benefit.BenefitProfile.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }
        }
        res.json({ message: 'Process successfully', benefit: listBenefits})
    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function sendPushNotificationMedias(req, res) {
    try {
        const { idProfile } = req.user
        const now = DateTime.now()
        const start = now.plus({hour: -5, minutes: -2}).toString();
        const end = now.plus({hour: -5, minutes: 2}).toString();

        const media = await tbl_medias.findOne({
            where: {

                state : 1
            },
            include: {
                model: tbl_media_profile,
                as: 'onProfile',
                where: { state: 1 },
                attributes: [ 'id' ],
                include: {
                    model: tbl_profile,
                    as: 'Profile',
                    where: { state: 1, id: idProfile },
                    //where: { state: 1, tbl_role_id: 2 },
                    attributes: [ 'id' ],
                    include: {
                        model: tbl_user,
                        as: 'User',
                        where: { state: 1 },
                        attributes: [ 'id', 'token_device' ]
                    }
                }
            },
            attributes: [ 'id']
        })
        const videos = await tbl_media_profile.findAndCountAll({where:{tbl_profile_id:idProfile}})
        const totalVideos= videos.count

            if (media.onProfile.Profile.User.token_device != null || media.onProfile.Profile.User.token_device != '') {

                const body = {
                    notification: {
                        title: "Tienes "+ totalVideos +" vídeos nuevos en tu reto semanal",
                        body: "Puedes ganar hasta 2500 puntos"
                    },
                    data: {
                        id: idProfile
                    },
                    to: media.onProfile.Profile.User.dataValues.token_device,
                    direct_boot_ok: true
                }
                fetch('https://fcm.googleapis.com/fcm/send', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                    body : JSON.stringify(body)
                })
                .then(json => {
                    console.log(json.status);
                });
            }

        res.json({ message: 'Process successfully', media: media})
    } catch (error) {
        console.error('You have a error on setTempoSession: ', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function notify(notification = { title: '', body: '' }, data = { idProfile, type }, to) {
    try {
        const now = DateTime.fromISO(DateTime.now(), { locale: 'es-ES', zone: 'UTC' })
        const body = { notification, data, to, direct_boot_ok: true }
        const notificationCreate = await tbl_notification.create({
            tbl_profile_id: data.idProfile,
            type: data.type,
            title: notification.title,
            description: notification.body,
            state: 1,
            created_by: 'JJohan',
            createdAt: now.plus({hour: -5}).toString()
        })
        if (notificationCreate) {
            const status = await fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'key=AAAAfmkN900:APA91bHLpfPAXlS2QhXkdTJNV_GdY8K8DAevrzNgjjhS-FaSaWKZoixrOCM-kQSVjvsyd2JIcUB3XWSlyxzPxqLI9D9mieai3pz4byHfMwmy9WXdtTM-0kBg132z4exec6kE44uUJeeN' },
                body : JSON.stringify(body)
            })
            .then(json => {
                return json.statusText === 'OK' ? true : false
            })
            return status
        } else {
            console.error('You have a error on notify')
            return false
        }
    } catch (error) {
        console.log('Something went error notify:', error)
        return false
    }
}