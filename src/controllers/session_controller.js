import {where} from "sequelize";
import { notify } from '../controllers/notification_controller'

const {
    tbl_user,
    tbl_session,
    tbl_profile,
    tbl_profile_attribute,
    tbl_session_attribute,
    tbl_avatar,
    tbl_session_note,
    tbl_room,
    tbl_room_note,
    tbl_chat_participants,
    tbl_session_cases,
    tbl_chat,
    tbl_room_participants,
    tbl_session_task,
    tbl_blocked
} = require('../models')
const {MottivaConstants} = require('./../helpers')
const {Op, json, Sequelize} = require('sequelize');
const {DateTime} = require('luxon');
const url = require('url');
import {createToken} from './token_controller';
import {twilioCreateRoom} from './room_controller'
import moment from 'moment-timezone'

export async function getListSession(req, res) {
    try {
        const sessions = await tbl_session.findAll();
        res.json({sessions});
    } catch (error) {
        console.log(error);
    }
}

export async function getSessions(req, res) {

    if (req.user.idRole == 2) {

        getSessionsPsychologist(req, res)

    } else if (req.user.idRole == 3) {

        getSessionsHelpdesk(req, res)

    } else {

        return res.status(401).send('Unauthorized')
    }
}

export async function getSessionsPsychologist(req, res) {

    try {
        let to = req.query.to,
            offset = 0,
            limit = 20,
            schedule = req.query.schedule,
            search = req.query.search,
            whereQuery,
            searchWhereQuery = {},
            data = []

        if (req.query.offset) {
            offset = parseInt(req.query.offset)
        }

        if (req.query.limit) {
            limit = parseInt(req.query.limit)
        }

        // Today, 00:00:00
        // const now = DateTime.fromObject({day: DateTime.now().day})
        const now = moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS')
        switch (to) {
            case 'today':
                whereQuery = {
                    date: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-DD 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'days').format('YYYY-MM-DD 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'week':
                const weekday = moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('d')
                whereQuery = {
                    date: {
                        // Day month - ( Day week - 1 )
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add((1 - weekday), 'days').format('YYYY-MM-DD 00:00:00'),
                        // Day month + ( 7 - Day week ) + 1
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(3, 'days').format('YYYY-MM-DD 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'month':
                whereQuery = {
                    date: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-01 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'months').format('YYYY-MM-01 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'year':
                whereQuery = {
                    date: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-01-01 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'years').format('YYYY-01-01 00:00:00')
                    },
                    state: 1
                }
                break;

            default:
                whereQuery = {state: 1}
                break;
        }
        console.log(whereQuery)

        if (search) {
            searchWhereQuery = {
                [Op.or]: [
                    {
                        firstname: {
                            [Op.iLike]: '%' + search + '%'
                        }
                    }, {
                        lastname: {
                            [Op.iLike]: '%' + search + '%'
                        }
                    }
                ]
            }
        }

        const {count, rows: sessions} = await tbl_session.findAndCountAll({
            where: whereQuery,
            include: [
                {
                    model: tbl_profile,
                    as: 'Psychologist',
                    where: {
                        tbl_user_id: req.user.id,
                        state: 1
                    },
                    attributes: []
                },
                {
                    model: tbl_profile,
                    as: 'Patient',
                    where: searchWhereQuery,
                    attributes: [
                        'firstname',
                        'lastname',
                        //'slug'
                    ],
                    include: {
                        model: tbl_avatar,
                        as: 'Avatar',
                        attributes: ['image']
                    }
                }
            ],
            attributes: [
                'id',
                [
                    'tbl_profile_pacient_id',
                    'patient_id'
                ],
                [
                    'tbl_profile_psychologist_id',
                    'psychologist_id'
                ],
                'date',
                'state',
                'order',
                'week'
            ],
            limit,
            offset,
            distinct: true
        });

        // if( sessions.length == 0 ){

        //     return res.status(404).json({
        //         message: 'No sessions were found'
        //     })
        // }

        let morning = [], afternoon = []
        await Promise.all(
            sessions.map(session => {
                return new Promise(async resolve => {
                    const total = await tbl_session.count({
                        where: {
                            tbl_profile_pacient_id: session.dataValues.patient_id,
                            tbl_profile_psychologist_id: session.dataValues.psychologist_id
                        }
                    })
                    session.setDataValue('total', total)
                    data.push(session)
                    if (schedule) {
                        const sessionDate = DateTime.fromJSDate(session.date)
                        if (sessionDate.hour < 12) {
                            morning.push(session)
                        } else {
                            afternoon.push(session)
                        }
                    }
                    resolve(1)
                })
            })
        )


        let urlPath = req.baseUrl + req.path
        if (urlPath.substring(urlPath.length - 1) == '/') {
            urlPath = urlPath.substring(0, urlPath.length - 1)
        }

        let previousPageUrl = null,
            nextPageUrl = null

        if (offset > 0) {
            let p_query_offset = offset - limit
            if ((offset - limit) < 0) {
                p_query_offset = 0
            }
            previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: p_query_offset,
                    limit
                }
            });
        }

        if (offset + limit < count) {
            nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: offset + limit,
                    limit
                }
            });
        }

        let whereQueryRoom = {}
        switch (to) {
            case 'today':
                whereQueryRoom = {
                    start: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-DD 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'days').format('YYYY-MM-DD 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'week':
                const weekday = now.weekday
                whereQueryRoom = {
                    start: {
                        // Day month - ( Day week - 1 )
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add((1 - weekday), 'days').format('YYYY-MM-DD 00:00:00'),
                        // Day month + ( 7 - Day week ) + 1
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(3, 'days').format('YYYY-MM-DD 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'month':
                whereQueryRoom = {
                    start: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-MM-01 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'months').format('YYYY-MM-01 00:00:00')
                    },
                    state: 1
                }
                break;

            case 'year':
                whereQueryRoom = {
                    start: {
                        [Op.gte]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').format('YYYY-01-01 00:00:00'),
                        [Op.lt]: moment(new Date(), 'YYYY-MM-DDTHH:mm:ss+SS').add(1, 'years').format('YYYY-01-01 00:00:00')
                    },
                    state: 1
                }
                break;

            default:
                whereQueryRoom = {
                    state: 1
                }
                break;
        }
        const rooms = await tbl_room.findAll({
            where: whereQueryRoom,
            attributes: [
                'id', 'title', 'image', ['start', 'date']
            ],
            include: [
                {
                    model: tbl_room_participants,
                    where: {
                        tbl_profile_participant_id: req.user.idProfile
                    }
                }
            ],
            order: [['start', 'ASC']]
        })

        res.json({
            message: 'Sessions found',
            count,
            previous: previousPageUrl,
            next: nextPageUrl,
            limit,
            offset,
            data,
            rooms
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getSessionsHelpdesk(req, res) {
    try {

        let to = req.query.to,
            limit = 20,
            offset = 0,
            search = req.query.search

        if (req.query.limit) {

            limit = parseInt(req.query.limit)
        }

        if (req.query.offset) {

            offset = parseInt(req.query.offset)
        }
        let whereQuery

        // Today, 00:00:00
        const now = DateTime.fromObject({day: DateTime.now().plus({hour: -5}).day})

        switch (to) {
            case 'today':

                whereQuery = {
                    date: {
                        [Op.gte]: now.toISO(),
                        [Op.lt]: now.plus({hour: -5, days: 1}).toISO()
                    }
                }

                break;

            case 'pending':

                whereQuery = {
                    patient_state: 2
                }

                break;

            case 'draft':

                whereQuery = {
                    patient_state: 3
                }

                break;

            // case 'year':

            //     whereQuery = {
            //         date: {
            //             [Op.gte]: DateTime.fromObject({day: 1, month: 1}).toISO(),
            //             [Op.lt]: DateTime.fromObject({day: 1, month: 1, year: (now.year + 1)}).toISO()
            //         }
            //     }

            //     break;

            default:

                whereQuery = {}

                break;
        }

        const session_attributes = [
            'id',
            'date',
            'state',
            'patient_state',
            'psychologist_state',
            'order',
            'week'
        ]

        const profile_attributes = [
            'id',
            'firstname',
            'lastname',
            'birthday'
        ]

        // Obtain array sessions id
        let {count, rows: sessions} = await tbl_session.findAndCountAll({
            where: whereQuery,
            attributes: ['id', 'date'],
            limit,
            offset,
            distinct: true,
            order: [['date', 'ASC']],
            include: [
                {
                    model: tbl_profile,
                    as: 'Helpdesk',
                    attributes: ['id']
                }
            ],
            group: ['"tbl_session.id"', '"Helpdesk.id"'],
            having:
                Sequelize.where(Sequelize.literal('"Helpdesk"."id"'),
                    {
                        [Op.or]: [{[Op.eq]: null}, {[Op.eq]: req.user.idProfile}]
                    }
                )
        })

        // if( sessions.length == 0 ){

        //     return res.status(404).json({
        //         message: 'No sessions were found'
        //     })
        // }

        const arr_sessions_id = sessions.map(session => session.id)

        // console.log('')
        // console.log('')
        // console.log('arr_sessions_id')
        // console.log(arr_sessions_id)
        // console.log('')
        // console.log('')

        if (search) {

            const searchWhereQuery = {
                [Op.or]: [
                    {
                        firstname: {
                            [Op.iLike]: '%' + search + '%'
                        }
                    }, {
                        lastname: {
                            [Op.iLike]: '%' + search + '%'
                        }
                    }
                ]
            }

            const p_search_psy = tbl_session.findAndCountAll({
                where: {
                    id: {
                        [Op.or]: arr_sessions_id
                    }
                },
                include: [
                    {
                        model: tbl_profile,
                        as: 'Psychologist',
                        where: searchWhereQuery,
                        attributes: profile_attributes
                    },
                    {
                        model: tbl_profile,
                        as: 'Patient',
                        attributes: profile_attributes
                    }
                ],
                attributes: session_attributes
            })

            const p_search_pat = tbl_session.findAndCountAll({
                where: {
                    id: {
                        [Op.or]: arr_sessions_id
                    }
                },
                include: [
                    {
                        model: tbl_profile,
                        as: 'Psychologist',
                        attributes: profile_attributes
                    },
                    {
                        model: tbl_profile,
                        as: 'Patient',
                        where: searchWhereQuery,
                        attributes: profile_attributes
                    }
                ],
                attributes: session_attributes
            })

            const [{count: s_psy_count, rows: search_psy}, {
                count: s_pat_count,
                rows: search_pat
            }] = await Promise.all([p_search_psy, p_search_pat])

            sessions = search_psy.concat(search_pat)

            let count_sum = s_psy_count + s_pat_count

            if (count_sum < sessions_count) {
                sessions_count = count_sum
            }
        } else {
            sessions = await tbl_session.findAll({
                where: {
                    id: {
                        [Op.or]: arr_sessions_id
                    }
                },
                include: [
                    {
                        model: tbl_profile,
                        as: 'Psychologist',
                        attributes: profile_attributes
                    },
                    {
                        model: tbl_profile,
                        as: 'Patient',
                        attributes: profile_attributes
                    },
                    {
                        model: tbl_profile,
                        as: 'Helpdesk',
                        attributes: ['id']
                    }
                ],
                attributes: session_attributes,
                order: [['date', 'ASC']],
                group: ['"tbl_session.id"',
                    '"Psychologist.id"', '"Patient.id"', '"Helpdesk.id"'],
                having:
                    Sequelize.where(Sequelize.literal('"Helpdesk"."id"'),
                        {
                            [Op.or]: [{[Op.eq]: null}, {[Op.eq]: req.user.idProfile}]
                        }
                    )
            })
        }

        sessions = await Promise.all(
            sessions.map(session => {

                return new Promise(async resolve => {

                    const p_concluded = tbl_session.count({
                        where: {
                            tbl_profile_pacient_id: session.Patient.id,
                            tbl_profile_psychologist_id: session.Psychologist.id,
                            date: {
                                [Op.lt]: now.toISO()
                            }
                        }
                    })

                    const p_total = tbl_session.count({
                        where: {
                            tbl_profile_pacient_id: session.Patient.id,
                            tbl_profile_psychologist_id: session.Psychologist.id
                        }
                    })
                    let notification = {
                        state: 'error',
                        name: 'Reagendar'
                    }

                    let fechaActual = new Date()
                    const year = fechaActual.getFullYear()
                    let birthdayPatient = session.Patient.birthday
                    const newBirthdayPatient = new Date(birthdayPatient)
                    const yearPatient = newBirthdayPatient.getFullYear()
                    const agePatient = year - yearPatient

                    let birthdayPsychologist = session.Psychologist.birthday
                    const newBirthdayPsychologist = new Date(birthdayPsychologist)
                    const yearPsychologist = newBirthdayPsychologist.getFullYear()
                    const agePsychologist = year - yearPsychologist
                    const personal = {"patient": "error", "psychologist": "error"}
                    const [concluded, total] = await Promise.all([p_concluded, p_total])
                    const nnn = new Date(session.date)
                    const hhh = fechaActual.getHours() - nnn.getHours()
                    const mmm = fechaActual.getMinutes() - nnn.getMinutes()

                    const timeLeft = hhh + ':' + Math.abs(mmm)

                    session.setDataValue('timeLeft', timeLeft)
                    session.Patient.setDataValue('age', agePatient)
                    session.Psychologist.setDataValue('age', agePsychologist)
                    session.setDataValue('total', total)
                    session.setDataValue('reasign', concluded)
                    session.setDataValue('personal', personal)
                    session.setDataValue('notification', notification)

                    const countCase = await tbl_session_cases.count({
                        where: {
                            tbl_session_id: session.id
                        }
                    })
                    console.log(countCase)
                    session.setDataValue('countCase', countCase)

                    resolve(session)
                })
            })
        )

        let urlPath = req.baseUrl + req.path

        if (urlPath.substring(urlPath.length - 1) == '/') {

            urlPath = urlPath.substring(0, urlPath.length - 1)
        }

        let previousPageUrl = null,
            nextPageUrl = null

        if (offset > 0) {

            let p_query_offset = offset - limit

            if ((offset - limit) < 0) {

                p_query_offset = 0
            }

            previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: p_query_offset,
                    limit
                }
            });
        }

        if (offset + limit < count) {

            nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: offset + limit,
                    limit
                }
            });
        }

        return res.json({
            message: 'Sessions found',
            count: count.length,
            previous: previousPageUrl,
            next: nextPageUrl,
            limit,
            offset,
            data: sessions
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getSession(req, res) {
    try {
        const {id} = req.params;
        const sessions = await tbl_session.findAll({
            where: {tbl_profile_pacient_id: id, state: 1},
            include: [
                {
                    model: tbl_profile,
                    as: 'Psychologist'
                },
                {
                    model: tbl_session_note,
                    as: 'Notes'
                }
            ],
            order: [['week', 'DESC']]
        });
        const Patient = await tbl_profile.findByPk(id, {
            where: {
                state: 1
            },
            include: [
                {
                    model: tbl_avatar,
                    as: 'Avatar'
                }
            ]
        })
        Patient.dataValues.attributes = await tbl_profile_attribute.findAll({
            where: {
                tbl_profile_id: id
            },
            attributes: ['key', 'value']
        })
        const chat_pacients = await tbl_chat_participants.findAll({
            where: {
                tbl_profile_id: id
            },
            attributes: [['tbl_chat_id', 'idChat']]
        })
        const chat_psychologists = await tbl_chat_participants.findAll({
            where: {
                tbl_profile_id: sessions[sessions.length - 1].Psychologist.dataValues.id
            },
            attributes: [['tbl_chat_id', 'idChat']]
        })
        let idChat = null
        const chat = await tbl_chat.findOne({
            attributes: [
                'id'
            ],
            where: {
                helpdesk: 0,
                tbl_room_id: null
            },
            include: [
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    attributes: [
                        'id',
                        ['tbl_chat_id', 'idChat'],
                        ['tbl_profile_id', 'idProfile']
                    ],
                    where: {
                        tbl_profile_id: id
                    }
                },
                {
                    model: tbl_chat_participants,
                    as: 'ChatParticipantPsychologist',
                    attributes: [],
                    where: {
                        tbl_profile_id: req.user.idProfile
                    }
                }
            ]
        })
        console.log(sessions[sessions.length - 1].Psychologist.dataValues.id)
        if (chat === null) {
            const chat = await tbl_chat.create({
                start: Date.now(),
                helpdesk: 0
            })

            const chatParticipantPsychologist = await tbl_chat_participants.create({
                tbl_chat_id: chat.id,
                tbl_profile_id: id
            })
            const chatParticipantPatient = await tbl_chat_participants.create({
                tbl_chat_id: chat.id,
                tbl_profile_id: req.user.idProfile
            })

            idChat = chat.id
        } else {
            idChat = chat.id
        }
        await Promise.all(sessions.map(async (item, key) => {
            item.dataValues.attributes = await tbl_session_attribute.findAll({
                where: {
                    state: 1, tbl_session_id: item.dataValues.id
                },
                attributes: ['key', 'value'],
                order: [['id', 'ASC']]
            })
            return item;
        })).then((sessions) => {
            res.json({
                Patient,
                idChat,
                sessions
            })
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getNotesList(req, res) {
    try {

        let type = req.query.type,
            offset = 0,
            limit = 20,
            data,
            count

        if (req.query.offset) {

            offset = parseInt(req.query.offset)
        }

        if (req.query.limit) {

            limit = parseInt(req.query.limit)
        }

        switch (type) {
            case 'room':

                const {count: count_room, rows: rows_room} = await tbl_room_note.findAndCountAll({
                    where: {
                        tbl_profile_psychologist_id: req.user.idProfile,
                        state: 1
                    },
                    attributes: [
                        'id',
                        [
                            'notes',
                            'text'
                        ],
                        'state',
                        'created_at'
                        //'name',
                        //'type'
                    ],
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Patient',
                            attributes: [
                                'firstname',
                                'lastname'
                            ],
                            include: {
                                model: tbl_avatar,
                                as: 'Avatar',
                                attributes: ['image']
                            }
                        },
                        {
                            model: tbl_room,
                            as: 'Room',
                            attributes: [
                                'id',
                                'title',
                                'start',
                                'end',
                                'rules',
                                'image'
                            ]
                        }
                    ],
                    limit,
                    offset,
                    distinct: true
                })

                count = count_room
                data = rows_room

                break;

            default:

                type = 'session'

                const {count: count_session, rows: rows_session} = await tbl_session_note.findAndCountAll({
                    where: {
                        state: 1
                    },
                    include: {
                        model: tbl_session,
                        as: 'Session',
                        attributes: [
                            'date',
                            'order',
                            'tbl_profile_pacient_id',
                            'tbl_profile_psychologist_id',
                        ],
                        required: true,
                        include: [
                            {
                                model: tbl_profile,
                                as: 'Psychologist',
                                where: {
                                    tbl_user_id: req.user.id,
                                    state: 1
                                },
                                attributes: []
                            },
                            {
                                model: tbl_profile,
                                as: 'Patient',
                                attributes: [
                                    'firstname',
                                    'lastname'
                                ],
                                include: {
                                    model: tbl_avatar,
                                    as: 'Avatar',
                                    attributes: ['image']
                                }
                            },
                            {
                                model: tbl_session_attribute,
                                as: 'SessionAttributes',
                                attributes: [
                                    'key',
                                    'value'
                                ]
                            }
                        ]
                    },
                    attributes: [
                        'id',
                        ['note', 'text'],
                        'state',
                        'created_at'
                        //'key',
                        //'value'
                    ],
                    limit,
                    offset,
                    distinct: true
                })

                data = await Promise.all(
                    rows_session.map(note => {

                        return new Promise(async resolve => {

                            const total = await tbl_session.count({
                                where: {
                                    tbl_profile_pacient_id: note.dataValues.Session.tbl_profile_pacient_id,
                                    tbl_profile_psychologist_id: note.dataValues.Session.tbl_profile_psychologist_id
                                }
                            })

                            note.Session.setDataValue('total', total)

                            resolve(note)
                        })
                    })
                )

                count = count_session

                break;
        }

        let urlPath = req.baseUrl + req.path

        if (urlPath.substring(urlPath.length - 1) == '/') {

            urlPath = urlPath.substring(0, urlPath.length - 1)
        }

        let previousPageUrl = null,
            nextPageUrl = null

        if (offset > 0) {

            let p_query_offset = offset - limit

            if ((offset - limit) < 0) {

                p_query_offset = 0
            }

            previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: p_query_offset,
                    limit
                }
            });
        }

        if (offset + limit < count) {

            nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: offset + limit,
                    limit
                }
            });
        }

        // Capitalize
        type = type[0].toUpperCase() + type.slice(1)

        res.json({
            message: type + ' notes found',
            count,
            previous: previousPageUrl,
            next: nextPageUrl,
            limit,
            offset,
            data
        });

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function disableNote(req, res) {

    try {

        let note_type = req.body.type,
            note,
            not_found,
            note_id = req.params.id

        switch (note_type) {
            case 'room':

                note = await tbl_room_note.findOne({
                    where: {
                        id: note_id,
                        tbl_profile_psychologist_id: req.user.idProfile
                    }
                })

                if (note) {

                    await note.update({
                        state: 0
                    })

                } else {

                    not_found = true
                }

                break;

            default:

                // session

                note = await tbl_session_note.findOne({
                    where: {
                        id: note_id,
                    },
                    include: {
                        model: tbl_session,
                        as: 'Session',
                        where: {
                            tbl_profile_psychologist_id: req.user.idProfile
                        }
                    }
                })

                if (note) {

                    await note.update({
                        state: 0
                    })

                } else {

                    not_found = true
                }

                break;
        }

        if (not_found) {

            return res.status(404).json({
                message: 'Note not found'
            })
        }

        res.json({
            message: 'Note disabled'
        })

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getPatientsSession(req, res) {
    try {
        const psycologyId = await tbl_profile.findOne({attributes: ['id'], where: {tbl_user_id: req.user.id}});

        const patients = await tbl_session.findAll({
            where: {state: 1, tbl_profile_psychologist_id: psycologyId.id},
            include:
                [{
                    model: tbl_profile,
                    as: 'Patient',
                    attributes: ['id', 'firstname', 'lastname'],
                    where: {state: 1},
                    include: {
                        model: tbl_avatar,
                        as: 'Avatar',
                        attributes: ['image']
                    }
                },
                    {
                        model: tbl_session_attribute,
                        as: 'SessionAttributes',
                        attributes: [['key', 'type'], 'value'],
                        where: {state: 1, key: 'type'}
                    }]
        })
        for (let patient of patients) {
            let sessions = await tbl_session.findAll({
                attributes: [['id', 'idSession'], 'date'],
                where: {
                    tbl_profile_pacient_id: patient.tbl_profile_pacient_id
                }
            })
        }
        res.json({
            data: patients
        });
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function postSession(req, res) {
    try {
        const idPatient = req.user.idProfile
        const idPsychologist = req.body.idPsychologist
        const date = req.body.date
        const week = req.body.week
        const sessionId = await tbl_session.findOne({
            attributes: ['id'],
            order: [['id', 'DESC']]
        })
        let idSession = 1
        if(sessionId) {
            idSession = sessionId.id + 1
        }
        const nameRoom = 'room-s-' + idSession
        const patientSession = await tbl_session.create({
            tbl_profile_pacient_id: idPatient,
            patient_state: 1,
            tbl_profile_psychologist_id: idPsychologist,
            psychologist_state: 1,
            date: date,
            week: week,
            twilio_uniquename: nameRoom,
            order: 1
        });
        //const type ='go'

        //await twilioCreateRoom(req,res,type,nameRoom)

        res.json({
            message: 'Session Created',
            data: patientSession
        });

    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function postNextSession(req, res) {
    try {
        const { idPatient, idPsychologist, date, week } = req.body
        const now = DateTime.fromISO((new Date(date)).toJSON(), { locale: 'es-ES', zone: 'UTC' })
        const newDate = now
        const sessionId = await tbl_session.findOne({
            attributes: ['id'],
            order: [['id', 'DESC']]
        })
        const idSession = sessionId.id + 1
        const nameRoom = 'room-s-' + idSession

        const sessionUpdate = await tbl_session.update({
            completed: true
        }, { where: {
            tbl_profile_pacient_id: idPatient,
            tbl_profile_psychologist_id: idPsychologist,
            week: { [Op.lt] : week },
            state: 1
        } })
        if (sessionUpdate) {
            const sameSession = await tbl_session.findOne({
                where: {
                    tbl_profile_pacient_id: idPatient,
                    tbl_profile_psychologist_id: idPsychologist,
                    week: week,
                    state: 1
                }
            })
            let patientSession = null
            if (sameSession === null) {
                patientSession = await tbl_session.create({
                    tbl_profile_pacient_id: idPatient,
                    patient_state: 1,
                    tbl_profile_psychologist_id: idPsychologist,
                    psychologist_state: 1,
                    date: newDate,
                    week: week,
                    twilio_uniquename: nameRoom,
                    order: 1
                });
            } else {
                patientSession = await tbl_session.update({
                    tbl_profile_pacient_id: idPatient,
                    patient_state: 1,
                    tbl_profile_psychologist_id: idPsychologist,
                    psychologist_state: 1,
                    date: newDate,
                    week: week,
                    twilio_uniquename: nameRoom,
                    order: 1
                }, {
                    where: {
                        id: sameSession.id
                    }
                });
            }
            if (patientSession) {
                const idNewSession = (sameSession === null) ? patientSession.dataValues.id : sameSession.id
                await tbl_session_task.update(
                    { state: 1 },
                    { where: { tbl_session_id: idNewSession } }
                );
                const session = await tbl_session.findOne({
                    where: {id: idNewSession, state: 1},
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Psychologist'
                        },
                        {
                            model: tbl_session_note,
                            as: 'Notes'
                        }
                    ]
                });
                session.dataValues.attributes = await tbl_session_attribute.findAll({
                    where: {
                        state: 1, tbl_session_id: idNewSession
                    },
                    attributes: ['key', 'value']
                })
                const pacient = await tbl_profile.findByPk(
                    idPatient,
                    {
                        attributes: [ 'id' ],
                        include: {
                            model: tbl_user,
                            as: 'User',
                            where: { state: 1 },
                            attributes: [ 'id', 'token_device' ]
                        }
                    }
                )
                if (pacient) {
                    const formatDate = moment((new Date(date)).toJSON(), 'YYYY-MM-DD HH:mm:ss').locale("es").format('dddd DD') + ' a las ' + moment((new Date(date)).toJSON(), 'YYYY-MM-DD HH:mm:ss').format('hh:mm a')
                    const notification = {
                        title: 'Siguiente sesión',
                        body: 'Tu sesión ha sido agendada exitosamente para el ' + formatDate
                    }
                    const data = {
                        idProfile: idPatient,
                        type: 'session'
                    }
                    const status = await notify(notification, data, pacient.dataValues.User.dataValues.token_device)
                    if (status) {
                        res.json({
                            message: 'Session Created',
                            data: session
                        })
                    } else {
                        res.json({
                            message: 'Session Created but with errors',
                            data: session
                        })
                    }
                }
            } else {
                console.error('You have a error in save next session:', error)
                res.status(500).json({
                    message: 'Something went wrong'
                });
            }
        } else {
            console.error('You have a error in update current session:', error)
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function addSessionNote(req, res) {

    try {

        const {note, priority} = req.body
        if (note) {
            let priorityVal = 0
            if (priority)
                priorityVal = priority
            const session = await tbl_session.findOne({
                where: {
                    id: req.params.id,
                    tbl_profile_psychologist_id: req.user.idProfile
                }
            })

            const currentNote = await tbl_session_note.findOne({
                where: {
                    tbl_session_id: session.id
                }
            })

            if (currentNote) {
                await tbl_session_note.update(
                    {
                        note: note,
                        priority: priorityVal
                    }, {
                        where: {
                            id: currentNote.id
                        }
                    })
            } else {
                await tbl_session_note.create(
                    {
                        tbl_session_id: session.id,
                        note: note,
                        priority: priorityVal
                    })
            }

            res.status(200).json({
                message: 'Note registered'
            })
        } else {
            res.status(200).json({
                message: 'Nothing to do'
            });
        }
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function completeSession(req, res) {

    try {


        const session = await tbl_session.findOne({
            where: {
                id: req.params.id,
                completed: false
            }
        })

        if (session) {

            await session.update({
                completed: true
            })

            const patient = await tbl_profile.findByPk(session.tbl_profile_pacient_id)

            if (patient) {

                await patient.update({
                    potins: patient.points + MottivaConstants.SESSION_POINTS
                })
                let token = await createToken(req, res, req.user.id, req.user.username)


                return res.json({
                    message: 'Session completed',
                    data: {token}
                })
            }

            return res.status(404).json({
                message: 'Patient not found'
            })
        }

        return res.status(404).json({
            message: 'Session not found'
        })


    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function closedSession(req, res) {
    try {
        const state = req.body.state
        const idSession = req.body.idSession

        const closedSession = await tbl_session.update({
                state: state
            },
            {
                where: {
                    id: idSession
                }
            });

        res.json({
            message: 'Session Closed!!'
        });
    } catch (error) {
        console.error('You have a error in:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function rescheduleSession(req, res) {

    try {

        console.log(req.body.date)
        await tbl_session.update({
            date: req.body.date
        }, {
            where: {
                id: req.params.id
            }
        })

        res.json({
            message: 'Session updated'
        })

    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function rescheduleReplaceSession(req, res) {

    try {
        const dateNow= new Date();
    await tbl_session_cases.create({
        tbl_session_id:req.params.id,
        tbl_profile_id:req.user.idProfile,
        action:req.body.action,
        theme:req.body.theme,
        reason:req.body.subject,
        emotion:req.body.emotion,
        date:dateNow
    })
        
        await tbl_session.update({
            tbl_profile_psychologist_id:req.body.psychologist,
            date: req.body.date
        }, {
            where: {
                id: req.params.id
            }
        })

        res.json({
            message: 'Session updated'
        })

    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function cancelSession(req, res) {

    try {
        const dateNow= new Date();
    await tbl_session_cases.create({
        tbl_session_id:req.params.id,
        tbl_profile_id:req.user.idProfile,
        action:req.body.action,
        theme:req.body.theme,
        reason:req.body.subject,
        emotion:req.body.emotion,
        date:dateNow
    })
        const session = await tbl_session.findOne({
            where: {
                id: req.params.id
            }
        })
        await tbl_session.update({
            state: 0
        }, {
            where: {
                tbl_profile_pacient_id: session.tbl_profile_pacient_id,
                tbl_profile_psychologist_id: session.tbl_profile_psychologist_id
            }
        })
        await tbl_blocked.create(
            {
                tbl_profile_pacient_id: session.tbl_profile_pacient_id,
                tbl_profile_psychologist_id: session.tbl_profile_psychologist_id,
                tbl_reason_id: null,
                comment:req.body.subject
            },
        )

        res.json({
            message: 'Session updated'
        })

    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getSessionDetails(req, res) {
    try {
        const sessionId = req.query.sessionId
        const sessionDetails = await tbl_session.findOne({
            attributes: ['id', 'title', 'start', 'rules', 'participants', 'image'],
            where: {
                id: sessionId,
                state: 1
            }
        });
        res.json({data: sessionDetails})
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getSessionCases(req, res) {
    try {

        const sessionCases = await tbl_session_cases.findAll({
            where: {
                tbl_session_id: req.params.id
            }
        });
        res.json({data: sessionCases})
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function getListSessionCases(req, res) {
    try {

        const sessionCases = await tbl_session_cases.findAll({
            include:[
                {
                    model: tbl_profile,
                    as:'Profile',
                    attributes:['id','firstname','lastname']
                },
                {
                    model: tbl_session,
                    as:'Session',
                    include:{
                        model:tbl_profile,
                        as:'Patient',
                        attributes:['id','firstname','lastname']
                    }

                }

            ]
        });
        res.json({data: sessionCases})
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getSessionCaseDetails(req, res) {
    try {
        const id = req.params.id
        const sessionCase = await tbl_session_cases.findOne({
            where: {
                id
            }
        });
        res.json({data: sessionCase})
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function assignSession(req, res) {
    try {
        await tbl_session.update({
            tbl_profile_helpdesk_id: req.user.idProfile
        }, {
            where: {
                id: req.params.id
            }
        })

        res.json({
            message: 'Session updated'
        })

    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function postCaseSession(req, res) {
    try {
        const {action, theme, subject, emotion, date} = req.body
        const idSession= req.body.idRoom ? null : req.body.idSession
        const idRoom = req.body.idRoom ? req.body.idRoom : null
        const dateNow= new Date();
        await tbl_session_cases.create({
            tbl_session_id: idSession,
            tbl_profile_id: req.user.idProfile,
            action: action,
            theme,
            reason: subject,
            emotion,
            date: dateNow,
            tbl_room_id: idRoom
        })

        if (action === 'reschedule') {
            await tbl_session.update({
                date: date
            }, {
                where: {
                    id: idSession
                }
            })
        }

        res.json({
            message: 'Session case created'
        })
    } catch (error) {
        console.error('You have a error in getListRoomByProfile:', error)
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}