const {tbl_alternative} = require('../models')

export async function getListAlternative(req, res){
    try {
        const alternative = await tbl_alternative.findAll();
        res.json({
            data: alternative
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createAlternative(req, res){
    const {alternative,score,question_id }=req.body;

    try{
        let newAlternative = await tbl_alternative.create({
            alternative,
            score,
            question_id
        });
        if(newAlternative){

            res.json({
                message: 'alternative created',
                date: newAlternative
            });
        }


    }catch(error){
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }

}