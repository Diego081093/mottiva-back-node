'use strict'

const { tbl_task, tbl_task_types, tbl_session_task, tbl_session, tbl_room_participants, tbl_profile, tbl_profile_attribute, tbl_avatar, tbl_profile_specialty, tbl_specialty, tbl_room, tbl_chat, tbl_chat_participants, tbl_notification } = require('../models')
const { DateTime } = require('luxon')
const { Op } = require('sequelize');

export async function getListSchedule(req, res) {
    try {
        const now = DateTime.fromISO(DateTime.now(), { locale: 'es-ES', zone: 'UTC' })
        const date = now.plus({hour: -5}).toString();
        const { idProfile } = req.user
        const lastSession = await tbl_session.findOne({
            attributes: [ 'id', 'date', 'week' ],
            where: { state: 1, date: { [Op.lt]: date } },
            include: {
                model: tbl_profile,
                as: 'ProfilePatient',
                attributes: [ ['id', 'idProfilePatient'] ],
                where: { state: 1, id: idProfile  }
            },
            order: [['date', 'DESC']]
        })
        let tasks = []
        if (lastSession !== null) {
            tasks = await tbl_session_task.findAll({
                where: { state: 1 },
                include: [
                    {
                        model: tbl_session,
                        as: 'Session',
                        attributes: [ 'id', 'date', 'week' ],
                        where: { state: 1, id: lastSession.dataValues.id }
                    }, {
                        model: tbl_task,
                        as: 'Task',
                        attributes: [ 'id', 'week', 'name', 'description', ['tbl_method_id', 'idMethod'] ],
                        where: { state: 1, week: lastSession.dataValues.week },
                        include: {
                            model: tbl_task_types,
                            as: 'TaskTypes',
                            where: { state: 1 },
                            attributes: [ 'id', 'name', 'alias' ]
                        }
                    }
                ],
                attributes: [ 'id', 'expiration' ]
            })
        }
        const nextSession = await tbl_session.findOne({
            attributes: [ 'id', 'date' ],
            // where: { state: 1, patient_state: 1, date:{[Op.gte]: date}},
            where: { state: 1, patient_state: 1},
            include: [
                {
                    model: tbl_profile,
                    as: 'ProfilePatient',
                    attributes: [],
                    where: { state: 1, id: idProfile }
                },
                {
                    model: tbl_profile,
                    as: 'ProfilePsychologist',
                    attributes: [ 'id', 'firstname', 'lastname', 'birthday' ],
                    where: { state: 1 },
                    include: [
                        {
                            model: tbl_avatar,
                            as: 'Avatar',
                            attributes: ['image'],
                            required: false
                        }
                    ]
                }
            ],
            order: [['id', 'DESC']]
        })
        if (nextSession !== null) {
            // Details of Psychologist
            const specialties = await tbl_specialty.findAll({
                where: { state: 1 },
                attributes: [ 'id', 'name' ],
                include: {
                    model: tbl_profile_specialty,
                    as: 'ProfileSpecialty',
                    attributes: [],
                    where: { state: 1, tbl_profile_id: nextSession.dataValues.ProfilePsychologist.dataValues.id }
                }
            })
            nextSession.dataValues.ProfilePsychologist.dataValues.Specialties = specialties

            const rowAttributes = await tbl_profile_attribute.findAll({
                where: { state: 1, tbl_profile_id: nextSession.ProfilePsychologist.id },
                attributes: [ 'id', 'key', 'value' ]
            })
            let attributes = {}
            rowAttributes.map(attribute => {
                attributes[attribute.key] = attribute.value
            })
            nextSession.dataValues.ProfilePsychologist.dataValues.ProfileAttribute = attributes
            // Details of Psychologist
        }

        const startRoom = now.plus({hour: -5, minutes: -50}).toString();

        const nextRooms = await tbl_room.findAll({
            where: { state: 1, start: { [Op.gt]: startRoom } },
            include: {
                model: tbl_chat,
                as: 'Chat',
                where: { state: 1 },
                attributes: ['id'],
                include: {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    where: { state: 1, tbl_profile_id: idProfile },
                    attributes: [
                        'id',
                        ['tbl_chat_id', 'idChat'],
                        ['tbl_profile_id', 'idProfile']
                    ],
                    required:true
                }
            },
            attributes: [ 'id', 'title', 'start', 'end', 'rules', 'participants', 'image','maxParticipants','inscription','image','twilio_uniquename','created_at' ],
            order: [[ 'start', 'ASC' ]]
        })
        let data = await Promise.all(
            nextRooms.map(async (item, key) => {
                const n_participants = await tbl_room_participants.count({
                    where: {
                        tbl_room_id: item.id
                    },
                    include: [
                        {
                            model: tbl_profile,
                            as: 'Profile_participant',
                            where: {tbl_role_id: 1},
                            attributes: [],
                            required: true
                        }
                    ]
                })
                item.setDataValue('participants', n_participants)
                return item;
            }))
            const countNotification = await tbl_notification.count({
                where:{
                    tbl_profile_id:req.user.idProfile,
                    done:0
                }
            })

        const schedule = {
            data: {
                tasks,
                nextSession,
                nextRooms,
                countNotification
            }
        }
        res.json( schedule )
    } catch(error) {
        console.error('You have a error in getListSchedule:', error)
        res.status(500).json({
            non_field_errors: 'Something went wrong'
        });
    }
}
export async function getListHistorySchedule(req, res) {
    try {
        const now = DateTime.now();
        const date = now.toString();
        const { idProfile } = req.user
        const { from, to, psychologist } = req.query
        
        const where = {
            state: 1, patient_state: 1
        }
        let wherePsychologist = {
            state: 1
        }
        if(psychologist)
        
        {
            wherePsychologist = {[Op.or]:[{firstname:{ [Op.like]: '%' + psychologist + '%' }},{lastname:{ [Op.like]: '%' + psychologist + '%' }}]}
           // wherePsychologist.lastname = { [Op.like]: '%' + psychologist + '%' }
        }
        if (typeof from !== 'undefined' && typeof to !== 'undefined') {
            const fromDate = DateTime.fromSQL(from).toString()
            const toDate = DateTime.fromSQL(to).toString()
            const toDateNew = new Date(toDate)
            const toNew = toDateNew.setDate(toDateNew.getDate() + 1);
            const toDateFinal = new Date(toNew)
            where.date = { [Op.between]: [ fromDate, toDateFinal ] }
            
       
        } else {
            where.date = { [Op.lt]: date }
        }
        const prevSessions = await tbl_session.findAll({
            attributes: [ 'date', 'week' ],
            where,
            include: [
                {
                    model: tbl_profile,
                    as: 'ProfilePatient',
                    attributes: [],
                    where: { state: 1, id: idProfile }
                },
                {
                    model: tbl_profile,
                    as: 'ProfilePsychologist',
                    attributes: [ 'id', 'firstname', 'lastname', 'birthday' ],
                    where: wherePsychologist,
                    include: [
                        {
                            model: tbl_avatar,
                            as: 'Avatar',
                            attributes: [ 'image' ]
                        }, {
                            model: tbl_profile_attribute,
                            as: 'ProfileAttribute',
                            attributes: [['value', 'shortDescription']],
                            where: { state: 1, key: 'short_description' }
                        },
                    ]
                }
            ],
            order: [['date', 'ASC']]
        })
    const prevRooms = await tbl_room.findAll({
            where: { state: 1, end: { [Op.lt]: date } },
            include: {
                model: tbl_chat,
                as: 'Chat',
                where: { state: 1 },
                attributes: [],
                include: {
                    model: tbl_chat_participants,
                    as: 'ChatParticipant',
                    where: { state: 1, tbl_profile_id: idProfile },
                    attributes: []
                }
            },
            attributes: [ 'id', 'title', 'start', 'end', 'rules', 'participants', 'image' ]
        })
        const schedule = {
            data: {
                prevSessions,
                prevRooms
            }
        }
        res.json( schedule )
    } catch(error) {
        console.error('You have a error in getListHistorySchedule:', error)
        res.status(500).json({
            non_field_errors: 'Something went wrong'
        });
    }
}