import { response } from 'express';
import faker from 'faker';
const { DateTime } = require('luxon');

const { Profile_attribute, tbl_profile, tbl_avatar, tbl_profile_attribute, tbl_profile_specialty, tbl_specialty, tbl_blocked, tbl_availability } = require('../models')

export async function getListAttribute(req, res){
    try {

        const attributes = await Profile_attribute.findAll({where:{state:1}});

        res.json({
            data: attributes
        });

    } catch (error) {
        console.log(error);
    }
}

export async function createAttribute(req, res){
    const {
        key,
        value,
        profile_id
    } = req.body;
    try {
        let newAttribute = await Profile_attribute.create({
            key,
            value,
            profile_id
        },{
            fields: [
                'key',
                'value',
                'profile_id'
            ]
        });

        if (newAttribute) {


            res.json({
                message: 'atribute created',
                date: attributes
            });
        }

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getListPsicology(req, res) {
    try {
        const dayOfWeek = req.query.dayOfWeek
        const hour = req.query.hour
        const gender = req.query.gender
        const { idRole } = req.user
        const query = {
            attributes: ['id', ['tbl_avatar_id', 'idAvatar'], 'slug', 'firstname', 'lastname', 'dni', 'phone', 'gender', 'birthday', 'email'],
            where: {
                state: 1,
                tbl_role_id: 2
            },
            include: [
                {
                    model: tbl_profile_attribute,
                    as:'ProfileAttribute',
                    attributes: [['value', 'available']],
                    where: {
                        state: 1, key: 'available'
                    },
                    required: false
                },
                {
                    model: tbl_availability,
                    as: 'Availabilities',
                    where: { state: 1 },
                    required: false,
                    attributes: [ 'id', 'day', 'start_hour', 'end_hour' ]
                }
            ]
        }
        if (gender !== undefined && gender !== '')
            query.where.gender = gender

        const psycologys = await tbl_profile.findAll(query)
        /* if (profiles.length > 0) {
            const psycologys = []
            profiles.forEach(el => {
                if (el.id )
                psycologys
            })
        } */

        let data = []

        await Promise.all(
            psycologys.map((item) => {
                console.log(item.dataValues)
                return new Promise(async resolve => {
                    const avatar = await tbl_avatar.findOne({
                        attributes: ['id', 'image'],
                        where: { id: item.dataValues.idAvatar }
                    })
                    const attributes = await tbl_profile_attribute.findAll({
                        where: {
                            "tbl_profile_id": item.dataValues.id
                        }
                    })
                    const specialty = await tbl_profile_specialty.findAll({
                        where: { state: 1, tbl_profile_id:item.id},
                        include:
                        {
                            model:tbl_specialty,
                            as:'Specialty',
                            where: { state: 1}
                        }
                    })
                    let attr = {}
                    
                    attributes.map(a => {
                        attr[a.key] = a.value
                    })
                    //let special = []
                    specialty.map(b => {
                        b.dataValues.especialidad=b.Specialty.name
                       // special.push(b)
                        //resolve(1)
                    })
                    item.dataValues.Specialty=specialty
                    const specialties = await tbl_specialty.findAll({
                        where: { state: 1 },
                        attributes: [ 'id', 'name' ],
                        include: {
                            model: tbl_profile_specialty,
                            as: 'ProfileSpecialty',
                            attributes: [],
                            where: { state: 1, tbl_profile_id: item.id }
                        }
                    })
                    item.dataValues.Specialties = specialties
                    item.dataValues.Avatar = avatar
                    if (typeof attr.available === 'undefined')
                        attr.available = []
                    item.dataValues.ProfileAttribute = attr

                    const blocked = await tbl_blocked.findOne({
                        where: {
                            tbl_profile_pacient_id: req.user.idProfile,
                            tbl_profile_psychologist_id: item.id,
                            state: 1
                        }
                    })

                    if(blocked === null){
                        data.push(item)
                    }
                    resolve(1)
                })
            })
        )
        res.json({ data: data })
    } catch(error) {
        console.error('You have a error getListPsicology:', error);
        res.status(500).json({
            message: 'Something went wrong',
        });
    }
}

export async function getProfileAttribute(req, res) {
    try {
        const { id } = req.params
        const attributes = await tbl_profile_attribute.findAll({
            where: { state: 1, tbl_profile_id: id },
            attributes: [ 'key', 'value' ]
        })
        res.json(attributes)
    } catch (error) {
        console.log('Something a went wrong:', error)
        res.status(500).json({
            state: false,
            message: 'Something went wrong:'
        })
    }
}

export async function setProfileAttribute(req, res) {
    try {
        const { id } = req.params
        const { key, value } = req.body
        const profileAttribute = await tbl_profile_attribute.findOne({
            where: { state: 1, tbl_profile_id: id, key }
        })
        let profileAttributeUpdate
        if (profileAttribute)
            profileAttributeUpdate = await tbl_profile_attribute.update(
                { value },
                { where: { id: profileAttribute.dataValues.id } }
            )
        else
            profileAttributeUpdate = await tbl_profile_attribute.create({
                tbl_profile_id: id,
                key,
                value
            })
        if (profileAttributeUpdate)
            res.status(200).json({
                state: true,
                message: 'Save attribute'
            })
        else
            res.status(200).json({
                state: false,
                message: 'Don´t save'
            })
    } catch (error) {
        console.log('You have a error:', error)
        res.status(500).json({
            message: 'Something went wrong'
        })
    }
}
