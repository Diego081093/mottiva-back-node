// import Role from '../models/role_model'
const { tbl_profile_attribute,tbl_avatar } = require('../models')
const { isValidBase64Img, s3UploadBase64Image } = require('./../helpers')
const multer = require('multer')
const { DateTime } = require('luxon');
const slugify = require('slugify')

let qwe = []

let fileType

const storage = multer.diskStorage({
    destination: (req, file, cb) => {

        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {

        const name = DateTime.now().toFormat('yyyyMMddHHmmss') + '_' + slugify(file.originalname)

        qwe.push(name)

        cb(null, name)
    }
})

const fileFilter = (req, file, cb) => {

    const validMimetypes = [
        'image/jpeg',
        'image/png',
        'application/pdf',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ]

    if( validMimetypes.includes(file.mimetype) ){

        fileType = 'document'

        if( ['image/jpeg', 'image/png',].includes(file.mimetype) ){

            fileType = 'image'
        }

        cb(null, true)
    }

    cb(null, false)
}

const limits = {
    // 1 KB = 1024 bytes
    fileSize: 1024 * 600
}

const upload = multer({
    storage,
    fileFilter,
    limits
})

// export async function getListRole(req, res){
//     try {
//         const roles = await tbl_role.findAll();
//         res.json({
//             data: roles
//         });
//     } catch (error) {
//         console.log(error);
//         res.status(500).json({
//             message: 'Something went wrong'
//         });
//     }
// }

export const uploadFiles =  upload.array('photos', 3)

export const uploadFile =  upload.single('file')

export async function storeFiles(req, res){

    try {

        res.json({
            message: 'Se guardaron las imágenes satisfactoriamente',
            files: qwe
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function storeFile(req, res){

    try {

        const user = req.user

        const profile_attribute = await tbl_profile_attribute.create({
            tbl_profile_id: req.user.idProfile,
            key: fileType,
            value: 'uploads/' + qwe[0],
            state: 1,
        })

        res.json({
            message: 'Se guardó la imagen satisfactoriamente',
            files: '/uploads/' + qwe[0]
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function uploadFileS3(req, res){
    try {

            console.log('req.file')
            console.log(req.file)
            console.log(req.file.location)
            console.log('')
            console.log('')
            const url = req.file.location
            return url
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function uploadBase64File(req, res){

    const { isValid, buffer, ext } = isValidBase64Img(req.body.image)

    if( ! isValid ){

        return res.status(422).json({
            message: 'The given data was invalid.',
            errors: {
                image: [
                    "The image must be a valid base64"
                ]
            }
        })
    }

    req.body.image = {
        buffer,
        ext
    }

    const imageUploaded = await s3UploadBase64Image('uploads/', req.body.image)
    const url = imageUploaded.data.Location
    const newAvatar = await tbl_avatar.create({
        image : url,
        default : 0
    })

    if( ! imageUploaded.success ){

        return res.status(500).json({
            message: 'Something went wrong uploading the image'
        })
    }

    res.json({
        message: 'File uploaded',
        data: newAvatar
    })
}

export async function uploadAvatar(req, res){
    try {

        console.log('req.file')
        console.log(req.file)
        console.log(req.file.location)
        console.log('')
        console.log('')
        const url = req.file.location
        const newAvatar = await tbl_avatar.create({
            image : url,
            default : 0
        })

        res.json({
           newAvatar
        })
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}