const {
    tbl_profile,
    tbl_user,
    tbl_avatar
} = require('../models')
import jwt from 'jsonwebtoken'
export async function createToken(req, res, usuarioID, username) {
    try {
        const profile = await tbl_profile.findOne({
            where: {
                tbl_user_id: usuarioID,
                state:1
            },
            include:[{
                model: tbl_avatar,
                as:'Avatar',
                attributes: [
                    'id',
                    'image'
                ]
            },
            {
                model: tbl_user,
                as:'User',
                attributes: [
                    'id',
                    'username',
                    'tutorial'
                ]
            }]
        })
    
        const avatar = await tbl_avatar.findOne({
            where: {
                id: profile.tbl_avatar_id,
                state:1
            }
        })
        let list = []
        if (profile.tbl_role_id === 1)
            list = await tbl_profile.findAll({
                attributes: ['id',['firstname','name']],
                include:[{
                    model: tbl_avatar,
                    as:'Avatar',
                    attributes: [
                        'id',
                        'image'
                    ]
                },
                {
                    model: tbl_user,
                    as:'User',
                    attributes: [
                        'id',
                        'username'
                    ]
                }],
                where: {
                    phone: profile.phone,
                    state:1
                }
            })
        const body = {
            id : usuarioID,
            idRole: profile.tbl_role_id,
            idProfile: profile.id,
            username : username,
            slug: profile.slug,
            firstname: profile.firstname,
            lastname: profile.lastname,
            dni: profile.dni,
            email: profile.email,
            phone: profile.phone,
            gender: profile.gender,
            birthday: profile.birthday,
            points: profile.points,
            avatar: avatar.image,
            tutorial: profile.User.tutorial,
            /*civilState: profile.civil_state,
            ocupation: profile.ocupation,
            studyNivel: profile.study_nivel,*/
            profiles:list
            
            
            //id_role: user.role_id
        }
        const token = await jwt.sign({ user: body }, 'globoazul_token')
        return token;
    } catch (error) {
        res.status(500).json({
            message: 'Something went error'
        })
    }
}