import {Op} from "sequelize";
import {DateTime} from "luxon";

const {
    tbl_profile,
    tbl_session,
    tbl_room,
    tbl_room_participants,
    tbl_diary
} = require('../models')

export async function getAll(req, res){
    try {

        const countPatient = await tbl_profile.count({
            where: {
                tbl_role_id: 1
            }
        })
        const countPsychologist = await tbl_profile.count({
            where: {
                tbl_role_id: 2
            }
        })
        const countHelpdesk = await tbl_profile.count({
            where: {
                tbl_role_id: 3
            }
        })
        const countSession = await tbl_session.count({
            where: {
                completed: true
            }
        })
        const countProgram = await tbl_session.count({
            where: {
                completed: true,
                week: 6
            }
        })
        const countRoom = await tbl_room.count({
            where: {
                state: 1
            }
        })
        const countRoomParticipant = await tbl_room_participants.count({
            where: {
                state: 1
            }
        })
        const now = DateTime.fromISO(DateTime.now(), { locale: 'es-ES', zone: 'UTC' })
        const countRoomCompleted = await tbl_room.count({
            where: {
                end: {
                    [Op.lt] : now.plus({hour: -5}).toString()
                }
            }
        })
        const countRoomCompletedParticipant = await tbl_room_participants.count({
            where: {
                state: 1
            },
            include: {
                model: tbl_room,
                as: 'Room',
                where: {
                    end: {
                        [Op.lt] : now.plus({hour: -5}).toString()
                    }
                }
            }
        })
        const countDiary = await tbl_diary.count()
        res.json({
            data: {
                patient: countPatient,
                psychologist: countPsychologist,
                helpdesk: countHelpdesk,
                session: countSession,
                program: countProgram,
                room: countRoom,
                roomParticipant: countRoomParticipant,
                roomCompleted: countRoomCompleted,
                roomCompletedParticipant: countRoomCompletedParticipant,
                diary: countDiary
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
