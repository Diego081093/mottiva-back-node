const {
    tbl_session_attribute
} = require('../models')

export async function setSessionAttribute(req, res) {
    try {
        const { id } = req.params
        const { key, value } = req.body
        const sessionAttribute = await tbl_session_attribute.findOne({
            where: { state: 1, tbl_session_id: id, key },
            order: [[ 'id', 'DESC' ]]
        })
        let sessionAttributeUpdate
        if (sessionAttribute)
            sessionAttributeUpdate = await tbl_session_attribute.update(
                { value },
                { where: { id: sessionAttribute.dataValues.id } }
            )
        else
            sessionAttributeUpdate = await tbl_session_attribute.create({
                tbl_session_id: id,
                key,
                value
            })
        if (sessionAttributeUpdate)
            res.status(200).json({
                state: true,
                message: 'Save attribute'
            })
        else
            res.status(200).json({
                state: false,
                message: 'Don´t save'
            })
    } catch (error) {
        console.log('You have a error:', error)
        res.status(500).json({
            message: 'Something went wrong'
        })
    }
}