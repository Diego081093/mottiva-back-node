const { tbl_media_profile, tbl_profile, tbl_category_medias, tbl_medias, tbl_method, tbl_media_role } = require('../models')
const { Op } = require('sequelize');
const url = require('url');

export async function getTrainings(req, res){
   try {

        let whereQuery = {},
            mediaRoleWhereQuery = {},
            // profileWhereQuery = {
            //     state: 1
            // },
            offset = 0,
            limit = 20,
            role = parseInt(req.query.role),
            type = parseInt(req.query.type),
            category = parseInt(req.query.category),
            type_cat_media = parseInt(req.query.type_cat_media),
            method = parseInt(req.query.method),
            search = req.query.search,
            mediaWhereQuery = {
                state: 1
            },
            categoryMediaWhereQuery = {
                state: 1
            },
            favorite = (req.query.favorite == true),
            archived = (req.query.archived == true),
            requiredProfile = true

        // if( req.user.idRole == 2 ){

        //     profileWhereQuery.tbl_user_id = req.user.id

        // } else if( req.user.idRole != 3 ){

        //     return res.status(401).send('Unauthorized')
        // }

        if( role ){

            if( role == 1 ){

                mediaRoleWhereQuery = {
                    tbl_role_id: 1
                }

            } else if (role == 2){

                mediaRoleWhereQuery = {
                    tbl_role_id: 2
                }
            } else {

                mediaRoleWhereQuery = {
                    tbl_role_id: 3
                }
            }
        }

        if( req.query.offset ){

            offset = parseInt(req.query.offset)
        }

        if( req.query.limit ){

            limit = parseInt(req.query.limit)
        }

        // audio or video
        if( type ){

            mediaWhereQuery.type = type
        }

        if( type_cat_media ){

            categoryMediaWhereQuery.type = type_cat_media
        }

        if( category ){

            mediaWhereQuery.tbl_category_media_id = category
        }

        if( method ){

            mediaWhereQuery.tbl_method_id = method
        }

        if( search ){

            mediaWhereQuery.name = {
                [Op.iLike]: '%' + search + '%'
            }
        }

        if( favorite ){

            whereQuery = {
                favorite: 1
            }
        }

        if( archived ){

            mediaWhereQuery.state = 0
        }

        const {count, rows: trainings} = await tbl_media_profile.findAndCountAll({
            //where: whereQuery,
            attributes: [
                'id',
                'favorite',
                'watch_later',
                'done'
            ],
            include: [
                {
                    model: tbl_media_role,
                    as: 'MediaRole',
                    where: mediaRoleWhereQuery,
                    attributes: []
                },
                {
                    model: tbl_profile,
                    as: 'Profile',
                    where: {
                        tbl_user_id: req.user.id,
                        state:1
                    },
                    attributes: [],
                    required: requiredProfile
                },
                {
                    model: tbl_medias,
                    as: 'Media',
                    where: mediaWhereQuery,
                    attributes: [
                        'id',
                        'file',
                        'image',
                        'duration',
                        'name',
                        'type',
                        'description'
                    ],
                    include: [
                        {
                            model: tbl_category_medias,
                            as: 'category_media',
                            where: categoryMediaWhereQuery,
                            attributes: [
                                'name',
                                'type',
                                'state'
                            ]
                        },
                        {
                            model: tbl_method,
                            as: 'method',
                            attributes: [
                                'name',
                                'image',
                                'state'
                            ]
                        }
                    ]
                }
            ],
            limit,
            offset,
            distinct: true
        });

        if( trainings.length == 0 ){

            return res.status(200).json({
                message: 'No trainings were found',
                data: []
            })
        }

        let urlPath = req.baseUrl + req.path

        if( urlPath.substring(urlPath.length - 1) == '/' ){

            urlPath = urlPath.substring(0, urlPath.length - 1)
        }

        let previousPageUrl = null,
            nextPageUrl = null

        if( offset > 0 ){

            let p_query_offset = offset - limit

            if( (offset - limit) < 0 ){

                p_query_offset = 0
            }

            previousPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: p_query_offset,
                    limit
                }
            });
        }

        if( offset + limit < count ){

            nextPageUrl = url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: urlPath,
                query: {
                    offset: offset + limit,
                    limit
                }
            });
        }

        res.json({
            message: 'Trainings found',
            count,
            previous: previousPageUrl,
            next: nextPageUrl,
            limit,
            offset,
            data: trainings
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getTrainingsFilters(req, res){
    try{

        const options = {
            where: {
                state: 1,
            },
            attributes: [
                'id',
                'name'
            ]
        }

        const p_cat_medias = tbl_category_medias.findAll(options)

        const p_methods = tbl_method.findAll(options)

        const media_types = [
            {
                id: 1,
                name: 'video'
            },
            {
                id: 2,
                name: 'audio'
            }
        ]

        const category_media_types = [
            {
                id: 1,
                name: 'capacitaciones'
            },
            {
                id: 2,
                name: 'libre'
            }
        ]

        const [cat_medias, methods] = await Promise.all([p_cat_medias, p_methods])

        res.json({
            message: 'Filters found',
            filters: {
                categories: cat_medias,
                methods,
                media_types,
                category_media_types
            }
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function getTraining(req, res){
    try {

        const training = await tbl_media_profile.findByPk(req.params.id, {
            attributes: [
                   'done'
            ],
            include: [
                {
                    model: tbl_profile,
                    as: 'Profile',
                    where: {
                        tbl_user_id: req.user.id,
                        state:1
                    },
                    attributes: []
                },
                {
                    model: tbl_medias,
                    as: 'Media',
                    attributes: [
                        //'id',
                        'id',
                        ['tbl_category_media_id', 'idCategory'],
                        ['tbl_method_id', 'idMethod'],
                        'name',
                        'type',
                        'file',
                        'image',
                        'duration',
                        'description',
                        'points',
                        'state'
                        //'type'
                    ],
                    include: [{
                        model: tbl_category_medias,
                        attributes: ['name']
                    }, {
                        model: tbl_media_role,
                        as: 'Role',
                        attributes: [
                            ['tbl_role_id', 'id']
                        ]
                    }, {
                        model: tbl_method,
                        as: 'method',
                        attributes: ['name']
                    }]
                }
            ]
        })

        if( ! training ){

            return res.status(404).json({
                message: 'Training not found'
            })
        }

        res.json({
            data: training
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}

export async function createTraining(req, res){
    try{

        const {
            category_media_id,
            method_id,
            name,
            duration,
            description,
            points,
            role_id
        } = req.body

        const media = await tbl_medias.create({
            tbl_category_media_id: category_media_id,
            tbl_method_id: method_id,
            name,
            type: 2,
            file: req.files.file[0].location,
            image: req.files.image[0].location,
            duration,
            description,
            points
        })
        if (media) {
            await tbl_media_role.create({
                tbl_media_id: media.dataValues.id,
                tbl_role_id: role_id,
                state: 1
            })
            res.json({
                message: 'Training created'
            })
        } else {
            console.log(error);
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
export async function createTrainingVideo(req, res){
    try{

        const {
            category_media_id,
            method_id,
            name,
            file,
            image,
            duration,
            description,
            points,
            role_id
        } = req.body

        const media = await tbl_medias.create({
            tbl_category_media_id: category_media_id,
            tbl_method_id: method_id,
            name,
            type: 1,
            file,
            image,
            duration,
            description,
            points
        })
        if (media) {
            await tbl_media_role.create({
                tbl_media_id: media.dataValues.id,
                tbl_role_id: role_id,
                state: 1
            })
            res.json({
                message: 'Training created'
            })
        } else {
            console.log(error);
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}


/*
SELECT "tbl_media_profile"."id", "tbl_media_profile"."done", "tbl_medium"."id" AS "tbl_medium.id", "tbl_medium"."duration" AS "tbl_medium.duration", "tbl_medium"."name" AS "tbl_medium.name", "tbl_medium"."link" AS "tbl_medium.link", "tbl_medium"."type" AS "tbl_medium.type"
FROM "tbl_media_profiles" AS "tbl_media_profile"
INNER JOIN "tbl_profiles" AS "tbl_profile" ON "tbl_media_profile"."tbl_profile_id" = "tbl_profile"."id" AND "tbl_profile"."tbl_user_id" = 1
LEFT OUTER JOIN "tbl_media" AS "tbl_medium" ON "tbl_media_profile"."tbl_media_id" = "tbl_medium"."id";
*/