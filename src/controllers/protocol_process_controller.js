const {
    tbl_protocol_process
} = require('../models')

export async function getAllProtocolProcess(req, res){
    try {

        const protocol_processes = await tbl_protocol_process.findAll({
            where: {
                state: 1
            },
            attributes: [
                'id',
                'name'
            ]
        })

        res.json({
            message: 'Protocol processes found',
            data: protocol_processes
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}
