const { tbl_chat, tbl_chat_participants, tbl_chat_participants_message, tbl_profile } = require('../models')
const Sequelize = require('sequelize');


export async function postChatMessage(req, res){
    try {
        const {id,message}=req.body
        const chatMessage = await tbl_chat_participants_message.create({
            tbl_chat_participants_id:id,
            message
        })
        res.json({
            message:'message save succesful',
            data:chatMessage
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something went wrong'
        });
    }
}