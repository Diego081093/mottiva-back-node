'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()

     const data = [
       {
         tbl_profile_id:2,
         tbl_task_id:3,
         text: 'nota en mi diario 1',
         audio:'diary1.mp3',
         feel:0,
         date:date,
         public:0,
         created_by: 'root',
         created_at: date,
         edited_by: 'root',
         edited_at: date

       },
       {
        tbl_profile_id:3,
        tbl_task_id:2,
        text: 'nota en mi diario 2',
        audio:'diary2.mp3',
        feel:0,
        date:date,
        public:0,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {
        tbl_profile_id:1,
        tbl_task_id:3,
        text: 'nota en mi diario 3',
        audio:'diary3.mp3',
        feel:0,
        date:date,
        public:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {
        tbl_profile_id:4,
        tbl_task_id:4,
        text: 'nota en mi diario 4',
        audio:'diary4.mp3',
        feel:0,
        date:date,
        public:0,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {
        tbl_profile_id:3,
        tbl_task_id:5,
        text: 'nota en mi diario 5',
        audio:'diary5.mp3',
        feel:0,
        date:date,
        public:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      }
     ]
     return queryInterface.bulkInsert('tbl_diaries', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
