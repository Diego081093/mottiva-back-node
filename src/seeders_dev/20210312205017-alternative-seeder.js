'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    

    let data = []

    const qwe = [...Array(5)].map((e, i) => {

     

        data.push({
          tbl_question_id: i+1,
          alternative:'Casi todos los dias',
          score: 0,
          state: 1,
          created_by:'root',
          created_at: date,
          edited_by:'root',
          edited_at: date
        },
        {
          tbl_question_id: i+1,
          alternative:'Más de la mitad de los dias',
          score: 1,
          state: 1,
          created_by:'root',
          created_at: date,
          edited_by:'root',
          edited_at: date
        },
        {
          tbl_question_id: i+1,
          alternative:'Varios dias',
          score: 2,
          state: 1,
          created_by:'root',
          created_at: date,
          edited_by:'root',
          edited_at: date
        },
        {
          tbl_question_id: i+1,
          alternative:'Para nada',
          score: 3,
          state: 1,
          created_by:'root',
          created_at: date,
          edited_by:'root',
          edited_at: date
        })
        
    })
    

    return queryInterface.bulkInsert('tbl_alternatives', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
