'use strict';
const faker = require('faker')
const { tbl_profile } = require('../models')

async function pacientAll() {
  return await tbl_profile.findAll({
    attributes: ['id'],
    where: { state: 1, tbl_role_id: 1 }
  })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const pacientsAll = await pacientAll()

    const pacients = []

    for(let pacient of pacientsAll) {
      pacients.push(pacient.dataValues.id)
    }

    const date = new Date()
    const root = 'root'

    const data = [...Array(10)].map((session, i) => {
      let start = faker.date.past(0, date)
      let end = faker.date.future(0, start)
      return ({
        tbl_profile_pacient_id: faker.random.arrayElement(pacients),
        patient_state: 1,
        tbl_profile_psychologist_id: 2,
        psychologist_state: 1,
        week: faker.random.arrayElement([1, 2, 3, 4]),
        date: faker.date.between(start, end),
        twilio_uniquename: faker.random.alphaNumeric(30),
        state: 1,
        order: 1,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_sessions', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
