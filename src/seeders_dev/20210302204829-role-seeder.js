'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const data = [
      {
        name: 'Pacient',
        alias: 'P'
      },
      {
        name: 'Psicology',
        alias: 'S'
      },
      {
        name: 'Hepldesk',
        alias: 'H'
      }
    ].map(role => ({
        ...role,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_roles', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
