'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const days = [
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sabado',
      'Domingo'
    ]

    const date = new Date()

    const data = [...Array(10)].map((e, i) => {

      return {
        tbl_profile_id: 2,
        day: days[faker.random.number({min:0, max:6})],
        start_hour: '11:00:00',
        end_hour: '13:30:00',
        state: 1,
        created_at: date,
        created_by: 'root',
        edited_at: date,
        edited_by: 'root'
      }
    })

    return queryInterface.bulkInsert('tbl_availabilities', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
