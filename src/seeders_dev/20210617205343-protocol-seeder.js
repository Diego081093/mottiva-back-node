'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [...Array(10)].map((e, i) => {

      return {
        code: '000' + (i+1 > 9 ? i+1 : ('0' + (i+1))),
        tbl_prot_process_id: faker.random.number({min:1, max:4}),
        goal: faker.lorem.sentence(),
        tbl_profile_responsible_id: 2,
        tbl_profile_approver_id: 2,
        state: 1,
        introduction: faker.lorem.sentences(faker.random.number({min:2, max:3})),
        procedure: faker.lorem.sentences(6),
        created_at: date,
        created_by: root,
        edited_at: date,
        edited_by: root
      }
    })

    return queryInterface.bulkInsert('tbl_protocols', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
