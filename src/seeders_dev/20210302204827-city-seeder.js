'use strict';
const faker = require('faker')
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()

     const root = 'root'
 
     const data = [
       'Lima',
       'Buenos Aires',
       'Sao Paulo',
       'Amsterdan',
       'islamabad',
       'Varsovia',
       'Berlín',
       'Bruselas',
       'Tegusigalpia',
       'Santiago'
     ].map(name => ({
       tbl_country_id: faker.random.number({min:1, max:10}),
       name,
       state: 1,
       created_by: root,
       edited_by: root,
       created_at: date,
       edited_at: date
     }))
 
     return queryInterface.bulkInsert('tbl_cities', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
