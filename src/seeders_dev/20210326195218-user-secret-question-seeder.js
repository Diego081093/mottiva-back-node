'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [...Array(10)].map((e, i) => ({
      tbl_user_id: i + 1,
      tbl_secret_question_id: 1,
      answer: 'qwe',
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_user_secret_questions', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
