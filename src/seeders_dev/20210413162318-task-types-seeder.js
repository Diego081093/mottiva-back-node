'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     const date = new Date()

     const root = 'root'

     const data = [
      {
        name: 'Formulario',
        alias: 'form',
        description:'Formulario de Ayuda',
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Diario',
        alias: 'diary',
        description:'Tareas Diarias',
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Audio o Vídeo',
        alias: 'media',
        description:'Tarea con Audio o Vídeo',
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Salas',
        alias: 'room',
        description:'Tareas en las salas',
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      }
    ]
     return queryInterface.bulkInsert('tbl_task_types', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
