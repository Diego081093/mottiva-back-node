'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const data = [...Array(20)].map((e, i) => {

      return {
        tbl_notification_id: i+1,
        key: faker.lorem.word(),
        value: faker.lorem.word(),
        state: 1,
        created_at: date,
        created_by: 'root',
        edited_at: date,
        edited_by: 'root'
      }
    })

    return queryInterface.bulkInsert('tbl_notification_attributes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
