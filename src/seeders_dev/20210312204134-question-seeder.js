'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const data = [
      'Me cuesta sentir tranquilidad',
      'Me resulta difícil poder dormir',
      'Siento que hay muchas cosas de mi que me gustaría cambiar',
      'Me siento fastidiada e irritada fácilmente',
      'Tiendo a sentirme preocupada'
    ].map(question => ({
        question,
        state: 1,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_questions', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
