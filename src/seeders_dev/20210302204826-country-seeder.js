'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()

     const root = 'root'
 
     const data = [
       'Perú',
       'Argentina',
       'Brasil',
       'Holanda',
       'Pakistan',
       'Polonia',
       'Alemania',
       'Bélgica',
       'Honduras',
       'Chile'
     ].map(name => ({
       name,
       state: 1,
       created_by: root,
       created_at: date,
       edited_by: root,
       edited_at: date
     }))
 
     return queryInterface.bulkInsert('tbl_countries', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
