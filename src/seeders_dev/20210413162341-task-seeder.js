'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const date = new Date()
    const data = [...Array(10)].map((e, i) => {
      return ({
        tbl_method_id: faker.random.number({ min: 1, max: 4 }),
        tbl_task_types_id: faker.random.arrayElement([1, 2, 3, 4]),
        week: faker.random.arrayElement([1, 2, 3, 4]),
        name: 'tarea ' + i + 1,
        description: faker.lorem.words(),
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })
    return queryInterface.bulkInsert('tbl_tasks', data)
  },

  down: async (queryInterface, Sequelize) => {
  }
};
