'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const data = [...Array(20)].map((e, i) => {

      return {
        tbl_profile_id: Math.ceil((i+1)/2),
        type: faker.lorem.word(),
        title: faker.lorem.sentence(3),
        description: faker.lorem.sentences(2),
        state: 1,
        created_at: date,
        created_by: 'root',
        edited_at: date,
        edited_by: 'root'
      }
    })

    return queryInterface.bulkInsert('tbl_notifications', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
