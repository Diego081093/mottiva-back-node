'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const date = new Date()

    const dataPsicologis = [...Array(10)].map((chatParticipant, i) => {
      return ({
        tbl_chat_id: i + 1,
        tbl_profile_id: 2,
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })

    const dataParticipants = [...Array(10)].map((chatParticipant, i) => {
      return ({
        tbl_chat_id: i + 1,
        tbl_profile_id: 1,
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_chat_participants', dataPsicologis.concat(dataParticipants))
  },

  down: async (queryInterface, Sequelize) => {
  }
};
