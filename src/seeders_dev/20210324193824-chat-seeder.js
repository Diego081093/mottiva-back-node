'use strict';

const faker = require('faker')
const { tbl_chat, tbl_room } = require('../models')

async function chatAll() {
  return await tbl_chat.findAll()
}
async function roomById(idRoom) {
  return await tbl_room.findOne({
      where: { id: idRoom }
  })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const date = new Date()
    const chats = await chatAll()
    let roomMatrix = [];
    for (let j=0;j<10;j++) {
      let roomData = await roomById(j + 1)
      roomMatrix.push({
        id : roomData.id,
        start : roomData.start
      })
    }

    const data = [...Array(10)].map((chat, i) => {
      let start = new Date(roomMatrix[i].start)
      start.setDate(start.getDate() + faker.random.number({ min: 1, max: 3 }))
      return ({
        id: chats.length + i + 1,
        tbl_room_id: roomMatrix[i].id,
        start,
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })
    return queryInterface.bulkInsert('tbl_chats', data)
  },

  down: async (queryInterface, Sequelize) => {
  }
};
