'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [
      {
        name: 'Depor'
      },
      {
        name: 'Libero'
      },
      {
        name: 'Expresso'
      },
      {
        name: 'Limite'
      },
      {
        name: 'Posición'
      },
    ].map(company_benefit => ({
      ...company_benefit,
      image: 'https://picsum.photos/100',
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_company_benefits', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
