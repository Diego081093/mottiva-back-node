'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [
      'Lorem ipsum',
      'Dolor sit',
      'Amet consectetur',
      'Qweqweqwe',
      'Asawfasd',
      'Fiohuwe',
      'Tneoajudn',
      'Niabnhsi',
      'Bpwnfiwpq',
      'Iuiuqonsgv'
    ].map((note, i) => ({
      tbl_session_id: i + 1,
      note,
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_session_notes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
