'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const data = [...Array(25)].map((e, i) => {

      return {
        tbl_room_id: Math.ceil((i+1)/5),
        tbl_profile_participant_id: faker.random.number({min:1, max:10}),
        created_at: date,
        edited_at: date
      }
    })

    return queryInterface.bulkInsert('tbl_room_participants', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
