'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const data_1 = [
      {
        tbl_role_id: 1,
        tbl_user_id: 1,
        tbl_avatar_id: 1,
        tbl_district_id: 1,
        slug: faker.lorem.slug(),
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        dni: faker.random.number({min: 10000000, max: 99999999}),
        email: faker.internet.email(),
        phone: '9' + faker.random.number({min: 20, max: 99}) + faker.random.number({min: 100000, max: 999999}),
        gender: ['M', 'F'][(faker.random.number({min: 0, max: 1}))],
        birthday: date,
        state: 1,
        points: 0,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      },
      {
        tbl_role_id: 2,
        tbl_user_id: 2,
        tbl_avatar_id: 2,
        tbl_district_id: 2,
        slug: faker.lorem.slug(),
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        dni: faker.random.number({min: 10000000, max: 99999999}),
        email: faker.internet.email(),
        phone: '9' + faker.random.number({min: 20, max: 99}) + faker.random.number({min: 100000, max: 999999}),
        gender: ['M', 'F'][(faker.random.number({min: 0, max: 1}))],
        birthday: date,
        state: 1,
        points: 0,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      },
      {
        tbl_role_id: 1,
        tbl_user_id: 3,
        tbl_avatar_id: 3,
        tbl_district_id: 3,
        slug: 'erick-valdez-3',
        firstname: 'erick',
        lastname: 'valdez',
        dni: 72766963,
        email: faker.internet.email(),
        phone: 922373214,
        gender:'M',
        birthday: date,
        state: 1,
        points: 0,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      }
    ]

    const data_2 = [...Array(7)].map((profile, i) => ({
      tbl_role_id: faker.random.number({min:1, max:3}),
      tbl_user_id: i+4,
      tbl_avatar_id: i+4,
      tbl_district_id: i+4,
      firstname: faker.name.firstName(),
      lastname: faker.name.lastName(),
      slug: faker.lorem.slug(),
      dni: faker.random.number({min: 10000000, max: 99999999}),
      email: faker.internet.email(),
      phone: '9' + faker.random.number({min: 20, max: 99}) + faker.random.number({min: 100000, max: 999999}),
      gender: ['M', 'F'][(faker.random.number({min: 0, max: 1}))],
      birthday: date,
      state: 1,
      points: 0,
      created_by: 'root',
      created_at: date,
      edited_by: 'root',
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_profiles', data_1.concat(data_2))
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
