'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [...Array(10)].map((e, i) => {

      return ({
        tbl_media_id: i + 1,
        tbl_profile_id: 2,
        done: faker.random.number({min:0, max:100}),
        favorite: faker.random.number({min:0, max:1}),
        state: 1,
        watch_later: (1 == faker.random.number({min:0, max:1})),
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_media_profiles', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
