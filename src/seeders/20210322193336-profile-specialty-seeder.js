'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [...Array(4)].map((e, i) => {

      return {
        tbl_profile_id: 2,
        tbl_specialty_id: i+1,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      }
    })

    return queryInterface.bulkInsert('tbl_profile_specialties', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
