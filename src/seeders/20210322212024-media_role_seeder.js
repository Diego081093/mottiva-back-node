'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()
     const root = 'root'
    const data = [...Array(10)].map((e, i) => {

      return ({
        tbl_media_id: i + 1,
        tbl_role_id: faker.random.number({min:1, max:3}),
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_media_roles', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
