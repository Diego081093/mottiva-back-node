'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [
      {
        name: 'Técnicas sencillas de meditación',
        tbl_category_media_id: 2,
        tbl_method_id: 1,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '15 min',
        description: 'Video o Audio de meditación',
        points: 100,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'La hidratación en el desarrollo',
        tbl_category_media_id: 1,
        tbl_method_id: 1,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '17 min',
        description: 'Video o Audio de meditación',
        points: 200,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Sonidos relajantes para meditar',
        tbl_category_media_id: 2,
        tbl_method_id: 3,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '12 min',
        description: 'Video o Audio de meditación',
        points: 100,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Técnicas de respiración anti estrés',
        tbl_category_media_id: 1,
        tbl_method_id: 2,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '10 min',
        description: 'Video o Audio de meditación',
        points: 200,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Técnicas sencillas de meditación',
        tbl_category_media_id: 2,
        tbl_method_id: 4,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '16 min',
        description: 'Video o Audio de meditación',
        points: 100,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'La hidratación en el desarrollo',
        tbl_category_media_id: 1,
        tbl_method_id: 2,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '11 min',
        description: 'Video o Audio de meditación',
        points: 300,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Sonidos relajantes para meditar',
        tbl_category_media_id: 2,
        tbl_method_id: 2,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '12 min',
        description: 'Video o Audio de meditación',
        points: 100,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Técnicas de respiración anti estrés',
        tbl_category_media_id: 1,
        tbl_method_id: 3,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '5 min',
        description: 'Video o Audio de meditación',
        points: 150,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'Técnicas sencillas de meditación',
        tbl_category_media_id: 2,
        tbl_method_id: 1,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '03 min',
        description: 'Video o Audio de meditación',
        points: 100,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      },
      {
        name: 'La hidratación en el desarrollo',
        tbl_category_media_id: 1,
        tbl_method_id: 4,
        type: 1,
        file: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5' ,
        image: 'https://mottiva.globoazul.pe/img/training.c5643124.png',
        duration: '7 min',
        description: 'Video o Audio de meditación',
        points: 200,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      }
    ]

    return queryInterface.bulkInsert('tbl_medias', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
