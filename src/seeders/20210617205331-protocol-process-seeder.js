'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [
      'Botón escuchame',
      'Recepción de llamada',
      'Lorem',
      'Ipsum'
    ].map(name => ({
      name,
      state: 1,
      created_at: date,
      created_by: root,
      edited_at: date,
      edited_by: root
    }))

    return queryInterface.bulkInsert('tbl_protocol_processes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
