'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [
      'Lorem ipsum',
      'Dolor sit',
      'Amet consectetur',
      'Qweqweqwe',
      'Asawfasd',
      'Fiohuwe',
      'Tneoajudn',
      'Niabnhsi',
      'Bpwnfiwpq',
      'Iuiuqonsgv'
    ].map((note, i) => ({
      tbl_profile_pacient_id: faker.random.number({min: 3, max: 10}),
      tbl_profile_psychologist_id: 2,
      tbl_room_id: i + 1,
      notes: note,
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_room_notes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
