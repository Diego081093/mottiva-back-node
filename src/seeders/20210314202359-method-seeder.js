'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()

     const data = [
       {
         
         name: 'Hidratación',
         image:'',
         state:1,
         created_by: 'root',
         created_at: date,
         edited_by: 'root',
         edited_at: date

       },
       {
        name: 'Respiración',
        image:'',
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {
        name: 'Meditación',
        image:'',
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {
        name: 'Enfoque',
        image:'',
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      }
     ]
     return queryInterface.bulkInsert('tbl_methods', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
