'use strict';



module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()


    const data = [
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_1.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_2.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_3.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_4.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_5.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_6.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_7.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_8.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_9.png',
      'https://mottiva.globoazul.pe/images-app/AVATARS/avatar_10.png'
    ].map(image => ({
      image,
      default : 1,
      state : 1,
      created_by:'root',
      created_at: date,
      edited_by:'root',
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_avatars', data)
  },



  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
