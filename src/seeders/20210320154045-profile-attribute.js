'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [
     
      {
        key: 'video',
        value: 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4?_=5'
      },
      {
        key: 'image',
        value: 'https://mottiva.globoazul.pe/images-app/IMG_PERFIL_APP_USUARIO/illus_sesion_perfil.svg'
      },
     
    ].map((profile_attribute, i) => {

      return ({
        tbl_profile_id: 2,
        ...profile_attribute,
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_profile_attributes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
