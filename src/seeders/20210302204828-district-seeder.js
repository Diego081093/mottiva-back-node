'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()
     const root = 'root'
     const data = [
      'Miraflores',
      'San Miguel',
      'Magdalena',
      'Pueblo Libre',
      'Surco',
      'Barranco',
      'Los Olivos',
      'Cercado de Lima',
      'Comas',
      'San Martin de Porres'
    ].map(name => ({
      tbl_city_id:1,
      name,
      state: 1,
      created_by: root,
      edited_by: root,
      created_at: date,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_districts', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
