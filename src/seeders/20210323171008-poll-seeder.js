'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [...Array(10)].map((e, i) => ({
      tbl_session_id: i + 1,
      experience: faker.random.number({min: 1, max:5}),
      qualification: faker.random.number({min: 1, max:5}),
      comment: faker.lorem.text(),
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_polls', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
