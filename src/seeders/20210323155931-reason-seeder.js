'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [
      {
        name: 'Carro',
        answer: 'Toyota'
      },
      {
        name: 'Gorra',
        answer: 'Azul'
      },
      {
        name: 'Parlante',
        answer: 'JBL'
      },
      {
        name: 'Procesador',
        answer: 'Ryzen'
      },
      {
        name: 'Lapicero',
        answer: 'Stabillo'
      }
    ].map((reason, i) => {

      return ({
        ...reason,
        tbl_secret_question_id: faker.random.number({min:1, max:5}),
        state: 1,
        created_by: root,
        created_at: date,
        edited_by: root,
        edited_at: date
      })
    })

    return queryInterface.bulkInsert('tbl_reasons', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
