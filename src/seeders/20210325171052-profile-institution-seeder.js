'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data_1 = [
      {
        tbl_profile_id: 2,
        tbl_institution_id: 1,
        academic_degree: 'Magister SCRUM'
      }
    ].map(profile_institution => ({
      ...profile_institution,
      state: 1,
      start_date: date,
      ending_date: date,
      document: 'https://picsum.photos/100',
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    const data_2 = [...Array(10)].map((e, i) => ({
      tbl_profile_id: i+1,
      tbl_institution_id: faker.random.number({min:1, max:5}),
      academic_degree: faker.lorem.words(),
      state: 1,
      start_date: date,
      ending_date: date,
      document: 'https://picsum.photos/100',
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_profile_institutions', data_1.concat(data_2))
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
