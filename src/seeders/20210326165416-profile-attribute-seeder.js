'use strict';

const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const date = new Date()

    const common_data = {
      created_by: 'root',
      created_at: date,
      edited_by: 'root',
      edited_at: date
    }

    const data_1 = [...Array(10)].map((profileAttribute, i) => {
      const days_week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']
      const value = []
      for(let day of days_week) {
        value.push({
          days: day,
          hours: [
            { name: '06:00 - 06:45', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '06:45 - 07:30', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '07:30 - 08:15', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '08:15 - 09:00', state: faker.random.number({ min: 0, max: 1 }) },

            { name: '09:00 - 09:45', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '09:45 - 10:30', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '10:30 - 11:15', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '11:15 - 12:00', state: faker.random.number({ min: 0, max: 1 }) },

            { name: '12:00 - 12:45', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '12:45 - 13:30', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '13:30 - 14:15', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '14:15 - 15:00', state: faker.random.number({ min: 0, max: 1 }) },

            { name: '15:00 - 15:45', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '15:45 - 16:30', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '16:30 - 17:15', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '17:15 - 18:00', state: faker.random.number({ min: 0, max: 1 }) },

            { name: '18:00 - 18:45', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '18:45 - 19:30', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '19:30 - 20:15', state: faker.random.number({ min: 0, max: 1 }) },
            { name: '20:15 - 21:00', state: faker.random.number({ min: 0, max: 1 }) },

            { name: '21:00 - 21:45', state: faker.random.number({ min: 0, max: 1 }) }
          ]
        })
      }
      const valueData = JSON.stringify(value)
      return ({
        tbl_profile_id: i + 1,
        key: 'available',
        value: valueData,
        state: 1,
        ...common_data
      })
    })

    const data_2 = [...Array(10)].map((e, i) => {
      return ({
        tbl_profile_id: i + 1,
        key: 'children',
        value: faker.random.number({min: 1, max: 3}),
        state: 1,
        ...common_data
      })
    })

    const dataDescription = [...Array(10)].map((profileAttribute, i) => {
      return ({
        tbl_profile_id: i + 1,
        key: 'description',
        value: faker.lorem.words(25),
        state: 1,
        ...common_data
      })
    })

    const dataDescriptionShort = [...Array(10)].map((profileAttribute, i) => {
      return ({
        tbl_profile_id: i + 1,
        key: 'short_description',
        value: ['Coach en familia', 'Coach en trabajo', 'Coach en trabajo y autoestima', 'Coach en Autoestima', 'Coach en amor'][faker.random.number({ min: 1, max: 3})],
        state: 1,
        ...common_data
      })
    })

    const data = [
      ...data_1,
      ...data_2,
      ...dataDescription,
      ...dataDescriptionShort
    ]

    return queryInterface.bulkInsert('tbl_profile_attributes', data)
  },

  down: async (queryInterface, Sequelize) => {
  }
};
