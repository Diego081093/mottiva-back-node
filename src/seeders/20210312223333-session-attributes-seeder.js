'use strict';
const faker = require('faker')
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const states = [
      'Normal',
      'Moderado',
      'Urgente'
    ]

    const types = [
      'Depresion',
      'Depresion',
      'Lorem',
      'Ipsum'
    ]

    const root = 'root'

    const default_data = {
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }

    const data_1 = [...Array(10)].map((session_attribute, i) => {

      return ({
        tbl_session_id: i+1,
        key: 'state',
        value: states[faker.random.number({min:0, max:3})],
        ...default_data
      })
    })

    const data_2 = [...Array(10)].map((session_attribute, i) => {

      return ({
        tbl_session_id: i+1,
        key: 'type',
        value: types[faker.random.number({min:0, max:2})],
        ...default_data
      })
    })

    const data = [
      ...data_1,
      ...data_2
    ]

    return queryInterface.bulkInsert('tbl_session_attributes', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
