'use strict';
const faker = require('faker')
const bcrypt = require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const hashed = await bcrypt.hash('123123', parseInt(process.env.BCRYPT_ROUNDS))

    const data_1 = [
      {
        username: 'usuario1',
        password: await bcrypt.hash('abc', parseInt(process.env.BCRYPT_ROUNDS)),
        // phonenumber: '987450123',
        token_device: 'dIF4NRGZRr-hyqBsxpXquA:APA91bGsYhdkPg6MOB9ilRRx2lwInOKgVRB6R4v1DWIQuydDVOuN-eAby1UsXgzQ5Xzal38EehCWDsW2p2yOgYVYKN9dX-E3H3ZJ8lEZIeoYCkoBwQ6pISV6pGRPB_BLoXGuU_wPU4-m',
        state: 1,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
      },
      {
        username: 'marco.condoric@gmail.com',
        password: await bcrypt.hash('abc', parseInt(process.env.BCRYPT_ROUNDS)),
        // phonenumber: '987450123',
        state: 1,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
      },
      {
        username: '72766963',
        password: await bcrypt.hash('1234', parseInt(process.env.BCRYPT_ROUNDS)),
        // phonenumber: '987450123',
        state: 1,
        created_by:'root',
        created_at: date,
        edited_by:'root',
        edited_at: date
      }
    ]

    const data_2 = [...Array(8)].map((user, i) => ({
      username: faker.internet.userName().toLowerCase(),
      password: hashed,
      // phonenumber: '9' + faker.random.number({min: 20, max: 99}) + faker.random.number({min: 100000, max: 999999}),
      // state: faker.random.number({min: 1, max: 3}),
      state: 1,
      created_by: 'root' ,
      edited_by: 'root' ,
      created_at: date,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_users', data_1.concat(data_2))
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
