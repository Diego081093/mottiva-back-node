'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date()

    const root = 'root'

    const data = [
      {
        name: 'Amor',
        alias: 'AM'
      },
      {
        name: 'Trabajo',
        alias: 'TR'
      },
      {
        name: 'Amistad',
        alias: 'AD'
      },{
        name: 'Lorem',
        alias: 'LO'
      }
    ].map(specialty => ({
      ...specialty,
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_specialties', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
