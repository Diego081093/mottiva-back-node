'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const date = new Date()

     const data = [
       {
         tbl_benefit_id: 1,
         tbl_profile_id: 2,
         state: 1,
         created_by:'root',
         created_at: date,
         edited_by:'root',
         edited_at: date
       }
      ]
      return queryInterface.bulkInsert('tbl_benefit_profiles', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
