'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const root = 'root'

    const data = [
      {
        name: 'Confianza'
      },
      {
        name: 'Arequipa'
      },
      {
        name: 'Bayern'
      },
      {
        name: 'Huancayo'
      },
      {
        name: 'Paris'
      },
    ].map((benefit, i) => ({
      tbl_company_benefit_id: i + 1,
      ...benefit,
      image: 'https://picsum.photos/100',
      description: faker.lorem.text(),
      point: faker.random.number({min:1, max:5}) * 10,
      state: 1,
      created_by: root,
      created_at: date,
      edited_by: root,
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_benefits', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
