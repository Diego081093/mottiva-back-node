'use strict';
const faker = require('faker')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     const date = new Date()

     const data = [
       {

         name: 'Familia',
         type:1,
         state:1,
         created_by: 'root',
         created_at: date,
         edited_by: 'root',
         edited_at: date

       },
       {

        name: 'Transtornos de Ansiedad',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {

        name: 'Terapia de pareja',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {

        name: 'Violencia de Pareja',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {

        name: 'Transtorno Depresivo',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {

        name: 'Terapia Cognitiva',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      },
      {

        name: 'Terapia Grupal',
        type:1,
        state:1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date

      }

     ]

    return queryInterface.bulkInsert('tbl_category_medias', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
