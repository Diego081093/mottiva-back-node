'use strict';

const faker = require('faker')
const { tbl_session, tbl_task } = require('../models')

async function sessionAll() {
  return await tbl_session.findAll({
    attributes: ['id'],
    where: { state: 1 }
  })
}
async function taskAll() {
  return await tbl_task.findAll({
    attributes: ['id'],
    where: { state: 1 }
  })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const sessionsAll = await sessionAll()
    const sessions = []
    sessionsAll.forEach(element => {
      sessions.push(element.dataValues.id)
    })
    const tasksAll = await taskAll()
    const tasks = []
    tasksAll.forEach(element => {
      tasks.push(element.dataValues.id)
    })

    const date = new Date()
    const data = [...Array(60)].map((e, i) => {
      let start = faker.date.past(0, date)
      return ({
        tbl_session_id: faker.random.arrayElement(sessions),
        tbl_task_id: faker.random.arrayElement(tasks),
        expiration: faker.date.between(start, date),
        state: 1,
        created_by: 'root',
        created_at: date,
        edited_by: 'root',
        edited_at: date
      })
    })
    return queryInterface.bulkInsert('tbl_session_tasks', data)
  },

  down: async (queryInterface, Sequelize) => {
  }
};
