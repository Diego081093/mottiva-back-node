'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const date = new Date()

    const data = [
      'Nombre de un amigo de la infancia',
      'Nombre de tu primera mascota',
      'Nombre de tu artista favorito',
      'Canción que te recuerde tu infancia',
      'Tu trabajo ideal'
    ].map((question) => ({
      question,
      state: 1,
      created_by:'root',
      created_at: date,
      edited_by:'root',
      edited_at: date
    }))

    return queryInterface.bulkInsert('tbl_secret_questions', data)
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
