import bcrypt from 'bcrypt';

const {s3, Bucket} = require('./config/aws')
const slugify = require('slugify')
const multer = require('multer')
const multerS3 = require('multer-s3')
const { DateTime } = require('luxon')
const { v4: uuidv4 } = require('uuid')

exports.MottivaConstants = {
    PATIENT_ROLE: 1,
    PSYCHOLOGIST_ROLE: 2,
    HELPDESK_ROLE: 3,
    SESSION_POINTS: 300
}

exports.isValidPassword = async (password, passwordEncrypt) => {
    try {
        const compare = await bcrypt.compare(password, passwordEncrypt)
        return compare
    } catch(err) {
        console.error('Error isValidPassword: ', err)
    }
}

exports.isValidBase64Img = (base64Img) => {

    const validMimes = ['jpeg', 'png', 'pdf']

    const splitArray = base64Img.split(',')

    if( splitArray.length == 2 ){

        const [dataToReplace, base64] = splitArray

        const mime = dataToReplace.replace('data:image/', '')
            .replace('data:application/', '')
            .replace(';base64', '')

        if( validMimes.includes(mime) ){

            return {
                isValid: true,
                buffer: Buffer.from(base64, 'base64'),
                ext: mime
            }
        }
    }

    return {
        isValid: false
    }
}

exports.s3UploadBase64Image = (prefix, {buffer: Body, ext}) => {

    const now = DateTime.utc().toISODate()

    const fileName = uuidv4() + '-' + now + '.' + ext

    const Key = prefix + fileName

    return new Promise((resolve, reject) => {

        const ContentType = ext === 'pdf' ? 'application/pdf' : 'image/' + ext

        const uploadParams = {
            Bucket,
            Key,
            Body,
            ContentType,
            ContentEncoding: 'base64'
        }

        s3.upload(uploadParams, (err, data) => {

            if( err ){

                resolve({success: false})

            } else{

                resolve({
                    success: true,
                    data,
                    fileName
                })
            }
        })
    })
}

exports.s3Upload = multer({
    storage: multerS3({
        s3,
        bucket: Bucket,
        key: (req, file, cb) => {

            // console.log('')
            // console.log('')
            // console.log('file')
            // console.log(file)
            // console.log('')
            // console.log('')

            cb(null, Date.now().toString() + '_' + slugify(file.originalname))
        },contentType: (req, file, cb) => {

            cb(null, file.mimetype)
        }
    })
})

exports.s3UploadWithDest = (dest) => multer({
    storage: multerS3({
        s3,
        bucket: Bucket,
        key: (req, file, cb) => {

            const regex = /(?:\.([^.]+))?$/;

            const ext = regex.exec(file.originalname)[1]

            if( ! ext ){

                const noExtError = new Error('¡Ups! No extension was found')

                return cb(noExtError)
            }

            const now = DateTime.utc().toISODate()

            const fileName = uuidv4() + '-' + now + '.' + ext

            cb(null, dest + fileName)

        },contentType: (req, file, cb) => {

            cb(null, file.mimetype)
        }
    })
})

exports.s3UploadImage = (prefix, {buffer: Body, ext}) => {

    const now = DateTime.utc().toISODate()

    const fileName = uuidv4() + '-' + now + '.' + ext

    Key = prefix + fileName

    return new Promise((resolve, reject) => {

        const uploadParams = {
            Bucket,
            Key,
            Body,
            ContentType: 'image/' + ext,
            ContentEncoding: 'base64'
        }

        s3.upload(uploadParams, (err, data) => {

            if( err ){

                resolve({success: false})

            } else{

                resolve({
                    success: true,
                    data,
                    fileName
                })
            }
        })
    })
}