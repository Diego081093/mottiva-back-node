import { favoriteMedia } from '../controllers/medias_controller'
import passport from 'passport';
import { Router } from 'express';
const router = Router();

router.post('/:id', passport.authenticate('jwt', { session: false }), favoriteMedia);

export default router;