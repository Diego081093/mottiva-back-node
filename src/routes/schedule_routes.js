import passport from 'passport'
import { Router } from 'express'
import { getListSchedule, getListHistorySchedule } from '../controllers/schedule_controller'

const router = Router();

router.get('/', passport.authenticate('jwt', { session: false }), getListSchedule)
router.get('/history', passport.authenticate('jwt', { session: false }), getListHistorySchedule)

export default router