import {Router} from 'express';
const router = Router();
import passport from 'passport'
import{createPoll} from '../controllers/poll_controller'

router.post('/',passport.authenticate('jwt', { session: false }),createPoll);

export default router;