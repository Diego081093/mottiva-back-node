import {Router} from 'express';
const router = Router();
import passport from 'passport'
import{createBlocked, getListBlocked} from '../controllers/blocked_controller'



router.post('/',passport.authenticate('jwt', { session: false }),createBlocked);
router.get('/',passport.authenticate('jwt', { session: false }),getListBlocked);

export default router;