import {Router} from 'express';
const router = Router();
import passport from 'passport';

import { getUserSecretQuestion, validateUserSecretQuestion } from '../controllers/user_secret_question_controller'

//api/rol
router.get('/', getUserSecretQuestion); //corregir a findone
router.post('/validate', passport.authenticate('jwt', { session: false }), validateUserSecretQuestion);

export default router;