import {Router} from 'express';
const router = Router();

import{ getAll } from '../controllers/institution_controller'

router.get('/', getAll);

export default router;