import { Router } from 'express';
const router = Router();
import {getListMethod} from '../controllers/method_controller'
import passport from "passport";


router.get('/', passport.authenticate('jwt', { session: false }), getListMethod);



export default router;