import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { getPatientsSession} from '../controllers/session_controller'
import { getListPatients} from '../controllers/profiles_controller'

router.get('/', passport.authenticate('jwt', { session: false }), getPatientsSession);
router.get('/list', passport.authenticate('jwt', { session: false }), getListPatients);


export default router;