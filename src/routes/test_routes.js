import {Router} from 'express';
const router = Router();

import { uploadFiles, storeFiles } from '../controllers/upload_controller'

//api/rol
// router.get('/', getListRole);
router.post('/', uploadFiles, storeFiles );
// router.delete('/',deleteRole);

export default router;