import { Router } from 'express'
import passport from 'passport'

const router = Router()

import { list } from '../controllers/specialty_controller'

router.get('/', passport.authenticate('jwt', { session: false }), list)

export default router