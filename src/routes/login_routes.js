import { Router } from 'express';
const router = Router();
import {signin} from '../controllers/users_controller'

//login
router.post('/', signin);
export default router;