import {Router} from 'express';
import passport from 'passport'
const router = Router();

import { filterAvailability } from '../controllers/therapy_controller'

//patient
router.get('/filter', passport.authenticate('jwt', { session: false }), filterAvailability);
export default router;