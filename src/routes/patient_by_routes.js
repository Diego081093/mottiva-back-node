import {Router} from 'express';
import passport from 'passport';
const router = Router();

import { indexPatients } from '../controllers/profiles_controller'

//patient-by
router.get('/', passport.authenticate('jwt', { session: false }), indexPatients);

export default router;