import {Router} from 'express';
const router = Router();
import{getListAlternative,createAlternative} from '../controllers/alternative_controller'


router.get('/',getListAlternative);
router.post('/',createAlternative);

export default router;