import {Router} from 'express';
const router = Router();

import {getResult} from '../controllers/question_result_controller'


router.get('/:score',getResult);

export default router;