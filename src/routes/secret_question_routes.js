import {Router} from 'express';
const router = Router();

import {
    getSecretQuestion,
    getListSecretQuestion,
    createSecretQuestion,
    updateSecretQuestion,
    deleteSecretQuestion,
    postValidateSecretQuestion
} from '../controllers/secret_question_controller'

//api/rol
router.get('/', getListSecretQuestion); //cuando va a traer una lista de varios
//router.get('/:id', getSecretQuestion); //traer solo uno
router.get('/:dni', getSecretQuestion);
router.post('/validate', postValidateSecretQuestion);

router.post('/', createSecretQuestion);
router.put('/:id', updateSecretQuestion);
router.delete('/:id', deleteSecretQuestion);

export default router;