import {Router} from 'express';
import passport from 'passport';
const { s3Upload } = require('./../helpers')
const router = Router();

import { uploadFile, storeFile,uploadFileS3, uploadBase64File } from '../controllers/upload_controller'

//api/rol
// router.get('/', getListRole);
router.post('/local', passport.authenticate('jwt', { session: false }), uploadFile, storeFile );
router.post('/s3', passport.authenticate('jwt', { session: false }), s3Upload.single('file'), uploadFileS3);
router.post('/base64', uploadBase64File);

export default router;