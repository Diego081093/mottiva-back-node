import {Router} from 'express';
import passport from 'passport'
const router = Router();

import {
    getSessions,
    getSession,
    // getPatientsSession,
    postSession,
    addSessionNote,
    completeSession,
    getSessionCases,
    closedSession,
    rescheduleSession,
    rescheduleReplaceSession,
    assignSession,
    postCaseSession,
    postNextSession,
    cancelSession
} from '../controllers/session_controller'

//api/rol
// router.post('/',createRole);
router.get('/', passport.authenticate('jwt', { session: false }), getSessions);
router.get('/:id', passport.authenticate('jwt', { session: false }), getSession);
router.get('/cases', passport.authenticate('jwt', { session: false }), getSessionCases);
router.post('/', passport.authenticate('jwt', { session: false }), postSession);
router.post('/:id/next', passport.authenticate('jwt', { session: false }), postNextSession);
router.post('/:id/notes', passport.authenticate('jwt', { session: false }), addSessionNote);
router.post('/:id/completed', passport.authenticate('jwt', { session: false }), completeSession);
router.post('/:id/case', passport.authenticate('jwt', { session: false }), postCaseSession);
router.put('/', passport.authenticate('jwt', { session: false }), closedSession);
router.put('/:id/reschedule', passport.authenticate('jwt', { session: false }), rescheduleSession);
router.put('/:id/replace', passport.authenticate('jwt', { session: false }), rescheduleReplaceSession);
router.put('/:id/assign', passport.authenticate('jwt', { session: false }), assignSession);
router.put('/:id/cancel', passport.authenticate('jwt', { session: false }), cancelSession);
// router.delete('/',deleteRole);

export default router;