import {Router} from 'express';
const router = Router();

import {getListQuestion} from '../controllers/question_controller'

router.get('/', getListQuestion);

export default router;