import { Router } from 'express';
const router = Router();
import {ListCategoryMedia} from '../controllers/medias_controller'
import passport from "passport";



router.get('/media', passport.authenticate('jwt', { session: false }), ListCategoryMedia);

export default router;