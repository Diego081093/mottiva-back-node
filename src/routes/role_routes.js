import {Router} from 'express';
const router = Router();

import { createRole, getListRole, deleteRole } from '../controllers/roles_controller'

//api/rol
router.get('/', getListRole);
router.post('/', createRole);
router.delete('/:id', deleteRole);

export default router;