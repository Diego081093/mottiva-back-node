import {Router} from 'express';
import passport from 'passport'
const router = Router();

import{ getAllProtocol, getProtocol, createProtocol } from '../controllers/protocol_controller'

router.get('/', passport.authenticate('jwt', { session: false }), getAllProtocol);
router.get('/:id', passport.authenticate('jwt', { session: false }), getProtocol);
router.post('/', passport.authenticate('jwt', { session: false }), createProtocol);

export default router;