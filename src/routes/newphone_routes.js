import { Router } from 'express';
const router = Router();
import {updatePhone} from '../controllers/users_controller'
import passport from "passport";


router.put('/:user',passport.authenticate('jwt', { session: false }),updatePhone);

export default router;