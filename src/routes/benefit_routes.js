import {Router} from 'express';
import passport from 'passport'
const router = Router();
import{
    getListBenefits,
    getListCompanyBenefits,
    exchangeBenefits,
    createBenefit,
    getBenefit,
    getHistoryBenefits,
    getBenefitsProfile
} from '../controllers/benefit_controller'

router.get('/', passport.authenticate('jwt', { session: false }), getListBenefits);
router.get('/company', passport.authenticate('jwt', { session: false }), getListCompanyBenefits);
router.get('/management', passport.authenticate('jwt', { session: false }), getBenefitsProfile)
router.get('/history', passport.authenticate('jwt', { session: false }), getHistoryBenefits);
router.get('/:id', passport.authenticate('jwt', { session: false }), getBenefit);
router.post('/', passport.authenticate('jwt', { session: false }), createBenefit);
router.post('/exchange', passport.authenticate('jwt', { session: false }), exchangeBenefits);

export default router;