import {Router} from 'express';
import passport from 'passport'
const { s3UploadWithDest } = require('./../helpers')

const s3Upload = s3UploadWithDest('trainings/')

const router = Router();

import { getTrainings, getTrainingsFilters, getTraining, createTraining, createTrainingVideo } from '../controllers/training_controller'

//api/training
router.get('/', passport.authenticate('jwt', { session: false }), getTrainings);
router.get('/filters', passport.authenticate('jwt', { session: false }), getTrainingsFilters);
router.get('/:id', passport.authenticate('jwt', { session: false }), getTraining);
router.post('/video', passport.authenticate('jwt', { session: false }), createTrainingVideo);
router.post('/',
    passport.authenticate('jwt', { session: false }),
    s3Upload.fields([
        {
            name: 'file',
            maxCount: 1
        },
        {
            name: 'image',
            maxCount: 1
        }
    ]),
    createTraining);

export default router;