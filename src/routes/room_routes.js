import passport from 'passport'
import { Router } from 'express'
import {
    getListRoomByProfile,
    getRoomDetails,
    postRoom,
    postEnterRoom,
    postSignupRoom,
    closedRoom,
    deleteUnsubscribeRoom,
    addRoomNote
} from '../controllers/room_controller'
import {addSessionNote} from "../controllers/session_controller";
const {s3Upload} = require('./../helpers')

const router = Router();

router.get('/', passport.authenticate('jwt', { session: false }), getListRoomByProfile)
router.get('/:id', passport.authenticate('jwt', { session: false }), getRoomDetails)
router.post('/', passport.authenticate('jwt', { session: false }), postRoom)
// router.post('/enter', passport.authenticate('jwt', { session: false }), postEnterRoom)
router.post('/:id/signup', passport.authenticate('jwt', { session: false }), postSignupRoom)
router.put('/', passport.authenticate('jwt', { session: false }), closedRoom)
router.delete('/:id/signup', passport.authenticate('jwt', { session: false }), deleteUnsubscribeRoom)
router.post('/:id/notes', passport.authenticate('jwt', { session: false }), addRoomNote);

export default router