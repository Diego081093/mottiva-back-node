import { Router } from 'express'
import passport from 'passport'

import {
    getNotifications,
    setTempoSession,
    sendPushNotificationUser,
    sendPushNotificationUsers,
    sendPushNotificationSessionStart,
    sendPushNotificationRoomStart,
    sendPushNotificationBenefits,
    sendPushNotificationMedias
} from '../controllers/notification_controller'

const  router = Router();

router.get('/', passport.authenticate('jwt', { session: false }), getNotifications)

router.get('/sessions', setTempoSession)

router.get('/sendPushNotificationUser/:id', passport.authenticate('jwt', { session: false }), sendPushNotificationUser)

router.get('/sendPushNotificationUsers/:id', passport.authenticate('jwt', { session: false }), sendPushNotificationUsers)

router.get('/sendPushNotificationSessionStart', passport.authenticate('jwt', { session: false }), sendPushNotificationSessionStart)

router.get('/sendPushNotificationRoomStart', passport.authenticate('jwt', { session: false }), sendPushNotificationRoomStart)

router.get('/sendPushNotificationBenefits', passport.authenticate('jwt', { session: false }), sendPushNotificationBenefits)
router.get('/sendPushNotificationMedias', passport.authenticate('jwt', { session: false }), sendPushNotificationMedias)

export default router