import { Router } from 'express';
const router = Router();
const {s3Upload} = require('./../helpers')
import { createAvatar, getListAvatar, getAvatar } from '../controllers/avatar_controller'
import { uploadAvatar } from '../controllers/upload_controller';

//avatar
router.post('/', createAvatar);
router.post('/upload', s3Upload.single('file'), uploadAvatar);
//avatar
router.get('/', getListAvatar);
//avatar?idProfile=100
router.get('/:idProfile', getAvatar); //lista de perfiles y cada uno con su avatar

export default router;

