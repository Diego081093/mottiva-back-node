import "@babel/polyfill";
const fs = require('fs');
const https = require('https');

exports.initSocket = async () => {
    const httpServer = require("http").createServer();
    const io = require("socket.io")(httpServer, {
        cors: {
            origin: '*',
        }
    });
    initIO(io)
    httpServer.listen(3000);
}
exports.initSocketHttps = async () => {
    const serverSocket = https.createServer({
        key: fs.readFileSync('../../cert/privkey.pem', 'utf8'),
        cert: fs.readFileSync('../../cert/cert.pem', 'utf8'),
        ca: fs.readFileSync('../../cert/chain.pem', 'utf8')
    })
    const options = {
        cors: {
            origin: '*',
        }
    };
    const io = require("socket.io")(serverSocket, options);
    initIO(io)
    serverSocket.listen(3000);
}
const user = {}
const userInTherapy = {}
const userInRoom = {}
const startedTherapy = []
function initIO(io) {
    io.on("connection", socket => {

        console.log('connected: ' + socket.id)

        //chat to group
        socket.on("messageToRoom", (data) => {
            io.to(data.roomName).emit('messageToUser', { message: data.message, author: data.author, idSocket: socket.id, to: data.roomName });
            io.to('helpdesk').emit('newHelpdesk', true)
            console.log('chat from: \' ' + data.author + ' \' to: \'' + data.roomName + '\' => (' + socket.id + ')')
        })

        //notify helpdesk to fetch conversations
        socket.on("notifyHelpdesk", (data) => {
            io.to('helpdesk').emit('newHelpdesk', true)
            console.log('notify to helpdesk for fetch')
        })

        //join to room chat (single or group)
        socket.on("joinRoom", (data) => {
            if(!socket.rooms.has(data.roomName)){
                socket.join(data.roomName)
                console.log('\'' + socket.id + '\' joined to: \'' + data.roomName + '\'')
            }
        })

        //join to helpdesk for tracking user and attending chats
        socket.on("joinHelpdesk", (data) => {
            if(!socket.rooms.has('helpdesk')){
                socket.join('helpdesk')
                console.log('\'' + socket.id + '\' joined to: \'helpdesk group\'')
            }
        })

        //show me as connected
        socket.on('addConnectedUser', (data) => {
            Object.keys(user).map(key => {
                if (user[key] === data.idProfile) {
                    delete user[key]
                }
            })

            user['user-' + socket.id] = data.idProfile
            console.log('user \'' + data.idProfile + '\' is connected')

            //notify to whom is observing
            socket.emit('connected-' + data.idProfile, true)

            //notify to user successful connection
            io.to('roomUser-' + data.idProfile).emit('connected-' + data.idProfile, true)
        })

        //validate if user is connected
        socket.on('isConnect', (data) => {
            if(!socket.rooms.has('roomUser-' + data.idProfile)){
                socket.join('roomUser-' + data.idProfile);
                console.log('socket \'' + socket.id + '\' listening single channel for: \'' + data.idProfile + '\'')
            }
            Object.keys(user).map(key => {
                if (user[key] === data.idProfile) {
                    io.to('roomUser-' + data.idProfile).emit('connected-' + data.idProfile, true)
                }
            })
            Object.keys(userInTherapy).map(key => {
                if (userInTherapy[key].idProfile === data.idProfile) {
                    io.to('roomUser-' + data.idProfile).emit('connected-in-' + data.idProfile, userInTherapy[key])
                }
            })
            Object.keys(userInRoom).map(key => {
                if (userInRoom[key].idProfile === data.idProfile) {
                    io.to('roomUser-' + data.idProfile).emit('connected-in-room-' + data.idProfile, userInRoom[key])
                }
            })
        })

        //notify waiting for therapy
        socket.on('waitingSession', (data) => {
            Object.keys(userInTherapy).map(key => {
                if (userInTherapy[key].idProfile === data.idProfile) {
                    delete userInTherapy[key]
                }
            })
            userInTherapy['user-' + socket.id] = data
            io.to('roomUser-' + data.idProfile).emit('waiting', data)
            console.log('\'' + data.idProfile + '\' is waiting for therapy')
        })

        //notify enter for room
        socket.on('enterRoom', (data) => {
            Object.keys(userInRoom).map(key => {
                if (userInRoom[key].idProfile === data.idProfile) {
                    delete userInRoom[key]
                }
            })
            userInRoom['user-' + socket.id] = data
            io.to('roomUser-' + data.idProfile).emit('enterRoom-' + data.idProfile, data)
            console.log('\'' + data.idProfile + '\' is in room: \'' + data.idRoom + '\'')
        })

        //notify mic state
        socket.on('mic', (data) => {
            console.log(data)
            io.to('roomUser-' + data.idProfile).emit('mic-' + data.idProfile, data)
            console.log('\'' + data.idProfile + '\' mic on: ' + data.mic)
        })

        //turn off mic patient
        socket.on('micOff', (data) => {
            io.to('roomUser-' + data.idProfile).emit('micOff-' + data.idProfile)
            console.log('\'' + data.idProfile + '\' mic turned off')
        })

        //notify mic state
        socket.on('camera', (data) => {
            console.log(data)
            io.to('roomUser-' + data.idProfile).emit('camera-' + data.idProfile, data)
            console.log('\'' + data.idProfile + '\' camera on: ' + data.cam)
        })

        //turn off cam patient
        socket.on('camOff', (data) => {
            io.to('roomUser-' + data.idProfile).emit('camOff-' + data.idProfile)
            console.log('\'' + data.idProfile + '\' mic turned off')
        })

        //new subscribe to room
        socket.on('subscribeToRoom', (data) => {
            io.to('room-' + data.idRoom).emit('newSubscribe', data)
            console.log('new subscribe to room: ' + data.idRoom)
        })

        //notify get out waiting for therapy
        socket.on('getOutWaitingSession', (data) => {
            Object.keys(userInTherapy).map(key => {
                if (userInTherapy[key].idProfile === data.idProfile) {
                    delete userInTherapy[key]
                }
            })
            io.to('roomUser-' + data.idProfile).emit('getOutWaitingSession', data)
            console.log('\'' + data.idProfile + '\' get out waiting for therapy')
        })

        //get out therapy
        socket.on('exitSession', (data) => {
            Object.keys(userInTherapy).map(key => {
                if (userInTherapy[key].idProfile === data.idProfile && userInTherapy[key].idSession === data.idSession) {
                    io.to('roomUser-' + data.idProfile).emit('connected-out-' + data.idProfile, userInTherapy[key])
                    delete userInTherapy[key]
                    console.log('\'' + data.idProfile + '\' left the session \'' + data.idSession + '\'')

                    startedTherapy.map((item, index) => {
                        if (item === data.idSession) {
                            delete startedTherapy[index]
                            io.to('helpdesk').emit('sessionInit', startedTherapy);
                        }
                    })
                }
            })
        })

        //get out therapy
        socket.on('exitRoom', (data) => {
            Object.keys(userInRoom).map(key => {
                if (userInRoom[key].idProfile === data.idProfile && userInRoom[key].idRoom === data.idRoom) {
                    io.to('roomUser-' + data.idProfile).emit('connected-out-room-' + data.idProfile, userInRoom[key])
                    delete userInRoom[key]
                    console.log('\'' + data.idProfile + '\' left the room \'' + data.idRoom + '\'')
                }
            })
        })

        //notify to close room
        socket.on('closeRoom', (data) => {
            io.to(data.roomName).emit('closeRoom', true);
            console.log('\'' + data.roomName + '\' notified for close')
        })

        //helpdesk user assigned
        socket.on('helpdeskAssigned', (data) => {
            io.to('helpdesk').emit('newHelpdeskAssigned', data);
            console.log('notify new helpdesk user has assigned')
        })

        //start and notify start session
        socket.on('sessionStart', (session) => {
            if (!startedTherapy.includes(session)) {
                startedTherapy.push(session)
            }
            console.log('session \'' + session + '\' started')
            io.to('session-' + session).emit('sessionStart', true);
            console.log('notify to helpdesk new sessions started')
            io.to('helpdesk').emit('sessionStart', startedTherapy);
        })

        //on disconnect socket
        socket.on('disconnect', () => {
            io.to('roomUser-' + user['user-' + socket.id]).emit('connected-' + user['user-' + socket.id], false)

            //remove from user connected list
            if(user['user-' + socket.id]) {
                console.log('user \'' + user['user-' + socket.id] + '\' is disconnected')
                delete user['user-' + socket.id]
            }

            //remove from users in therapy
            if(userInTherapy['user-' + socket.id]) {
                io.to('roomUser-' + userInTherapy['user-' + socket.id].idProfile).emit('connected-out-' + userInTherapy['user-' + socket.id].idProfile, userInTherapy['user-' + socket.id])
                startedTherapy.map((item, index) => {
                    if (item === userInTherapy['user-' + socket.id].idSession) {
                        console.log('user disconnected \'' + userInTherapy['user-' + socket.id].idProfile + '\' left the session \'' + userInTherapy['user-' + socket.id].idSession + '\'')
                        delete startedTherapy[index]
                        io.to('helpdesk').emit('sessionStart', startedTherapy);
                    }
                })
                console.log('user disconnected \'' + userInTherapy['user-' + socket.id].idProfile + '\' left therapies')
                delete userInTherapy['user-' + socket.id]
            }

            //remove from users in room
            if(userInRoom['user-' + socket.id]) {
                io.to('roomUser-' + userInRoom['user-' + socket.id].idProfile).emit('connected-out-room-' + userInRoom['user-' + socket.id].idProfile, userInRoom['user-' + socket.id])
                console.log('user disconnected \'' + userInRoom['user-' + socket.id].idProfile + '\' left rooms')
                delete userInRoom['user-' + socket.id]
            }
        });
    });
    console.log('Socket on port 3000');
}